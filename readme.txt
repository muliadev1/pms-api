#### Sequelize
1. in the root project create new folder called "db"
2. in the rott project create another file called .sequelizerc in the root folder and update like below
    
    const path = require("path");
    module.exports = {
        config: path.resolve("db/config", "config.json"),
        "models-path": path.resolve("db", "models"),
        "migrations-path": path.resolve("db", "migrations"),
        "seeders-path": path.resolve("db", "seeders"),
    };

2. on the command promt, run "sequelize init" to create these folders under /database folder.

3. after that go to folder db/config and open config.json file, adjust the file like below

{
  "development": {
    "username": "root",
    "password": "123456",
    "database": "pms_db",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}

4. create migrate and model , like sample models below

npx sequelize-cli model:generate --name User --attributes first_name:string,last_name:string,image_name:string,image_url:string,username:string,password:string,role_id:integer,refresh_token:string,email:string,phone:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Category --attributes category_name:string,category_description:string,is_active:integer
npx sequelize-cli migration:create --name modify_categories_add_created_by_and_updated_by_fields

npx sequelize-cli model:generate --name SubCategory --attributes subcategory_name:string,subcategory_description:string,category_id:integer,is_active:integer
npx sequelize-cli migration:create --name modify_sub_categories_add_created_by_and_updated_by_fields

npx sequelize-cli model:generate --name Item --attributes item_code:string,item_description:string,subcategory_id:integer,unit:string,price:float,stock:float,is_active:integer,stockHK:float,stockFO:float,stockFB:float,stockBO:float,stockBAR:float,stockSEC:float,stockPG:float,stockENG:float,vendor_id:integer,is_HK:integer,is_FO:integer,is_FB:integer,is_BO:integer,is_BAR:integer,is_SEC:integer,is_PG:integer,is_ENG:integer,last_price:float,account_HK:integer,account_FO:integer,account_FB:integer,account_BO:integer,account_BAR:integer,account_SEC:integer,account_PG:integer,account_ENG:integer,is_proccess_opname:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Vendor --attributes vendor_name:string,address:string,phone:string,email:string,contact:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name PR_Main --attributes purchase_req_code:string,purchase_req_date:date,department_id:integer,notes:string,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name PR_Detail --attributes purchase_req_id:integer,item_code:string,description:string,unit:string,price:double,qty_order:double,current_stock:double,notes:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name PO_Main --attributes purchase_order_code:string,vendor_id:integer,order_date:date,expected_date:date,total:double,method_id:integer,is_checked:integer,status_id:integer,verify_by:string,notes:string,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name PO_Detail --attributes purchase_order_id:integer,item_code:string,description:string,unit:string,price:double,qty_order:double,qty_stock:double,subtotal:double,notes:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name RO_Main --attributes receive_order_code:string,purchase_order_code:string,receive_date:date,vendor_id:integer,operator_id:integer,is_invoicing:integer,receipt_no:string,total:double,discount:double,total_after_disc:double,is_credit:integer,due_date:date,is_paid:integer,down_payment:double,remaind:double,department_id:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name RO_Detail --attributes receive_order_id:integer,item_code:string,description:string,unit:string,price:double,qty_order:double,qty_stock:double,subtotal:double,notes:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Element_Category --attributes id:integer,element_category:string,status:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Element --attributes id:integer,element_code:string,element_name:string,price:double,service:double,tax:double,final_price:double,element_category_id:integer,department_id:integer,price_type_id:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Price_Type --attributes id:integer,price_type:string,status:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Package_Main --attributes package_name:string,start_stay:date,end_stay:date,start_booking:date,end_booking:date,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name Package_Sub --attributes package_id:integer,room_id:integer,room_name:string,room_price:double,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name Package_Detail --attributes package_sub_id:integer,item_id:integer,item_code:string,item_name:string,price:double,qty:integer,createdBy:string,updatedBy:string

=============================================================================================================

npx sequelize-cli model:generate --name SR_Main --attributes store_req_code:string,department_id:integer,department:string,store_date:date,operator_id:integer,desciption:string,status_in_out:integer,is_SO:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name SR_Detail --attributes store_req_id:integer,item_code:string,description:string,unit:string,price:double,qty_order:double,curr_stock:double,department_id:integer,department:string,operator_id:integer,status_in_out:integer,is_SO:integer,notes:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name BadStokMain --attributes badstock_code:string,department_id:integer,department:string,date_add:date,operator_id:integer,desciption:string
npx sequelize-cli model:generate --name BadStokDetail --attributes badstock_code:string,item_code:string,description:string,unit:string,price:double,qty:double,
curr_stock:double,department_id:integer,department:string,operator_id:integer,node:string


npx sequelize-cli model:generate --name StockOpname --attributes stock_date:date,item_code:string,description:string,unit:string,price:double,stock:double,input_stock:double,diff:double,
department_id:integer,department:string

npx sequelize-cli model:generate --name SubcategoryDepartment --attributes department_id:integer,subcategory_id:integer,item_code:string,desciption:string

npx sequelize-cli model:generate --name Department --attributes department:string,is_active:integer

npx sequelize-cli model:generate --name Account --attributes account_no:string,account_name:string,subaccount_no:string,balance:double,open_balance:double,is_active:integer,
is_counter:integer

npx sequelize-cli model:generate --name Asset --attributes asset_code:string,item_code:string,subaccount_no:string,balance:double,open_balance:double,is_active:integer,
is_counter:integer

npx sequelize-cli model:generate --name Source --attributes source_name:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Segment_Group --attributes segment_group_name:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Segment --attributes segment_name:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name RoomType --attributes roomtype_name:string,description:string,rate:double,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name RoomStatus --attributes status_name:string,description:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Room --attributes room_name:string,description:string,roomtype_id:integer,roomstatus_id:integer,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Table --attributes table_number:string,guest_id:integer

npx sequelize-cli model:generate --name FBTrxMain --attributes trx_date:datetime,table_id:integer,guest_id:string,reservation_id:string,room_id:integer,total:double,
discount:double,total2:double,service:double,tax:double,grand_total:double,article_id:integer,total_payment:double,remaind:double,payment_status_id:integer,
reservation_charge_id:string,room_charge_id:string,notes:string,operator_openorder_id:integer,operator_process_id:integer,closing_date:datetime,closing_date_na:datetime,
is_closng:integer,is_na:integer,waiter_id:integer,waiter_name:string,payment_status_id:integer

npx sequelize-cli model:generate --name FBTrxDetail --attributes trx_id:string,trx_date:datetime,menu_id:string,menu:string,price:double,disp:double,dism:double,
qty:integer,subtotal:double,add_request:string,operator_id:integer,status_trx:integer,desciption:string,na_date:datetime,waiter_id:integer,waiter_name:string,
orderno_manual:string,price1:double

npx sequelize-cli model:generate --name Guest --attributes saluation:string,firstname:string,lastname:string,address:string,country:string,state:string,zipcode:string,email:string,phone:string,gender:string,birthday:date,idtype:string,idnumber:string,description:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name User --attributes first_name:string,last_name:string,username:string,password:string,parent_id:integer,parent:string,
outlet_name:string,is_active:integer

npx sequelize-cli model:generate --name Waiter --attributes waiter:string,is_active:integer

npx sequelize-cli model:generate --name PaymentType --attributes payment_type_name:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Menu --attributes menu_name:string,subcategory_menu_id:integer,desciption:string,price:double,tax:double,service:double,final_price:double,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name MenuCategory --attributes category_name:string,description:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name SubmenuCategory --attributes submenu_category_name:string,description:string,menu_category_id:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name ReservationStatus --attributes status_name:string,description:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Company --attributes company_name:string,address:string,country:string,state:string,zipcode:string,email:string,phone:string,type:string,description:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name ItemPriceHistory --attributes item_code:string,price:double,new_price:double,date:datetime,vendor_id:integer,hpp_price:double,
recorder_code:string

npx sequelize-cli model:generate --name Department --attributes department_name:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Role --attributes role_name:string,role_description:string,is_active:integer

npx sequelize-cli model:generate --name Menu --attributes menu_name:string,menu_description:string,menu_icon:string

npx sequelize-cli model:generate --name MenuRole --attributes menu_id:integer,role_id:integer,sort:integer

npx sequelize-cli model:generate --name UserRole --attributes user_id:integer,role_id:integer

npx sequelize-cli model:generate --name Submenu --attributes submenu_name:string,submenu_link:string,parent_menu_id:integer,submenu_icon:string

npx sequelize-cli model:generate --name Purpose --attributes purpose_name:string,is_active:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name PaymentMain --attributes voucher_no:string,voucher_date:date,vendor:string,address:string,payment_method_id:integer,
bank:string,check_no:string,total:double,operator_id:integer

npx sequelize-cli model:generate --name PaymentDetail --attributes voucher_no:string,amount:double,description:string,receive_order_date:date,receive_order_code:string

npx sequelize-cli model:generate --name Bank --attributes bank_name:string

npx sequelize-cli model:generate --name CheckMain --attributes check_no:string,input_date:date,bank_id:integer,total:decimal,vendor_id:integer,operator_id:integer

npx sequelize-cli model:generate --name CheckDetail --attributes check_main_id:integer,payment_id:string,amount:decimal

npx sequelize-cli model:generate --name Module --attributes module_name:string,module_description:string,parent_id:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name User_Module --attributes user_id:integer,module_id:integer,is_create:integer,is_read:integer,is_update:integer,is_delete:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Reservation --attributes reservation_no:string,rservervation_date:date,is_group:integer,parent_group_id:integer,room_type_id:integer,room_id:integer,reservation_status_id:integer,guest_id:integer,adult:integer,child:integer,check_in:date,check_out:date,duration:integer,purpose_id:integer,source_id:integer,segment_id:integer,is_vip:integer,company_id:integer,deposit:double,cut_off_date:date,agent_it:integer,remarks:string,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name Reservation_Detail --attributes reservation_id:integer,night_no:integer,date_exp:date,package_id:integer,rate_price:double,is_running:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name Reservation_Guest --attributes reservation_id:integer,guest_id:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name FoTrx --attributes reservation_id:integer,reservation_no:string,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name FoTrx_Detail --attributes reservation_no:string,invoice_no:string,room_type_id:integer,room_id:integer,checkout:date,package_id:integer,description:string,charge:double,payment:double,is_closing:integer,closing_date:date,is_night_audit:integer,night_audit_date:date,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name MasterBill --attributes masterbill_code:string,masterbill_date:date,bill_to:string,charge:double,payment:double,balance:double,is_roombill:integer,guest_id:integer,reservation_id:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name MasterBill_Detail --attributes masterbill_id:integer,masterbill_date:date,reservation_id:integer,invoice_manual:string,room_id:integer,is_charge:integer,package_id:integer,description:string,charge:double,payment:double,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name FbTrx --attributes fbtrx_no:string,fbtrx_date:date,table_id:integer,guest_id:integer,reservation_no:string,room_id:integer,total:double,discount:double,total2:double,service:double,tax:double,package_id:integer,total_payment:double,remind:double,status_payment_id:integer,reservation_charge_id:string,room_charge_id:string,notes:string,operator_id_open:integer,operator_id_process:integer,is_closing:integer,closing_date:date,is_night_audit:integer,night_audit_date:date,waiter_id:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name FbTrx_Detail --attributes fbtrx_no:string,fbtrx_date:date,menu_id:integer,menu_name:string,price:double,disp:double,dism:double,qty:integer,sub_total:double,request:string,operator_id:integer,status_id:integer,description:string,night_audit_date:date,waiter_id:integer,waiter_name:string,order_manual:string,price1:double,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Table --attributes table_no:integer,guest_id:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name HistoryItemPrice --attributes item_code:string,price:double,new_price:double,history_date:date,vendor_id:integer,price_hpp:double,receive_order_code:string,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Payment --attributes voucher_no:string,voucher_date:date,vendor:string,address:string,payment_method_id:integer,bank:string,check_no:string,total:double,operator_id:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name Payment_Detail --attributes payment_id:integer,amount:double,description:string,receive_order_code:string,receive_order_date:date,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Asset --attributes asset_code:string,item_code:string,baik:integer,kurangbaik:integer,used:integer,lose:integer,out:integer,jumlah:integer,baikHK:integer,kurangbaikHK:integer,usedHK:integer,loseHK:integer,outHK:integer,jumlahHK:integer,baikFO:integer,kurangbaikFO:integer,usedFO:integer,loseFO:integer,outFO:integer,jumlahFO:integer,baikFB:integer,kurangbaikFB:integer,usedFB:integer,loseFB:integer,outFB:integer,jumlahFB:integer,baikBO:integer,kurangbaikBO:integer,usedBO:integer,loseBO:integer,outBO:integer,jumlahBO:integer,baikEng:integer,kurangbaikEng:integer,usedEng:integer,loseEng:integer,outEng:integer,jumlahEng:integer,baikPG:integer,kurangbaikPG:integer,usedPG:integer,losePG:integer,outPG:integer,jumlahPG:integer,baikSec:integer,kurangbaikSec:integer,usedSec:integer,loseSec:integer,outSec:integer,jumlahSec:integer,baikBar:integer,kurangbaikBar:integer,usedBar:integer,loseBar:integer,outBar:integer,jumlahBar:integer,is_active:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name Asset_Detail --attributes asset_id:integer,qty:integer,kondisi_awal:string,kondisi_akhir:string,department_id:integer,department_name:string,operator_id:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name Journal --attributes journal_no:string,bukti_transaksi:string,keterangan:string,tanggal_input:date,tanggal_sistem:date,operator_id:integer,is_active:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name Journal_Detail --attributes journal_id:integer,account_no:string,debet:double,kredit:double,tanggal_input:date,tanggal_sistem:date,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name BadStock --attributes badstock_code:string,date_add:date,department_id:integer,department_name:string,operator_id:integer,description:string,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name BadStock_Detail --attributes badstock_id:integer,item_code:string,description:string,unit:string,price:double,qty:double,curr_stock:double,department_id:integer,department_name:string,operator_id:integer,notes:string,createdBy:string,updatedBy:string

npx sequelize-cli migration:create --name modify_company_add_contact_person_fields

npx sequelize-cli migration:create --name modify_element_add_new_fields

npx sequelize-cli model:generate --name StockOpname --attributes stock_opname_date:date,item_code:string,description:string,unit:string,price:double,stock:double,input_stock:double,diff_stock:double,department_id:integer,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name ChartOfAccount --attributes account_no:string,account_sub_no:string,account_name:string,balance:double,open_balance:double,is_kontra:integer,is_active:integer,createdBy:string,updatedBy:string
npx sequelize-cli model:generate --name COASubClass --attributes account_sub_no:string,account_sub_name:string,is_active:integer,createdBy:string,updatedBy:string


5. after command above, on the folder db/migrations and db/models automaticly create mogrations files and models files

6. on the command fromt runj "npx sequelize-cli db:migrate" to update table to database


npx sequelize-cli model:generate --name RateCode_element --attributes rate_code_id:integer,element_id:integer,adult_rate:double,child_rate:double,per_room_rate:double,qty:integer,name:string,total_price:double,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name RateCode_roomrate --attributes rate_code_id:integer,room_type_id:integer,adult_rate:double,child_rate:double,per_night_price:double,createdBy:string,updatedBy:string

npx sequelize-cli model:generate --name RateCode_surcharge --attributes ratecode_room_type_id:integer,start_date:date,end_date:date,amount:double,createdBy:string,updatedBy:string


npx sequelize-cli model:generate --name CurrentYear_Roomtype --attributes room_type_id:integer,start_date:date,end_date:date,amount:double,createdBy:string,updatedBy:string
