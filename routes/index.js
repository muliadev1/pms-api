'use strict';
const { Master } = require('../db/models/index');
const jwt = require('jsonwebtoken');
const refreshToken = require('../controllers/RefreshToken');
const verifyToken = require('../middleware/VerifyToken');
const fs = require('fs');
const { Sequelize } = require('sequelize');
const upload = require('../middleware/upload');

const mysql = require('mysql')
const multer = require('multer')
const path = require('path');
const csv = require('fast-csv');  
const util = require('util');


const HomeController = require('../controllers/Home');
const CatagoryController = require('../controllers/Categories');
const SubCatagoryController = require('../controllers/SubCategories');
const VendorController = require('../controllers/Vendors');
const GuestController = require('../controllers/Guests');
const CompanyController = require('../controllers/Companies');
const SegmentController = require('../controllers/Segments');
const SourceController = require('../controllers/Sources');
const RoomtypeController = require('../controllers/RoomTypes');
const RoomstatusController = require('../controllers/RoomStatus');
const RoomController = require('../controllers/Rooms');
const RsvStatusController = require('../controllers/ReservationStatus');
const PurposeController = require('../controllers/Purpose');
const MenuCategoryController = require('../controllers/MenuCategories');
const SubmenuCategoryController = require('../controllers/SubmenuCategories');
const MenuController = require('../controllers/Menus');
const PaymentTypeController = require('../controllers/PaymentTypes');
const ItemController = require('../controllers/Items');
const RequisitionController = require('../controllers/PurchaseRequisitions');
const DepartmentController = require('../controllers/Departments');
const PurchaseOrderController = require('../controllers/PurchaseOrders');
const ReceiveOrderController = require('../controllers/ReceiveOrders');
const StoreReqController = require('../controllers/StoreRequisitions');
const DirectOrderController = require('../controllers/DirectOrders');
const ElementCategoryController = require('../controllers/ElementCategories');
const PriceTypeController = require('../controllers/PriceTypes');
const ElementController = require('../controllers/Elements');
const PackageController = require('../controllers/Packages');
const RoleController = require('../controllers/Roles');
const UserController = require('../controllers/Users');
const ModuleController = require('../controllers/Modules');
const ModuleMenuController = require('../controllers/ModuleMenus');  
const UserModuleController = require('../controllers/UserModules');
const ReservationController = require('../controllers/Reservation/Reservation');
const FrontOfficeController = require('../controllers/FrontOffices');
const MasterBillController = require('../controllers/MasterBills');
const HistoryItemPriceController = require('../controllers/HistoryItemPrices');
const PaymentController = require('../controllers/Payments');
const PaymentMethodController = require('../controllers/PaymentMethod');
const BookingStatusController = require('../controllers/BookingStatus');
const FloorController = require('../controllers/Floor');
const AddonController = require('../controllers/Addon');
const BlackoutController = require('../controllers/RateCode/BlackOut');
const RoomRateController = require('../controllers/RoomRate');
const SurchargeController = require('../controllers/Surcharge');
const FeautureController = require('../controllers/Feauture');
const BedTypeController = require('../controllers/BedType');
const PaymentStatusController = require('../controllers/PaymentStatus');
const AssetController = require('../controllers/Assets');
const HotelController = require('../controllers/Hotel');
const JournalController = require('../controllers/Journals');
const BadStockController = require('../controllers/BadStocks');
const PaymentBookingController = require('../controllers/PaymentBooking');
const StockOpnameController = require('../controllers/StockOpnames');
const BookingController = require('../controllers/Booking');
const DesignationController = require('../controllers/Designation');
const CityController = require('../controllers/City');
const StateController = require('../controllers/State');
const CountryController = require('../controllers/Country');
const Identity_typeController = require('../controllers/Identity_type');
const AccountController = require('../controllers/Accounts');
const CalendarController = require('../controllers/Calendar');
const EmployeeController = require('../controllers/Employee');
const SettingsController = require('../controllers/Settings');
const HouseKeepingController = require('../controllers/HouseKeeping');
const Mail_templateController = require('../controllers/Mail_template');
const GalleryController = require('../controllers/Gellery');
const PageController = require('../controllers/Page');
const BannerController = require('../controllers/Banner');
const CurrenciesController = require('../controllers/Currencies');
const LanguagesController = require('../controllers/Languages');

const RoleMenuController = require('../controllers/RoleMenus');
const AppController = require('../controllers/Apps');
const VipController = require('../controllers/Vip');
const RateCodeController = require('../controllers/RateCode/RateCode');
const RateCodeSurchargeController = require('../controllers/RateCode/AddSurcharge');
const RateCodeSearchController = require('../controllers/RateCode/RateCodeSearch');
const CurrentYearRoomtypeController = require('../controllers/RateCode/CurrentYear');
const PromoCodeController = require('../controllers/RateCode/Promo_code');
const PromoRatecodeController = require('../controllers/RateCode/Promo_ratecode');
const RoomCalendarController = require('../controllers/Reservation/RoomCalendar');
   
module.exports = function (app) {
    // home
    app.get('/', HomeController.index);
    // users
    app.get('/users', verifyToken, UserController.getUsers);
    app.get('/users/:appid', verifyToken, UserController.getUserByApp);
    app.get('/users/list-account', verifyToken, UserController.getListAccount);
    app.post('/users', UserController.createUser);
    app.get('/users/:id', verifyToken, UserController.detailUser);
    app.put('/users/:id', verifyToken, UserController.updateUser);
    app.delete('/users/:id', verifyToken, UserController.deleteUser);

    app.post('/login', UserController.Login);
    app.get('/token', refreshToken);
    app.delete('/logout', UserController.Logout);

    //module apps
    app.get('/apps', verifyToken, AppController.getApps);   

    //categories
    app.get('/categories', verifyToken, CatagoryController.getCategories);
    app.post('/categories', CatagoryController.createCategory);
    app.get('/categories/:id', verifyToken, CatagoryController.getCategory);
    app.put('/categories/:id', verifyToken, CatagoryController.updateCategory);
    app.delete('/categories/:id', verifyToken, CatagoryController.deleteCategory);

    //sub-categories
    app.get('/subcategories', verifyToken, SubCatagoryController.getSubCategories);
    app.post('/subcategories', SubCatagoryController.createSubCategory);
    app.get('/subcategories/:id', verifyToken, SubCatagoryController.getSubCategory);
    app.put('/subcategories/:id', verifyToken, SubCatagoryController.updateSubCategory);
    app.delete('/subcategories/:id', verifyToken, SubCatagoryController.deleteSubCategory);

    //vendors
    app.get('/vendors', verifyToken, VendorController.getVendors);
    app.post('/vendors', VendorController.createVendor);
    app.get('/vendors/:id', verifyToken, VendorController.getVendor);
    app.put('/vendors/:id', verifyToken, VendorController.updateVendor);
    app.delete('/vendors/:id', verifyToken, VendorController.deleteVendor);

    //guests
    app.get('/guests', verifyToken, GuestController.getGuests);
    app.post('/guests', GuestController.createGuest);
    app.get('/guests/:id', verifyToken, GuestController.getGuest);
    app.put('/guests/:id', verifyToken, GuestController.updateGuest);
    app.delete('/guests/:id', verifyToken, GuestController.deleteGuest);
    app.get('/filter/guest/:name', verifyToken, GuestController.getGuestsName);

    //companies
    app.get('/companies', verifyToken, CompanyController.getCompanies);
    app.post('/companies', CompanyController.createCompany);
    app.get('/companies/:id', verifyToken, CompanyController.getCompany);
    app.put('/companies/:id', verifyToken, CompanyController.updateCompany);
    app.delete('/companies/:id', verifyToken, CompanyController.deleteCompany);
    app.get('/getfilter/namecompanies/:name', verifyToken, CompanyController.getCompaniesName);   


    //segment groups
    app.get('/segment-groups', verifyToken, SegmentController.getGroups);
    app.post('/segment-groups', SegmentController.createGroup);
    app.get('/segment-groups/:id', verifyToken, SegmentController.getGroup);
    app.put('/segment-groups/:id', verifyToken, SegmentController.updateGroup);
    app.delete('/segment-groups/:id', verifyToken, SegmentController.deleteGroup);

    //segments
    app.get('/segments', verifyToken, SegmentController.getSegments);
    app.post('/segments', SegmentController.createSegment);
    app.get('/segments/:id', verifyToken, SegmentController.getSegment);
    app.put('/segments/:id', verifyToken, SegmentController.updateSegment);
    app.delete('/segments/:id', verifyToken, SegmentController.deleteSegment);

    //sources
    app.get('/sources', verifyToken, SourceController.getSources);
    app.post('/sources', SourceController.createSource);
    app.get('/sources/:id', verifyToken, SourceController.getSource);
    app.put('/sources/:id', verifyToken, SourceController.updateSource);
    app.delete('/sources/:id', verifyToken, SourceController.deleteSource);

    //roomtypes
    app.get('/roomtypes', verifyToken, RoomtypeController.getRoomTypes);
    app.post('/roomtypes', RoomtypeController.createRoomType);
    app.get('/roomtypes/:id', verifyToken, RoomtypeController.getRoomType);
    app.put('/roomtypes/:id', verifyToken, RoomtypeController.updateRoomType);
    app.delete('/roomtypes/:id', verifyToken, RoomtypeController.deleteRoomType);
    app.get('/roomtypeprice/:id', verifyToken, RoomtypeController.getRoomTypePrice);
    app.get('/roomtypebyhotel/:hotelid', verifyToken, RoomtypeController.getRoomTypebyhotel);
    app.get('/roomtypehotel/:hotelid', verifyToken, RoomtypeController.getRoomTypebyhotelnochildandadult);

    //roomstatus
    app.get('/roomstatus', verifyToken, RoomstatusController.getRoomStatuses);
    app.post('/roomstatus', verifyToken, RoomstatusController.createRoomStatus);
    app.get('/roomstatus/:id', verifyToken, RoomstatusController.getRoomStatus);
    app.put('/roomstatus/:id', verifyToken, RoomstatusController.updateRoomStatus);
    app.delete('/roomstatus/:id', verifyToken, RoomstatusController.deleteRoomStatus);

     // room-rate
     app.get('/room-rate', verifyToken, RoomRateController.getRoomRate);
     app.get('/room-rate/:id', verifyToken, RoomRateController.getRoomRateDetail);
     app.post('/room-rate', verifyToken, RoomRateController.createRoomRate);
     app.put('/room-rate/:id', verifyToken, RoomRateController.updateRoomRate);
     app.delete('/room-rate/:id', verifyToken, RoomRateController.deleteRoomRate);

    //rooms
    app.get('/rooms', verifyToken, RoomController.getRooms);
    app.post('/rooms', verifyToken, RoomController.createRoom);
    app.get('/rooms/:id', verifyToken, RoomController.getRoom);
    app.put('/rooms/:id', verifyToken, RoomController.updateRoom);
    app.delete('/rooms/:id', verifyToken, RoomController.deleteRoom);

    //reservations status
    app.get('/rsvstatus', verifyToken, RsvStatusController.getReservationStatuses);
    app.post('/rsvstatus', verifyToken, RsvStatusController.createReservationStatus);
    app.get('/rsvstatus/:id', verifyToken, RsvStatusController.getReservationStatus);
    app.put('/rsvstatus/:id', verifyToken, RsvStatusController.updateReservationStatus);
    app.delete('/rsvstatus/:id', verifyToken, RsvStatusController.deleteReservationStatus);

    //purposes
    app.get('/purposes', verifyToken, PurposeController.getPurposes);
    app.post('/purposes', verifyToken, PurposeController.createPurpose);
    app.get('/purposes/:id', verifyToken, PurposeController.getPurpose);
    app.put('/purposes/:id', verifyToken, PurposeController.updatePurpose);
    app.delete('/purposes/:id', verifyToken, PurposeController.deletePurpose);

    //menu categories
    app.get('/menucategories', verifyToken, MenuCategoryController.getMenuCategories);
    app.post('/menucategories', verifyToken, MenuCategoryController.createMenuCategory);
    app.get('/menucategories/:id', verifyToken, MenuCategoryController.getMenuCategory);
    app.put('/menucategories/:id', verifyToken, MenuCategoryController.updateMenuCategory);
    app.delete('/menucategories/:id', verifyToken, MenuCategoryController.deleteMenuCategory);

    //submenu categories
    app.get('/submenucategories', verifyToken, SubmenuCategoryController.getSubmenuCategories);
    app.post('/submenucategories', verifyToken, SubmenuCategoryController.createSubmenuCategory);
    app.get('/submenucategories/:id', verifyToken, SubmenuCategoryController.getSubmenuCategory);
    app.put('/submenucategories/:id', verifyToken, SubmenuCategoryController.updateSubmenuCategory);
    app.delete('/submenucategories/:id', verifyToken, SubmenuCategoryController.deleteSubmenuCategory);

    //menus
    app.get('/menus', verifyToken, MenuController.getMenus);
    app.post('/menus', verifyToken, MenuController.createMenu);
    app.get('/menus/:id', verifyToken, MenuController.getMenu);
    app.put('/menus/:id', verifyToken, MenuController.updateMenu);
    app.delete('/menus/:id', verifyToken, MenuController.deleteMenu);

    //payment types
    app.get('/paymenttypes', verifyToken, PaymentTypeController.getPaymentTypes);
    app.post('/paymenttypes', verifyToken, PaymentTypeController.createPaymentType);
    app.get('/paymenttypes/:id', verifyToken, PaymentTypeController.getPaymentType);
    app.put('/paymenttypes/:id', verifyToken, PaymentTypeController.updatePaymentType);
    app.delete('/paymenttypes/:id', verifyToken, PaymentTypeController.deletePaymentType);

    //departments
    app.get('/departments', verifyToken, DepartmentController.getDepartments);
    app.post('/departments', verifyToken, DepartmentController.createDepartment);
    app.get('/departments/:id', verifyToken, DepartmentController.getDepartment);
    app.put('/departments/:id', verifyToken, DepartmentController.updateDepartment);
    app.delete('/departments/:id', verifyToken, DepartmentController.deleteDepartment);

    // element categories
    app.get('/elementcategories', verifyToken, ElementCategoryController.getElementCategories);
    app.post('/elementcategories', verifyToken, ElementCategoryController.createElementCategory);
    app.get('/elementcategories/:id', verifyToken, ElementCategoryController.getElementCategory);
    app.put('/elementcategories/:id', verifyToken, ElementCategoryController.updateElementCategory);
    app.delete('/elementcategories/:id', verifyToken, ElementCategoryController.deleteElementCategory);

    // price types
    app.get('/pricetypes', verifyToken, PriceTypeController.getPriceTypes);
    app.post('/pricetypes', verifyToken, PriceTypeController.createPriceType);
    app.get('/pricetypes/:id', verifyToken, PriceTypeController.getPriceType);
    app.put('/pricetypes/:id', verifyToken, PriceTypeController.updatePriceType);
    app.delete('/pricetypes/:id', verifyToken, PriceTypeController.deletePriceType);

    // elements
    app.get('/elements', verifyToken, ElementController.getElements);
    app.post('/elements', verifyToken, ElementController.createElement);
    app.get('/elements/:id', verifyToken, ElementController.getElement);
    app.put('/elements/:id', verifyToken, ElementController.updateElement);
    app.delete('/elements/:id', verifyToken, ElementController.deleteElement);
    
    app.get('/elementbyhotel/:id', verifyToken, ElementController.getElementByHotelId);

    
    // vip
    app.get('/vips', verifyToken, VipController.getVip);
    app.post('/vips', verifyToken, VipController.createVip);
    app.get('/vips/:id', verifyToken, VipController.getVip);
    app.put('/vips/:id', verifyToken, VipController.updateVip);
    app.delete('/vips/:id', verifyToken, VipController.deleteVip);

    // packages
    app.get('/packages', verifyToken, PackageController.getPackages);
    app.post('/packages', verifyToken, PackageController.createPackage);
    app.get('/packages/:id', verifyToken, PackageController.getPackage);
    app.put('/packages/:id', verifyToken, PackageController.updatePackage);
    app.delete('/packages/:id', verifyToken, PackageController.deletePackage);
    app.post('/packages-bulk', verifyToken, PackageController.createPackagesBulk);
    app.put('/packages-bulk/:id', verifyToken, PackageController.updatePackageBulk);
    app.get('/package-reservation', verifyToken, RateCodeSearchController.getPackagesRev);

    app.get('/packages/sub/:packgId', verifyToken, PackageController.getPackageSubs);
    app.post('/packages/sub/:packgId', verifyToken, PackageController.createPackageSub);
    app.post('/packages/bulk-sub/:packgId', verifyToken, PackageController.createBulkPackageSub);
    app.get('/packages/sub/getById/:id', verifyToken, PackageController.getPackageSub);
    app.put('/packages/sub/updateById/:id', verifyToken, PackageController.updatePackageSub);
    app.delete('/packages/sub/deleteById/:id', verifyToken, PackageController.deletePackageSub);

    app.get('/packages/detail/:packgId', verifyToken, PackageController.getPackageDetails);
    app.post('/packages/detail/:packgId', verifyToken, PackageController.createBulkPackageDetail);
    app.get('/packages/detail/getById/:id', verifyToken, PackageController.getPackageDetail);
    app.put('/packages/detail/updateById/:id', verifyToken, PackageController.updatePackageDetail);
    app.delete('/packages/detail/deleteById/:id', verifyToken, PackageController.deletePackageDetail);

    // ratecode
    app.get('/ratecodes', verifyToken, RateCodeController.getRateCode);
    app.get('/ratecode/:id', verifyToken, RateCodeController.getRateCodeDetail);
    app.post('/ratecode', verifyToken, RateCodeController.createRateCode);
    app.put('/ratecode/:id', verifyToken, RateCodeController.updateRateCode);
    app.delete('/ratecode/:id', verifyToken, RateCodeController.deleteRateCode);
    app.get('/barratecode/:hotelid', verifyToken, RateCodeSearchController.getBar);
    app.get('/ratecodehotel/category', verifyToken, RateCodeSearchController.getCategoryRate);

     // ratecode_surcharge
     app.get('/ratecode/surcharges/:rateid', verifyToken, RateCodeSurchargeController.getRateCodeSurcharge);
     app.get('/ratecode/surcharge/:code', verifyToken, RateCodeSurchargeController.getRateCodeSurchargeDetail);
     app.post('/ratecode/surcharge', verifyToken, RateCodeSurchargeController.createRateCodeSurcharge);
     app.put('/ratecode/surcharge/:code', verifyToken, RateCodeSurchargeController.updateRateCodeSurcharge);
     app.delete('/ratecode/surcharge/:code', verifyToken, RateCodeSurchargeController.deleteRateCodeSurcharge);

      // ratecode_blackout
      app.get('/ratecode/blackouts/:rateid', verifyToken, BlackoutController.getBlackOut);
      app.get('/ratecode/blackout/:code', verifyToken, BlackoutController.getBlackOutDetail);
      app.post('/ratecode/blackout', verifyToken, BlackoutController.createBlackOut);
      app.put('/ratecode/blackout/:code', verifyToken, BlackoutController.updateBlackOut);
      app.delete('/ratecode/blackout/:code', verifyToken, BlackoutController.deleteBlackOut);
 
    //items
    app.get('/items', verifyToken, ItemController.getItems);
    app.post('/items', verifyToken, ItemController.createItem);
    app.get('/items/:id', verifyToken, ItemController.getItem);
    app.put('/items/:id', verifyToken, ItemController.updateItem);
    app.delete('/items/:id', verifyToken, ItemController.deleteItem);
    app.put('/items/account/:id', verifyToken, ItemController.updateAccount);

    //Purchase Requisitions
    app.get('/purchase-requesitions', verifyToken, RequisitionController.getRequisitions);
    app.post('/purchase-requesitions', verifyToken, RequisitionController.createRequisition);
    app.get('/purchase-requesitions/:id', verifyToken, RequisitionController.getRequisition);
    app.put('/purchase-requesitions/:id', verifyToken, RequisitionController.updateRequisition);
    app.delete('/purchase-requesitions/:id', verifyToken, RequisitionController.deleteRequisition);
    app.get('/purchase-requesitions-code', verifyToken, RequisitionController.getRequisitionsCode);

    app.post('/purchase-requesitions-bulk',verifyToken, RequisitionController.createRequisitionBulk);
    
    app.put('/purchase-requesitions-bulk/:id',verifyToken, RequisitionController.updateRequisitionBulk);

    app.get('/purchase-requesitions/detail/:reqId', verifyToken, RequisitionController.getReqDetails);
    app.post('/purchase-requesitions/detail/:reqId', verifyToken, RequisitionController.createReqDetail);
    app.get('/purchase-requesitions/detail/getById/:id', verifyToken, RequisitionController.getReqDetail);
    app.put('/purchase-requesitions/detail/updateById/:id', verifyToken, RequisitionController.updateReqDetail);
    app.delete('/purchase-requesitions/detail/deleteById/:id', verifyToken, RequisitionController.deleteReqDetail);

    //Purchase Orders
    app.get('/purchase-orders', verifyToken, PurchaseOrderController.getOrders);
    app.post('/purchase-orders', verifyToken, PurchaseOrderController.createOrder);
    app.get('/purchase-orders/:id', verifyToken, PurchaseOrderController.getOrder);
    app.put('/purchase-orders/:id', verifyToken, PurchaseOrderController.updateOrder);
    app.delete('/purchase-orders/:id', verifyToken, PurchaseOrderController.deleteOrder);
    app.get('/purchase-orders-code', verifyToken, PurchaseOrderController.load_po_forreport);

    app.get('/purchase-orders/detail/:purchaseId', verifyToken, PurchaseOrderController.getOrderDetails);
    app.post('/purchase-orders/detail/:purchaseId', verifyToken, PurchaseOrderController.createOrderDetail);
    app.get('/purchase-orders/detail/getById/:id', verifyToken, PurchaseOrderController.getOrderDetail);
    app.put('/purchase-orders/detail/updateById/:id', verifyToken, PurchaseOrderController.updateOrderDetail);
    app.delete('/purchase-orders/detail/deleteById/:id', verifyToken, PurchaseOrderController.deleteOrderDetail);

    // receive item
    
    app.get('/receive-item', verifyToken, PurchaseOrderController.getOrdersReciveirtem);

    // received po
    app.get('/received-po', verifyToken, PurchaseOrderController.loadPoChecked);

    //Receive Orders
    app.get('/receive-orders', verifyToken, ReceiveOrderController.getOrders);
    app.post('/receive-orders', verifyToken, ReceiveOrderController.createOrder);
    app.get('/receive-orders/:id', verifyToken, ReceiveOrderController.getOrder);
    app.put('/receive-orders/:id', verifyToken, ReceiveOrderController.updateOrder);
    app.delete('/receive-orders/:id', verifyToken, ReceiveOrderController.deleteOrder);

    // get stock card
    app.get('/stockcard', verifyToken, ReceiveOrderController.getCardStock);
    
    //report back office
    app.get('/memoranduminvoice/:recicode', verifyToken, ReceiveOrderController.loadMi);
    app.get('/memoranduminvoicedate', verifyToken, ReceiveOrderController.loadMidate);
    app.get('/paymentvoucher',verifyToken,PaymentController.getPaymentVoucher);

    app.get('/receive-orders/detail/:recId', verifyToken, ReceiveOrderController.getOrderDetails);
    app.post('/receive-orders/detail/:recId', verifyToken, ReceiveOrderController.createOrderDetail);
    app.get('/receive-orders/detail/getById/:id', verifyToken, ReceiveOrderController.getOrderDetail);
    app.put('/receive-orders/detail/updateById/:id', verifyToken, ReceiveOrderController.updateOrderDetail);
    app.delete('/receive-orders/detail/deleteById/:id', verifyToken, ReceiveOrderController.deleteOrderDetail);

    //Store Requisition
    app.get('/store-requisitions', verifyToken, StoreReqController.getStoreReqs);
    app.post('/store-requisitions', verifyToken, StoreReqController.createStoreReq);
    app.get('/store-requisitions/:id', verifyToken, StoreReqController.getStoreReq);
    app.put('/store-requisitions/:id', verifyToken, StoreReqController.updateStoreReq);
    app.delete('/store-requisitions/:id', verifyToken, StoreReqController.deleteStoreReq);
    app.get('/store-requisitionsbycode', verifyToken, StoreReqController.getStoreReqsBycode);
    app.get('/store-requisitionsbydate', verifyToken, StoreReqController.getStoreReqsBydate);

    app.get('/store-requisitions/detail/:sreqId', verifyToken, StoreReqController.getStoreReqDetails);
    app.post('/store-requisitions/detail/:sreqId', verifyToken, StoreReqController.createStoreReqDetail);
    app.get('/store-requisitions/detail/getById/:id', verifyToken, StoreReqController.getStoreReqDetail);
    app.put('/store-requisitions/detail/updateById/:id', verifyToken, StoreReqController.updateStoreReqDetail);
    app.delete('/store-requisitions/detail/deleteById/:id', verifyToken, StoreReqController.deleteStoreReqDetail);

    // history item price
    app.get('/history-item-prices', verifyToken, HistoryItemPriceController.getHistoryItemPrices);
    app.post('/history-item-prices', verifyToken, HistoryItemPriceController.createHistoryItemPrice); 
    app.get('/history-item-prices/:id', verifyToken, HistoryItemPriceController.getHistoryItemPrice);
    app.put('/history-item-prices/:id', verifyToken, HistoryItemPriceController.updateHistoryItemPrice);
    app.delete('/history-item-prices/:id', verifyToken, HistoryItemPriceController.deleteHistoryItemPrice);

    // stock opname
    app.get('/stock-opname', verifyToken, StockOpnameController.getStockOpnames);
    app.post('/stock-opname', verifyToken, StockOpnameController.createStockOpname); 
    app.get('/stock-opname/:id', verifyToken, StockOpnameController.getStockOpname);
    app.put('/stock-opname/:id', verifyToken, StockOpnameController.updateStockOpname);
    app.delete('/stock-opname/:id', verifyToken, StockOpnameController.deleteStockOpname);
    app.post('/bulkstockopname', verifyToken, StockOpnameController.createBulkStockOpname);
    
    app.get('/stock-opname-subscategory', verifyToken, StockOpnameController.getSubscategory);
    app.get('/stock-opname-form', verifyToken, StockOpnameController.getFormStockOpname);
    app.get('/stock-opname-report', verifyToken, StockOpnameController.getStockOpnamesReport);

    // payments
    app.get('/payments', verifyToken, PaymentController.getPayments);
    app.post('/payments', verifyToken, PaymentController.createPayment);
    app.get('/payments/:id', verifyToken, PaymentController.getPayment);
    app.put('/payments/:id', verifyToken, PaymentController.updatePayment);
    app.delete('/payments/:id', verifyToken, PaymentController.deletePayment);

    app.get('/payments/detail/:payId', verifyToken, PaymentController.getPaymentDetails);
    app.post('/payments/detail/:payId', verifyToken, PaymentController.createPaymentDetail);
    app.get('/payments/detail/getById/:id', verifyToken, PaymentController.getPaymentDetail);
    app.put('/payments/detail/updateById/:id', verifyToken, PaymentController.updatePaymentDetail);
    app.delete('/payments/detail/deleteById/:id', verifyToken, PaymentController.deletePaymentDetail);

    app.get('/paymentmethods', verifyToken, PaymentMethodController.getPaymentsMethods);
    app.get('/paymentmethods/:id', verifyToken, PaymentMethodController.getPaymentsMethodDetail);
    app.post('/paymentmethods', verifyToken, PaymentMethodController.createPaymentMethods);
    app.put('/paymentmethods/:id', verifyToken, PaymentMethodController.updatePaymentMethods);
    app.delete('/paymentmethods/:id', verifyToken, PaymentMethodController.deletePaymentMethods);

    app.get('/payment-status', verifyToken, PaymentStatusController.getPaymentStatus);
    app.get('/payment-status/:id', verifyToken, PaymentStatusController.getPaymentStatusDetail);
    app.post('/payment-status', verifyToken, PaymentStatusController.createPaymentStatus);
    app.put('/payment-status/:id', verifyToken, PaymentStatusController.updatePaymentStatus);
    app.delete('/payment-status/:id', verifyToken, PaymentStatusController.deletePaymentStatus);

    // Booking
    app.get('/booking', verifyToken, BookingController.getBooking);
    app.get('/booking/:id', verifyToken, BookingController.getBookingDetail);
    app.post('/booking', verifyToken, BookingController.createBooking);
    app.put('/booking/:id', verifyToken, BookingController.updateBooking);
    app.delete('/booking/:id', verifyToken, BookingController.deleteBooking);

    app.get('/booking-status', verifyToken, BookingStatusController.getBookingStatus);
    app.get('/booking-status/:id', verifyToken, BookingStatusController.getBookingStatusDetail);
    app.post('/booking-status', verifyToken, BookingStatusController.createBookingStatus);
    app.put('/booking-status/:id', verifyToken, BookingStatusController.updateBookingStatus);
    app.delete('/booking-status/:id', verifyToken, BookingStatusController.deleteBookingStatus);

    // floor
    app.get('/floor', verifyToken, FloorController.getFloor);
    app.get('/floor/:id', verifyToken, FloorController.getFloorDetail);
    app.post('/floor', verifyToken, FloorController.createFloor);
    app.put('/floor/:id', verifyToken, FloorController.updateFloor);
    app.delete('/floor/:id', verifyToken, FloorController.deleteFloor);

    // addon
    app.get('/addon', verifyToken, AddonController.getAddon);
    app.get('/addon/:id', verifyToken, AddonController.getAddonDetail);
    app.post('/addon', verifyToken, AddonController.createAddon);
    app.put('/addon/:id', verifyToken, AddonController.updateAddon);
    app.delete('/addon/:id', verifyToken, AddonController.deleteAddon);

     // designation
     app.get('/designations', verifyToken, DesignationController.getDesignation);
     app.get('/designation/:id', verifyToken, DesignationController.getDesignationDetail);
     app.post('/designation', verifyToken, DesignationController.createDesignation);
     app.put('/designation/:id', verifyToken, DesignationController.updateDesignation);
     app.delete('/designation/:id', verifyToken, DesignationController.deleteDesignation);

    //  calendar
    app.get('/load-data-hotel-calendar/:hotelid',verifyToken,CalendarController.getCalendarHotel);
    app.get('/load-data-roomtype-calendar/:roomtypeid',verifyToken,CalendarController.getCalendarHotelRoomtype);
    app.get('/load-data-calendar/:hotel_room_id', verifyToken, CalendarController.loadDatesCalenadar);
    app.post('/create-calendar', verifyToken, CalendarController.createCalendar);
    app.delete('/delete-calendar', verifyToken, CalendarController.deleteCalendar);
    
    // room Calendar
    app.get('/load-data-room-calendar',verifyToken,RoomCalendarController.loadDatesCalenadar);
    app.post('/create-room-calendar',verifyToken,RoomCalendarController.createRoomCalendar);
    app.post('/create-autoroom-calendar',verifyToken,RoomCalendarController.createAutoRoomCalendar);
    app.delete('/delete-room-calendar',verifyToken,RoomCalendarController.deleteCalendar);
    app.get('/reservation-room-calendar',verifyToken,ReservationController.getReservationsDay);
    app.get('/show-room-reservation',verifyToken,RoomCalendarController.getRoomShowReservation);
    app.get('/show-room-for-hk',verifyToken,RoomCalendarController.getRoomForHousekeeping);

     // country
     app.get('/countries', verifyToken, CountryController.getCountry);
     app.get('/country/:id', verifyToken, CountryController.getCountryDetail);
     app.post('/country', verifyToken, CountryController.createCountry);
     app.put('/country/:id', verifyToken, CountryController.updateCountry);
     app.delete('/country/:id', verifyToken, CountryController.deleteCountry);

     // promocode
     app.get('/promocode', verifyToken, PromoCodeController.getPromoCode);
     app.get('/promocode/:id', verifyToken, PromoCodeController.getPromoCodeDetail);
     app.post('/promocode', verifyToken, PromoCodeController.createPromoCode);
     app.put('/promocode/:id', verifyToken, PromoCodeController.updatePromoCode);
     app.delete('/promocode/:id', verifyToken, PromoCodeController.deletePromoCode);

      // ratecodpromo
      app.get('/ratecodpromo', verifyToken, PromoRatecodeController.getPromoRatecode);
      app.get('/ratecodpromo/:id', verifyToken, PromoRatecodeController.getPromoRatecodeDetail);
      app.post('/ratecodpromo', verifyToken, PromoRatecodeController.createPromoRatecode);
      app.put('/ratecodpromo/:id', verifyToken, PromoRatecodeController.updatePromoRatecode);
      app.delete('/ratecodpromo/:id', verifyToken, PromoRatecodeController.deletePromoRatecode);

    // current year
    
    app.get('/currentyear', verifyToken, CurrentYearRoomtypeController.getCurrentYearRoomtypeAll);
    app.get('/currentyear/:hotelid', verifyToken, CurrentYearRoomtypeController.getCurrentYearRoomtype);
    app.get('/currentyeardetail/:id', verifyToken, CurrentYearRoomtypeController.getCurrentYearRoomtypeDetail);
    app.post('/currentyear', verifyToken, CurrentYearRoomtypeController.createCurrentYearRoomtype);
    app.put('/currentyear/:id', verifyToken, CurrentYearRoomtypeController.updateCurrentYearRoomtype);
    app.delete('/currentyear/:id', verifyToken, CurrentYearRoomtypeController.deleteCurrentYearRoomtype);

    // state
    app.get('/states', verifyToken, StateController.getState);
    app.get('/state/:id', verifyToken, StateController.getStateDetail);
    app.post('/state', verifyToken, StateController.createState);
    app.put('/state/:id', verifyToken, StateController.updateState);
    app.delete('/state/:id', verifyToken, StateController.deleteState);

    // employee
    app.get('/employees', verifyToken, EmployeeController.getEmployee);
    app.get('/employees-hk', verifyToken, EmployeeController.getEmployeeHk);
    app.get('/employee/:id', verifyToken, EmployeeController.getEmployeeDetail);
    app.post('/employee', verifyToken, EmployeeController.createEmployee);
    app.put('/employee/:id', verifyToken, EmployeeController.updateEmployee);
    app.delete('/employee/:id', verifyToken, EmployeeController.deleteEmployee);
    
    // city
    app.get('/cities', verifyToken, CityController.getCity);
    app.get('/city/:id', verifyToken, CityController.getCityDetail);
    app.post('/city', verifyToken, CityController.createCity);
    app.put('/city/:id', verifyToken, CityController.updateCity);
    app.delete('/city/:id', verifyToken, CityController.deleteCity);

     // identity-type
     app.get('/identity-types', verifyToken, Identity_typeController.getIdentity_type);
     app.get('/identity-type/:id', verifyToken, Identity_typeController.getIdentity_typeDetail);
     app.post('/identity-type', verifyToken, Identity_typeController.createIdentity_type);
     app.put('/identity-type/:id', verifyToken, Identity_typeController.updateIdentity_type);
     app.delete('/identity-type/:id', verifyToken, Identity_typeController.deleteIdentity_type);

    //  // blackout
    //  app.get('/blackout', verifyToken, BlackoutController.getBlackout);
    //  app.get('/blackout/:id', verifyToken, BlackoutController.getBlackoutDetail);
    //  app.post('/blackout', verifyToken, BlackoutController.createBlackout);
    //  app.put('/blackout/:id', verifyToken, BlackoutController.updateBlackout);
    //  app.delete('/blackout/:id', verifyToken, BlackoutController.deleteBlackout);

      // blackout
      app.get('/payment-booking', verifyToken, PaymentBookingController.getPaymentBooking);
      app.get('/payment-booking/:id', verifyToken, PaymentBookingController.getPaymentBookingDetail);
      app.post('/payment-booking', verifyToken, PaymentBookingController.createPaymentBooking);
      app.put('/payment-booking/:id', verifyToken, PaymentBookingController.updatePaymentBooking);
      app.delete('/payment-booking/:id', verifyToken, PaymentBookingController.deletePaymentBooking);
     
     // hotel
     app.get('/hotel', verifyToken, HotelController.getHotel);
     app.get('/hotel/:id', verifyToken, HotelController.getHotelDetail);
     app.post('/hotel', verifyToken, HotelController.createHotel);
     app.put('/hotel/:id', verifyToken, HotelController.updateHotel);
     app.delete('/hotel/:id', verifyToken, HotelController.deleteHotel);

     // surcharge
     app.get('/surcharge', verifyToken, SurchargeController.getSurcharge);
     app.get('/surcharge/:id', verifyToken, SurchargeController.getSurchargeDetail);
     app.post('/surcharge', verifyToken, SurchargeController.createSurcharge);
     app.put('/surcharge/:id', verifyToken, SurchargeController.updateSurcharge);
     app.delete('/surcharge/:id', verifyToken, SurchargeController.deleteSurcharge);
    
     // bedtype
     app.get('/bedtype', verifyToken, BedTypeController.getBedType);
     app.get('/bedtype/:id', verifyToken, BedTypeController.getBedTypeDetail);
     app.post('/bedtype', verifyToken, BedTypeController.createBedType);
     app.put('/bedtype/:id', verifyToken, BedTypeController.updateBedType);
     app.delete('/bedtype/:id', verifyToken, BedTypeController.deleteBedType);

    // feauture
    app.get('/feauture', verifyToken, FeautureController.getFeature);
    app.get('/feauture/:id', verifyToken, FeautureController.getFeatureDetail);
    app.post('/feauture', verifyToken, FeautureController.createFeature);
    app.put('/feauture/:id', verifyToken, FeautureController.updateFeature);
    app.delete('/feauture/:id', verifyToken, FeautureController.deleteFeature);

    // assets
    app.get('/assets', verifyToken, AssetController.getAssets);
    app.post('/assets', verifyToken, AssetController.createAsset);
    app.get('/assets/:id', verifyToken, AssetController.getAsset);
    app.put('/assets/:id', verifyToken, AssetController.updateAsset);
    app.delete('/assets/:id', verifyToken, AssetController.deleteAsset);

    app.get('/assets/detail/:assetId', verifyToken, AssetController.getAssetDetails);
    app.post('/assets/detail/:assetId', verifyToken, AssetController.createAssetDetail);
    app.get('/assets/detail/getById/:id', verifyToken, AssetController.getAssetDetail);
    app.put('/assets/detail/updateById/:id', verifyToken, AssetController.updateAssetDetail);
    app.delete('/assets/detail/deleteById/:id', verifyToken, AssetController.deleteAssetDetail);

    // journals
    app.get('/journals', verifyToken, JournalController.getJournalMasters);
    app.post('/journals', verifyToken, JournalController.createJournalMaster);
    app.get('/journals/:id', verifyToken, JournalController.getJournalMaster);
    app.put('/journals/:id', verifyToken, JournalController.updateJournalMaster);
    app.delete('/journals/:id', verifyToken, JournalController.deleteJournalMaster);

    app.get('/journals/detail/:journalId', verifyToken, JournalController.getJournalDetails);
    app.post('/journals/detail/:journalId', verifyToken, JournalController.createJournalDetail);
    app.get('/journals/detail/getById/:id', verifyToken, JournalController.getJournalDetail);
    app.put('/journals/detail/updateById/:id', verifyToken, JournalController.updateJournalDetail);
    app.delete('/journals/detail/deleteById/:id', verifyToken, JournalController.deleteJournalDetail);

    // badstocks
    app.get('/badstocks', verifyToken, BadStockController.getBadStocks);
    app.post('/badstocks', verifyToken, BadStockController.createBadStock);
    app.get('/badstocks/:id', verifyToken, BadStockController.getBadStock);
    app.put('/badstocks/:id', verifyToken, BadStockController.updateBadStock);
    app.delete('/badstocks/:id', verifyToken, BadStockController.deleteBadStock);
    
    app.get('/badstockscode', verifyToken, BadStockController.getBadStocksCode);

    app.get('/badstocks/detail/:bstockId', verifyToken, BadStockController.getBadStockDetails);
    app.post('/badstocks/detail/:bstockId', verifyToken, BadStockController.createBadStockDetail);
    app.get('/badstocks/detail/getById/:id', verifyToken, BadStockController.getBadStockDetail);
    app.put('/badstocks/detail/updateById/:id', verifyToken, BadStockController.updateBadStockDetail);
    app.delete('/badstocks/detail/deleteById/:id', verifyToken, BadStockController.deleteBadStockDetail);


    //Direct Orders
    app.post('/direct-orders', verifyToken, DirectOrderController.createOrder);

    //roles
    app.get('/roles/:appid', verifyToken, RoleController.getRoles);  
    app.post('/roles', RoleController.createRole);
    app.get('/roles/:id', verifyToken, RoleController.getRole);
    app.put('/roles/:id', verifyToken, RoleController.updateRole);
    app.delete('/roles/:id', verifyToken, RoleController.deleteRole);

    //modules
    app.get('/modules', verifyToken, ModuleController.getModules);
    app.get('/modules/:appid', verifyToken, ModuleController.getModulesByApp);  
    app.post('/modules', ModuleController.createModule);
    app.get('/modules/:id', verifyToken, ModuleController.getModule);
    app.put('/modules/:id', verifyToken, ModuleController.updateModule);
    app.delete('/modules/:id', verifyToken, ModuleController.deleteModule);

    //module menus
    app.get('/modulemenus', verifyToken, ModuleMenuController.getModuleMenus);  
    app.get('/modulemenus/:moduleid', verifyToken, ModuleMenuController.getMenusByModule);  
    app.post('/modulemenus', ModuleMenuController.createModuleMenu);  
    app.get('/modulemenus/:id', verifyToken, ModuleMenuController.getModuleMenu);  
    app.put('/modulemenus/:id', verifyToken, ModuleMenuController.updateModuleMenu);  
    app.delete('/modulemenus/:id', verifyToken, ModuleMenuController.deleteModuleMenu);  

    //user modules
    app.get('/usermodules', verifyToken, UserModuleController.getUserModules);
    app.post('/usermodules', UserModuleController.createUserModule);
    app.get('/usermodules/:id', verifyToken, UserModuleController.getUserModule);
    app.put('/usermodules/:id', verifyToken, UserModuleController.updateUserModule);
    app.delete('/usermodules/:id', verifyToken, UserModuleController.deleteUserModule);

    // setting
    app.get('/settings', verifyToken, SettingsController.getSettings);
    app.get('/setting/:id', verifyToken, SettingsController.getSettingsDetail);
    app.post('/setting', verifyToken, SettingsController.createSettings);
    app.put('/setting/:id', verifyToken, SettingsController.updateSettings);
    app.delete('/setting/:id', verifyToken, SettingsController.deleteSettings);
    
    // page
    app.get('/pages', verifyToken, PageController.getPage);
    app.get('/page/:id', verifyToken, PageController.getPageDetail);
    app.post('/page', verifyToken, PageController.createPage);
    app.put('/page/:id', verifyToken, PageController.updatePage);
    app.delete('/page/:id', verifyToken, PageController.deletePage);

      // banner
    app.get('/banners', verifyToken, BannerController.getBanner);
    app.get('/banner/:id', verifyToken, BannerController.getBannerDetail);
    app.post('/banner', verifyToken, BannerController.createBanner);
    app.put('/banner/:id', verifyToken, BannerController.updateBanner);
    app.delete('/banner/:id', verifyToken, BannerController.deleteBanner);
      
      // currencie
    app.get('/currencies', verifyToken, CurrenciesController.getCurrencies);
    app.get('/currencie/:id', verifyToken, CurrenciesController.getCurrenciesDetail);
    app.post('/currencie', verifyToken, CurrenciesController.createCurrencies);
    app.put('/currencie/:id', verifyToken, CurrenciesController.updateCurrencies);
    app.delete('/currencie/:id', verifyToken, CurrenciesController.deleteCurrencies);

    // language
    app.get('/languages', verifyToken, LanguagesController.getLanguages);
    app.get('/language/:id', verifyToken, LanguagesController.getLanguagesDetail);
    app.post('/language', verifyToken, LanguagesController.createLanguages);
    app.put('/language/:id', verifyToken, LanguagesController.updateLanguages);
    app.delete('/language/:id', verifyToken, LanguagesController.deleteLanguages);
    
    // housekeeping
    app.get('/housekeepings', verifyToken, HouseKeepingController.getHouseKeeping);
    app.get('/housekeeping/:id', verifyToken, HouseKeepingController.getHouseKeepingDetail);
    app.post('/housekeeping', verifyToken, HouseKeepingController.createHouseKeeping);
    app.put('/housekeeping/:id', verifyToken, HouseKeepingController.updateHouseKeeping);
    app.delete('/housekeeping/:id', verifyToken, HouseKeepingController.deleteHouseKeeping);

     // mail-template
     app.get('/mail-templates', verifyToken, Mail_templateController.getMail_template);
     app.get('/mail-template/:id', verifyToken, Mail_templateController.getMail_templateDetail);
     app.post('/mail-template', verifyToken, Mail_templateController.createMail_template);
     app.put('/mail-template/:id', verifyToken, Mail_templateController.updateMail_template);
     app.delete('/mail-template/:id', verifyToken, Mail_templateController.deleteMail_template);

     // gallery
     app.get('/gallerys', verifyToken, GalleryController.getGallery);
     app.get('/gallery/:id', verifyToken, GalleryController.getGalleryDetail);
     app.post('/gallery', verifyToken, GalleryController.createGallery);
     app.put('/gallery/:id', verifyToken, GalleryController.updateGallery);
     app.delete('/gallery/:id', verifyToken, GalleryController.deleteGallery);
    //Role Menus
    app.get('/rolemenus/:roleId', verifyToken, RoleMenuController.getRoleMenus);
    app.get('/rolemenus/appmenus/:roleId', verifyToken, RoleMenuController.getAppMenus);   
    app.post('/rolemenus', RoleMenuController.createRoleMenu);
    app.get('/rolemenus/:id', verifyToken, RoleMenuController.detailRoleMenu);
    app.put('/rolemenus/:id', verifyToken, RoleMenuController.updateRoleMenu);
    app.delete('/rolemenus/:id', verifyToken, RoleMenuController.deleteRoleMenu);

    // Front Office 
    app.get('/list-fo-invoice', verifyToken, FrontOfficeController.getFoInvoces);
    app.get('/list-guest-inhouse', verifyToken, FrontOfficeController.getGuestInHouse);
    app.get('/list-reservation', verifyToken, FrontOfficeController.getListReservations);
    app.get('/list-expected-arrival', verifyToken, FrontOfficeController.getExpectedArrival);
    app.get('/list-expected-departure', verifyToken, FrontOfficeController.getExpectedDeparture);
    app.get('/list-op-closing', verifyToken, FrontOfficeController.getOpClosing);
    app.get('/list-resto-closing', verifyToken, FrontOfficeController.getRestoClosing);
    app.get('/list-resto-closing-na', verifyToken, FrontOfficeController.getRestoClosingNa);
    app.get('/list-fonightaudit', verifyToken, FrontOfficeController.getNightAuditfotrx);

    // Report front office
    app.get('/report/roomrevenuebreakdown', verifyToken, FrontOfficeController.getRevenueRoomBreakdown);
    app.get('/report/closing', verifyToken,FrontOfficeController.getReportClosing);
    app.get('/report/masterbill',verifyToken, MasterBillController.getReportMasterBills);

    app.get('/guestmasterbill',verifyToken, MasterBillController.getGuestMasterBills);

    // reservations
    app.get('/reservations', verifyToken, ReservationController.getReservations);
    // app.post('/reservations', ReservationController.createReservation);
    app.get('/reservations/:id', verifyToken, ReservationController.getReservationById);
    // app.put('/reservations/:id', verifyToken, ReservationController.updateReservation);
    // app.delete('/reservations/:id', verifyToken, ReservationController.deleteReservation);
    app.post('/createreservationbulk', verifyToken, ReservationController.createReservationBulk);
    app.put('/updatereservationbulk/:id', verifyToken, ReservationController.updateReservation);
    // app.put('/updatereservationbulk/:id', verifyToken, ReservationController.updateReservationBulk);
    // app.get('/reservationbulkdetail/:id', verifyToken,ReservationController.getByIdReservation);

    // reservation details
    // app.get('/reservations/details/:resId', verifyToken, ReservationController.getReservationDetails);
    // app.post('/reservations/details/:resId', ReservationController.createReservationDetail);
    // app.get('/reservations/detail/:id', verifyToken, ReservationController.getReservationDetail);
    // app.put('/reservations/detail/:id', verifyToken, ReservationController.updateReservationDetail);
    // app.delete('/reservations/detail/:id', verifyToken, ReservationController.deleteReservationDetail);

    // reservation guests
    // app.get('/reservations/guests/:resId', verifyToken, ReservationController.getReservationGuests);
    // app.post('/reservations/guests/:resId', ReservationController.createReservationGuest);
    // app.get('/reservations/guest/:id', verifyToken, ReservationController.getReservationGuest);
    // app.put('/reservations/guest/:id', verifyToken, ReservationController.updateReservationGuest);
    // app.delete('/reservations/guest/:id', verifyToken, ReservationController.deleteReservationGuest);

    // FO Transactions
    app.get('/fotransactions', verifyToken, FrontOfficeController.getFoTrxes);
    app.post('/fotransactions', FrontOfficeController.createFoTrx);
    app.get('/fotransactions/:id', verifyToken, FrontOfficeController.getFoTrx);
    app.put('/fotransactions/:id', verifyToken, FrontOfficeController.updateFoTrx);
    app.delete('/fotransactions/:id', verifyToken, FrontOfficeController.deleteFoTrx);

    // FO Transaction details
    app.get('/fotransactions/details/:resvNo', verifyToken, FrontOfficeController.getFoTrxDetails);
    app.post('/fotransactions/details/:resvNo', FrontOfficeController.createFoTrxDetail);
    app.get('/fotransactions/detail/:id', verifyToken, FrontOfficeController.getFoTrxDetail);
    app.put('/fotransactions/detail/:id', verifyToken, FrontOfficeController.updateFoTrxDetail);
    app.put('/fotransactions/detail/closing/:id', verifyToken, FrontOfficeController.closingFoTrxDetail);
    app.post('/fotransaction/detail/allclosing', verifyToken, FrontOfficeController.closingallFoTrxDetail);
    app.put('/fotransaction/detail/nightaudit/:id',verifyToken, FrontOfficeController.nightauditFoTrxDetail);
    app.post('/fotransaction/detail/allnightaudit',verifyToken, FrontOfficeController.nightauditallFoTrxDetail);
    app.delete('/fotransactions/detail/:id', verifyToken, FrontOfficeController.deleteFoTrxDetail);

    // Master Bill
    app.get('/masterbills', verifyToken, MasterBillController.getMasterBills);
    app.post('/masterbills', MasterBillController.createMasterBill);
    app.get('/masterbills/:id', verifyToken, MasterBillController.getMasterBill);
    app.put('/masterbills/:id', verifyToken, MasterBillController.updateMasterBill);
    app.delete('/masterbills/:id', verifyToken, MasterBillController.deleteMasterBill);

    // Master Bill details
    app.get('/masterbills/details/:masterId', verifyToken, MasterBillController.getMasterBillDetails);
    app.post('/masterbills/details/:masterId', MasterBillController.createMasterBillDetail);
    app.get('/masterbills/detail/:id', verifyToken, MasterBillController.getMasterBillDetail);
    app.put('/masterbills/detail/:id', verifyToken, MasterBillController.updateMasterBillDetail);
    app.delete('/masterbills/detail/:id', verifyToken, MasterBillController.deleteMasterBillDetail);

    //--------------------------------
    // Sub Class
    // --------------------------------
    app.get('/account-sub-class', verifyToken, AccountController.getSubClases);
    app.post('/account-sub-class', AccountController.createSubClass);
    app.get('/account-sub-class/:id', verifyToken, AccountController.getSubClass);
    app.put('/account-sub-class/:id', verifyToken, AccountController.updateSubClass);
    app.delete('/account-sub-class/:id', verifyToken, AccountController.deleteSubClass);

    //--------------------------------
    // Chat Of Account
    // --------------------------------

    app.get('/accounts/subclass/:accsubNo', verifyToken, AccountController.getAccountBySubClass);

    app.get('/accounts', verifyToken, AccountController.getAccounts);
    app.post('/accounts', AccountController.createAccount);
    app.get('/accounts/:id', verifyToken, AccountController.getAccount);
    app.put('/accounts/:id', verifyToken, AccountController.updateAccount);
    app.delete('/accounts/:id', verifyToken, AccountController.deleteAccount);

    
};
