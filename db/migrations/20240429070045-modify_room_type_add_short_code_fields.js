'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn(
        'RoomTypes', // table name
        'short_code', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'description', // add this option
        },
      ),
      queryInterface.addColumn(
        'RoomTypes', // table name
        'min_occupancy', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'description', // add this option
        },
      ),
      queryInterface.addColumn(
        'RoomTypes', // table name
        'max_occupancy', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'description', // add this option
        },
      ),
      queryInterface.addColumn(
        'RoomTypes', // table name
        'is_extrabed', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'description', // add this option
        },
      ),
      queryInterface.addColumn(
        'RoomTypes', // table name
        'extrabed_occupancy', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'description', // add this option
        },
      ),
      queryInterface.addColumn(
        'RoomTypes', // table name
        'extrabed_price', // new field name
        {
          type: Sequelize.DOUBLE,
          allowNull: true,
          after: 'description', // add this option
        },
      ),
    
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
