'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Journal_Details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      journal_id: {
        type: Sequelize.INTEGER
      },
      account_no: {
        type: Sequelize.STRING
      },
      debet: {
        type: Sequelize.DOUBLE
      },
      kredit: {
        type: Sequelize.DOUBLE
      },
      tanggal_input: {
        type: Sequelize.DATE
      },
      tanggal_sistem: {
        type: Sequelize.DATE
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Journal_Details');
  }
};