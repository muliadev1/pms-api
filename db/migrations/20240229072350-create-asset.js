'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Assets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      asset_code: {
        type: Sequelize.STRING
      },
      item_code: {
        type: Sequelize.STRING
      },
      baik: {
        type: Sequelize.INTEGER
      },
      kurangbaik: {
        type: Sequelize.INTEGER
      },
      used: {
        type: Sequelize.INTEGER
      },
      lose: {
        type: Sequelize.INTEGER
      },
      out: {
        type: Sequelize.INTEGER
      },
      jumlah: {
        type: Sequelize.INTEGER
      },
      baikHK: {
        type: Sequelize.INTEGER
      },
      kurangbaikHK: {
        type: Sequelize.INTEGER
      },
      usedHK: {
        type: Sequelize.INTEGER
      },
      loseHK: {
        type: Sequelize.INTEGER
      },
      outHK: {
        type: Sequelize.INTEGER
      },
      jumlahHK: {
        type: Sequelize.INTEGER
      },
      baikFO: {
        type: Sequelize.INTEGER
      },
      kurangbaikFO: {
        type: Sequelize.INTEGER
      },
      usedFO: {
        type: Sequelize.INTEGER
      },
      loseFO: {
        type: Sequelize.INTEGER
      },
      outFO: {
        type: Sequelize.INTEGER
      },
      jumlahFO: {
        type: Sequelize.INTEGER
      },
      baikFB: {
        type: Sequelize.INTEGER
      },
      kurangbaikFB: {
        type: Sequelize.INTEGER
      },
      usedFB: {
        type: Sequelize.INTEGER
      },
      loseFB: {
        type: Sequelize.INTEGER
      },
      outFB: {
        type: Sequelize.INTEGER
      },
      jumlahFB: {
        type: Sequelize.INTEGER
      },
      baikBO: {
        type: Sequelize.INTEGER
      },
      kurangbaikBO: {
        type: Sequelize.INTEGER
      },
      usedBO: {
        type: Sequelize.INTEGER
      },
      loseBO: {
        type: Sequelize.INTEGER
      },
      outBO: {
        type: Sequelize.INTEGER
      },
      jumlahBO: {
        type: Sequelize.INTEGER
      },
      baikEng: {
        type: Sequelize.INTEGER
      },
      kurangbaikEng: {
        type: Sequelize.INTEGER
      },
      usedEng: {
        type: Sequelize.INTEGER
      },
      loseEng: {
        type: Sequelize.INTEGER
      },
      outEng: {
        type: Sequelize.INTEGER
      },
      jumlahEng: {
        type: Sequelize.INTEGER
      },
      baikPG: {
        type: Sequelize.INTEGER
      },
      kurangbaikPG: {
        type: Sequelize.INTEGER
      },
      usedPG: {
        type: Sequelize.INTEGER
      },
      losePG: {
        type: Sequelize.INTEGER
      },
      outPG: {
        type: Sequelize.INTEGER
      },
      jumlahPG: {
        type: Sequelize.INTEGER
      },
      baikSec: {
        type: Sequelize.INTEGER
      },
      kurangbaikSec: {
        type: Sequelize.INTEGER
      },
      usedSec: {
        type: Sequelize.INTEGER
      },
      loseSec: {
        type: Sequelize.INTEGER
      },
      outSec: {
        type: Sequelize.INTEGER
      },
      jumlahSec: {
        type: Sequelize.INTEGER
      },
      baikBar: {
        type: Sequelize.INTEGER
      },
      kurangbaikBar: {
        type: Sequelize.INTEGER
      },
      usedBar: {
        type: Sequelize.INTEGER
      },
      loseBar: {
        type: Sequelize.INTEGER
      },
      outBar: {
        type: Sequelize.INTEGER
      },
      jumlahBar: {
        type: Sequelize.INTEGER
      },
      is_active: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Assets');
  }
};