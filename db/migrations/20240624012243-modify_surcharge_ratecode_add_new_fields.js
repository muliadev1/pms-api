'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'ratecode_surcharges', // table name
        'period_start', // new field name
        {
          type: Sequelize.DATE,
          allowNull: true,
          after: 'amount', // add this option
        },
      ),
      queryInterface.addColumn(
        'ratecode_surcharges', // table name
        'period_end', // new field name
        {
          type: Sequelize.DATE,
          allowNull: true,
          after: 'amount', // add this option
        },
      ),queryInterface.addColumn(
        'ratecode_surcharges', // table name
        'code', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'amount', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
