'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    
    await queryInterface.renameTable('designations', 'designation');
    await queryInterface.renameTable('identity_types', 'identity_type');
    await queryInterface.renameTable('countries', 'country');
    await queryInterface.renameTable('states', 'state');
    await queryInterface.renameTable('employees', 'employee');
    await queryInterface.renameTable('housekeepings', 'housekeeping');
    await queryInterface.renameTable('calendars', 'calendar');
    await queryInterface.renameTable('mail_templates', 'mail_template');
    await queryInterface.renameTable('pages', 'page');
    await queryInterface.renameTable('banners', 'banner');
    await queryInterface.renameTable('galleries', 'gallery');

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
