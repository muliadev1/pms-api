'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'FoTrx_Details', // table name
        'reservation_id', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'reservation_no', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
