'use strict';

//** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Categories', // table name
        'createdBy', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'Categories', // table name
        'updatedBy', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
     // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn('Categories', 'createdBy'),
      queryInterface.removeColumn('Categories', 'updatedBy'),
    ]);
  }
};
