'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn(
        'reservation_details', // table name
        'room_id', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'night_no', // add this option
        },
      ),
      queryInterface.addColumn(
        'reservation_details', // table name
        'room_type_id', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'night_no', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
