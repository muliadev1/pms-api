'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Companies', // table name
        'contact_first_name', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'Companies', // table name
        'contact_last_name', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'Companies', // table name
        'contact_address', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'Companies', // table name
        'contact_phone', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'Companies', // table name
        'contact_email', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn('Companies', 'contact_first_name'),
      queryInterface.removeColumn('Companies', 'contact_last_name'),
      queryInterface.removeColumn('Companies', 'contact_address'),
      queryInterface.removeColumn('Companies', 'contact_phone'),
      queryInterface.removeColumn('Companies', 'contact_email'),
    ]);
  }
};
