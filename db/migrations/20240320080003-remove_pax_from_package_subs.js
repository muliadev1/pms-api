'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn(
        'Package_Mains', // table name
        'pax', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'package_name', // add this option
        },
      ),
     queryInterface.removeColumn('Package_Subs', 'pax'),
    
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
