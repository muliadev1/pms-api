'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Payments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      voucher_no: {
        type: Sequelize.STRING
      },
      voucher_date: {
        type: Sequelize.DATE
      },
      vendor: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.STRING
      },
      payment_method_id: {
        type: Sequelize.INTEGER
      },
      bank: {
        type: Sequelize.STRING
      },
      check_no: {
        type: Sequelize.STRING
      },
      total: {
        type: Sequelize.DOUBLE
      },
      operator_id: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Payments');
  }
};