'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Rate_codes', // table name
        'deposit_policy', // new field name
        {
          type: Sequelize.TEXT,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'cancelation_policy', // new field name
        {
          type: Sequelize.TEXT,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'other_conditions', // new field name
        {
          type: Sequelize.TEXT,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
