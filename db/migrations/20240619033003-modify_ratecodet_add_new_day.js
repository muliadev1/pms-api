'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Rate_codes', // table name
        'is_sun', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'is_mon', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'is_tue', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'is_wed', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'is_thu', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'is_fri', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'is_sat', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
