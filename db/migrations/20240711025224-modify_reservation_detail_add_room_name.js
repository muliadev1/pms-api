'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
  
    await queryInterface.addColumn('Reservation_Details', 'per_night_price', {
      type: Sequelize.DOUBLE,
      allowNull: true,
      after: 'room_id'
    });
    await queryInterface.addColumn('Reservation_Details', 'roomtype_name', {
      type: Sequelize.STRING,
      allowNull: true,
      after: 'per_night_price'
    });
    await queryInterface.addColumn('Reservation_Details', 'description_roomtype', {
      type: Sequelize.STRING,
      allowNull: true,
      after: 'roomtype_name'
    });
    await queryInterface.addColumn('Reservation_Details', 'short_code_roomtype', {
      type: Sequelize.STRING,
      allowNull: true,
      after: 'description_roomtype'
    });
    await queryInterface.addColumn('Reservation_Details', 'price_input_roomtype', {
      type: Sequelize.DOUBLE,
      allowNull: true,
      after: 'short_code_roomtype'
    });
    await queryInterface.addColumn('Reservation_Details', 'adult_roomtype', {
      type: Sequelize.INTEGER,
      allowNull: true,
      after: 'price_input_roomtype'
    });
    await queryInterface.addColumn('Reservation_Details', 'child_roomtype', {
      type: Sequelize.INTEGER,
      allowNull: true,
      after: 'adult_roomtype'
    });
    await queryInterface.addColumn('Reservation_Details', 'is_extrabed_roomtype', {
      type: Sequelize.INTEGER,
      allowNull: true,
      after: 'child_roomtype'
    });
    await queryInterface.addColumn('Reservation_Details', 'hotel_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      after: 'is_extrabed_roomtype'
    });
    await queryInterface.addColumn('Reservation_Details', 'ratecode_json', {
      type: Sequelize.JSON,
      allowNull: true,
      after: 'hotel_id'
    });
    await queryInterface.addColumn('Reservation_Details', 'promo_json', {
      type: Sequelize.JSON,
      allowNull: true,
      after: 'ratecode_json'
    });

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
