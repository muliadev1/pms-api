'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn(
        'Rooms', // table name
        'floor_id', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'is_active', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rooms', // table name
        'room_number', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'is_active', // add this option
        },
      ),
    
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
