'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Rate_codes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      hotel_id: {
        type: Sequelize.INTEGER
      },
      rate_code: {
        type: Sequelize.STRING
      },
      rate_description: {
        type: Sequelize.STRING
      },
      min_night: {
        type: Sequelize.INTEGER
      },
      start_booking: {
        type: Sequelize.DATE
      },
      end_booking: {
        type: Sequelize.DATE
      },
      start_stay: {
        type: Sequelize.DATE
      },
      end_stay: {
        type: Sequelize.DATE
      },
      adult: {
        type: Sequelize.INTEGER
      },
      child: {
        type: Sequelize.INTEGER
      },
      pax: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Rate_codes');
  }
};