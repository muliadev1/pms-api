'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Reservations', 'reservation_comments', {
      type: Sequelize.TEXT,
      allowNull: true,
      after: 'is_group'
    });

    await queryInterface.addColumn('Reservations', 'hotel_special_intruction', {
      type: Sequelize.TEXT,
      allowNull: true,
      after: 'is_group'
    });

    await queryInterface.addColumn('Reservations', 'payment_detail_json', {
      type: Sequelize.JSON,
      allowNull: true,
      after: 'is_group'
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
