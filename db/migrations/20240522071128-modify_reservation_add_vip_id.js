'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn(
        'reservations', // table name
        'vip_id', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'deposit', // add this option
        },
      ),
      queryInterface.addColumn(
        'reservations', // table name
        'payment_type_id', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'deposit', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
