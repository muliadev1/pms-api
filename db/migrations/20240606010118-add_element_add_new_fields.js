'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn(
        'elements', // table name
        'adult_rate', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'price', // add this option
        },
      ),
      queryInterface.addColumn(
        'elements', // table name
        'child_rate', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'price', // add this option
        },
      ),
      queryInterface.addColumn(
        'elements', // table name
        'room_rate', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'price', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
