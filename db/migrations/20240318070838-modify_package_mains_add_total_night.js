'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Package_Mains', // table name
        'totalnight', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'package_name', // add this option
        },
      ),
      queryInterface.addColumn(
        'Package_Details', // table name
        'daily', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'qty', // add this option
        },
      ),
      queryInterface.addColumn(
        'Package_Subs', // table name
        'pax', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'room_price', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
