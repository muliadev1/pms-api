'use strict';

//** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'SubCategory', // table name
        'createdBy', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'SubCategory', // table name
        'updatedBy', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
     // logic for reverting the changes
    return Promise.all([
      queryInterface.removeColumn('SubCategory', 'createdBy'),
      queryInterface.removeColumn('SubCategory', 'updatedBy'),
    ]);
  }
};

