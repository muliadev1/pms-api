'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'Rate_codes', // table name
        'category_rate', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'id', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'percentage', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
      queryInterface.addColumn(
        'Rate_codes', // table name
        'amount', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
    ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
