'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('SR_Details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      store_req_id: {
        type: Sequelize.INTEGER
      },
      item_code: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      unit: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.DOUBLE
      },
      qty_order: {
        type: Sequelize.DOUBLE
      },
      curr_stock: {
        type: Sequelize.DOUBLE
      },
      department_id: {
        type: Sequelize.INTEGER
      },
      department: {
        type: Sequelize.STRING
      },
      operator_id: {
        type: Sequelize.INTEGER
      },
      status_in_out: {
        type: Sequelize.INTEGER
      },
      is_SO: {
        type: Sequelize.INTEGER
      },
      notes: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('SR_Details');
  }
};