'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Settings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      hotel_id: {
        type: Sequelize.INTEGER
      },
      logo: {
        type: Sequelize.STRING
      },
      footer_text: {
        type: Sequelize.STRING
      },
      min_booking: {
        type: Sequelize.STRING
      },
      checkin_time: {
        type: Sequelize.STRING
      },
      checkout_time: {
        type: Sequelize.STRING
      },
      default_currency: {
        type: Sequelize.STRING
      },
      default_language: {
        type: Sequelize.STRING
      },
      default_date_format: {
        type: Sequelize.STRING
      },
      advance_payment_percentage: {
        type: Sequelize.STRING
      },
      taxes: {
        type: Sequelize.STRING
      },
      is_maintenance: {
        type: Sequelize.INTEGER
      },
      sender_email: {
        type: Sequelize.STRING
      },
      sender_name: {
        type: Sequelize.STRING
      },
      is_use_smtp: {
        type: Sequelize.INTEGER
      },
      smtp_security: {
        type: Sequelize.STRING
      },
      is_smtp_authentication: {
        type: Sequelize.INTEGER
      },
      smtp_host: {
        type: Sequelize.STRING
      },
      smtp_port: {
        type: Sequelize.STRING
      },
      email_username: {
        type: Sequelize.STRING
      },
      email_password: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Settings');
  }
};