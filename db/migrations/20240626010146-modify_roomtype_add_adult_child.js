'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'room_type', // table name
        'adult', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'rate', // add this option
        },
      ),
      queryInterface.addColumn(
        'room_type', // table name
        'child', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'rate', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
