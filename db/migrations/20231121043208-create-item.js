'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Items', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      item_code: {
        type: Sequelize.STRING
      },
      item_description: {
        type: Sequelize.STRING
      },
      subcategory_id: {
        type: Sequelize.INTEGER
      },
      unit: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.FLOAT
      },
      stock: {
        type: Sequelize.FLOAT
      },
      is_active: {
        type: Sequelize.INTEGER
      },
      stockHK: {
        type: Sequelize.FLOAT
      },
      stockFO: {
        type: Sequelize.FLOAT
      },
      stockFB: {
        type: Sequelize.FLOAT
      },
      stockBO: {
        type: Sequelize.FLOAT
      },
      stockBAR: {
        type: Sequelize.FLOAT
      },
      stockSEC: {
        type: Sequelize.FLOAT
      },
      stockPG: {
        type: Sequelize.FLOAT
      },
      stockENG: {
        type: Sequelize.FLOAT
      },
      vendor_id: {
        type: Sequelize.INTEGER
      },
      is_HK: {
        type: Sequelize.INTEGER
      },
      is_FO: {
        type: Sequelize.INTEGER
      },
      is_FB: {
        type: Sequelize.INTEGER
      },
      is_BO: {
        type: Sequelize.INTEGER
      },
      is_BAR: {
        type: Sequelize.INTEGER
      },
      is_SEC: {
        type: Sequelize.INTEGER
      },
      is_PG: {
        type: Sequelize.INTEGER
      },
      is_ENG: {
        type: Sequelize.INTEGER
      },
      last_price: {
        type: Sequelize.FLOAT
      },
      account_HK: {
        type: Sequelize.INTEGER
      },
      account_FO: {
        type: Sequelize.INTEGER
      },
      account_FB: {
        type: Sequelize.INTEGER
      },
      account_BO: {
        type: Sequelize.INTEGER
      },
      account_BAR: {
        type: Sequelize.INTEGER
      },
      account_SEC: {
        type: Sequelize.INTEGER
      },
      account_PG: {
        type: Sequelize.INTEGER
      },
      account_ENG: {
        type: Sequelize.INTEGER
      },
      is_proccess_opname: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Items');
  }
};