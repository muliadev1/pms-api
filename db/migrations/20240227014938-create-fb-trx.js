'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('FbTrxes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fbtrx_no: {
        type: Sequelize.STRING
      },
      fbtrx_date: {
        type: Sequelize.DATE
      },
      table_id: {
        type: Sequelize.INTEGER
      },
      guest_id: {
        type: Sequelize.INTEGER
      },
      reservation_no: {
        type: Sequelize.STRING
      },
      room_id: {
        type: Sequelize.INTEGER
      },
      total: {
        type: Sequelize.DOUBLE
      },
      discount: {
        type: Sequelize.DOUBLE
      },
      total2: {
        type: Sequelize.DOUBLE
      },
      service: {
        type: Sequelize.DOUBLE
      },
      tax: {
        type: Sequelize.DOUBLE
      },
      package_id: {
        type: Sequelize.INTEGER
      },
      total_payment: {
        type: Sequelize.DOUBLE
      },
      remind: {
        type: Sequelize.DOUBLE
      },
      status_payment_id: {
        type: Sequelize.INTEGER
      },
      reservation_charge_id: {
        type: Sequelize.STRING
      },
      room_charge_id: {
        type: Sequelize.STRING
      },
      notes: {
        type: Sequelize.STRING
      },
      operator_id_open: {
        type: Sequelize.INTEGER
      },
      operator_id_process: {
        type: Sequelize.INTEGER
      },
      is_closing: {
        type: Sequelize.INTEGER
      },
      closing_date: {
        type: Sequelize.DATE
      },
      is_night_audit: {
        type: Sequelize.INTEGER
      },
      night_audit_date: {
        type: Sequelize.DATE
      },
      waiter_id: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('FbTrxes');
  }
};