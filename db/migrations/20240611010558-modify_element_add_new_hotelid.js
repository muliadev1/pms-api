'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'elements', // table name
        'hotel_id', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'price', // add this option
        },
      ),
      queryInterface.addColumn(
        'rate_codes', // table name
        'promo_code', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          after: 'pax', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
