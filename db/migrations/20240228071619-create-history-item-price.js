'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('HistoryItemPrices', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      item_code: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.DOUBLE
      },
      new_price: {
        type: Sequelize.DOUBLE
      },
      history_date: {
        type: Sequelize.DATE
      },
      vendor_id: {
        type: Sequelize.INTEGER
      },
      price_hpp: {
        type: Sequelize.DOUBLE
      },
      receive_order_code: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('HistoryItemPrices');
  }
};