'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('RO_Mains', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      receive_order_code: {
        type: Sequelize.STRING
      },
      purchase_order_code: {
        type: Sequelize.STRING
      },
      receive_date: {
        type: Sequelize.DATE
      },
      vendor_id: {
        type: Sequelize.INTEGER
      },
      operator_id: {
        type: Sequelize.INTEGER
      },
      is_invoicing: {
        type: Sequelize.INTEGER
      },
      receipt_no: {
        type: Sequelize.STRING
      },
      total: {
        type: Sequelize.DOUBLE
      },
      discount: {
        type: Sequelize.DOUBLE
      },
      total_after_disc: {
        type: Sequelize.DOUBLE
      },
      is_credit: {
        type: Sequelize.INTEGER
      },
      due_date: {
        type: Sequelize.DATE
      },
      is_paid: {
        type: Sequelize.INTEGER
      },
      down_payment: {
        type: Sequelize.DOUBLE
      },
      remaind: {
        type: Sequelize.DOUBLE
      },
      department_id: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('RO_Mains');
  }
};