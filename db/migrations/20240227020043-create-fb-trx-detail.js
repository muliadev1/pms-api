'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('FbTrx_Details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fbtrx_no: {
        type: Sequelize.STRING
      },
      fbtrx_date: {
        type: Sequelize.DATE
      },
      menu_id: {
        type: Sequelize.INTEGER
      },
      menu_name: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.DOUBLE
      },
      disp: {
        type: Sequelize.DOUBLE
      },
      dism: {
        type: Sequelize.DOUBLE
      },
      qty: {
        type: Sequelize.INTEGER
      },
      sub_total: {
        type: Sequelize.DOUBLE
      },
      request: {
        type: Sequelize.STRING
      },
      operator_id: {
        type: Sequelize.INTEGER
      },
      status_id: {
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.STRING
      },
      night_audit_date: {
        type: Sequelize.DATE
      },
      waiter_id: {
        type: Sequelize.INTEGER
      },
      waiter_name: {
        type: Sequelize.STRING
      },
      order_manual: {
        type: Sequelize.STRING
      },
      price1: {
        type: Sequelize.DOUBLE
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('FbTrx_Details');
  }
};