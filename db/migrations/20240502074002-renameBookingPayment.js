'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.renameTable('paymentbookings', 'booking_payment');
    await queryInterface.renameTable('paymentstatuses', 'payment_status');
    await queryInterface.renameTable('bookingstatuses', 'booking_status');

    await queryInterface.renameTable('bookingaddons', 'booking_addon');
    await queryInterface.renameTable('bookingrooms', 'booking_room');
    await queryInterface.renameTable('roomstatuses', 'room_status');

    
    await queryInterface.renameTable('roomrates', 'room_rate');
    await queryInterface.renameTable('hotelroomtypes', 'hotel_room_type');
    await queryInterface.renameTable('roomrateblackouts', 'room_rate_blackout');
    await queryInterface.renameTable('roomratesurcharges', 'room_rate_surcharge');
    await queryInterface.dropTable('roomtypeblackouts');
    await queryInterface.dropTable('roomtypesurcharges');
    await queryInterface.renameTable('roomtypebedtypes', 'room_type_bed_type');
    await queryInterface.renameTable('roomtypefeatures', 'room_type_feature');

    
    await queryInterface.renameTable('hotelfeatures', 'hotel_feature');
    await queryInterface.renameTable('bedtypes', 'bed_type');
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
