'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'RateCode_elements', // table name
        'child_price_input', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'element_id', // add this option
        },
      ),
      queryInterface.addColumn(
        'RateCode_elements', // table name
        'adult_price_input', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'element_id', // add this option
        },
      ),
      queryInterface.addColumn(
        'RateCode_elements', // table name
        'per_room_input', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'element_id', // add this option
        },
      ),
      queryInterface.addColumn(
        'ratecode_roomtypes', // table name
        'price_input', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'per_night_price', // add this option
        },
      ),
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
