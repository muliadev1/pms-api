'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'room_type', // table name
        'hotel_id', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          after: 'id', // add this option
        },
      ),
      queryInterface.renameColumn('currentyear_roomtypes', 'room_type_id', 'hotel_id'),
      queryInterface.renameColumn('ratecode_surcharges', 'ratecode_room_type_id', 'ratecode_id'),
      queryInterface.createTable('Promo_code', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        code: {
          type: Sequelize.STRING
        },
        start_booking: {
          type: Sequelize.DATE
        },
        end_booking: {
          type: Sequelize.DATE
        },
        start_stay: {
          type: Sequelize.DATE
        },
        end_stay: {
          type: Sequelize.DATE
        },
        amount: {
          type: Sequelize.DOUBLE
        },
        createdBy: {
          type: Sequelize.STRING
        },
        updatedBy: {
          type: Sequelize.STRING
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      queryInterface.createTable('Ratecode_promo', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        id_ratecode: {
          type: Sequelize.STRING
        },
        id_promo: {
          type: Sequelize.INTEGER
        },
        createdBy: {
          type: Sequelize.STRING
        },
        updatedBy: {
          type: Sequelize.STRING
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      })
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
