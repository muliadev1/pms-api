'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.changeColumn('payment_details', 'receive_order_date', {
        type: Sequelize.DATEONLY,
    });

    await queryInterface.changeColumn('Package_Mains', 'start_stay', {
        type: Sequelize.DATEONLY,
    });

    await queryInterface.changeColumn('Package_Mains', 'end_stay', {
      type: Sequelize.DATEONLY,
    });

    await queryInterface.changeColumn('Package_Mains', 'start_booking', {
      type: Sequelize.DATEONLY,
    });

    await queryInterface.changeColumn('Package_Mains', 'end_booking', {
      type: Sequelize.DATEONLY,
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.changeColumn('payment_details', 'receive_order_date', {
        type: Sequelize.DATE,
    });

    await queryInterface.changeColumn('Package_Mains', 'start_stay', {
        type: Sequelize.DATE,
    });

    await queryInterface.changeColumn('Package_Mains', 'end_stay', {
        type: Sequelize.DATE,
    });

    await queryInterface.changeColumn('Package_Mains', 'start_booking', {
      type: Sequelize.DATE,
    });

    await queryInterface.changeColumn('Package_Mains', 'end_booking', {
      type: Sequelize.DATE,
    });
  }
};
