'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('RoomRates', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date_from: {
        type: Sequelize.DATE
      },
      date_to: {
        type: Sequelize.DATE
      },
      base_price: {
        type: Sequelize.DOUBLE
      },
      is_sun: {
        type: Sequelize.INTEGER
      },
      is_mon: {
        type: Sequelize.INTEGER
      },
      is_tue: {
        type: Sequelize.INTEGER
      },
      is_wed: {
        type: Sequelize.INTEGER
      },
      is_thu: {
        type: Sequelize.INTEGER
      },
      is_fri: {
        type: Sequelize.INTEGER
      },
      is_sat: {
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('RoomRates');
  }
};