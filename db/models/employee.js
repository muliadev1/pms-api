'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Employee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Employee.belongsTo(models.Department, { as: 'departement', foreignKey: 'departement_id' });
      Employee.belongsTo(models.Designation, { as: 'designation', foreignKey: 'designation_id' });
      Employee.belongsTo(models.Country, { as: 'country', foreignKey: 'country_id' });
      Employee.belongsTo(models.State, { as: 'state', foreignKey: 'state_id' });
      Employee.belongsTo(models.City, { as: 'city', foreignKey: 'city_id' });
      Employee.belongsTo(models.Identity_type, { as: 'idendiy_type', foreignKey: 'identity_type_id' });
    }
  }
  Employee.init({
    title: DataTypes.STRING,
    gender: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    phone: DataTypes.STRING,
    departement_id: DataTypes.INTEGER,
    designation_id: DataTypes.INTEGER,
    country_id: DataTypes.INTEGER,
    state_id: DataTypes.INTEGER,
    city_id: DataTypes.INTEGER,
    address: DataTypes.TEXT,
    identity_type_id: DataTypes.INTEGER,
    identity_number: DataTypes.STRING,
    identity_image: DataTypes.STRING,
    date_of_joining: DataTypes.DATE,
    salary: DataTypes.DOUBLE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Employee',
    tableName: 'employee'
  });
  return Employee;
};