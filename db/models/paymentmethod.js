'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PaymentMethod extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PaymentMethod.hasMany(models.Payment, { as: 'payment', foreignKey: 'payment_method_id' });

      PaymentMethod.hasMany(models.PaymentBooking, { as: 'paymentbooking', foreignKey: 'payment_method_id' });
    }
  }
  PaymentMethod.init({
    payment_method_name: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'PaymentMethod',
    tableName: 'payment_method' // Nama tabel dalam database
  });
  return PaymentMethod;
};