'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Package_Sub extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // Package_Sub.hasMany(models.Package_Detail, { as: 'packagedetail', foreignKey: 'package_sub_id' });

      Package_Sub.belongsTo(models.Package_Main, { as: 'packagemain', foreignKey: 'package_id' });
      Package_Sub.belongsTo(models.Room, { as: 'roomdetail', foreignKey: 'room_id' });
    }
  }
  Package_Sub.init({
    package_id: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    room_name: DataTypes.STRING,
    room_price: DataTypes.DOUBLE, 
    night: DataTypes.INTEGER,
    child: DataTypes.INTEGER,
    adult: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Package_Sub',
  });
  return Package_Sub;
};