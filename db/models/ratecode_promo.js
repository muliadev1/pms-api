'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Ratecode_promo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Ratecode_promo.belongsTo(models.Promo_code, { as: 'promo', foreignKey: 'id_promo' }); 
    }
  }
  Ratecode_promo.init({
    id_ratecode: DataTypes.INTEGER,
    id_promo: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Ratecode_promo',
    tableName: 'ratecode_promo'
  });
  return Ratecode_promo;
};