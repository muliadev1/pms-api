'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Menu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Menu.belongsTo(models.SubmenuCategory, { as: 'submenucategory', foreignKey: 'subcategory_menu_id' });
    }
  }
  Menu.init({
    menu_name: DataTypes.STRING,
    subcategory_menu_id: DataTypes.INTEGER,
    desciption: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    tax: DataTypes.DOUBLE,
    service: DataTypes.DOUBLE,
    final_price: DataTypes.DOUBLE,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Menu',
  });
  return Menu;
};