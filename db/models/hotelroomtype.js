'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class HotelRoomType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      HotelRoomType.belongsTo(models.Hotel, { as: 'hotel', foreignKey: 'hotel_id' });
      HotelRoomType.belongsTo(models.RoomType, { as: 'roomtype', foreignKey: 'room_type_id' });
    }
  }
  HotelRoomType.init({
    hotel_id: DataTypes.INTEGER,
    room_type_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'HotelRoomType',
    tableName:'hotel_room_type'
  });
  return HotelRoomType;
};