'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ModuleApp extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with User_Module
      ModuleApp.hasMany(models.Module, { foreignKey: 'module_app_id', as: 'module_app' });
    }
  }
  ModuleApp.init({
    module_app_name: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ModuleApp',
    tableName: 'module_app'
  });
  return ModuleApp;
};