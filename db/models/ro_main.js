'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RO_Main extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RO_Main.hasMany(models.RO_Detail, { as: 'receiveorderdetail', foreignKey: 'receive_order_id' });
      RO_Main.belongsTo(models.Vendor, { as: 'vendor', foreignKey: 'vendor_id' }); 
    }
  }
  RO_Main.init({
    receive_order_code: DataTypes.STRING,
    purchase_order_code: DataTypes.STRING,
    receive_date: DataTypes.DATE,
    vendor_id: DataTypes.INTEGER,
    operator_id: DataTypes.INTEGER,
    is_invoicing: DataTypes.INTEGER,
    receipt_no: DataTypes.STRING,
    total: DataTypes.DOUBLE,
    discount: DataTypes.DOUBLE,
    total_after_disc: DataTypes.DOUBLE,
    is_credit: DataTypes.INTEGER,
    due_date: DataTypes.DATE,
    is_paid: DataTypes.INTEGER,
    down_payment: DataTypes.DOUBLE,
    remaind: DataTypes.DOUBLE,
    department_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RO_Main',
  });
  return RO_Main;
};