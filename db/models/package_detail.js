'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Package_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Package_Detail.belongsTo(models.Package_Main, { as: 'packagemain', foreignKey: 'package_sub_id' }); 
    }
  }
  Package_Detail.init({
    package_sub_id: DataTypes.INTEGER,
    item_id: DataTypes.INTEGER,
    item_code: DataTypes.STRING,
    item_name: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    qty: DataTypes.INTEGER,
    daily: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Package_Detail',
  });
  return Package_Detail;
};