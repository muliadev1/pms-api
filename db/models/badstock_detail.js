'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BadStock_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BadStock_Detail.belongsTo(models.BadStock, {
        foreignKey: 'badstock_id', // Foreign key in MasterBill_Detail table
        as: 'badstockmaster' // Alias for the association
      });
    }
  }
  BadStock_Detail.init({
    badstock_id: DataTypes.INTEGER,
    item_code: DataTypes.STRING,
    description: DataTypes.STRING,
    unit: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    qty: DataTypes.DOUBLE,
    curr_stock: DataTypes.DOUBLE,
    department_id: DataTypes.INTEGER,
    department_name: DataTypes.STRING,
    operator_id: DataTypes.INTEGER,
    notes: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'BadStock_Detail',
  });
  return BadStock_Detail;
};