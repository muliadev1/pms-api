'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SubCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SubCategory.belongsTo(models.Category, { as: 'category', foreignKey: 'category_id' });
      SubCategory.hasMany(models.Item, { as: 'item', foreignKey: 'subcategory_id' });
      SubCategory.hasMany(models.SubCategoryDepartement, { as: 'subcategorydepartement', foreignKey: 'subcategory_id' });
    }
  }
  SubCategory.init({
    subcategory_name: DataTypes.STRING,
    subcategory_description: DataTypes.STRING,
    category_id: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING
    },
    updatedBy: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'SubCategory',
  });
  return SubCategory;
};