'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BadStock extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BadStock.hasMany(models.BadStock_Detail, { as: 'badstockdetail', foreignKey: 'badstock_id' });
    }
  }
  BadStock.init({
    badstock_code: DataTypes.STRING,
    date_add: DataTypes.DATE,
    department_id: DataTypes.INTEGER,
    department_name: DataTypes.STRING,
    operator_id: DataTypes.INTEGER,
    description: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'BadStock',
  });
  return BadStock;
};