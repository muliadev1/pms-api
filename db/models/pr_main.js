'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PR_Main extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PR_Main.hasMany(models.PR_Detail, { as: 'purchasereqdetail', foreignKey: 'purchase_req_id' });
      PR_Main.belongsTo(models.Department, { as: 'department', foreignKey: 'department_id' });
    }
  }
  PR_Main.init({
    purchase_req_code: DataTypes.STRING,
    purchase_req_date: DataTypes.DATE,
    department_id: DataTypes.INTEGER,
    notes: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'PR_Main',
  });
  return PR_Main;
};