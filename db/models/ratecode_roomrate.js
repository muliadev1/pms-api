'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RateCode_roomrate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
      RateCode_roomrate.belongsTo(models.RoomType, { as: 'roomtype', foreignKey: 'room_type_id' }); 
    }
  }
  RateCode_roomrate.init({
    rate_code_id: DataTypes.INTEGER,
    room_type_id: DataTypes.INTEGER,
    per_night_price: DataTypes.DOUBLE,
    price_input: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RateCode_roomrate',
    tableName: 'ratecode_roomtypes'
  });
  return RateCode_roomrate;
};