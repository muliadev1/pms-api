'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.Role, { as: 'role', foreignKey: 'role_id' });
    }
  }
  User.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    image_name: DataTypes.STRING,
    image_url: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    app_id: DataTypes.INTEGER,
    role_id: DataTypes.INTEGER,
    refresh_token: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users'
  });
  return User;
};