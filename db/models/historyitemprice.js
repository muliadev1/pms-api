'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class HistoryItemPrice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  HistoryItemPrice.init({
    item_code: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    new_price: DataTypes.DOUBLE,
    history_date: DataTypes.DATE,
    vendor_id: DataTypes.INTEGER,
    price_hpp: DataTypes.DOUBLE,
    receive_order_code: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'HistoryItemPrice',
  });
  return HistoryItemPrice;
};