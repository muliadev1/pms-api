'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CurrentYear_Roomtype extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      CurrentYear_Roomtype.belongsTo(models.Hotel, { as: 'hotel', foreignKey: 'hotel_id' }); 
    }
  }
  CurrentYear_Roomtype.init({
    hotel_id: DataTypes.INTEGER,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    amount: DataTypes.DOUBLE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'CurrentYear_Roomtype',
  });
  return CurrentYear_Roomtype;
};