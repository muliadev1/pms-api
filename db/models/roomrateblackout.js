'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomRateBlackout extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RoomRateBlackout.belongsTo(models.RoomRate, { as: 'roomrate', foreignKey: 'room_rate_id' });
      RoomRateBlackout.belongsTo(models.Blackout, { as: 'blackout', foreignKey: 'blackout_id' });
    }
  }
  RoomRateBlackout.init({
    room_rate_id: DataTypes.INTEGER,
    blackout_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomRateBlackout',
    tableName: 'room_rate_blackout'
  });
  return RoomRateBlackout;
};