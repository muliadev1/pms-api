'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Settings extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Settings.init({
    hotel_id: DataTypes.INTEGER,
    logo: DataTypes.STRING,
    footer_text: DataTypes.STRING,
    min_booking: DataTypes.STRING,
    checkin_time: DataTypes.STRING,
    checkout_time: DataTypes.STRING,
    default_currency: DataTypes.STRING,
    default_language: DataTypes.STRING,
    default_date_format: DataTypes.STRING,
    advance_payment_percentage: DataTypes.STRING,
    taxes: DataTypes.STRING,
    is_maintenance: DataTypes.INTEGER,
    sender_email: DataTypes.STRING,
    sender_name: DataTypes.STRING,
    is_use_smtp: DataTypes.INTEGER,
    smtp_security: DataTypes.STRING,
    is_smtp_authentication: DataTypes.INTEGER,
    smtp_host: DataTypes.STRING,
    smtp_port: DataTypes.STRING,
    email_username: DataTypes.STRING,
    email_password: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Settings',
    tableName: 'settings'
  });
  return Settings;
};