'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Mail_template extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Mail_template.init({
    mail_template_name: DataTypes.STRING,
    subject: DataTypes.STRING,
    content: DataTypes.TEXT,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Mail_template',
    tableName: 'mail_template'
  });
  return Mail_template;
};