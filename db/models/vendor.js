'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vendor extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Vendor.hasMany(models.Item, { as: 'item', foreignKey: 'vendor_id' });
      Vendor.hasMany(models.PO_Main, { as: 'purchaseordermain', foreignKey: 'vendor_id' });
      Vendor.hasMany(models.RO_Main, { as: 'receiveordermain', foreignKey: 'vendor_id' });
    }
  }
  Vendor.init({
    vendor_name: DataTypes.STRING,
    address: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    contact: DataTypes.STRING,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Vendor',
  });
  return Vendor;
};