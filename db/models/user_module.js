'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_Module extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with User
      User_Module.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });

      // Define association with Module
      User_Module.belongsTo(models.Module, { foreignKey: 'module_id', as: 'module' });
    }
  }
  User_Module.init({
    user_id: DataTypes.INTEGER,
    module_id: DataTypes.INTEGER,
    is_create: DataTypes.INTEGER,
    is_read: DataTypes.INTEGER,
    is_update: DataTypes.INTEGER,
    is_delete: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_Module',
  });
  return User_Module;
};