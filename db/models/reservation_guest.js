'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reservation_Guest extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with Reservation
      Reservation_Guest.belongsTo(models.Reservation, {
        foreignKey: 'reservation_id', // Foreign key in Reservation_Guest table
        as: 'reservation' // Alias for the association
      });

      // Define association with Guest
      Reservation_Guest.belongsTo(models.Guest, {
        foreignKey: 'guest_id', // Foreign key in Reservation_Guest table
        as: 'guest' // Alias for the association
      });
    }
  }
  Reservation_Guest.init({
    reservation_id: DataTypes.INTEGER,
    guest_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Reservation_Guest',
  });
  return Reservation_Guest;
};