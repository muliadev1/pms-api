'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BookingAddon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      BookingAddon.belongsTo(models.Booking, { as: 'booking', foreignKey: 'booking_id' });
      BookingAddon.belongsTo(models.Addon, { as: 'addon', foreignKey: 'addon_id' });
    }
  }
  BookingAddon.init({
    booking_id: DataTypes.INTEGER,
    addon_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'BookingAddon',
    tableName:'booking_addon'
  });
  return BookingAddon;
};