'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reservation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Reservation.hasMany(models.Reservation_Detail, {
        foreignKey: 'reservation_id', // Foreign key in ReservationDetail table
        as: 'details' // Alias for the association
      });
      // Define association with Reservation_Guest
      Reservation.hasMany(models.Reservation_Guest, {
        foreignKey: 'reservation_id', // Foreign key in Reservation_Guest table
        as: 'reservationguests' // Alias for the association
      });
      // Define association with Guest
      Reservation.belongsTo(models.Guest, {
        foreignKey: 'guest_id', // Foreign key in Reservation table
        as: 'guest' // Alias for the association
      });
      // Define association with Reservation_Status
      Reservation.belongsTo(models.ReservationStatus, {
        foreignKey: 'reservation_status_id', // Foreign key in Reservation table
        as: 'status' // Alias for the association
      });
      // Define association with Room
      // Reservation.belongsTo(models.Room, {
      //   foreignKey: 'room_id', // Foreign key in Reservation table
      //   as: 'room' // Alias for the association
      // });
      // Define association with Room Type
      // Reservation.belongsTo(models.RoomType, {
      //   foreignKey: 'room_type_id', // Foreign key in Reservation table
      //   as: 'roomtype' // Alias for the association
      // });
      // Define association with Company
      Reservation.belongsTo(models.Company, {
        foreignKey: 'company_id', // Foreign key in Reservation table
        as: 'company' // Alias for the association
      });
      // Define association with segment
      Reservation.belongsTo(models.Segment, {
        foreignKey: 'segment_id', // Foreign key in Reservation table
        as: 'segment' // Alias for the association
      });
       // Define association with purpose
       Reservation.belongsTo(models.Purpose, {
        foreignKey: 'purpose_id', // Foreign key in Reservation table
        as: 'purpose' // Alias for the association
      });
      // Define association with purpose
      Reservation.belongsTo(models.Source, {
        foreignKey: 'source_id', // Foreign key in Reservation table
        as: 'source' // Alias for the association
      });

      Reservation.belongsTo(models.Vip, {
        foreignKey: 'vip_id', // Foreign key in Reservation table
        as: 'vipdetail' // Alias for the association
      });

      Reservation.belongsTo(models.PaymentType, {
        foreignKey: 'payment_type_id', // Foreign key in Reservation table
        as: 'paymenttype' // Alias for the association
      });

      Reservation.hasMany(models.RoomCalendar, {
        foreignKey: 'reservation_id', // Foreign key in Reservation_Guest table
        as: 'roomcalendar' // Alias for the association
      });
    }
  }
  Reservation.init({
    reservation_no: DataTypes.STRING,
    reservation_date: DataTypes.DATE,
    is_group: DataTypes.INTEGER,
    parent_group_id: DataTypes.INTEGER,
    // room_type_id: DataTypes.INTEGER,
    // room_id: DataTypes.INTEGER,
    reservation_status_id: DataTypes.INTEGER,
    guest_id: DataTypes.INTEGER,
    adult: DataTypes.INTEGER,
    child: DataTypes.INTEGER,
    check_in: DataTypes.DATE,
    check_out: DataTypes.DATE,
    duration: DataTypes.INTEGER,
    purpose_id: DataTypes.INTEGER,
    source_id: DataTypes.INTEGER,
    segment_id: DataTypes.INTEGER,
    is_vip: DataTypes.INTEGER,
    company_id: DataTypes.INTEGER,
    deposit: DataTypes.DOUBLE,
    cut_off_date: DataTypes.DATE,
    agent_it: DataTypes.INTEGER,
    remarks: DataTypes.STRING,
    payment_detail_json: DataTypes.JSON,
    hotel_special_intruction	: DataTypes.TEXT,
    reservation_comments : DataTypes.TEXT,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Reservation',
  });
  return Reservation;
};