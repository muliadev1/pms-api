'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FbTrx_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  FbTrx_Detail.init({
    fbtrx_no: DataTypes.STRING,
    fbtrx_date: DataTypes.DATE,
    menu_id: DataTypes.INTEGER,
    menu_name: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    disp: DataTypes.DOUBLE,
    dism: DataTypes.DOUBLE,
    qty: DataTypes.INTEGER,
    sub_total: DataTypes.DOUBLE,
    request: DataTypes.STRING,
    operator_id: DataTypes.INTEGER,
    status_id: DataTypes.INTEGER,
    description: DataTypes.STRING,
    night_audit_date: DataTypes.DATE,
    waiter_id: DataTypes.INTEGER,
    waiter_name: DataTypes.STRING,
    order_manual: DataTypes.STRING,
    price1: DataTypes.DOUBLE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'FbTrx_Detail',
  });
  return FbTrx_Detail;
};