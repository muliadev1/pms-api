'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BookingRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      BookingRoom.belongsTo(models.Booking, { as: 'booking', foreignKey: 'booking_id' });
      BookingRoom.belongsTo(models.Room, { as: 'room', foreignKey: 'room_id' });
    }
  }
  BookingRoom.init({
    booking_id: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'BookingRoom',
    tableName:'booking_room'
  });
  return BookingRoom;
};