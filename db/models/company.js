'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Company extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define associate with reservation
      Company.hasMany(models.Reservation, {
        foreignKey: 'company_id', // Foreign key in Reservation table
        as: 'reservations' // Alias for the association
      });
    }
  }
  Company.init({
    company_name: DataTypes.STRING,
    address: DataTypes.STRING,
    country: DataTypes.STRING,
    state: DataTypes.STRING,
    zipcode: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    type: DataTypes.STRING,
    description: DataTypes.STRING,
    contact_first_name: DataTypes.STRING,
    contact_last_name: DataTypes.STRING,
    contact_address: DataTypes.STRING,
    contact_phone: DataTypes.STRING,
    contact_email: DataTypes.STRING,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Company',
  });
  return Company;
};