'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Module extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with User_Module
      Module.hasMany(models.User_Module, { foreignKey: 'module_id', as: 'user_modules' });
      Module.belongsTo(models.ModuleApp, { as: 'moduleapp', foreignKey: 'module_app_id' });
    }
  }
  Module.init({
    module_code: DataTypes.STRING,
    module_name: DataTypes.STRING,
    icon: DataTypes.STRING,
    order_no: DataTypes.INTEGER,
    url: DataTypes.STRING, 
    is_parent: DataTypes.INTEGER,
    is_trx: DataTypes.INTEGER,
    module_app_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Module',
    tableName: 'modules'
  });
  return Module;
};