'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class HotelFeature extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      HotelFeature.belongsTo(models.Hotel, { as: 'hotel', foreignKey: 'hotel_id' });
      HotelFeature.belongsTo(models.Feature, { as: 'feature', foreignKey: 'feature_id' });
    }
  }
  HotelFeature.init({
    hotel_id: DataTypes.INTEGER,
    feature_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'HotelFeature',
    tableName:'hotel_feature'
  });
  return HotelFeature;
};