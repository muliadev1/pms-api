'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Asset_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with Assets
      Asset_Detail.belongsTo(models.Asset, {
        foreignKey: 'asset_id', // Foreign key in MasterBill_Detail table
        as: 'assetmaster' // Alias for the association
      });
    }
  }
  Asset_Detail.init({
    asset_id: DataTypes.INTEGER,
    qty: DataTypes.INTEGER,
    kondisi_awal: DataTypes.STRING,
    kondisi_akhir: DataTypes.STRING,
    department_id: DataTypes.INTEGER,
    department_name: DataTypes.STRING,
    operator_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Asset_Detail',
  });
  return Asset_Detail;
};