'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SR_Main extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SR_Main.hasMany(models.SR_Detail, { as: 'storereqdetail', foreignKey: 'store_req_id' });
    }
  }
  SR_Main.init({
    store_req_code: DataTypes.STRING,
    department_id: DataTypes.INTEGER,
    department: DataTypes.STRING,
    store_date: DataTypes.DATE,
    operator_id: DataTypes.INTEGER,
    desciption: DataTypes.STRING,
    status_in_out: DataTypes.INTEGER,
    is_SO: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'SR_Main',
  });
  return SR_Main;
};