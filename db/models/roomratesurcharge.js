'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomRateSurcharge extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RoomRateSurcharge.belongsTo(models.RoomRate, { as: 'roomrate', foreignKey: 'room_rate_id' });
      RoomRateSurcharge.belongsTo(models.Surcharge, { as: 'surcharge', foreignKey: 'surcharge_id' });
    }
  }
  RoomRateSurcharge.init({
    room_rate_id: DataTypes.INTEGER,
    surcharge_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomRateSurcharge',
    tableName:'room_rate_surcharge'
  });
  return RoomRateSurcharge;
};