'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RateCode_element extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  RateCode_element.init({
    rate_code_id: DataTypes.INTEGER,
    element_id: DataTypes.INTEGER,
    is_daily:DataTypes.INTEGER,
    adult_rate: DataTypes.DOUBLE,
    child_rate: DataTypes.DOUBLE,
    per_room_rate: DataTypes.DOUBLE,
    qty: DataTypes.INTEGER,
    name: DataTypes.STRING,
    total_price: DataTypes.DOUBLE,
    child_price_input: DataTypes.INTEGER,
    adult_price_input: DataTypes.INTEGER,
    per_room_input: DataTypes.INTEGER,
    price_type_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RateCode_element',
  });
  return RateCode_element;
};