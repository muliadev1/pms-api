'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Payment_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Payment_Detail.belongsTo(models.Payment, { as: 'paymentmain', foreignKey: 'payment_id' });
    }
  }
  Payment_Detail.init({
    payment_id: DataTypes.INTEGER,
    amount: DataTypes.DOUBLE,
    description: DataTypes.STRING,
    receive_order_code: DataTypes.STRING,
    receive_order_date: DataTypes.DATE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Payment_Detail',
  });
  return Payment_Detail;
};