'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Hotel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Hotel.hasMany(models.HotelFeature, { as: 'hotelfeature', foreignKey: 'hotel_id' });
      Hotel.hasMany(models.HotelRoomType, { as: 'hotelroomtype', foreignKey: 'hotel_id' });
    }
  }
  Hotel.init({
    title: DataTypes.STRING,
    subtitle: DataTypes.STRING,
    alias: DataTypes.STRING,
    description: DataTypes.TEXT,
    destination: DataTypes.TEXT,
    class: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    website: DataTypes.STRING,
    address: DataTypes.TEXT,
    latitude: DataTypes.STRING,
    longtitude: DataTypes.STRING,
    release: DataTypes.STRING,
    hotel_code: DataTypes.STRING,
    max_booking: DataTypes.INTEGER,
    is_homepage: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Hotel',
    tableName: 'hotels'
  });
  return Hotel;
};