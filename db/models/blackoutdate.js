'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BlackOutDate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BlackOutDate.init({
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    start_period: DataTypes.DATE,
    end_period: DataTypes.DATE,
    room_rate_id: DataTypes.INTEGER,
    code: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'BlackOutDate',
  });
  return BlackOutDate;
};