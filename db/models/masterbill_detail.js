'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MasterBill_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with MasterBill
      MasterBill_Detail.belongsTo(models.MasterBill, {
        foreignKey: 'masterbill_id', // Foreign key in MasterBill_Detail table
        as: 'masterBill' // Alias for the association
      });
    }
  }
  MasterBill_Detail.init({
    masterbill_id: DataTypes.INTEGER,
    masterbill_date: DataTypes.DATE,
    reservation_id: DataTypes.INTEGER,
    invoice_manual: DataTypes.STRING,
    room_id: DataTypes.INTEGER,
    is_charge: DataTypes.INTEGER,
    package_id: DataTypes.INTEGER,
    description: DataTypes.STRING,
    charge: DataTypes.DOUBLE,
    payment: DataTypes.DOUBLE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'MasterBill_Detail',
  });
  return MasterBill_Detail;
};