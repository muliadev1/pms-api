'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RoomType.hasMany(models.Room, { as: 'room', foreignKey: 'roomtype_id' });
      RoomType.hasMany(models.HotelRoomType, { as: 'hotelroomtype', foreignKey: 'room_type_id' });

      // define associate with reservation
      RoomType.hasMany(models.Reservation, {
        foreignKey: 'room_type_id', // Foreign key in Reservation table
        as: 'reservations' // Alias for the association
      });
      // define associate with reservation
      RoomType.hasMany(models.FoTrx_Detail, {
        foreignKey: 'room_type_id', // Foreign key in Reservation table
        as: 'fotrxdetails' // Alias for the association
      });
      RoomType.belongsTo(models.Hotel, { as: 'hotel', foreignKey: 'hotel_id' }); 
      RoomType.hasMany(models.RoomTypeBedType, { as: 'roomtypebedtype', foreignKey: 'room_type_id' });
      RoomType.hasMany(models.RoomTypeFeature, { as: 'roomtypefeature', foreignKey: 'room_type_id' });
      RoomType.hasMany(models.RoomTypeRoomRate, { as: 'roomtyperate', foreignKey: 'room_type_id' });
    }
  }
  RoomType.init({
    roomtype_name: DataTypes.STRING,
    description: DataTypes.STRING,
    rate: DataTypes.DOUBLE,
    is_active: DataTypes.INTEGER,
    hotel_id: DataTypes.INTEGER,
    extrabed_price:DataTypes.DOUBLE,
    short_code:DataTypes.STRING,
    min_occupancy:DataTypes.INTEGER,
    max_occupancy:DataTypes.INTEGER,
    extrabed_occupancy:DataTypes.INTEGER,
    is_extrabed:DataTypes.INTEGER,
    adult:DataTypes.INTEGER,
    child:DataTypes.INTEGER,
    extrabed_occupancy:DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomType',
    tableName: 'room_type'
  });
  return RoomType;
};