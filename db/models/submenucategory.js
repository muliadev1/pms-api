'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SubmenuCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SubmenuCategory.belongsTo(models.MenuCategory, { as: 'menucategory', foreignKey: 'menu_category_id' });

      SubmenuCategory.hasMany(models.Menu, { as: 'menu', foreignKey: 'subcategory_menu_id' });
    }
  }
  SubmenuCategory.init({
    submenu_category_name: DataTypes.STRING,
    description: DataTypes.STRING,
    menu_category_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'SubmenuCategory',
  });
  return SubmenuCategory;
};