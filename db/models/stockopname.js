'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StockOpname extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  StockOpname.init({
    stock_opname_date: DataTypes.DATE,
    item_code: DataTypes.STRING,
    description: DataTypes.STRING,
    unit: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    stock: DataTypes.DOUBLE,
    input_stock: DataTypes.DOUBLE,
    diff_stock: DataTypes.DOUBLE,
    department_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'StockOpname',
  });
  return StockOpname;
};