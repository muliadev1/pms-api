'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SR_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      SR_Detail.belongsTo(models.SR_Main, { as: 'storereqmain', foreignKey: 'store_req_id' });
    }
  }
  SR_Detail.init({
    store_req_id: DataTypes.INTEGER,
    item_code: DataTypes.STRING,
    description: DataTypes.STRING,
    unit: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    qty_order: DataTypes.DOUBLE,
    curr_stock: DataTypes.DOUBLE,
    department_id: DataTypes.INTEGER,
    department: DataTypes.STRING,
    operator_id: DataTypes.INTEGER,
    status_in_out: DataTypes.INTEGER,
    is_SO: DataTypes.INTEGER,
    notes: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'SR_Detail',
  });
  return SR_Detail;
};