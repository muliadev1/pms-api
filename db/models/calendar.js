'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Calendar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Calendar.init({
    hotel_id: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    price: DataTypes.DOUBLE,
    start_period: DataTypes.DATE,
    end_period: DataTypes.DATE,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    min_stay: DataTypes.INTEGER,
    no_checkin: DataTypes.INTEGER,
    no_checkout: DataTypes.INTEGER,
    allotment: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Calendar',
    tableName: 'calendar'
  });
  return Calendar;
};