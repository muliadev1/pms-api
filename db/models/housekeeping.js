'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class HouseKeeping extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      HouseKeeping.belongsTo(models.Room, { as: 'room', foreignKey: 'room_id' });
      HouseKeeping.belongsTo(models.RoomStatus, { as: 'room_status', foreignKey: 'room_status_id' });
    }
  }
  HouseKeeping.init({
    room_id: DataTypes.INTEGER,
    room_status_id: DataTypes.INTEGER,
    assigned_date: DataTypes.DATE,
    assigned_to: DataTypes.INTEGER,
    remarks: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'HouseKeeping',
    tableName: 'housekeeping'
  });
  return HouseKeeping;
};