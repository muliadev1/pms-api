'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ChartOfAccount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ChartOfAccount.init({
    account_no: DataTypes.STRING,
    account_sub_no: DataTypes.STRING,
    account_name: DataTypes.STRING,
    balance: DataTypes.DOUBLE,
    open_balance: DataTypes.DOUBLE,
    is_kontra: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ChartOfAccount',
  });
  return ChartOfAccount;
};