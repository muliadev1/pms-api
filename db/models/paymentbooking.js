'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PaymentBooking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      PaymentBooking.belongsTo(models.Booking, { as: 'booking', foreignKey: 'booking_id' });
      PaymentBooking.belongsTo(models.PaymentMethod, { as: 'paymentmethod', foreignKey: 'payment_method_id' });
    }
  }
  PaymentBooking.init({
    booking_id: DataTypes.INTEGER,
    payment_method_id: DataTypes.INTEGER,
    invoice_no: DataTypes.STRING,
    added_date: DataTypes.DATE,
    payment_due: DataTypes.DATE,
    amount: DataTypes.DOUBLE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'PaymentBooking',
    tableName:'booking_payment'
  });
  return PaymentBooking;
};