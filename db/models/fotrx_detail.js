'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FoTrx_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with FoTrx
      FoTrx_Detail.belongsTo(models.FoTrx, {
        foreignKey: 'reservation_no', // Foreign key in FoTrx_Detail table
        targetKey: 'reservation_no', // Target key in FoTrx table
        as: 'foTrx' // Alias for the association
      });
      // Define association with Room
      FoTrx_Detail.belongsTo(models.Room, {
        foreignKey: 'room_id', // Foreign key in Reservation table
        as: 'room' // Alias for the association
      });
      // Define association with Room Type
      FoTrx_Detail.belongsTo(models.RoomType, {
        foreignKey: 'room_type_id', // Foreign key in Reservation table
        as: 'roomtype' // Alias for the association
      });
      // Define association with Package_Main
      FoTrx_Detail.belongsTo(models.Package_Main, {
        foreignKey: 'package_id', // Foreign key in Reservation table
        as: 'package' // Alias for the association
      });
      
    }
  }
  FoTrx_Detail.init({
    reservation_no: DataTypes.STRING,
    reservation_id: DataTypes.INTEGER,
    invoice_no: DataTypes.STRING,
    room_type_id: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    checkout: DataTypes.DATE,
    package_id: DataTypes.INTEGER,
    description: DataTypes.STRING,
    charge: DataTypes.DOUBLE,
    payment: DataTypes.DOUBLE,
    is_closing: DataTypes.INTEGER,
    closing_date: DataTypes.DATE,
    is_night_audit: DataTypes.INTEGER,
    night_audit_date: DataTypes.DATE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'FoTrx_Detail',
  });
  return FoTrx_Detail;
};