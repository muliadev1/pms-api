'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomCalendar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RoomCalendar.belongsTo(models.Reservation, {
        foreignKey: 'reservation_id', // Foreign key in Reservation table
        as: 'reservation' // Alias for the association
      });
      RoomCalendar.belongsTo(models.Room, {
        foreignKey: 'room_id', // Foreign key in Reservation table
        as: 'room' // Alias for the association
      });
      RoomCalendar.belongsTo(models.RoomType, {
        foreignKey: 'room_type_id', // Foreign key in Reservation table
        as: 'roomtype' // Alias for the association
      });
    }
  }
  RoomCalendar.init({
    room_type_id: DataTypes.INTEGER,
    hotel_id: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    reservation_id: DataTypes.INTEGER,
    checkin: DataTypes.DATE,
    checkout: DataTypes.DATE,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    status: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomCalendar',
  });
  return RoomCalendar;
};