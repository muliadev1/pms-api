'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FbTrx extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
       // Define association with Guest
       FbTrx.belongsTo(models.Guest, {
        foreignKey: 'guest_id', // Foreign key in Reservation table
        as: 'guest' // Alias for the association
      });

      // Define association with Room
      FbTrx.belongsTo(models.Room, {
        foreignKey: 'room_id', // Foreign key in Reservation table
        as: 'room' // Alias for the association
      });

      // Define association with Table
      FbTrx.belongsTo(models.Table, {
        foreignKey: 'table_id', // Foreign key in Reservation table
        as: 'table' // Alias for the association
      });
    }
  }
  FbTrx.init({
    fbtrx_no: DataTypes.STRING,
    fbtrx_date: DataTypes.DATE,
    table_id: DataTypes.INTEGER,
    guest_id: DataTypes.INTEGER,
    reservation_no: DataTypes.STRING,
    room_id: DataTypes.INTEGER,
    total: DataTypes.DOUBLE,
    discount: DataTypes.DOUBLE,
    total2: DataTypes.DOUBLE,
    service: DataTypes.DOUBLE,
    tax: DataTypes.DOUBLE,
    package_id: DataTypes.INTEGER,
    total_payment: DataTypes.DOUBLE,
    remind: DataTypes.DOUBLE,
    status_payment_id: DataTypes.INTEGER,
    reservation_charge_id: DataTypes.STRING,
    room_charge_id: DataTypes.STRING,
    notes: DataTypes.STRING,
    operator_id_open: DataTypes.INTEGER,
    operator_id_process: DataTypes.INTEGER,
    is_closing: DataTypes.INTEGER,
    closing_date: DataTypes.DATE,
    is_night_audit: DataTypes.INTEGER,
    night_audit_date: DataTypes.DATE,
    waiter_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'FbTrx',
  });
  return FbTrx;
};