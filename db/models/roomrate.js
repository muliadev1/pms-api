'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomRate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RoomRate.hasMany(models.RoomRateBlackout, { as: 'roomrateblackout', foreignKey: 'room_rate_id' });
      RoomRate.hasMany(models.RoomRateSurcharge, { as: 'roomratesurcharge', foreignKey: 'room_rate_id' });
    }
  }
  RoomRate.init({
    date_from: DataTypes.DATE,
    date_to: DataTypes.DATE,
    base_price: DataTypes.DOUBLE,
    is_sun: DataTypes.INTEGER,
    is_mon: DataTypes.INTEGER,
    is_tue: DataTypes.INTEGER,
    is_wed: DataTypes.INTEGER,
    is_thu: DataTypes.INTEGER,
    is_fri: DataTypes.INTEGER,
    is_sat: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomRate',
    tableName:'room_rate'
  });
  return RoomRate;
};