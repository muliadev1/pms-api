'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Item.belongsTo(models.SubCategory, { as: 'subcategory', foreignKey: 'subcategory_id' });
      
      Item.hasMany(models.SubCategoryDepartement, { as: 'subcategorydepartement', foreignKey: 'item_id' });
    }
  }
  Item.init({
    item_code: DataTypes.STRING,
    item_description: DataTypes.STRING,
    subcategory_id: DataTypes.INTEGER,
    unit: DataTypes.STRING,
    price: DataTypes.FLOAT,
    stock: DataTypes.FLOAT,
    is_active: DataTypes.INTEGER,
    stockHK: DataTypes.FLOAT,
    stockFO: DataTypes.FLOAT,
    stockFB: DataTypes.FLOAT,
    stockBO: DataTypes.FLOAT,
    stockBAR: DataTypes.FLOAT,
    stockSEC: DataTypes.FLOAT,
    stockPG: DataTypes.FLOAT,
    stockENG: DataTypes.FLOAT,
    vendor_id: DataTypes.INTEGER,
    is_HK: DataTypes.INTEGER,
    is_FO: DataTypes.INTEGER,
    is_FB: DataTypes.INTEGER,
    is_BO: DataTypes.INTEGER,
    is_BAR: DataTypes.INTEGER,
    is_SEC: DataTypes.INTEGER,
    is_PG: DataTypes.INTEGER,
    is_ENG: DataTypes.INTEGER,
    last_price: DataTypes.FLOAT,
    account_HK: DataTypes.INTEGER,
    account_FO: DataTypes.INTEGER,
    account_FB: DataTypes.INTEGER,
    account_BO: DataTypes.INTEGER,
    account_BAR: DataTypes.INTEGER,
    account_SEC: DataTypes.INTEGER,
    account_PG: DataTypes.INTEGER,
    account_ENG: DataTypes.INTEGER,
    is_proccess_opname: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Item',
  });
  return Item;
};