'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PR_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PR_Detail.belongsTo(models.PR_Main, { as: 'purchasereqmain', foreignKey: 'purchase_req_id' });
    }
  }
  PR_Detail.init({
    purchase_req_id: DataTypes.INTEGER,
    item_code: DataTypes.STRING,
    description: DataTypes.STRING,
    unit: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    qty_order: DataTypes.DOUBLE,
    current_stock: DataTypes.DOUBLE,
    notes: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'PR_Detail',
  });
  return PR_Detail;
};