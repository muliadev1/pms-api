'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RO_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RO_Detail.belongsTo(models.RO_Main, { as: 'receiveordermain', foreignKey: 'receive_order_id' });
    }
  }
  RO_Detail.init({
    receive_order_id: DataTypes.INTEGER,
    item_code: DataTypes.STRING,
    description: DataTypes.STRING,
    unit: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    qty_order: DataTypes.DOUBLE,
    qty_stock: DataTypes.DOUBLE,
    subtotal: DataTypes.DOUBLE,
    notes: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RO_Detail',
  });
  return RO_Detail;
};