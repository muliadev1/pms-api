'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MasterBill extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with MasterBill_Detail
      MasterBill.hasMany(models.MasterBill_Detail, {
        foreignKey: 'masterbill_id', // Foreign key in MasterBill_Detail table
        as: 'masterBillDetails' // Alias for the association
      });
   
      // Define association with reservation
      MasterBill.belongsTo(models.Reservation, {
        foreignKey: 'reservation_id', // Foreign key in reservation table
        as: 'reservation' // Alias for the association
      });

       // Define association with reservation
       MasterBill.belongsTo(models.Guest, {
        foreignKey: 'guest_id', // Foreign key in reservation table
        as: 'guest' // Alias for the association
      });
    }
  }
  MasterBill.init({
    masterbill_code: DataTypes.STRING,
    masterbill_date: DataTypes.DATE,
    bill_to: DataTypes.STRING,
    charge: DataTypes.DOUBLE,
    payment: DataTypes.DOUBLE,
    balance: DataTypes.DOUBLE,
    is_roombill: DataTypes.INTEGER,
    guest_id: DataTypes.INTEGER,
    reservation_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'MasterBill',
  });
  return MasterBill;
};