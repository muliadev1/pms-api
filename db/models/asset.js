'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Asset extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association with Asset_Detail
      Asset.hasMany(models.Asset_Detail, { as: 'assetdetail', foreignKey: 'asset_id' });
    }
  }
  Asset.init({
    asset_code: DataTypes.STRING,
    item_code: DataTypes.STRING,
    baik: DataTypes.INTEGER,
    kurangbaik: DataTypes.INTEGER,
    used: DataTypes.INTEGER,
    lose: DataTypes.INTEGER,
    out: DataTypes.INTEGER,
    jumlah: DataTypes.INTEGER,
    baikHK: DataTypes.INTEGER,
    kurangbaikHK: DataTypes.INTEGER,
    usedHK: DataTypes.INTEGER,
    loseHK: DataTypes.INTEGER,
    outHK: DataTypes.INTEGER,
    jumlahHK: DataTypes.INTEGER,
    baikFO: DataTypes.INTEGER,
    kurangbaikFO: DataTypes.INTEGER,
    usedFO: DataTypes.INTEGER,
    loseFO: DataTypes.INTEGER,
    outFO: DataTypes.INTEGER,
    jumlahFO: DataTypes.INTEGER,
    baikFB: DataTypes.INTEGER,
    kurangbaikFB: DataTypes.INTEGER,
    usedFB: DataTypes.INTEGER,
    loseFB: DataTypes.INTEGER,
    outFB: DataTypes.INTEGER,
    jumlahFB: DataTypes.INTEGER,
    baikBO: DataTypes.INTEGER,
    kurangbaikBO: DataTypes.INTEGER,
    usedBO: DataTypes.INTEGER,
    loseBO: DataTypes.INTEGER,
    outBO: DataTypes.INTEGER,
    jumlahBO: DataTypes.INTEGER,
    baikEng: DataTypes.INTEGER,
    kurangbaikEng: DataTypes.INTEGER,
    usedEng: DataTypes.INTEGER,
    loseEng: DataTypes.INTEGER,
    outEng: DataTypes.INTEGER,
    jumlahEng: DataTypes.INTEGER,
    baikPG: DataTypes.INTEGER,
    kurangbaikPG: DataTypes.INTEGER,
    usedPG: DataTypes.INTEGER,
    losePG: DataTypes.INTEGER,
    outPG: DataTypes.INTEGER,
    jumlahPG: DataTypes.INTEGER,
    baikSec: DataTypes.INTEGER,
    kurangbaikSec: DataTypes.INTEGER,
    usedSec: DataTypes.INTEGER,
    loseSec: DataTypes.INTEGER,
    outSec: DataTypes.INTEGER,
    jumlahSec: DataTypes.INTEGER,
    baikBar: DataTypes.INTEGER,
    kurangbaikBar: DataTypes.INTEGER,
    usedBar: DataTypes.INTEGER,
    loseBar: DataTypes.INTEGER,
    outBar: DataTypes.INTEGER,
    jumlahBar: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Asset',
  });
  return Asset;
};