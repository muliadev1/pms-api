'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PO_Main extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PO_Main.hasMany(models.PO_Detail, { as: 'purchaseorderdetail', foreignKey: 'purchase_order_id' });
      PO_Main.belongsTo(models.Vendor, { as: 'vendor', foreignKey: 'vendor_id' }); 
    }
  }
  PO_Main.init({
    purchase_order_code: DataTypes.STRING,
    vendor_id: DataTypes.INTEGER,
    order_date: DataTypes.DATE,
    expected_date: DataTypes.DATE,
    total: DataTypes.DOUBLE,
    method_id: DataTypes.INTEGER,
    is_checked: DataTypes.INTEGER,
    status_id: DataTypes.INTEGER,
    verify_by: DataTypes.STRING,
    notes: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'PO_Main',
  });
  return PO_Main;
};