'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Booking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Booking.hasMany(models.BookingRoom, { as: 'booking_room', foreignKey: 'booking_id' });
      Booking.hasMany(models.BookingAddon, { as: 'booking_addon', foreignKey: 'booking_id' });
      Booking.hasMany(models.PaymentBooking, { as: 'paymentbooking', foreignKey: 'booking_id' });
      Booking.belongsTo(models.Guest, { as: 'guest', foreignKey: 'guest_id' });
      Booking.belongsTo(models.Hotel, { as: 'hotel', foreignKey: 'hotel_id' });
      Booking.belongsTo(models.BookingStatus, { as: 'booking_status', foreignKey: 'booking_status_id' });
      Booking.belongsTo(models.PaymentStatus, { as: 'payment_status', foreignKey: 'payment_status_id' });
    }
  }
  Booking.init({
    guest_id: DataTypes.INTEGER,
    payment_status_id: DataTypes.INTEGER,
    booking_status_id: DataTypes.INTEGER,
    booking_date: DataTypes.DATE,
    checkin_date: DataTypes.DATE,
    checkout_date: DataTypes.DATE,
    num_adult: DataTypes.INTEGER,
    num_children: DataTypes.INTEGER,
    booking_amount: DataTypes.DOUBLE,
    taxes_amount: DataTypes.DOUBLE,
    discount: DataTypes.DOUBLE,
    total_amount: DataTypes.DOUBLE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Booking',
    tableName: 'bookings'
  });
  return Booking;
};