'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Element extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Element.belongsTo(models.Hotel, { as: 'hotel', foreignKey: 'hotel_id' }); 
    }
  }
  Element.init({
    element_code: DataTypes.STRING,
    element_name: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    hotel_id: DataTypes.INTEGER,
    room_rate: DataTypes.DOUBLE,
    child_rate: DataTypes.DOUBLE,
    adult_rate: DataTypes.DOUBLE,
    is_tax_service: DataTypes.INTEGER,
    service: DataTypes.DOUBLE,
    tax: DataTypes.DOUBLE,
    final_price: DataTypes.DOUBLE,
    element_category_id: DataTypes.INTEGER,
    price_type_id: DataTypes.INTEGER,
    department_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Element',
  });
  return Element;
};