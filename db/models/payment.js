'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Payment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Payment.hasMany(models.Payment_Detail, { as: 'paymentdetail', foreignKey: 'payment_id' });

      Payment.belongsTo(models.PaymentMethod, { as: 'paymentmethod', foreignKey: 'payment_method_id' });
    }
  }
  Payment.init({
    voucher_no: DataTypes.STRING,
    voucher_date: DataTypes.DATE,
    vendor: DataTypes.STRING,
    address: DataTypes.STRING,
    payment_method_id: DataTypes.INTEGER,
    bank: DataTypes.STRING,
    check_no: DataTypes.STRING,
    total: DataTypes.DOUBLE,
    operator_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Payment',
    tableName: 'payment'
  });
  return Payment;
};