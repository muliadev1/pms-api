'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FoTrx extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with FoTrx_Detail
      FoTrx.belongsTo(models.FoTrx_Detail, {
        foreignKey: 'reservation_no', // Foreign key in FoTrx table
        targetKey: 'reservation_no', // Target key in FoTrx_Detail table
        as: 'foTrxDetails' // Alias for the association
      });
    }
  }
  FoTrx.init({
    reservation_id: DataTypes.INTEGER,
    reservation_no: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'FoTrx',
  });
  return FoTrx;
};