'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reservation_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Reservation_Detail.belongsTo(models.Reservation, {
        foreignKey: 'reservation_id', // Foreign key in Reservation_Detail table
        as: 'reservation' // Alias for the association
      });
      Reservation_Detail.belongsTo(models.Hotel, {
        foreignKey: 'hotel_id', // Foreign key in Reservation_Detail table
        as: 'hotel' // Alias for the association
      });
      Reservation_Detail.belongsTo(models.RoomType, {
        foreignKey: 'room_type_id', // Foreign key in Reservation_Detail table
        as: 'roomtype' // Alias for the association
      });
    }
  }
  Reservation_Detail.init({
    reservation_id: DataTypes.INTEGER,
    night_no: DataTypes.INTEGER,
    room_type_id: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    rate_code_id: DataTypes.INTEGER,
    rate_price: DataTypes.DOUBLE,
    is_running: DataTypes.INTEGER,
    per_night_price: DataTypes.DOUBLE,
    roomtype_name: DataTypes.STRING,
    description_roomtype: DataTypes.STRING,
    short_code_roomtype: DataTypes.STRING,
    price_input_roomtype: DataTypes.DOUBLE,
    adult_roomtype: DataTypes.INTEGER,
    child_roomtype: DataTypes.INTEGER,
    is_extrabed_roomtype: DataTypes.INTEGER,
    hotel_id: DataTypes.INTEGER,
    ratecode_json: DataTypes.JSON,
    promo_json: DataTypes.JSON,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Reservation_Detail',
  });
  return Reservation_Detail;
};