'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ModuleMenu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ModuleMenu.belongsTo(models.Module, { as: 'module', foreignKey: 'module_id' });
      ModuleMenu.hasMany(models.RolesMenu, { as: 'rolesmenu', foreignKey: 'menu_id' });

    }
  }
  ModuleMenu.init({
    menu_code: DataTypes.STRING,
    menu_name: DataTypes.STRING,
    module_id: DataTypes.INTEGER,
    order_no: DataTypes.INTEGER,
    icon: DataTypes.STRING,
    url: DataTypes.STRING,
    is_trx: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ModuleMenu',
    tableName: 'module_menus'
  });
  return ModuleMenu;
};