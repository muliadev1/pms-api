'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Promo_code extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Promo_code.init({
    code: DataTypes.STRING,
    amount: DataTypes.DOUBLE,
    start_booking: DataTypes.DATE,
    end_booking: DataTypes.DATE,
    start_stay: DataTypes.DATE,
    end_stay: DataTypes.DATE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Promo_code',
    tableName:'promo_code'
  });
  return Promo_code;
};