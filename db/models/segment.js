'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Segment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Segment.belongsTo(models.Segment_Group, { as: 'segmentgroup', foreignKey: 'segment_group_id' });
      // define associate with reservation
      Segment.hasMany(models.Reservation, {
        foreignKey: 'segment_id', // Foreign key in Reservation table
        as: 'reservations' // Alias for the association
      });
    }
  }
  Segment.init({
    segment_name: DataTypes.STRING,
    segment_group_id: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Segment',
  });
  return Segment;
};