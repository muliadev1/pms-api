'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Journal_Detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with Jourmal
      Journal_Detail.belongsTo(models.Journal, {
        foreignKey: 'journal_id', // Foreign key in MasterBill_Detail table
        as: 'journalmaster' // Alias for the association
      });
    }
  }
  Journal_Detail.init({
    journal_id: DataTypes.INTEGER,
    account_no: DataTypes.STRING,
    debet: DataTypes.DOUBLE,
    kredit: DataTypes.DOUBLE,
    tanggal_input: DataTypes.DATE,
    tanggal_sistem: DataTypes.DATE,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Journal_Detail',
  });
  return Journal_Detail;
};