'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomTypeBedType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RoomTypeBedType.belongsTo(models.RoomType, { as: 'roomtype', foreignKey: 'room_type_id' });
      RoomTypeBedType.belongsTo(models.BedType, { as: 'bedtype', foreignKey: 'bed_type_id' });
    }
  }
  RoomTypeBedType.init({
    room_type_id: DataTypes.INTEGER,
    bed_type_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomTypeBedType',
    tableName:'room_type_bed_type'
  });
  return RoomTypeBedType;
};