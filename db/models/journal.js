'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Journal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association with Journal_Detail
      Journal.hasMany(models.Journal_Detail, { as: 'journaldetail', foreignKey: 'journal_id' });
    }
  }
  Journal.init({
    journal_no: DataTypes.STRING,
    bukti_transaksi: DataTypes.STRING,
    keterangan: DataTypes.STRING,
    tanggal_input: DataTypes.DATE,
    tanggal_sistem: DataTypes.DATE,
    operator_id: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Journal',
  });
  return Journal;
};