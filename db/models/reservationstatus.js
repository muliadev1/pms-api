'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ReservationStatus extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with Reservation
      ReservationStatus.hasMany(models.Reservation, {
        foreignKey: 'reservation_status_id', // Foreign key in Reservation table
        as: 'reservations' // Alias for the association
      });
    }
  }
  ReservationStatus.init({
    status_name: DataTypes.STRING,
    description: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ReservationStatus',
  });
  return ReservationStatus;
};