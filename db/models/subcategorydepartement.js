'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SubCategoryDepartement extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      SubCategoryDepartement.belongsTo(models.Department, { as: 'departement', foreignKey: 'iddepartement' });
      SubCategoryDepartement.belongsTo(models.Item, { as: 'items', foreignKey: 'item_id' });
      SubCategoryDepartement.belongsTo(models.SubCategory, { as: 'sub_category', foreignKey: 'subcategory_id' });
    }
  }
  SubCategoryDepartement.init({
    iddepartement: DataTypes.INTEGER,
    item_id: DataTypes.INTEGER,
    itemcode: DataTypes.STRING,
    subcategory_id: DataTypes.INTEGER,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'SubCategoryDepartement',
  });
  return SubCategoryDepartement;
};