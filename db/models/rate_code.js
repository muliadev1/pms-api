'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Rate_code extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Rate_code.hasMany(models.RateCode_element, { as: 'ratecode_element', foreignKey: 'rate_code_id' });
      Rate_code.hasMany(models.RateCode_roomrate, { as: 'ratecode_roomtype', foreignKey: 'rate_code_id' });
      Rate_code.hasMany(models.Ratecode_promo, { as: 'ratepromo', foreignKey: 'id_ratecode' });
      Rate_code.hasMany(models.BlackOutDate, { as: 'blackoutdate', foreignKey: 'room_rate_id' });
      Rate_code.hasMany(models.RateCode_surcharge, { as: 'surcharge_ratecode', foreignKey: 'ratecode_id' });
      Rate_code.belongsTo(models.Hotel, { as: 'hotel', foreignKey: 'hotel_id' }); 
    }
  }
  Rate_code.init({
    hotel_id: DataTypes.INTEGER,
    rate_code: DataTypes.STRING,
    rate_description: DataTypes.STRING,
    min_night: DataTypes.INTEGER,
    start_booking: DataTypes.DATE,
    end_booking: DataTypes.DATE,
    start_stay: DataTypes.DATE,
    end_stay: DataTypes.DATE,
    adult: DataTypes.INTEGER,
    child: DataTypes.INTEGER,
    pax: DataTypes.INTEGER,
    is_sun: DataTypes.INTEGER,
    is_mon: DataTypes.INTEGER,
    is_tue: DataTypes.INTEGER,
    is_wed: DataTypes.INTEGER,
    is_thu: DataTypes.INTEGER,
    is_fri: DataTypes.INTEGER,
    is_sat: DataTypes.INTEGER,
    percentage: DataTypes.INTEGER,
    deposit_policy: DataTypes.TEXT,
    cancelation_policy: DataTypes.TEXT,
    other_conditions: DataTypes.TEXT,
    amount: DataTypes.INTEGER,
    category_rate: DataTypes.INTEGER,
    promo_code: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Rate_code',
  });
  return Rate_code;
};