'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Room.belongsTo(models.RoomType, { as: 'roomtype', foreignKey: 'roomtype_id' });
      Room.belongsTo(models.RoomStatus, { as: 'roomstatus', foreignKey: 'roomstatus_id' });
      // Define association with Reservation
      Room.hasMany(models.Reservation, {
        foreignKey: 'room_id', // Foreign key in Reservation table
        as: 'reservations' // Alias for the association
      });

      // Define association with FoTrx Detail
      Room.hasMany(models.FoTrx_Detail, {
        foreignKey: 'room_id', // Foreign key in Reservation table
        as: 'fotrxdetails' // Alias for the association
      });

      // Define association with FbTrx 
      Room.hasMany(models.FbTrx, {
        foreignKey: 'room_id', // Foreign key in Reservation table
        as: 'fbtrx' // Alias for the association
      });
    }
  }
  Room.init({
    room_name: DataTypes.STRING,
    description: DataTypes.STRING,
    roomtype_id: DataTypes.INTEGER,
    roomstatus_id: DataTypes.INTEGER,
    is_active: DataTypes.INTEGER,
    floor_id:DataTypes.INTEGER,
    room_number: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Room',
    tableName: 'rooms'
  });
  return Room;
};