'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Guest extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Define association with Reservation Guests
      Guest.hasMany(models.Reservation_Guest, {
        foreignKey: 'guest_id', // Foreign key in Reservation_Guest table
        as: 'guestreservation' // Alias for the association
      });

      // Define association with Reservation
      Guest.hasMany(models.Reservation, {
        foreignKey: 'guest_id', // Foreign key in Reservation table
        as: 'reservation' // Alias for the association
      });

      // Define association with FbTrx
      Guest.hasMany(models.FbTrx, {
        foreignKey: 'guest_id', // Foreign key in Reservation table
        as: 'fbtrx' // Alias for the association
      });

      Guest.hasMany(models.MasterBill, {
        foreignKey: 'guest_id', // Foreign key in Reservation table
        as: 'masterbill' // Alias for the association
      });
    }
  }
  Guest.init({
    saluation: DataTypes.STRING,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    address: DataTypes.STRING,
    country: DataTypes.STRING,
    state: DataTypes.STRING,
    zipcode: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    gender: DataTypes.STRING,
    birthday: DataTypes.DATE,
    idtype: DataTypes.STRING,
    idnumber: DataTypes.STRING,
    description: DataTypes.STRING,
    is_active: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Guest',
  });
  return Guest;
};