'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Page extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Page.init({
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    short_description: DataTypes.TEXT,
    long_description: DataTypes.TEXT,
    page_url: DataTypes.STRING,
    page_images: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Page',
    tableName: 'page'
  });
  return Page;
};