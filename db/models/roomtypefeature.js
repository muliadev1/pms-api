'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomTypeFeature extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RoomTypeFeature.belongsTo(models.RoomType, { as: 'roomtype', foreignKey: 'room_type_id' });
      RoomTypeFeature.belongsTo(models.Feature, { as: 'feature', foreignKey: 'feature_id' });
    }
  }
  RoomTypeFeature.init({
    room_type_id: DataTypes.INTEGER,
    feature_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomTypeFeature',
    tableName:'room_type_feature'
  });
  return RoomTypeFeature;
};