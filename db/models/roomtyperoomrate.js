'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomTypeRoomRate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RoomTypeRoomRate.belongsTo(models.RoomType, { as: 'roomtype', foreignKey: 'room_type_id' });
      RoomTypeRoomRate.belongsTo(models.RoomRate, { as: 'roomrate', foreignKey: 'room_rate_id' });
    }
  }
  RoomTypeRoomRate.init({
    room_type_id: DataTypes.INTEGER,
    room_rate_id: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'RoomTypeRoomRate',
    tableName:'room_type_room_rate'
  });
  return RoomTypeRoomRate;
};