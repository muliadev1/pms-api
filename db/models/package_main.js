'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Package_Main extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Package_Main.hasMany(models.Package_Sub, { as: 'packagesub', foreignKey: 'package_id' });
      Package_Main.hasMany(models.Package_Detail, { as: 'packagedetail', foreignKey: 'package_sub_id' });

      // define associate with reservation
      Package_Main.hasMany(models.FoTrx_Detail, {
        foreignKey: 'package_id', // Foreign key in Reservation table
        as: 'fotrxdetails' // Alias for the association
      });

      Package_Main.hasMany(models.Reservation_Detail, {
        foreignKey: 'package_id', // Foreign key in Reservation table
        as: 'reservation_detail' // Alias for the association
      });
    }
  }
  Package_Main.init({
    package_name: DataTypes.STRING,
    start_stay: DataTypes.DATE,
    end_stay: DataTypes.DATE,
    start_booking: DataTypes.DATE,
    end_booking: DataTypes.DATE,
    totalnight: DataTypes.INTEGER,
    night: DataTypes.INTEGER,
    child: DataTypes.INTEGER,
    adult: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    pax: DataTypes.INTEGER,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Package_Main',
  });
  return Package_Main;
};