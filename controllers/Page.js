const { Page } = require("../db/models/index");
const Sequelize = require('sequelize');
class PageController {
    static async getPage(req, res, next) {
        try {
          const data = await Page.findAll({
           
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getPageDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Page.findOne({
           
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createPage(req, res, next) {
        const {
            title,
            slug,
            short_description,
            long_description,
            page_url,
            page_images,
            updatedby
        } = req.body;
    
        try {
          await Page.create({
            title,
            slug,
            short_description,
            long_description,
            page_url,
            page_images,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Page is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updatePage(req, res) {
        const id = req.params.id;
        const {
            title,
            slug,
            short_description,
            long_description,
            page_url,
            page_images,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Page.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Page is not exist" });
    
    
        try {
          await Page.update(
            {
                title,
                slug,
                short_description,
                long_description,
                page_url,
                page_images,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Page is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deletePage(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Page.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Page is not exist" });
    
        try {
          await Page.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Page is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = PageController;