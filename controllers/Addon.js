const { Addon } = require("../db/models/index");
const Sequelize = require('sequelize');

class AddonController {
    static async getAddon(req, res, next) {
        try {
          const data = await Addon.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getAddonDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Addon.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createAddon(req, res, next) {
        const {
          addon_name,
          price,
          updatedby
        } = req.body;
    
        try {
          await Addon.create({
            addon_name: addon_name,
            price,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Add on is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateAddon(req, res) {
        const id = req.params.id;
        const {
           addon_name,
           price,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await Addon.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Add on is not exist" });
    
    
        try {
          await Addon.update(
            {
              addon_name:  addon_name,
              price,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Add on is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteAddon(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const paytype = await Addon.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Add on is not exist" });
    
        try {
          await Addon.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Add on is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = AddonController;