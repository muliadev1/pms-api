const { Banner } = require("../db/models/index");
const Sequelize = require('sequelize');
class BannerController {
    static async getBanner(req, res, next) {
        try {
          const data = await Banner.findAll({
           
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getBannerDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Banner.findOne({
           
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createBanner(req, res, next) {
        const {
            title,
            subtitle,
            banner_image,
            release,
            is_active,
            updatedby
        } = req.body;
    
        try {
          await Banner.create({
            title,
            subtitle,
            banner_image,
            release,
            is_active,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Banner is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateBanner(req, res) {
        const id = req.params.id;
        const {
            title,
            subtitle,
            banner_image,
            release,
            is_active,
            updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Banner.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Banner is not exist" });
    
    
        try {
          await Banner.update(
            {
                title,
                subtitle,
                banner_image,
                release,
                is_active,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Banner is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteBanner(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Banner.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Banner is not exist" });
    
        try {
          await Banner.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Banner is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = BannerController;