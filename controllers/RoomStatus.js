const { RoomStatus } = require("../db/models/index");
const Sequelize = require('sequelize');

class RoomstatusController {
  static async getRoomStatuses_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const roomstatus = await RoomStatus.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await RoomStatus.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          roomstatus: roomstatus,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getRoomStatuses(req, res, next) {
    try {
      const roomstatus = await RoomStatus.findAll({
      });
      res.status(200).json(roomstatus);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createRoomStatus(req, res, next) {
    const {
      status_name,
      description,
      updatedby
    } = req.body;

    try {
      await RoomStatus.create({
        room_status_name: status_name,
        description: description,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Roomstatus is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getRoomStatus(req, res) {
    const roomstatusId = req.params.id;
    try {
      const roomstatus = await RoomStatus.findOne({
        where: {
          id: roomstatusId,
        },
      });
      res.status(200).json(roomstatus);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateRoomStatus(req, res) {
    const roomstatusId = req.params.id;
    const {
      status_name,
      description,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const roomstatus = await RoomStatus.findAll({
      where: {
        id: roomstatusId,
      },
    });

    if (!roomstatus[0]) return res.status(400).json({ msg: "Roomstatus is not exist" });


    try {
      await RoomStatus.update(
        {
          room_status_name: status_name,
          description: description,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: roomstatusId,
          },
        }
      );

      res.json({ msg: "Update Roomstatus is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteRoomStatus(req, res) {
    const roomstatusId = req.params.id;
    //cek apakah user sdh ada
    const roomstatus = await RoomStatus.findAll({
      where: {
        id: roomstatusId,
      },
    });

    if (!roomstatus[0]) return res.status(400).json({ msg: "Roomstatus is not exist" });

    try {
      await RoomStatus.destroy({
        where: {
          id: roomstatusId,
        },
      });

      res.json({ msg: "Delete Roomstatus is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = RoomstatusController;
