const { Guest } = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op, literal, fn, col } = require('sequelize');

class GuestController {
  static async getGuests_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const guests = await Guest.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Guest.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          guests: guests,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getGuests(req, res, next) {
    try {
      const guests = await Guest.findAll({
      });
      res.status(200).json(guests);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getGuestsName(req, res, next) {
    try {
      // Ambil query parameter name dari request
      const name = req.params.name;

      // Cari tamu-tamu yang sesuai dengan name yang diberikan
      const guests = await Guest.findAll({
          where: literal(`concat(firstname, ' ', lastname) LIKE '%${name}%'`)
      });

      // Kirim respons dengan status 200 dan data tamu-tamu yang ditemukan
      res.status(200).json(guests);
  } catch (error) {
      // Tangani kesalahan jika terjadi dan kirim respons dengan status 500
      res.status(500).json({ message: 'Internal Server Error', error: error });
  }
}

  static async createGuest(req, res, next) {
    const {
      saluation,
      firstname,
      lastname,
      address,
      country,
      state,
      zipcode,
      email,
      phone,
      gender,
      birthday,
      idtype,
      idnumber,
      description,
      is_active,
      updatedby
    } = req.body;

    try {
      await Guest.create({
        saluation: saluation,
        firstname: firstname,
        lastname: lastname,
        address: address,
        country: country,
        state: state,
        zipcode: zipcode,
        email: email,
        phone: phone,
        gender: gender,
        birthday: birthday,
        idtype: idtype,
        idnumber: idnumber,
        description: description,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Guest is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getGuest(req, res) {
    const guestId = req.params.id;
    try {
      const guest = await Guest.findOne({
        where: {
          id: guestId,
        },
      });
      res.status(200).json(guest);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateGuest(req, res) {
    const guestId = req.params.id;
    const {
      saluation,
      firstname,
      lastname,
      address,
      country,
      state,
      zipcode,
      email,
      phone,
      gender,
      birthday,
      idtype,
      idnumber,
      description,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const guest = await Guest.findAll({
      where: {
        id: guestId,
      },
    });

    if (!guest[0]) return res.status(400).json({ msg: "Guest is not exist" });


    try {
      await Guest.update(
        {
          saluation: saluation,
          firstname: firstname,
          lastname: lastname,
          address: address,
          country: country,
          state: state,
          zipcode: zipcode,
          email: email,
          phone: phone,
          gender: gender,
          birthday: birthday,
          idtype: idtype,
          idnumber: idnumber,
          description: description,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: guestId,
          },
        }
      );

      res.json({ msg: "Update Guest is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteGuest(req, res) {
    const guestId = req.params.id;
    //cek apakah user sdh ada
    const guest = await Guest.findAll({
      where: {
        id: guestId,
      },
    });

    if (!guest[0]) return res.status(400).json({ msg: "Guest is not exist" });

    try {
      await Guest.destroy({
        where: {
          id: guestId,
        },
      });

      res.json({ msg: "Delete Guest is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = GuestController;
