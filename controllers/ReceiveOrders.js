const { RO_Main, RO_Detail, Vendor } = require("../db/models/index");
const Sequelize = require('sequelize');

const { Op, literal, fn, col } = require('sequelize');

class ReceiveOrderController {
    static async getOrders_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const orders = await RO_Main.findAll({
                include: ["department"],
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await RO_Main.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    orders: orders,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getOrders(req, res, next) {
        try {
            const orders = await RO_Main.findAll({
                include: ["receiveorderdetail","vendor"],
            });
            res.status(200).json(orders);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getCardStock(req, res, next) {
        let { itemcode, tglAwal,tglAkhir } = req.query
        try {
            const orders = await RO_Main.findAll({
                include: [{
                    model: RO_Detail,
                    as: 'receiveorderdetail',
                    where: { item_code: itemcode }
                }, {
                    model: Vendor,
                    as: 'vendor'
                }],
                where: {
                    receive_date: {
                        [Op.between]: [tglAwal, tglAkhir]
                    }
                },
                order: [
                    ['receive_date', 'DESC']
                ]
            });
            res.status(200).json(orders);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async loadMi(req, res, next) {
        const recCode = req.params.recicode;
        try {
            const orders = await RO_Main.findAll({
                include: ["receiveorderdetail","vendor"],
                where:{
                    receive_order_code:recCode
                }
            });
            res.status(200).json(orders);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async loadMidate(req, res, next) {
        try {
            const { tglAwal, tglAkhir } = req.query;
            const orders = await RO_Main.findAll({
                include: ["receiveorderdetail","vendor"],
                where: {
                    receive_date: {
                        [Op.between]: [tglAwal, tglAkhir]
                    }
                },
                order: [
                    ['receive_date', 'DESC']
                ]
            });
            res.status(200).json(orders);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createOrder(req, res, next) {
        const {
            receive_order_code,
            purchase_order_code,
            receive_date,
            vendor_id,
            is_invoicing,
            receipt_no,
            total,
            discount,
            total_after_disc,
            is_credit,
            due_date,
            is_paid,
            down_payment,
            remaind,
            department_id,
            operator_id,
            updatedby
        } = req.body;

        try {
            await RO_Main.create({
                receive_order_code: receive_order_code,
                purchase_order_code: purchase_order_code,
                receive_date: receive_date,
                vendor_id: vendor_id,
                is_invoicing: is_invoicing,
                receipt_no: receipt_no,
                total: total,
                discount: discount,
                total_after_disc: total_after_disc,
                is_credit: is_credit,
                due_date: due_date,
                is_paid: is_paid,
                down_payment: down_payment,
                remaind: remaind,
                department_id: department_id,
                operator_id: operator_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Receive Order is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getOrder(req, res) {
        const orderId = req.params.id;
        try {
            const order = await RO_Main.findOne({
                include: ["receiveorderdetail","vendor"],
                where: {
                    id: orderId,
                },
            });
            res.status(200).json(order);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateOrder(req, res) {
        const orderId = req.params.id;
        const {
            purchase_order_code,
            receive_date,
            vendor_id,
            is_invoicing,
            receipt_no,
            total,
            discount,
            total_after_disc,
            is_credit,
            due_date,
            is_paid,
            down_payment,
            remaind,
            department_id,
            operator_id,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const order = await RO_Main.findAll({
            where: {
                id: orderId,
            },
        });

        if (!order[0]) return res.status(400).json({ msg: "Resecive Order is not exist" });


        try {
            await RO_Main.update(
                {
                    purchase_order_code: purchase_order_code,
                    receive_date: receive_date,
                    vendor_id: vendor_id,
                    is_invoicing: is_invoicing,
                    receipt_no: receipt_no,
                    total: total,
                    discount: discount,
                    total_after_disc: total_after_disc,
                    is_credit: is_credit,
                    due_date: due_date,
                    is_paid: is_paid,
                    down_payment: down_payment,
                    remaind: remaind,
                    department_id: department_id,
                    operator_id: operator_id,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: orderId,
                    },
                }
            );

            res.json({ msg: "Update Receive Order is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteOrder(req, res) {
        const orderId = req.params.id;
        //cek apakah user sdh ada
        const order = await RO_Main.findAll({
            where: {
                id: orderId,
            },
        });

        if (!order[0]) return res.status(400).json({ msg: "Receive Order is not exist" });

        try {
            await RO_Main.destroy({
                where: {
                    id: orderId,
                },
            });

            res.json({ msg: "Delete Receive Order is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getOrderDetails(req, res) {
        const recId = req.params.recId;
        try {
            const ordDetails = await RO_Detail.findAll({
                where: {
                    receive_order_id: recId,
                },
            });
            res.status(200).json(ordDetails);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createOrderDetail(req, res) {
        const recId = req.params.recId;
        const {
            item_code,
            description,
            unit,
            price,
            qty_order,
            qty_stock,
            subtotal,
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const order = await RO_Main.findAll({
            where: {
                id: recId,
            },
        });

        if (!order[0]) return res.status(400).json({ msg: "Receive Order is not exist" });

        try {
            await RO_Detail.create({
                receive_order_id: recId,
                item_code: item_code,
                description: description,
                unit: unit,
                price: price,
                qty_order: qty_order,
                qty_stock: qty_stock,
                subtotal: subtotal,
                notes: notes,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Receive Order Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getOrderDetail(req, res) {
        const detailId = req.params.id;

        try {
            const ordDetail = await RO_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(ordDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateOrderDetail(req, res) {
        const detailId = req.params.id;
        const {
            item_code,
            description,
            unit,
            price,
            qty_order,
            qty_stock,
            subtotal,
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const reqDetail = await RO_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!reqDetail[0]) return res.status(400).json({ msg: "Receive Order Detail is not exist" });


        try {
            await RO_Detail.update(
                {
                    item_code: item_code,
                    description: description,
                    unit: unit,
                    price: price,
                    qty_order: qty_order,
                    qty_stock: qty_stock,
                    subtotal: subtotal,
                    notes: notes,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Receive Order Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteOrderDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const ordDetail = await RO_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!ordDetail[0]) return res.status(400).json({ msg: "Receive Order Detail is not exist" });

        try {
            await RO_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Receive Order Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = ReceiveOrderController;

