const { ModuleMenu } = require("../db/models/index");
const Sequelize = require('sequelize');

class ModuleMenuController {
    static async getModuleMenus_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const modulemenus = await ModuleMenu.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await ModuleMenu.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    modulemenus: modulemenus,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async getModuleMenus(req, res, next) {
        try {
            const modulemenus = await ModuleMenu.findAll({});
            res.status(200).json(modulemenus);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async getMenusByModule(req, res, next) {
        const moduleId = req.params.moduleid;
        try {
            const modulemenus = await ModuleMenu.findAll({
                where: {
                    module_id: moduleId,
                },
            });
            res.status(200).json(modulemenus);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async createModuleMenu(req, res, next) {
        const {
            menu_code,
            menu_name,
            module_id,
            order_no,
            icon,
            url,
            is_trx,
            updatedBy,
        } = req.body;

        try {
            await ModuleMenu.create({
                menu_code: menu_code,
                menu_name: menu_name,
                module_id: module_id,
                order_no: order_no,
                icon: icon,
                url: url,
                is_trx: is_trx,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedBy,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedBy
            });
            res.status(200).json({ msg: "Add Module menu is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async getModuleMenu(req, res) {
        const modmenuId = req.params.id;
        try {
            const modulemenu = await ModuleMenu.findOne({
                where: {
                    id: modmenuId,
                },
            });
            res.status(200).json(modulemenu);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async updateModuleMenu(req, res) {
        const modmenuId = req.params.id;
        const {
            menu_code,
            menu_name,
            module_id,
            order_no,
            icon,
            url,
            is_trx,
            updatedBy
        } = req.body;

        //cek apakah user sdh ada
        const modulemenu = await ModuleMenu.findAll({
            where: {
                id: modmenuId,
            },
        });

        if (!modulemenu[0]) return res.status(400).json({ msg: "Module menu is not exist" });


        try {
            await ModuleMenu.update(
                {
                    menu_code: menu_code,
                    menu_name: menu_name,
                    module_id: module_id,
                    order_no: order_no,
                    icon: icon,
                    url: url,
                    is_trx: is_trx,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedBy
                },
                {
                    where: {
                        id: modmenuId,
                    },
                }
            );

            res.json({ msg: "Update Module menu is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async deleteModuleMenu(req, res) {
        const modmenuId = req.params.id;
        //cek apakah user sdh ada
        const modulemenu = await ModuleMenu.findAll({
            where: {
                id: modmenuId,
            },
        });

        if (!modulemenu[0]) return res.status(400).json({ msg: "Module menu is not exist" });

        try {
            await ModuleMenu.destroy({
                where: {
                    id: modmenuId,
                },
            });

            res.json({ msg: "Delete Module menu is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

}

module.exports = ModuleMenuController;
