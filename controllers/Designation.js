const { Designation } = require("../db/models/index");
const Sequelize = require('sequelize');

class DesignationController {
    static async getDesignation(req, res, next) {
        try {
          const data = await Designation.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getDesignationDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Designation.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createDesignation(req, res, next) {
        const {
          designation_name,
          updatedby
        } = req.body;
    
        try {
          await Designation.create({
            designation_name: designation_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Designation is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateDesignation(req, res) {
        const id = req.params.id;
        const {
           designation_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Designation.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Designation is not exist" });
    
    
        try {
          await Designation.update(
            {
              designation_name:  designation_name,

              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Designation is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteDesignation(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Designation.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Designation is not exist" });
    
        try {
          await Designation.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Designation is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = DesignationController;