const { Company } = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op, literal, fn, col } = require('sequelize');

class CompanyController {
  static async getCompanies_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const companies = await Company.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Company.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          companies: companies,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getCompanies(req, res, next) {
   
    try {
      
      const companies = await Company.findAll({
        
      });
      res.status(200).json(companies);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getCompaniesName(req, res, next) { 
    try {
      const name = req.params.name;

      const companies = await Company.findAll({   
        where: literal(`concat(company_name) LIKE '%${name}%'`)   
      });
      res.status(200).json(companies);
    } catch (error) {   
      res.status(500).json({ messege: 'Internal Server Error', error:error});   
    }
  }    

  static async createCompany(req, res, next) {
    const {
      company_name,
      address,
      country,
      state,
      zipcode,
      email,
      phone,
      type,
      description,
      contact_first_name,
      contact_last_name,
      contact_address,
      contact_phone,
      contact_email,
      is_active,
      updatedby
    } = req.body;

    try {
      await Company.create({
        company_name: company_name,
        address: address,
        country: country,
        state: state,
        zipcode: zipcode,
        email: email,
        phone: phone,
        type: type,
        description: description,
        contact_first_name: contact_first_name,
        contact_last_name: contact_last_name,
        contact_address: contact_address,
        contact_phone: contact_phone,
        contact_email: contact_email,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Company is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getCompany(req, res) {
    const compId = req.params.id;
    try {
      const company = await Company.findOne({
        where: {
          id: compId,
        },
      });
      res.status(200).json(company);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateCompany(req, res) {
    const compId = req.params.id;
    const {
      company_name,
      address,
      country,
      state,
      zipcode,
      email,
      phone,
      type,
      description,
      contact_first_name,
      contact_last_name,
      contact_address,
      contact_phone,
      contact_email,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const company = await Company.findAll({
      where: {
        id: compId,
      },
    });

    if (!company[0]) return res.status(400).json({ msg: "Company is not exist" });


    try {
      await Company.update(
        {
          company_name: company_name,
          address: address,
          country: country,
          state: state,
          zipcode: zipcode,
          email: email,
          phone: phone,
          type: type,
          description: description,
          contact_first_name: contact_first_name,
          contact_last_name: contact_last_name,
          contact_address: contact_address,
          contact_phone: contact_phone,
          contact_email: contact_email,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: compId,
          },
        }
      );

      res.json({ msg: "Update Company is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteCompany(req, res) {
    const compId = req.params.id;
    //cek apakah user sdh ada
    const company = await Company.findAll({
      where: {
        id: compId,
      },
    });

    if (!company[0]) return res.status(400).json({ msg: "Company is not exist" });

    try {
      await Company.destroy({
        where: {
          id: compId,
        },
      });

      res.json({ msg: "Delete Company is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = CompanyController;
