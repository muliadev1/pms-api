const { RoomRate,RoomRateBlackout,RoomRateSurcharge } = require("../db/models/index");
const Sequelize = require('sequelize');

class RoomRateController {
    static async getRoomRate(req, res, next) {
        try {
          const data = await RoomRate.findAll({
            include:[
              {
                model:RoomRateBlackout,
                as:'roomrateblackout',
                include:['blackout']
              },
              {
                model:RoomRateSurcharge,
                as:'roomratesurcharge',
                include:['surcharge']
              },
    
            ]
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getRoomRateDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await RoomRate.findOne({
            include:[
              {
                model:RoomRateBlackout,
                as:'roomrateblackout',
                include:['blackout']
              },
              {
                model:RoomRateSurcharge,
                as:'roomratesurcharge',
                include:['surcharge']
              },
    
            ],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createRoomRate(req, res, next) {
        const {
          date_from,
          date_to,
          base_price,
          is_sun,
           is_mon,
           is_tue, 
           is_wed, 
           is_thu, 
           is_fri, 
           is_sat,
           blackout,
           surcharge,
          updatedby
        } = req.body;

        try {
          const dataroomrate = await RoomRate.create({
              date_from: date_from, // formatteddate_from + "T00:00:00Z",
              date_to: date_to,
              base_price:base_price,
              is_sun:is_sun,
              is_mon:is_mon,
              is_tue:is_tue, 
              is_wed:is_wed, 
              is_thu:is_thu, 
              is_fri:is_fri, 
              is_sat:is_sat,
              createdAt: Sequelize.fn('NOW'),
              createdBy: updatedby,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby,
            });
            
            const bulkdata = blackout.map(subData => ({
              room_rate_id: dataroomrate.id,
              blackout_id: subData.blackout_id,
              createdAt: Sequelize.fn('NOW'),
              createdBy: updatedby,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby,
          }));
      
          const bulkdata2 = surcharge.map(subData => ({
              room_rate_id: dataroomrate.id,
              surcharge_id: subData.surcharge_id,
              createdAt: Sequelize.fn('NOW'),
              createdBy: updatedby,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby,
          }));
      
          await RoomRateBlackout.bulkCreate(bulkdata);
          await RoomRateSurcharge.bulkCreate(bulkdata2);

          res.status(200).json({ msg: "Add RoomRate is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateRoomRate(req, res) {
        const id = req.params.id;
        const {
            date_from,
            date_to,
            base_price,
            is_sun,
            is_mon,
            is_tue,
            is_wed,
            is_thu,
            is_fri,
            is_sat,
            updatedby,
            blackout,
            surcharge
        } = req.body;
    
        try {
            // Cek apakah room rate dengan ID yang diberikan ada
            const existingRoomRate = await RoomRate.findOne({
                where: {
                    id: id,
                },
            });
    
            if (!existingRoomRate) {
                return res.status(400).json({ msg: "Room rate does not exist" });
            }
    
            // Update data room rate
            await RoomRate.update(
                {
                    date_from,
                    date_to,
                    base_price,
                    is_sun,
                    is_mon,
                    is_tue,
                    is_wed,
                    is_thu,
                    is_fri,
                    is_sat,
                    updatedAt: new Date(), // Menggunakan new Date() untuk mendapatkan waktu saat ini
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: id,
                    },
                }
            );
    
            // Hapus data blackout dan surcharge yang terkait dengan room rate yang akan diperbarui
            await RoomRateBlackout.destroy({
                where: {
                    room_rate_id: id,
                },
            });
    
            await RoomRateSurcharge.destroy({
                where: {
                    room_rate_id: id,
                },
            });
    
            // Tambahkan data baru yang terkait dengan room rate yang diperbarui
            const bulkBlackoutData = blackout.map(subData => ({
                room_rate_id: id,
                blackout_id: subData.blackout_id,
                createdAt: new Date(),
                createdBy: updatedby,
                updatedAt: new Date(),
                updatedBy: updatedby,
            }));
    
            const bulkSurchargeData = surcharge.map(subData => ({
                room_rate_id: id,
                surcharge_id: subData.surcharge_id,
                createdAt: new Date(),
                createdBy: updatedby,
                updatedAt: new Date(),
                updatedBy: updatedby,
            }));
    
            await RoomRateBlackout.bulkCreate(bulkBlackoutData);
            await RoomRateSurcharge.bulkCreate(bulkSurchargeData);
    
            res.json({ msg: "Room rate updated successfully" });
        } catch (error) {
            console.error("Error in updateRoomRate:", error);
            res.status(500).json({ message: 'Internal Server Error', error: error.message });
        }
    }
    
    
      static async deleteRoomRate(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await RoomRate.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "RoomRate is not exist" });
    
        try {
          await RoomRate.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete RoomRate is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = RoomRateController;