const { Languages } = require("../db/models/index");
const Sequelize = require('sequelize');

class LanguagesController {
    static async getLanguages(req, res, next) {
        try {
          const data = await Languages.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getLanguagesDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Languages.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createLanguages(req, res, next) {
        const {
            language_name,
            language_code,
            iso_code,
            updatedby
        } = req.body;

        try {
          await Languages.create({
            language_name,
            language_code,
            iso_code,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Languages is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateLanguages(req, res) {
        const id = req.params.id;
        const {
            language_name,
            language_code,
            iso_code,
            updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await Languages.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Languages is not exist" });
    
    
        try {
          await Languages.update(
            {
                language_name,
                language_code,
                iso_code,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Languages is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteLanguages(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Languages.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Languages is not exist" });
    
        try {
          await Languages.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Languages is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = LanguagesController;