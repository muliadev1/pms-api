const { Package_Main, Package_Sub, Package_Detail,Room } = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op } = Sequelize;

function formatDate(dateString) {
    // Parse the dateString to create a Date object
    var date = new Date(dateString);

    // Extract day, month, and year
    var day = date.getUTCDate();
    var month = date.getUTCMonth() + 1; // Month is zero-based, so we add 1
    var year = date.getUTCFullYear();

    // Pad day and month with leading zeros if necessary
    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;

    // Construct the formatted date string in "dd/mm/yyyy" format
    var formattedDate = month + '/' + day + '/' + year;

    return formattedDate;
}

class PackageController {

    // -----------------------------
    //  Package mains
    // ------------------------------
    static async getPackages_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const packages = await Package_Main.findAll({
                include: ["packagedetail"],
                offset: startIndex,
                limit: Number(pageSize)
            });

            //! Retrieve total count of records for pagination info
            const totalCount = await Package_Main.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    packages: packages,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getPackages(req, res, next) {
        try {
            const packages = await Package_Main.findAll({
                include: [
                    {
                        model: Package_Sub,
                        as: 'packagesub'
                    },
                    {
                        model: Package_Detail,
                        as: 'packagedetail'
                    }
                ],
                order: [['createdAt', 'DESC']]
            });

            // Mengubah format start_stay pada setiap paket
            const formattedPackages = packages.map(pkg => {
                if (pkg.dataValues.start_stay !== null) {
                    pkg.dataValues.start_stay = formatDate(pkg.dataValues.start_stay);
                }
                if (pkg.dataValues.end_stay !== null) {
                    pkg.dataValues.end_stay = formatDate(pkg.dataValues.end_stay);
                }
                if (pkg.dataValues.start_booking !== null) {
                    pkg.dataValues.start_booking = formatDate(pkg.dataValues.start_booking);
                }
                if (pkg.dataValues.end_booking !== null) {
                    pkg.dataValues.end_booking = formatDate(pkg.dataValues.end_booking);
                }
                return pkg;
            });


            res.status(200).json(formattedPackages);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getPackagesRev(req, res, next) {
        let { checkin, checkout, adult, child } = req.query;
        try {
            const packages = await Package_Main.findAll({
                include: [
                    {
                        model: Package_Sub,
                        as: 'packagesub',
                    },
                    {
                        model: Package_Detail,
                        as: 'packagedetail',
                    }
                ],
                where: {
                    child: child,
                    adult: adult,
                    [Op.or]: [
                        {
                            start_stay: {
                                [Op.lte]: new Date(checkout)
                            },
                            end_stay: {
                                [Op.gte]: new Date(checkin)
                            }
                        },
                        {
                            start_booking: {
                                [Op.lte]: new Date(),
                                [Op.gte]: new Date(checkin)
                            },
                            end_booking: {
                                [Op.gte]: new Date(),
                                [Op.lte]: new Date(checkout)
                            }
                        }
                    ]
                },
                order: [['createdAt', 'DESC']]
            });
    
            // Mengubah format start_stay pada setiap paket
            const formattedPackages = packages.map(pkg => {
                if (pkg.dataValues.start_stay !== null) {
                    pkg.dataValues.start_stay = formatDate(pkg.dataValues.start_stay);
                }
                if (pkg.dataValues.end_stay !== null) {
                    pkg.dataValues.end_stay = formatDate(pkg.dataValues.end_stay);
                }
                if (pkg.dataValues.start_booking !== null) {
                    pkg.dataValues.start_booking = formatDate(pkg.dataValues.start_booking);
                }
                if (pkg.dataValues.end_booking !== null) {
                    pkg.dataValues.end_booking = formatDate(pkg.dataValues.end_booking);
                }
                return pkg;
            });
    
            res.status(200).json(formattedPackages);
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    

    static async createPackage(req, res, next) {
        const {
            package_name,
            start_stay,
            end_stay,
            start_booking,
            pax,
            night,
            child,
            adult,
            end_booking,
            updatedby
        } = req.body;

        try {
             // Parsing tanggal dari "MM/DD/YYYY" ke format yang diinginkan ("YYYY-MM-DD")
        const [startMonth, startDay, startYear] = start_stay.split("/");
        const [endMonth, endDay, endYear] = end_stay.split("/");
        const [startMonth1, startDay1, startYear1] = start_booking.split("/");
        const [endMonth1, endDay1, endYear1] = end_booking.split("/");

        // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
        const formattedStartStay = `${startYear}-${startMonth}-${startDay}`;
        const formattedEndStay = `${endYear}-${endMonth}-${endDay}`;

        const formattedStartBooking = `${startYear1}-${startMonth1}-${startDay1}`;
        const formattedEndBooking = `${endYear1}-${endMonth1}-${endDay1}`;

        // Menghitung totalnight
        const startDate = new Date(formattedStartStay);
        const endDate = new Date(formattedEndStay);
        const timeDifference = endDate.getTime() - startDate.getTime();
        const Totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));

        await Package_Main.create({
            package_name: package_name,
            start_stay: formattedStartStay + "T00:00:00Z", // Menambahkan format jam
            end_stay: formattedEndStay + "T00:00:00Z", // Menambahkan format jam
            start_booking: formattedStartBooking + "T00:00:00Z",
            end_booking: formattedEndBooking + "T00:00:00Z",
            totalnight: Totalnight,
            pax:pax,
            night:night,
            child:child,
            adult:adult,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
        });
            res.status(200).json({ msg: "Add Package is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createPackagesBulk(req, res) {
        try {
            const {
                package_name,
                start_stay,
                end_stay,
                start_booking,
                pax,
                night,
                child,
                adult,
                end_booking,
                package_sub_data,
                package_detail_data,
                updatedby
            } = req.body;
    
            // Parsing tanggal dari "MM/DD/YYYY" ke format yang diinginkan ("YYYY-MM-DD")
            const parseDate = (dateString) => {
                const [month, day, year] = dateString.split("/");
                return `${year}-${month}-${day}T00:00:00Z`;
            };
    
            const formattedStartStay = parseDate(start_stay);
            const formattedEndStay = parseDate(end_stay);
            const formattedStartBooking = parseDate(start_booking);
            const formattedEndBooking = parseDate(end_booking);
    
            // Menghitung totalnight
            const startDate = new Date(formattedStartStay);
            const endDate = new Date(formattedEndStay);
            const timeDifference = endDate.getTime() - startDate.getTime();
            const totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));
    
            const packagemain = await Package_Main.create({
                package_name: package_name,
                start_stay: formattedStartStay,
                end_stay: formattedEndStay,
                start_booking: formattedStartBooking,
                end_booking: formattedEndBooking,
                totalnight: totalnight,
                pax: pax,
                night: night,
                child: child,
                adult: adult,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
    
            const bulkdata = package_sub_data.map(subData => ({
                package_id: packagemain.id,
                room_id: subData.room_id,
                room_name: subData.room_name,
                room_price: subData.room_price,
                night: subData.night,
                child: subData.child,
                adult: subData.adult,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            const bulkdata2 = package_detail_data.map(detailData => ({
                package_sub_id: packagemain.id,
                item_id: detailData.item_id,
                item_code: detailData.item_code,
                item_name: detailData.item_name,
                price: detailData.price,
                qty: detailData.qty,
                daily: detailData.daily,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));

            await Package_Sub.bulkCreate(bulkdata);

            await Package_Detail.bulkCreate(bulkdata2);
            res.status(200).json({ msg: "Add Package is successful" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }

    static async updatePackageBulk(req, res) {
        try {
            const package_id = req.params.id;
            const {
                package_name,
                start_stay,
                end_stay,
                start_booking,
                pax,
                night,
                child,
                adult,
                end_booking,
                package_sub_data,
                package_detail_data,
                updatedby
            } = req.body;
    
            // Parsing tanggal dari "MM/DD/YYYY" ke format yang diinginkan ("YYYY-MM-DD")
            const parseDate = (dateString) => {
                const [month, day, year] = dateString.split("/");
                return `${year}-${month}-${day}T00:00:00Z`;
            };
    
            const formattedStartStay = parseDate(start_stay);
            const formattedEndStay = parseDate(end_stay);
            const formattedStartBooking = parseDate(start_booking);
            const formattedEndBooking = parseDate(end_booking);
    
            // Menghitung totalnight
            const startDate = new Date(formattedStartStay);
            const endDate = new Date(formattedEndStay);
            const timeDifference = endDate.getTime() - startDate.getTime();
            const totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));
    
            // Update data pada tabel Package_Main
            await Package_Main.update({
                package_name: package_name,
                start_stay: formattedStartStay,
                end_stay: formattedEndStay,
                start_booking: formattedStartBooking,
                end_booking: formattedEndBooking,
                totalnight: totalnight,
                pax: pax,
                night: night,
                child: child,
                adult: adult,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }, {
                where: {
                    id: package_id
                }
            });
    
            // Menghapus data lama dari tabel Package_Sub dan Package_Detail
            await Package_Sub.destroy({
                where: {
                    package_id: package_id
                }
            });
    
            await Package_Detail.destroy({
                where: {
                    package_sub_id: package_id
                }
            });
    
            // Menambahkan data baru ke dalam tabel Package_Sub dan Package_Detail
            const bulkdata = package_sub_data.map(subData => ({
                package_id: package_id,
                room_id: subData.room_id,
                room_name: subData.room_name,
                room_price: subData.room_price,
                night: subData.night,
                child: subData.child,
                adult: subData.adult,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            const bulkdata2 = package_detail_data.map(detailData => ({
                package_sub_id: package_id,
                item_id: detailData.item_id,
                item_code: detailData.item_code,
                item_name: detailData.item_name,
                price: detailData.price,
                qty: detailData.qty,
                daily: detailData.daily,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            await Package_Sub.bulkCreate(bulkdata);
            await Package_Detail.bulkCreate(bulkdata2);
    
            res.status(200).json({ message: "Package updated successfully" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    
    

    static async getPackage(req, res) {
        const packgId = req.params.id;
        try {
            const regPackage = await Package_Main.findOne({
                include: [
                    {
                        model: Package_Sub,
                        as: 'packagesub'
                    },
                    {
                        model: Package_Detail,
                        as: 'packagedetail'
                    }
                ],
                where: {
                    id: packgId,
                },
            });

            res.status(200).json(regPackage);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updatePackage(req, res) {
        const packgId = req.params.id;
        const {
            package_name,
            start_stay,
            end_stay,
            start_booking,
            end_booking,
            pax,
            night,
            child,
            adult,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const regPackage = await Package_Main.findAll({
            where: {
                id: packgId,
            },
        });

        if (!regPackage[0]) return res.status(400).json({ msg: "Package is not exist" });


        try {
            // Menghitung totalnight
            const startDate = new Date(start_stay);
            const endDate = new Date(end_stay);
            const timeDifference = endDate.getTime() - startDate.getTime();
            const totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));

            await Package_Main.update(
                {
                    package_name: package_name,
                    start_stay: start_stay,
                    end_stay: end_stay,
                    start_booking: start_booking,
                    end_booking: end_booking,
                    totalnight: totalnight,
                    pax:pax,
                    night:night,
                    child:child,
                    adult:adult,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: packgId,
                    },
                }
            );

            res.json({ msg: "Update Package is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deletePackage(req, res) {
        const packgId = req.params.id;
        //cek apakah user sdh ada
        const regPackage = await Package_Main.findAll({
            where: {
                id: packgId,
            },
        });

        if (!regPackage[0]) return res.status(400).json({ msg: "Package is not exist" });

        try {
            await Package_Main.destroy({
                where: {
                    id: packgId,
                },
            });

            
            // Menghapus data lama dari tabel Package_Sub dan Package_Detail
            await Package_Sub.destroy({
                where: {
                    package_id: packgId
                }
            });
    
            await Package_Detail.destroy({
                where: {
                    package_sub_id: packgId
                }
            });

            res.json({ msg: "Delete Package is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    // -----------------------------
    //  Package subs
    // ------------------------------

    static async getPackageSubs(req, res) {
        const packgId = req.params.packgId;
        try {
            const packgSubs = await Package_Sub.findAll({
                where: {
                    package_id: packgId,
                },
            });
            res.status(200).json(packgSubs);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createPackageSub(req, res) {
        const packgId = req.params.packgId;
        const {
            room_id,
            room_name,
            room_price,
            night,
            child,
            adult,
            updatedby,
        } = req.body;

        //cek apakah package sdh ada
        const regPackage = await Package_Main.findAll({
            where: {
                id: packgId,
            },
        });


        if (!regPackage[0]) return res.status(400).json({ msg: "Package is not exist" });

        try {
            await Package_Sub.create({
                package_id: packgId,
                room_id: room_id,
                room_name: room_name,
                room_price: room_price,
                night:night,
                child:child,
                adult:adult,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Package Sub is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createBulkPackageSub(req, res) {
        const packgId = req.params.packgId;
        const {
            package_sub_data, // Ini adalah array yang berisi data untuk banyak entri Package_Sub
            updatedby
        } = req.body;

        //cek apakah package sdh ada
        const regPackage = await Package_Main.findAll({
            where: {
                id: packgId,
            },
        });

        if (!regPackage[0]) return res.status(400).json({ msg: "Package is not exist" });

        try {
            const packageSubEntries = package_sub_data.map(subData => ({
                package_id: packgId,
                room_id: subData.room_id,
                room_name: subData.room_name,
                room_price: subData.room_price,
                night:subData.night,
                child:subData.child,
                adult:subData.adult,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));


            await Package_Sub.bulkCreate(packageSubEntries);
            res.status(200).json({ msg: "Add Package Sub is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getPackageSub(req, res) {
        const detailId = req.params.id;

        try {
            const packgSub = await Package_Sub.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(packgSub);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updatePackageSub(req, res) {
        const detailId = req.params.id;
        const {
            room_id,
            room_name,
            room_price,
            updatedby,
            night,
            child,
            adult,
        } = req.body;

        //cek apakah user sdh ada
        const packgSub = await Package_Sub.findAll({
            where: {
                id: detailId,
            },
        });

        if (!packgSub[0]) return res.status(400).json({ msg: "Package Sub is not exist" });


        try {
            await Package_Sub.update(
                {
                    room_id: room_id,
                    room_name: room_name,
                    room_price: room_price,
                    night:night,
                    child:child,
                    adult:adult,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Package Sub is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deletePackageSub(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const packgSub = await Package_Sub.findAll({
            where: {
                id: detailId,
            },
        });

        if (!packgSub[0]) return res.status(400).json({ msg: "Package Sub is not exist" });

        try {
            await Package_Sub.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Package Sub is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    // -----------------------------
    //  Package details
    // ------------------------------
    static async getPackageDetails(req, res) {
        const packgId = req.params.packgId;
        try {
            // Lakukan pencarian package_main berdasarkan packageId
            const packageMain = await Package_Main.findOne({
                where: { id: packgId },
               
            });

            // Jika package_main tidak ditemukan, kembalikan null
            if (!packageMain) return res.status(400).json({ msg: "Package Main is not exist" });


            // const packgDetails = packageMain.packagesub.map(sub => sub.packagedetail);

            res.status(200).json(packageMain);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }

    }

    static async createBulkPackageDetail(req, res) {
        const packgId = req.params.packgId;

        // ambil data package sub berdasarkan package_id
        const packageSubs = await Package_Sub.findAll({
            where: { package_id: packgId }
        });


        // Pastikan setidaknya ada satu Package_Sub yang terkait dengan packgId
        if (packageSubs.length === 0) {
            return res.status(400).json({ msg: "No Package_Sub found for the given Package" });
        }

        const {
            package_detail_data, // Ini adalah array yang berisi data untuk banyak entri Package_Detail
            updatedby
        } = req.body;


        let current_time = new Date()
        
        try {
            for (const packageSub of packageSubs) {
                // Buat entri Package_Detail untuk setiap Package_Sub
                const packageDetail = package_detail_data.map(detailData => ({
                    package_sub_id: packageSub.id,
                    item_id: detailData.item_id,
                    item_code: detailData.item_code,
                    item_name: detailData.item_name,
                    price: detailData.price,
                    qty: detailData.qty,
                    daily: detailData.daily,
                    createdAt: current_time,
                    createdBy: updatedby,
                    updatedAt: current_time,
                    updatedBy: updatedby,
                }));
                //simpan ke package_details
                await Package_Detail.bulkCreate(packageDetail);
            }

            res.status(200).json({ msg: "Add Package Detail successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
            res.status(500).json({ msg: "Error creating Package Detail" });
        }
    }

    static async getPackageDetail(req, res) {
        const detailId = req.params.id;

        try {
            const packgDetail = await Package_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(packgDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updatePackageDetail(req, res) {
        const detailId = req.params.id;
        const {
            item_id,
            item_code,
            item_name,
            price,
            qty,
            daily,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const packgDetail = await Package_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!packgDetail[0]) return res.status(400).json({ msg: "Package Detail is not exist" });


        try {
            await Package_Detail.update(
                {
                    item_id: item_id,
                    item_code: item_code,
                    item_name: item_name,
                    price: price,
                    qty: qty,
                    daily: daily,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Package Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deletePackageDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const packgDetail = await Package_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!packgDetail[0]) return res.status(400).json({ msg: "Package Detail is not exist" });

        try {
            await Package_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Package Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = PackageController;

