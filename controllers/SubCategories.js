const { SubCategory, Category } = require("../db/models/index");
const Sequelize = require('sequelize');

class SubCategoryController {
  static async getSubCategories_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const subcategories = await SubCategory.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await SubCategory.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          subcategories: subcategories,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getSubCategories(req, res, next) {
    try {
      const subcategories = await SubCategory.findAll({
        include: [
          {
            model: Category,
            attributes: ['category_name'],
            as: 'category'
          },
        ],
      });
      res.status(200).json(subcategories);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createSubCategory(req, res, next) {
    const {
      subcategory_name,
      subcategory_description,
      category_id,
      is_active,
      updatedby
    } = req.body;

    try {
      await SubCategory.create({
        subcategory_name: subcategory_name,
        subcategory_description: subcategory_description,
        category_id: category_id,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Sub Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getSubCategory(req, res) {
    const scatId = req.params.id;
    try {
      const subcategory = await SubCategory.findOne({
        include: [
          {
            model: Category,
            attributes: ['category_name'],
            as: 'category'
          },
        ],
        where: {
          id: scatId,
        },
      });
      res.status(200).json(subcategory);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateSubCategory(req, res) {
    const scatId = req.params.id;
    const {
      subcategory_name,
      subcategory_description,
      category_id,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const subcategory = await SubCategory.findAll({
      where: {
        id: scatId,
      },
    });

    if (!subcategory[0]) return res.status(400).json({ msg: "Sub Category is not exist" });


    try {
      await SubCategory.update(
        {
          subcategory_name: subcategory_name,
          subcategory_description: subcategory_description,
          category_id: category_id,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: scatId,
          },
        }
      );

      res.json({ msg: "Update Sub Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteSubCategory(req, res) {
    const scatId = req.params.id;
    //cek apakah user sdh ada
    const subcategory = await SubCategory.findAll({
      where: {
        id: scatId,
      },
    });

    if (!subcategory[0]) return res.status(400).json({ msg: "Sub Category is not exist" });

    try {
      await SubCategory.destroy({
        where: {
          id: scatId,
        },
      });

      res.json({ msg: "Delete Sub Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = SubCategoryController;
