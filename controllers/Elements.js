const { Element,Hotel } = require("../db/models/index");
const Sequelize = require('sequelize');

class ElementController {
  static async getElements_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const elements = await Element.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Element.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          elements: elements,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getElements(req, res, next) {
    try {
      const categories = await Element.findAll({
        include:[
          {
            model: Hotel,
            as: 'hotel',
            attributes: ['hotel_code','title','subtitle']
          }
          
        ]
      });
      res.status(200).json(categories);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createElement(req, res, next) {
    const {
      element_code,
      element_name,
      adult_rate,
      child_rate,
      room_rate,
      hotel_id,
      is_tax_service,
      element_category_id,
      department_id,
      price_type_id,
      updatedby
    } = req.body;

    try {
      let price = 0;

      if (price_type_id === 1) {
        price = adult_rate + child_rate;
      } else {
        price = room_rate;
      }

      const dFinal = (is_tax_service === 1 ? (price - (0.1 * price + 0.11 * price)) : price);
      const dService = (0.1 * dFinal);
      const dTax = (0.11 * dFinal);

      await Element.create({
        element_code,
        element_name,
        price,
        adult_rate,
        child_rate,
        room_rate,
        is_tax_service,
        service: dService,
        tax: dTax,
        final_price: dFinal,
        element_category_id,
        hotel_id,
        department_id,
        price_type_id,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Element is successful" });
    } catch (error) {
      res.status(500).json({ message: 'Internal Server Error', error: error });
    }
}

  static async getElement(req, res) {
    const elementId = req.params.id;
    try {
      const element = await Element.findOne({
        where: {
          id: elementId,
        },
      });
      res.status(200).json(element);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getElementByHotelId(req, res) {
    const hotelId = req.params.id;
    try {
      const element = await Element.findAll({
        where: {
          hotel_id: hotelId,
        },
      });
      res.status(200).json(element);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateElement(req, res) {
    const elementId = req.params.id;
    const {
      element_code,
      element_name,
      adult_rate,
      child_rate,
      room_rate,
      is_tax_service,
      element_category_id,
      department_id,
      price_type_id,
      hotel_id,
      updatedby
    } = req.body;

    try {
      // Cek apakah elemen ada
      const element = await Element.findByPk(elementId);
      if (!element) {
        return res.status(400).json({ msg: "Element does not exist" });
      }

      let price = 0;

      if (price_type_id === 1) {
        price = adult_rate + child_rate;
      } else {
        price = room_rate;
      }

      const dFinal = (is_tax_service === 1 ? (price - (0.1 * price + 0.11 * price)) : price);
      const dService = (0.1 * dFinal);
      const dTax = (0.11 * dFinal);

      await element.update({
        element_code,
        element_name,
        price,
        adult_rate,
        child_rate,
        room_rate,
        is_tax_service,
        service: dService,
        tax: dTax,
        final_price: dFinal,
        element_category_id,
        hotel_id,
        department_id,
        price_type_id,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby
      });

      res.json({ msg: "Update Element is successful" });
    } catch (error) {
      res.status(500).json({ message: 'Internal Server Error', error: error });
    }
}


  static async deleteElement(req, res) {
    const elementId = req.params.id;
    //cek apakah user sdh ada
    const element = await Element.findAll({
      where: {
        id: elementId,
      },
    });

    if (!element[0]) return res.status(400).json({ msg: "Element is not exist" });

    try {
      await Element.destroy({
        where: {
          id: elementId,
        },
      });

      res.json({ msg: "Delete Element is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = ElementController;
