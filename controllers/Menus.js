const { Menu } = require("../db/models/index");
const Sequelize = require('sequelize');

class MenuController {
  static async getMenus_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const menus = await Menu.findAll({
        include: ["submenucategory"],
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Menu.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          menus: menus,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getMenus(req, res, next) {
    try {
      const menus = await Menu.findAll({
        include: ["submenucategory"],
      });
      res.status(200).json(menus);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createMenu(req, res, next) {
    const {
      menu_name,
      subcategory_menu_id,
      desciption,
      price,
      tax,
      service,
      final_price,
      is_active,
      updatedby
    } = req.body;

    try {
      await Menu.create({
        menu_name: menu_name,
        subcategory_menu_id: subcategory_menu_id,
        desciption: desciption,
        price: price,
        tax: tax,
        service: service,
        final_price: final_price,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Menu is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getMenu(req, res) {
    const menuId = req.params.id;
    try {
      const menu = await Menu.findOne({
        where: {
          id: menuId,
        },
      });
      res.status(200).json(menu);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateMenu(req, res) {
    const menuId = req.params.id;
    const {
      menu_name,
      subcategory_menu_id,
      desciption,
      price,
      tax,
      service,
      final_price,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const menu = await Menu.findAll({
      where: {
        id: menuId,
      },
    });

    if (!menu[0]) return res.status(400).json({ msg: "Menu is not exist" });


    try {
      await Menu.update(
        {
          menu_name: menu_name,
          subcategory_menu_id: subcategory_menu_id,
          desciption: desciption,
          price: price,
          tax: tax,
          service: service,
          final_price: final_price,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: menuId,
          },
        }
      );

      res.json({ msg: "Update Menu is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteMenu(req, res) {
    const menuId = req.params.id;
    //cek apakah user sdh ada
    const menu = await Menu.findAll({
      where: {
        id: menuId,
      },
    });

    if (!menu[0]) return res.status(400).json({ msg: "Menu is not exist" });

    try {
      await Menu.destroy({
        where: {
          id: menuId,
        },
      });

      res.json({ msg: "Delete Menu is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = MenuController;
