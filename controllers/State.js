const { State } = require("../db/models/index");
const Sequelize = require('sequelize');

class StateController {
    static async getState(req, res, next) {
        try {
          const data = await State.findAll({
            include:['country']
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getStateDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await State.findOne({
            include:['country'],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createState(req, res, next) {
        const {
          country_id,
          state_name,
          updatedby
        } = req.body;
    
        try {
          await State.create({
            country_id: country_id,
            state_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add State is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateState(req, res) {
        const id = req.params.id;
        const {
           country_id,
           state_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await State.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "State is not exist" });
    
    
        try {
          await State.update(
            {
              country_id:  country_id,
              state_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update State is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteState(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await State.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "State is not exist" });
    
        try {
          await State.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete State is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = StateController;