const { ModuleApp } = require("../db/models/index");
const Sequelize = require('sequelize');


class AppController {

    static async getApps(req, res, next) {  
        try {
            const apps = await ModuleApp.findAll({});
            res.status(200).json(apps);
        } catch (error) {  
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createRole(req, res, next) {
        const {
            role_name,
            role_description,
            is_active
        } = req.body;


        try {
            await Role.create({
                role_name: role_name,
                role_description: role_description,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                updatedAt: Sequelize.fn('NOW')
            });
            res.status(200).json({ msg: "Add Role is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getRole(req, res) {
        const roleId = req.params.id;
        try {
            const role = await Role.findOne({
                where: {
                    id: roleId,
                },
            });
            res.status(200).json(role);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateRole(req, res) {
        const roleId = req.params.id;
        const {
            role_name,
            role_description,
            is_active
        } = req.body;

        //cek apakah user sdh ada
        const role = await Role.findAll({
            where: {
                id: roleId,
            },
        });

        if (!role[0]) return res.status(400).json({ msg: "Role is not exist" });


        try {
            await Role.update(
                {
                    role_name: role_name,
                    role_description: role_description,
                    is_active: is_active,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: roleId,
                    },
                }
            );

            res.json({ msg: "Update Role is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteRole(req, res) {
        const roleId = req.params.id;
        //cek apakah user sdh ada
        const role = await Role.findAll({
            where: {
                id: roleId,
            },
        });

        if (!role[0]) return res.status(400).json({ msg: "Role is not exist" });

        try {
            await Role.destroy({
                where: {
                    id: roleId,
                },
            });

            res.json({ msg: "Delete Role is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

}

module.exports = AppController;
