const { Reservation, ReservationStatus, RoomType, Company, Package_Main, Segment, Room, Reservation_Detail,
    Guest, FoTrx, FoTrx_Detail, FbTrx, Hotel,RoomCalendar, Table } = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op, literal, fn, col } = require('sequelize');

class FrontOfficeController {
    //-----------------------
    // List Guest in house
    //-----------------------
    static async getFoInvoces(req, res, next) {
        try {
            const foInvoice = await Guest.findAll({
                include: [
                    {
                        model: Reservation,
                        where: {
                            reservation_status_id: {
                                [Sequelize.Op.eq]: 4 // Filter reservation_status_id equal to 4
                            }
                        },
                        as: 'reservation' // Adjust alias based on your association
                    },
                ],
   
            });
            res.status(200).json(foInvoice);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-----------------------
    // REPORT ROOM REVENUE BREAKDOWN
    //-----------------------

    static async getRevenueRoomBreakdown(req, res) {
        // const { datefrom, dateto } = req.query;
        try {
             // Mendapatkan data menggunakan Sequelize
            const data = await Reservation.findAll({
                attributes: [
                    'room.room_name',
                    'roomtype.roomtype_name',
                    'details.rate_price',
                    [Sequelize.literal('CONCAT(guest.firstname, " ", guest.lastname)'), 'name'],
                    'check_in',
                    'check_out'
                ],
                include: [
                    {
                        model: Reservation_Detail,
                        as:"details", 
                        required: true,
                        include: [
                            {
                                model: Package_Main,
                                as: "package", // Alias asosiasi untuk Package_Main
                                required: true
                            }
                        ]
                    },
                    {
                        model: Room,
                        as: "room",
                        required: true
                    },
                    {
                        model: RoomType,
                        as: "roomtype",
                        required: true
                    },
                    {
                        model: Guest,
                        as: "guest",
                        required: true
                    }
                ],
                // where: {
                //     check_in: {
                //         [Op.gte]: datefrom, // pastikan format ini sesuai dengan 'YYYY-MM-DD'
                //         [Op.lte]: dateto    // pastikan format ini sesuai dengan 'YYYY-MM-DD'
                //     }
                // },
                // group: ['room.room_name', 'roomtype.roomtype_name', 'details.rate_price', 'guest.firstname', 'guest.lastname', 
                // // 'Reservation.check_in', 'Reservation.check_out',
                // 'Reservation.id',]
            });
    
            res.status(200).json(data);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-----------------------
    // List Guest in house
    //-----------------------
    static async getGuestInHouse(req, res, next) {
        const { startDate, endDate, hotelId } = req.query;
        console.log(startDate, endDate);
        let whereClause = {};
        if (startDate == '') {
            whereClause = {
                // reservation_status_id: {
                //     [Sequelize.Op.eq]: 4 // Filter reservation_status_id equal to 4
                // },
                check_in: {
                    [Sequelize.Op.between]: [
                        new Date(new Date().toISOString().split('T')[0] + 'T00:00:00.000Z'),
                        new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0, 23, 59, 59, 999)
                    ] // Filter stay_date between today and end of month
                },
            };
        } else {
            whereClause = {
                check_in: {
                    [Sequelize.Op.between]: [startDate, endDate] // Filter stay_date less than or equal to '2024-02-15'
                },
                // reservation_status_id: {
                //     [Sequelize.Op.eq]: 4 // Filter reservation_status_id equal to 4
                // },
            };
        }
        try { 
            const guestInhouse = await Reservation.findAll({
                include: [
                    {
                        model: Guest,
                        as: 'guest' // Adjust alias based on your association
                    },
                    {
                        model: ReservationStatus,
                        as: 'status' // Adjust alias based on your association
                    },
                    {
                        model: Company,
                       
                        as: 'company' // Adjust alias based on your association
                    },
                    {
                        model: Segment,
                        as: 'segment' // Adjust alias based on your association
                    },
                    {
                        model: RoomCalendar,
                        as: 'roomcalendar',
                        include: ['room'
                        ]
                      },
                    {
                        model: Reservation_Detail,
                        as: 'details',
                        where:{
                            'hotel_id': hotelId
                        },
                        include: [
                            {
                                model: Hotel,
                                attributes: [
                                    'title',
                                    'subtitle'
                                ],
                                as: 'hotel' // Adjust alias based on your association
                            },
                            {
                                model: RoomType,
                                attributes: [
                                    'roomtype_name'
                                ],
                                as: 'roomtype' // Adjust alias based on your association
                            }
                        ],
                        attributes: [
                            'reservation_id',
                            'room_type_id',
                            'per_night_price',
                            'roomtype_name',
                            'hotel_id'
                          ],
                    }

                ],
                where: whereClause,
            });
            const formattedData = guestInhouse.map(reservation => {
                // Ambil data `RoomCalendar` dan kelompokkan berdasarkan `room_id`
                const roomCalendars = reservation.roomcalendar.map(roomCalendar => ({
                  id: roomCalendar.id,
                  reservation_id: roomCalendar.reservation_id,
                  room_id: roomCalendar.room_id, // Anggap bahwa `room` adalah objek dengan `id`
                  room: roomCalendar.room?.room_number, // Anda dapat menambahkan detail lebih lanjut tentang room
                }));
    
                // Hapus duplikasi berdasarkan `room_id`
                const uniqueRoomCalendars = Array.from(new Map(
                  roomCalendars.map(item => [item.room_id, item])
                ).values());
    
          
                return {
                  reservation_no : reservation.reservation_no,
                  status_name: reservation.status?.status_name,
                  check_in: reservation.check_in,
                  check_out: reservation.check_out,
                  guest_name: reservation.guest.saluation + ' ' + reservation.guest.firstname + ' ' + reservation.guest.lastname,
                  reservation_date: reservation.reservation_date,
                  cut_off_date: reservation.cut_off_date,
                  roomCalendars: uniqueRoomCalendars,
                  details:reservation.details
                };
              });
          
                
                res.status(200).json(formattedData);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-----------------------
    // List Reservation
    //-----------------------
    static async getListReservations(req, res, next) {
        try {
            const resvLists = await Reservation.findAll({
                include: [
                    {
                        model: Guest,
                        as: 'guest' // Adjust alias based on your association
                    },
                    {
                        model: ReservationStatus,
                        as: 'status' // Adjust alias based on your association
                    },
                    {
                        model: Room,
                        as: 'room' // Adjust alias based on your association
                    },
                    {
                        model: RoomType,
                        as: 'roomtype' // Adjust alias based on your association
                    },
                    {
                        model: Company,
                        as: 'company' // Adjust alias based on your association
                    }
                ],
                where: {
                    reservation_status_id: {
                        [Sequelize.Op.not]: 4 // Filter reservation_status_id not equal to 4
                    }
                },
            });
            res.status(200).json(resvLists);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-------------------------
    // Expcted Arrival
    //--------------------------
    static async getExpectedArrival(req, res, next) {
        const { startDate, endDate, hotelId } = req.query;

        let whereClause = {};

        if (startDate == '' || endDate == '') {
            whereClause = {
                check_in: {
                    [Sequelize.Op.between]: [
                        new Date(new Date().toISOString().split('T')[0] + 'T00:00:00.000Z'),
                        new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0, 23, 59, 59, 999)
                    ] // Filter stay_date between today and end of month
                },
                // reservation_status_id: {
                //     [Sequelize.Op.eq]: 5 // Filter reservation_status_id equal to 4
                // },
            };
        } else {
            whereClause = {
                check_in: {
                    [Sequelize.Op.between]: [startDate, endDate] // Filter stay_date less than or equal to '2024-02-15'
                },
                // reservation_status_id: {
                //     [Sequelize.Op.eq]: 5 // Filter reservation_status_id equal to 4
                // },
            };
        }
        try {
            const expArrival = await Reservation.findAll({
                
                include: [
                    {
                        model: Guest,
                        as: 'guest' // Adjust alias based on your association
                    },
                    {
                        model: ReservationStatus,
                        as: 'status' // Adjust alias based on your association
                    },
                    {
                        model: Company,
                       
                        as: 'company' // Adjust alias based on your association
                    },
                    {
                        model: Segment,
                        as: 'segment' // Adjust alias based on your association
                    },
                    {
                        model: RoomCalendar,
                        as: 'roomcalendar',
                        include: ['room'
                        ]
                      },
                    {
                        model: Reservation_Detail,
                        as: 'details',
                        where:{
                            'hotel_id': hotelId
                        },
                        include: [
                            {
                                model: Hotel,
                                attributes: [
                                    'title',
                                    'subtitle'
                                ],
                                as: 'hotel' // Adjust alias based on your association
                            },
                            {
                                model: RoomType,
                                attributes: [
                                    'roomtype_name'
                                ],
                                as: 'roomtype' // Adjust alias based on your association
                            }
                        ],
                        attributes: [
                            'reservation_id',
                            'room_type_id',
                            'per_night_price',
                            'roomtype_name',
                            'hotel_id'
                          ],
                    }

                ],
                where: whereClause,
            });
                // Format data
          const formattedData = expArrival.map(reservation => {
            // Ambil data `RoomCalendar` dan kelompokkan berdasarkan `room_id`
            const roomCalendars = reservation.roomcalendar.map(roomCalendar => ({
              id: roomCalendar.id,
              reservation_id: roomCalendar.reservation_id,
              room_id: roomCalendar.room_id, // Anggap bahwa `room` adalah objek dengan `id`
              room: roomCalendar.room?.room_number, // Anda dapat menambahkan detail lebih lanjut tentang room
            }));

            // Hapus duplikasi berdasarkan `room_id`
            const uniqueRoomCalendars = Array.from(new Map(
              roomCalendars.map(item => [item.room_id, item])
            ).values());

      
            return {
              reservation_no : reservation.reservation_no,
              status_name: reservation.status?.status_name,
              check_in: reservation.check_in,
              check_out: reservation.check_out,
              guest_name: reservation.guest.saluation + ' ' + reservation.guest.firstname + ' ' + reservation.guest.lastname,
              reservation_date: reservation.reservation_date,
              cut_off_date: reservation.cut_off_date,
              roomCalendars: uniqueRoomCalendars,
              details:reservation.details,
            };
          });
      
            
            res.status(200).json(formattedData);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error.message });
        }
    }

    //-------------------------
    // Expcted Departure
    //--------------------------
    static async getExpectedDeparture(req, res, next) {
        const { startDate, endDate, hotelId } = req.query;

        let whereClause = {};

        if (startDate == '' || endDate == '') {
            whereClause = {
                check_out: new Date().toISOString().split('T')[0] + 'T00:00:00.000Z',
                // reservation_status_id: {
                //     [Sequelize.Op.eq]: 6 // Filter reservation_status_id equal to 4
                // },
            };
        } else {
            whereClause = {
                check_out: {
                    [Sequelize.Op.between]: [startDate, endDate] // Filter stay_date less than or equal to '2024-02-15'
                },
                // reservation_status_id: {
                //     [Sequelize.Op.eq]: 6 // Filter reservation_status_id equal to 4
                // },
            };
        }
        try {
            const expDeparture = await Reservation.findAll({
               
                include: [
                    {
                        model: Guest,
                        as: 'guest' // Adjust alias based on your association
                    },
                    {
                        model: ReservationStatus,
                        as: 'status' // Adjust alias based on your association
                    },
                    {
                        model: Company,
                       
                        as: 'company' // Adjust alias based on your association
                    },
                    {
                        model: Segment,
                        as: 'segment' // Adjust alias based on your association
                    },
                    {
                        model: RoomCalendar,
                        as: 'roomcalendar',
                        include: ['room'
                        ]
                      },
                    {
                        model: Reservation_Detail,
                        as: 'details',
                        where:{
                            'hotel_id': hotelId
                        },
                        include: [
                            {
                                model: Hotel,
                                attributes: [
                                    'title',
                                    'subtitle'
                                ],
                                as: 'hotel' // Adjust alias based on your association
                            },
                            {
                                model: RoomType,
                                attributes: [
                                    'roomtype_name'
                                ],
                                as: 'roomtype' // Adjust alias based on your association
                            }
                        ],
                        attributes: [
                            'reservation_id',
                            'room_type_id',
                            'per_night_price',
                            'roomtype_name',
                            'hotel_id'
                          ],
                    }

                ],
                where: whereClause,
                
            });
                 // Format data
          const formattedData = expDeparture.map(reservation => {
            // Ambil data `RoomCalendar` dan kelompokkan berdasarkan `room_id`
            const roomCalendars = reservation.roomcalendar.map(roomCalendar => ({
              id: roomCalendar.id,
              reservation_id: roomCalendar.reservation_id,
              room_id: roomCalendar.room_id, // Anggap bahwa `room` adalah objek dengan `id`
              room: roomCalendar.room?.room_number, // Anda dapat menambahkan detail lebih lanjut tentang room
            }));

            // Hapus duplikasi berdasarkan `room_id`
            const uniqueRoomCalendars = Array.from(new Map(
              roomCalendars.map(item => [item.room_id, item])
            ).values());

      
            return {
              reservation_no : reservation.reservation_no,
              status_name: reservation.status?.status_name,
              check_in: reservation.check_in,
              check_out: reservation.check_out,
              guest_name: reservation.guest.saluation + ' ' + reservation.guest.firstname + ' ' + reservation.guest.lastname,
              reservation_date: reservation.reservation_date,
              cut_off_date: reservation.cut_off_date,
              roomCalendars: uniqueRoomCalendars,
              details:reservation.details
            };
          });
      
            
            res.status(200).json(formattedData);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-------------------------
    // Op Closing
    //--------------------------
    static async getOpClosing(req, res, next) {

        const { username } = req.query;

        try {
            const opClosing = await FoTrx_Detail.findAll({
                attributes: [
                    'id',
                    [Sequelize.col('foTrx.reservation_no'), 'reservation_no'],
                    'invoice_no',
                    [Sequelize.col('room.room_name'), 'room_name'], 
                    [Sequelize.col('roomtype.roomtype_name'), 'roomtype_name'],
                    [Sequelize.literal(`DATE_FORMAT(checkout, '%m/%d/%Y')`), 'trx_date'],
                    [Sequelize.col('package.package_name'), 'package_name'], 
                    'charge',
                    [Sequelize.col('foTrx.createdBy'), 'createdBy'],
                ],
                include: [
                    {
                        model: FoTrx,
                        attributes: [],
                        as: 'foTrx', // Adjust alias based on your association
                        where: {
                            createdBy: username // Filter createdBy equal to username
                        }
                    },
                    {
                        model: Room,
                        attributes: [],
                        as: 'room' // Adjust alias based on your association
                    },
                    {
                        model: RoomType,
                        attributes: [],
                        as: 'roomtype' // Adjust alias based on your association
                    },
                    {
                        model: Package_Main,
                        attributes: [],
                        as: 'package' // Adjust alias based on your association
                    },
                ],
                where: {
                    is_closing: 0
                },
            });
            res.status(200).json(opClosing);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-------------------------
    // Report Closing
    //--------------------------

    static async getReportClosing(req, res, next) {
        const { username } = req.query;
    
        try {
            const opClosing = await FoTrx_Detail.findAll({
                attributes: [
                    'id',
                    [Sequelize.col('foTrx.reservation_no'), 'reservation_no'],
                    'invoice_no',
                    [Sequelize.col('room.room_name'), 'room_name'],
                    [Sequelize.col('roomtype.roomtype_name'), 'roomtype_name'],
                    [Sequelize.literal(`DATE_FORMAT(checkout, '%m/%d/%Y')`), 'trx_date'],
                    [Sequelize.col('package.package_name'), 'package_name'],
                    'charge',
                    [Sequelize.col('foTrx.createdBy'), 'createdBy'],
                    'is_closing',
                    [Sequelize.literal(`DATE(closing_date)`), 'closing_date'] // Ambil hanya tanggal dari closing_date
                ],
                include: [
                    {
                        model: FoTrx,
                        as: 'foTrx',
                        where: {
                            createdBy: username
                        }
                    },
                    {
                        model: Room,
                        as: 'room'
                    },
                    {
                        model: RoomType,
                        as: 'roomtype'
                    },
                    {
                        model: Package_Main,
                        as: 'package'
                    },
                ],
                where: {
                    is_closing: 1
                },
            });
            res.status(200).json(opClosing);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }
    

    //-------------------------
    // Resto Closing
    //--------------------------
    static async getRestoClosing(req, res, next) {

        try {
            const restoClosing = await FbTrx.findAll({
                attributes: [
                    'id',
                    'fbtrx_no',
                    'fbtrx_date',
                    [Sequelize.col('guest.firstname'), 'firstname'],
                    [Sequelize.col('guest.lastname'), 'lastname'],
                    [Sequelize.col('room.room_name'), 'room_name'], 
                    [Sequelize.col('table.table_no'), 'table_no'], 
                    'createdBy'
                ],
                include: [
                    {
                        model: Guest,
                        attributes: [],
                        as: 'guest' // Adjust alias based on your association
                    },
                    {
                        model: Room,
                        attributes: [],
                        as: 'room' // Adjust alias based on your association
                    },
                    {
                        model: Table,
                        attributes: [],
                        as: 'table' // Adjust alias based on your association
                    },
                ],
                where: {
                    is_closing: 0
                },
            });
            res.status(200).json(restoClosing);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-------------------------
    // Resto Closing Night Audit
    //--------------------------
    static async getRestoClosingNa(req, res, next) {

        try {
            const naClosing = await FbTrx.findAll({
                attributes: [
                    'id',
                    'fbtrx_no',
                    'fbtrx_date',
                    [Sequelize.col('guest.firstname'), 'firstname'],
                    [Sequelize.col('guest.lastname'), 'lastname'],
                    [Sequelize.col('room.room_name'), 'room_name'], 
                    [Sequelize.col('table.table_no'), 'table_no'], 
                    'createdBy'
                ],
                include: [
                    {
                        model: Guest,
                        attributes: [],
                        as: 'guest' // Adjust alias based on your association
                    },
                    {
                        model: Room,
                        attributes: [],
                        as: 'room' // Adjust alias based on your association
                    },
                    {
                        model: Table,
                        attributes: [],
                        as: 'table' // Adjust alias based on your association
                    },
                ],
                where: {
                    is_closing: 1,
                    is_night_audit: 0
                },
            });
            res.status(200).json(naClosing);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //-----------------------
    // FoTrx
    //-----------------------
    static async getFoTrxes_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const fotrxes = await FoTrx.findAll({
                include: ["foTrxDetails"],
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await FoTrx.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    fotrxes: fotrxes,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getFoTrxes(req, res, next) {
        try {
            const fotrxes = await FoTrx.findAll({
                include: ["foTrxDetails"],
            });
            res.status(200).json(fotrxes);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createFoTrx(req, res, next) {
        const {
            reservation_id,
            reservation_no,
            updatedby
        } = req.body;

        try {
            await FoTrx.create({
                reservation_id: reservation_id,
                reservation_no: reservation_no,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Fo Trx is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getFoTrx(req, res) {
        const foTrxId = req.params.id;
        try {
            const fotrx = await FoTrx.findOne({
                include: ["foTrxDetails"],
                where: {
                    id: foTrxId,
                },
            });
            res.status(200).json(fotrx);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateFoTrx(req, res) {
        const foTrxId = req.params.id;
        const {
            reservation_id,
            reservation_no,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const fotrx = await FoTrx.findAll({
            where: {
                id: foTrxId,
            },
        });

        if (!fotrx[0]) return res.status(400).json({ msg: "Fo Trx is not exist" });


        try {
            await FoTrx.update(
                {
                    reservation_id: reservation_id,
                    reservation_no: reservation_no,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: foTrxId,
                    },
                }
            );

            res.json({ msg: "Update Fo Trx is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteFoTrx(req, res) {
        const foTrxId = req.params.id;
        //cek apakah user sdh ada
        const fotrx = await FoTrx.findAll({
            where: {
                id: foTrxId,
            },
        });

        if (!fotrx[0]) return res.status(400).json({ msg: "FO Trx is not exist" });

        try {
            await FoTrx.destroy({
                where: {
                    id: foTrxId,
                },
            });

            res.json({ msg: "Delete Fo Trx is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //----------------------------
    // FoTrx details
    //----------------------------
    static async getFoTrxDetails(req, res) {
        const resvNo = req.params.resvNo;
        try {
            const fotrxDetails = await FoTrx_Detail.findAll({
                where: {
                    reservation_no: resvNo,
                },
            });
            res.status(200).json(fotrxDetails);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createFoTrxDetail(req, res) {
        const resvNo = req.params.resvNo;
        const {
            invoice_no,
            room_type_id,
            room_id,
            checkout,
            package_id,
            description,
            charge,
            payment,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const fotrx = await FoTrx.findAll({
            where: {
                reservation_no: resvNo,
            },
        });

        if (!fotrx[0]) return res.status(400).json({ msg: "Fo Trx is not exist" });

        try {
            await FoTrx_Detail.create({
                reservation_no: resvNo,
                invoice_no: invoice_no,
                room_type_id: room_type_id,
                room_id: room_id,
                checkout: checkout,
                package_id: package_id,
                description: description,
                charge: charge,
                payment: payment,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add FO Trx Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getFoTrxDetail(req, res) {
        const detailId = req.params.id;

        try {
            const fotrxDetail = await FoTrx_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(fotrxDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateFoTrxDetail(req, res) {
        const detailId = req.params.id;
        const {
            package_id,
            description,
            charge,
            payment,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const fotrxDetail = await FoTrx_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!fotrxDetail[0]) return res.status(400).json({ msg: "Fo Trx Detail is not exist" });


        try {
            await FoTrx_Detail.update(
                {
                    package_id: package_id,
                    description: description,
                    charge: charge,
                    payment: payment,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Fo Trx Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async closingFoTrxDetail(req, res) {
        const detailId = req.params.id;

        const {
            is_closing,
            closing_date,
            updatedby
        } = req.body;

        const [closing_dateMonth, closing_dateDay, closing_dateYear] = closing_date.split("/");

        // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
        const formattedclosing_date = `${closing_dateYear}-${closing_dateMonth}-${closing_dateDay}`;
        //cek apakah user sdh ada
        const fotrxDetail = await FoTrx_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!fotrxDetail[0]) return res.status(400).json({ msg: "Fo Trx Detail is not exist" });


        try {
            await FoTrx_Detail.update(
                {
                    is_closing: is_closing,
                    closing_date: formattedclosing_date,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Closing Fo Trx Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async closingallFoTrxDetail(req, res) {
        const { is_closing, closing_date, updatedby } = req.body;
    
        const [closing_dateMonth, closing_dateDay, closing_dateYear] = closing_date.split("/");

        // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
        const formattedclosing_date = `${closing_dateYear}-${closing_dateMonth}-${closing_dateDay}`;
        
        try {
            // Mengubah nilai is_closing dan closing_date untuk semua data FoTrx_Detail yang memenuhi kriteria tertentu
            await FoTrx_Detail.update(
                {
                    is_closing: is_closing,
                    closing_date: formattedclosing_date,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        // Definisikan kriteria sesuai kebutuhan, misalnya, Anda ingin mengubah semua data yang memiliki is_closing = 0
                        is_closing: 0
                    },
                }
            );
    
            res.json({ msg: "Closing all Fo Trx Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
            res.status(500).json({ msg: "Internal Server Error", error:error });
        }
    }

    static async nightauditFoTrxDetail(req, res) {
        const detailId = req.params.id;

        const {
            is_night_audit,
            night_audit_date,
            updatedby
        } = req.body;
        const [night_audit_dateMonth, night_audit_dateDay, night_audit_dateYear] = night_audit_date.split("/");

        // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
        const formattednight_audit_date = `${night_audit_dateYear}-${night_audit_dateMonth}-${night_audit_dateDay}`;

        //cek apakah user sdh ada
        const fotrxDetail = await FoTrx_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!fotrxDetail[0]) return res.status(400).json({ msg: "Fo Trx Detail is not exist" });


        try {
            await FoTrx_Detail.update(
                {
                    is_night_audit: is_night_audit,
                    night_audit_date: formattednight_audit_date,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Night audit Fo Trx Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
            res.status(500).json({ message: "Internal Server Error" });
        }
    }
    static async nightauditallFoTrxDetail(req, res) {

        const {
            is_night_audit,
            night_audit_date,
            updatedby
        } = req.body;


        const [night_audit_dateMonth, night_audit_dateDay, night_audit_dateYear] = night_audit_date.split("/");

        // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
        const formattednight_audit_date = `${night_audit_dateYear}-${night_audit_dateMonth}-${night_audit_dateDay}`;

        
        try {
            await FoTrx_Detail.update(
                {
                    is_night_audit: is_night_audit,
                    night_audit_date: formattednight_audit_date,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        is_night_audit: 0,
                    },
                }
            );

            res.json({ msg: "Night audit Fo Trx Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
            res.status(500).json({ message: "Internal Server Error" });
        }
    }

    static async getNightAuditfotrx(req, res, next) {

        const { username } = req.query;

        try {
            const NightAudit = await FoTrx_Detail.findAll({
                attributes: [
                    'id',
                    [Sequelize.col('foTrx.reservation_no'), 'reservation_no'],
                    'invoice_no',
                    [Sequelize.col('room.room_name'), 'room_name'], 
                    [Sequelize.col('roomtype.roomtype_name'), 'roomtype_name'],
                    [Sequelize.literal(`DATE_FORMAT(checkout, '%m/%d/%Y')`), 'trx_date'],
                    [Sequelize.col('package.package_name'), 'package_name'], 
                    'charge',
                    [Sequelize.col('foTrx.createdBy'), 'createdBy'],
                ],
                include: [
                    {
                        model: FoTrx,
                        attributes: [],
                        as: 'foTrx', // Adjust alias based on your association
                        where: {
                            createdBy: username // Filter createdBy equal to username
                        }
                    },
                    {
                        model: Room,
                        attributes: [],
                        as: 'room' // Adjust alias based on your association
                    },
                    {
                        model: RoomType,
                        attributes: [],
                        as: 'roomtype' // Adjust alias based on your association
                    },
                    {
                        model: Package_Main,
                        attributes: [],
                        as: 'package' // Adjust alias based on your association
                    },
                ],
                where: {
                    is_night_audit: 0
                },
            });
            res.status(200).json(NightAudit);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteFoTrxDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const fotrxDetail = await FoTrx_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!fotrxDetail[0]) return res.status(400).json({ msg: "Fo Trx Detail is not exist" });

        try {
            await FoTrx_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Fo Trx Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = FrontOfficeController;

