const { Element_Category } = require("../db/models/index");
const Sequelize = require('sequelize');

class ElementCategoryController {
  static async getElementCategories_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const categories = await Element_Category.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Element_Category.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          categories: categories,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getElementCategories(req, res, next) {
    try {
      const categories = await Element_Category.findAll({
      });
      res.status(200).json(categories);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createElementCategory(req, res, next) {
    const {
      element_category,
      status,
      updatedby
    } = req.body;

    try {
      await Element_Category.create({
        element_category: element_category,
        status: status,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Element Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getElementCategory(req, res) {
    const catId = req.params.id;
    try {
      const category = await Element_Category.findOne({
        where: {
          id: catId,
        },
      });
      res.status(200).json(category);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateElementCategory(req, res) {
    const catId = req.params.id;
    const {
      element_category,
      status,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const category = await Element_Category.findAll({
      where: {
        id: catId,
      },
    });

    if (!category[0]) return res.status(400).json({ msg: "Element Category is not exist" });


    try {
      await Element_Category.update(
        {
          element_category: element_category,
          status: status,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: catId,
          },
        }
      );

      res.json({ msg: "Update Element Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteElementCategory(req, res) {
    const catId = req.params.id;
    //cek apakah user sdh ada
    const category = await Element_Category.findAll({
      where: {
        id: catId,
      },
    });

    if (!category[0]) return res.status(400).json({ msg: "Element Category is not exist" });

    try {
      await Element_Category.destroy({
        where: {
          id: catId,
        },
      });

      res.json({ msg: "Delete Element Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = ElementCategoryController;
