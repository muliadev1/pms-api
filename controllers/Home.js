
class HomeController {
    static async index(req, res) {
        try {
            const data = null;
            const meta = {
                code: 200,
                message: 'Property Management System API',
            };

            res.formatter.ok(data, meta);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    };

}

module.exports = HomeController


