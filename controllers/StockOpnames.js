const { StockOpname, Vendor,SubCategoryDepartement,SubCategory } = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op } = Sequelize;

class StockOpnameController {

    static async getStockOpnames(req, res, next) {
        try {
            const stocks = await StockOpname.findAll({});

            res.status(200).json(stocks);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getStockOpnamesReport(req, res, next) {
        const {startdate,enddate,iddepartement} = req.query;
        try {
            const stocks = await StockOpname.findAll({
                where:{
                    department_id:iddepartement,
                    stock_opname_date: {
                        [Op.between]: [startdate, enddate]
                    }
                }
            });

            res.status(200).json(stocks);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createStockOpname(req, res, next) {
        const {
            stock_opname_date,
            item_code,
            description,
            unit,
            price,
            stock,
            input_stock,
            diff_stock,
            department_id,
            updatedby
        } = req.body;

        try {
            await StockOpname.create({
                stock_opname_date: stock_opname_date,
                item_code: item_code,
                description: description,
                unit: unit,
                price: price,
                stock: stock,
                input_stock: input_stock,
                diff_stock: diff_stock,
                department_id: department_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Stock Opname is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createBulkStockOpname(req, res) {
        const {
            stock_opname_data, // Ini adalah array yang berisi data untuk banyak entri Package_Sub
            updatedby,
            stock_opname_date,
            department_id
        } = req.body;

        try {
            const stockOpnameDate = stock_opname_data.map(subData => ({
               stock_opname_date: stock_opname_date,
                item_code: subData.item_code,
                description: subData.description,
                unit: subData.unit,
                price: subData.price,
                stock: subData.stock,
                input_stock: subData.input_stock,
                diff_stock: subData.diff_stock,
                department_id: department_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));


            await StockOpname.bulkCreate(stockOpnameDate);
            res.status(200).json({ msg: "Add Stock opname is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getStockOpname(req, res) {
        const stockId = req.params.id;
        try {
            const stock = await StockOpname.findOne({
                where: {
                    id: stockId,
                },
            });
            res.status(200).json(stock);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateStockOpname(req, res) {
        const stockId = req.params.id;
        const {
            stock_opname_date,
            item_code,
            description,
            unit,
            price,
            stock,
            input_stock,
            diff_stock,
            department_id,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const stockDetail = await StockOpname.findAll({
            where: {
                id: stockId,
            },
        });

        if (!stockDetail[0]) return res.status(400).json({ msg: "Stock Opname is not exist" });


        try {
            await StockOpname.update(
                {
                    stock_opname_date: stock_opname_date,
                    item_code: item_code,
                    description: description,
                    unit: unit,
                    price: price,
                    stock: stock,
                    input_stock: input_stock,
                    diff_stock: diff_stock,
                    department_id: department_id,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: stockId,
                    },
                }
            );

            res.json({ msg: "Update Stock Opname is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteStockOpname(req, res) {
        const stockId = req.params.id;
        //cek apakah user sdh ada
        const stock = await StockOpname.findAll({
            where: {
                id: stockId,
            },
        });

        if (!stock[0]) return res.status(400).json({ msg: "Stock Opname is not exist" });

        try {
            await StockOpname.destroy({
                where: {
                    id: stockId,
                },
            });

            res.json({ msg: "Delete Stock Opname is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getSubscategory(req, res, next) {
        const { iddepartement } = req.query;
    
        try {
            const data = await SubCategoryDepartement.findAll({
                include: [{
                    model: SubCategory,
                    as: 'sub_category',
                }],
                where: { iddepartement: iddepartement },
                attributes: ['subcategory_id'], // Hapus penggunaan Sequelize.fn
                group: ['subcategory_id'] // Gunakan group untuk mengelompokkan berdasarkan subcategory_id
            });

            res.status(200).json(data);
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }

    static async getFormStockOpname(req, res, next) {
        const { iddepartement,subid } = req.query;
    
        try {
            const data = await SubCategoryDepartement.findAll({
                include: [{
                    model: SubCategory,
                    as: 'sub_category',
                },"items"],
                where: { 
                    iddepartement: iddepartement,
                    subcategory_id: subid
                }
            });

            res.status(200).json(data);
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
}

module.exports = StockOpnameController;

