const { Booking,BookingRoom,BookingAddon,Room } = require("../db/models/index");
const Sequelize = require('sequelize');

class BookingController {
    static async getBooking(req, res, next) {
        try {
          const data = await Booking.findAll({
            include:[
            {
                model: BookingRoom,
                as: 'booking_room',
                include: [
                    {
                        model:Room,
                        as: 'room',
                        include:["roomtype"]
                    }
                ]
            },
            {
                model: BookingAddon,
                as: 'booking_addon',
                include: ["addon"]
            }
            ,"guest","booking_status","payment_status","hotel","paymentbooking"],
            order: [['createdAt', 'DESC']], 
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getBookingDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Booking.findOne({
            include:[
                {
                    model: BookingRoom,
                    as: 'booking_room',
                    include: [
                        {
                            model:Room,
                            as: 'room',
                            include:["roomtype"]
                        }
                    ]
                },
                {
                    model: BookingAddon,
                    as: 'booking_addon',
                    include: ["addon"]
                }
                ,"guest","booking_status","hotel"],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createBooking(req, res, next) {
        const {
            guest_id,
            hotel_id,
            payment_status_id,
            booking_status_id,
            booking_date,
            checkin_date,
            checkout_date,
            num_adult,
            num_children,
            booking_amount,
            taxes_amount,
            discount,
            total_amount,
            bookingroom,
            bookingaddon,
            updatedby
        } = req.body;
    
        try {
          const book = await Booking.create({
            guest_id,
            hotel_id,
            payment_status_id,
            booking_status_id,
            booking_date,
            checkin_date,
            checkout_date,
            num_adult,
            num_children,
            booking_amount,
            taxes_amount,
            discount,
            total_amount,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });

          const bulkdata = bookingroom.map(subData => ({
                booking_id: book.id,
                room_id: subData.room_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));

            const addon = bookingaddon.map(subData => ({
                booking_id: book.id,
                addon_id: subData.addon_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));

            await BookingRoom.bulkCreate(bulkdata);
            await BookingAddon.bulkCreate(addon);
            
          res.status(200).json({ msg: "Add Booking is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateBooking(req, res) {
        const id = req.params.id;
        const {
            guest_id,
            hotel_id,
            payment_status_id,
            booking_status_id,
            booking_date,
            checkin_date,
            checkout_date,
            num_adult,
            num_children,
            booking_amount,
            taxes_amount,
            discount,
            total_amount,
            bookingroom,
            bookingaddon,
            updatedby
        } = req.body;
    
        try {
            // Cek apakah booking dengan ID yang diberikan ada
            const existingBooking = await Booking.findOne({
                where: {
                    id: id,
                },
            });
    
            if (!existingBooking) {
                return res.status(400).json({ msg: "Booking does not exist" });
            }
    
            // Update data booking
            await Booking.update(
                {
                    guest_id,
                    hotel_id,
                    payment_status_id,
                    booking_status_id,
                    booking_date,
                    checkin_date,
                    checkout_date,
                    num_adult,
                    num_children,
                    booking_amount,
                    taxes_amount,
                    discount,
                    total_amount,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: id,
                    },
                }
            );
    
            // Update atau tambahkan data booking room
            await BookingRoom.destroy({
                where: {
                    booking_id: id,
                },
            });
    
            const bulkBookingRoom = bookingroom.map(subData => ({
                booking_id: id,
                room_id: subData.room_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            await BookingRoom.bulkCreate(bulkBookingRoom);
    
            // Update atau tambahkan data booking addon
            await BookingAddon.destroy({
                where: {
                    booking_id: id,
                },
            });
    
            const bulkBookingAddon = bookingaddon.map(subData => ({
                booking_id: id,
                addon_id: subData.addon_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            await BookingAddon.bulkCreate(bulkBookingAddon);
    
            res.json({ msg: "Booking updated successfully" });
        } catch (error) {
            console.error("Error in updateBooking:", error);
            res.status(500).json({ message: 'Internal Server Error', error: error.message });
        }
    }
    
      static async deleteBooking(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Booking.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Booking is not exist" });
    
        try {
          await Booking.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Booking is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = BookingController;