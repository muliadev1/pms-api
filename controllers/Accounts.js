const { ChartOfAccount, COASubClass } = require("../db/models/index");
const Sequelize = require('sequelize');

class AccountController {
    //-----------------------
    // COA Subclass
    //-----------------------
    static async getSubClases(req, res, next) {
        try {
            const subclases = await COASubClass.findAll({
            });
            res.status(200).json(subclases);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createSubClass(req, res, next) {
        const {
            account_sub_no, // get from funtion 
            account_sub_name,
            is_active,
            updatedby
        } = req.body;

        try {
            await COASubClass.create({
                account_sub_no: account_sub_no,
                account_sub_name: account_sub_name,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Sub Class is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getSubClass(req, res) {
        const classId = req.params.id;
        try {
            const subclass = await COASubClass.findOne({
                where: {
                    id: classId,
                },
            });
            res.status(200).json(subclass);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateSubClass(req, res) {
        const classId = req.params.id;
        const {
            account_sub_no, // get from funtion 
            account_sub_name,
            is_active,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const subclass = await COASubClass.findAll({
            where: {
                id: classId,
            },
        });

        if (!subclass[0]) return res.status(400).json({ msg: "Sub Class is not exist" });


        try {
            await COASubClass.update(
                {
                    account_sub_no: account_sub_no,
                    account_sub_name: account_sub_name,
                    is_active: is_active,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: classId,
                    },
                }
            );

            res.json({ msg: "Update Sub Class is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteSubClass(req, res) {
        const classId = req.params.id;
        //cek apakah user sdh ada
        const subclass = await COASubClass.findAll({
            where: {
                id: classId,
            },
        });

        if (!subclass[0]) return res.status(400).json({ msg: "Sub Class is not exist" });

        try {
            await COASubClass.destroy({
                where: {
                    id: classId,
                },
            });

            res.json({ msg: "Delete Sub Class is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //----------------------------
    // Account
    //----------------------------
    static async getAccountBySubClass(req, res) {
        const accsubNo = req.params.accsubNo;
        try {
            const accounts = await ChartOfAccount.findAll({
                where: {
                    account_sub_no: accsubNo
                },
            });
            res.status(200).json(accounts);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async getAccounts(req, res) {
        try {
            const accounts = await ChartOfAccount.findAll({
            });
            res.status(200).json(accounts);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createAccount(req, res) {
        const {
            account_sub_no,
            account_no,
            account_name,
            balance,
            open_balance,
            is_kontra,
            is_active,
            updatedby
        } = req.body;

        try {
            await ChartOfAccount.create({
                account_sub_no: account_sub_no,
                account_no: account_no,
                account_name: account_name,
                balance: balance,
                open_balance: open_balance,
                is_kontra: is_kontra,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Char Of Account is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getAccount(req, res) {
        const detailId = req.params.id;

        try {
            const account = await ChartOfAccount.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(account);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateAccount(req, res) {
        const detailId = req.params.id;
        const {
            account_sub_no,
            account_no,
            account_name,
            balance,
            open_balance,
            is_kontra,
            is_active,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const account = await ChartOfAccount.findAll({
            where: {
                id: detailId,
            },
        });

        if (!account[0]) return res.status(400).json({ msg: "Chat Of Account is not exist" });


        try {
            await ChartOfAccount.update(
                {
                    account_sub_no: account_sub_no,
                    account_no: account_no,
                    account_name: account_name,
                    balance: balance,
                    open_balance: open_balance,
                    is_kontra: is_kontra,
                    is_active: is_active,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Chat Of Account is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteAccount(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const account = await ChartOfAccount.findAll({
            where: {
                id: detailId,
            },
        });

        if (!account[0]) return res.status(400).json({ msg: "Chat Of Account is not exist" });

        try {
            await ChartOfAccount.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Chat Of Account is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = AccountController;

