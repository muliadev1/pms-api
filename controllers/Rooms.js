const { Room, RoomType, RoomStatus } = require("../db/models/index");
const Sequelize = require('sequelize');
const room = require("../db/models/room");

class RoomController {
  static async getRooms_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const rooms = await Room.findAll({
        include: ["roomtype", "roomstatus"],
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Room.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          rooms: rooms,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getRooms(req, res, next) {
    try {
      const rooms = await Room.findAll({
        include: ["roomtype", "roomstatus"],
      });
      res.status(200).json(rooms);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createRoom(req, res, next) {
    const {
      room_name,
      description,
      roomtype_id,
      roomstatus_id,
      is_active,
      room_number,
      floor_id,
      updatedby
    } = req.body;

    try {
      await Room.create({
        room_name: room_name,
        description: description,
        roomtype_id: roomtype_id,
        roomstatus_id: roomstatus_id,
        is_active: is_active,
        floor_id,
        room_number,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Room is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getRoom(req, res) {
    const roomId = req.params.id;
    try {
      const room = await Room.findOne({
        include: ["roomtype", "roomstatus"],
        where: {
          id: roomId,
        },
      });
      res.status(200).json(room);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateRoom(req, res) {
    const roomId = req.params.id;
    const {
      room_name,
      description,
      roomtype_id,
      roomstatus_id,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const room = await Room.findAll({
      where: {
        id: roomId,
      },
    });

    if (!room[0]) return res.status(400).json({ msg: "Room is not exist" });


    try {
      await Room.update(
        {
          room_name: room_name,
          description: description,
          roomtype_id: roomtype_id,
          roomstatus_id: roomstatus_id,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: roomId,
          },
        }
      );

      res.json({ msg: "Update Room is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteRoom(req, res) {
    const roomId = req.params.id;
    //cek apakah user sdh ada
    const room = await Room.findAll({
      where: {
        id: roomId,
      },
    });

    if (!room[0]) return res.status(400).json({ msg: "Room is not exist" });

    try {
      await Room.destroy({
        where: {
          id: roomId,
        },
      });

      res.json({ msg: "Delete Room is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = RoomController;
