const { PaymentBooking,Booking } = require("../db/models/index");
const Sequelize = require('sequelize');

class PaymentBookingController {
    static async getPaymentBooking(req, res, next) {
        try {
          const data = await PaymentBooking.findAll({
            include:['paymentmethod','booking'],
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getPaymentBookingDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await PaymentBooking.findOne({
            include:['paymentmethod','booking'],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createPaymentBooking(req, res, next) {
        const {
            booking_id,
            payment_method_id,
            invoice_no,
            added_date,
            payment_due,
            amount,
            payment_status_id,
            updatedby
        } = req.body;
    
        try {
          await PaymentBooking.create({
            booking_id,
            payment_method_id,
            invoice_no,
            added_date,
            payment_due,
            amount,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });

          const existingBooking = await Booking.findOne({
              where: {
                  id: booking_id,
              },
          });

        if (existingBooking) {
            // Update data booking
            await Booking.update(
              {
                  payment_status_id,
              },
              {
                  where: {
                      id: booking_id,
                  },
              }
          );
        }


          res.status(200).json({ msg: "Add PaymentBooking is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updatePaymentBooking(req, res) {
        const id = req.params.id;
        const {
            booking_id,
            payment_method_id,
            invoice_no,
            added_date,
            payment_due,
            amount,
            updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await PaymentBooking.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "PaymentBooking is not exist" });
    
    
        try {
          await PaymentBooking.update(
            {
                booking_id,
                payment_method_id,
                invoice_no,
                added_date,
                payment_due,
                amount,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update PaymentBooking is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deletePaymentBooking(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await PaymentBooking.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "PaymentBooking is not exist" });
    
        try {
          await PaymentBooking.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete PaymentBooking is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = PaymentBookingController;