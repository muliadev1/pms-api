const { Asset, Asset_Detail } = require("../db/models/index");
const Sequelize = require('sequelize');


class AssetController {
  static async getAssets_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const assets = await Asset.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Asset.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          assets: assets,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getAssets(req, res, next) {
    try {
      const assets = await Asset.findAll({
        include: ["assetdetail"],
      });
      res.status(200).json(assets);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createAsset(req, res, next) {
    const {
      asset_code,
      item_code,
      baik,
      kurangbaik,
      used,
      lose,
      out,
      jumlah,
      baikHK,
      kurangbaikHK,
      usedHK,
      loseHK,
      outHK,
      jumlahHK,
      baikFO,
      kurangbaikFO,
      usedFO,
      loseFO,
      outFO,
      jumlahFO,
      baikFB,
      kurangbaikFB,
      usedFB,
      loseFB,
      outFB,
      jumlahFB,
      baikBO,
      kurangbaikBO,
      usedBO,
      loseBO,
      outBO,
      jumlahBO,
      baikEng,
      kurangbaikEng,
      usedEng,
      loseEng,
      outEng,
      jumlahEng,
      baikPG,
      kurangbaikPG,
      usedPG,
      losePG,
      outPG,
      jumlahPG,
      baikSec,
      kurangbaikSec,
      usedSec,
      loseSec,
      outSec,
      jumlahSec,
      baikBar,
      kurangbaikBar,
      usedBar,
      loseBar,
      outBar,
      jumlahBar,
      is_active,
      updatedby
    } = req.body;

    try {
      await Asset.create({
        asset_code: asset_code,
        item_code: item_code,
        baik: baik,
        kurangbaik: kurangbaik,
        used: used,
        lose: lose,
        out: out,
        jumlah: jumlah,
        baikHK: baikHK,
        kurangbaikHK: kurangbaikHK,
        usedHK: usedHK,
        loseHK: loseHK,
        outHK: outHK,
        jumlahHK: jumlahHK,
        baikFO: baikFO,
        kurangbaikFO: kurangbaikFO,
        usedFO: usedFO,
        loseFO: loseFO,
        outFO: outFO,
        jumlahFO: jumlahFO,
        baikFB: baikFB,
        kurangbaikFB: kurangbaikFB,
        usedFB: usedFB,
        loseFB: loseFB,
        outFB: outFB,
        jumlahFB: jumlahFB,
        baikBO: baikBO,
        kurangbaikBO: kurangbaikBO,
        usedBO: usedBO,
        loseBO: loseBO,
        outBO: outBO,
        jumlahBO: jumlahBO,
        baikEng: baikEng,
        kurangbaikEng: kurangbaikEng,
        usedEng: usedEng,
        loseEng: loseEng,
        outEng: outEng,
        jumlahEng: jumlahEng,
        baikPG: baikPG,
        kurangbaikPG: kurangbaikPG,
        usedPG: usedPG,
        losePG: losePG,
        outPG: outPG,
        jumlahPG: jumlahPG,
        baikSec: baikSec,
        kurangbaikSec: kurangbaikSec,
        usedSec: usedSec,
        loseSec: loseSec,
        outSec: outSec,
        jumlahSec: jumlahSec,
        baikBar: baikBar,
        kurangbaikBar: kurangbaikBar,
        usedBar: usedBar,
        loseBar: loseBar,
        outBar: outBar,
        jumlahBar: jumlahBar,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Asset is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getAsset(req, res) {
    const assetId = req.params.id;
    try {
      const asset = await Asset.findOne({
        include: ["assetdetail"],
        where: {
          id: assetId,
        },
      });
      res.status(200).json(asset);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateAsset(req, res) {
    const assetId = req.params.id;
    const {
      asset_code,
      item_code,
      baik,
      kurangbaik,
      used,
      lose,
      out,
      jumlah,
      baikHK,
      kurangbaikHK,
      usedHK,
      loseHK,
      outHK,
      jumlahHK,
      baikFO,
      kurangbaikFO,
      usedFO,
      loseFO,
      outFO,
      jumlahFO,
      baikFB,
      kurangbaikFB,
      usedFB,
      loseFB,
      outFB,
      jumlahFB,
      baikBO,
      kurangbaikBO,
      usedBO,
      loseBO,
      outBO,
      jumlahBO,
      baikEng,
      kurangbaikEng,
      usedEng,
      loseEng,
      outEng,
      jumlahEng,
      baikPG,
      kurangbaikPG,
      usedPG,
      losePG,
      outPG,
      jumlahPG,
      baikSec,
      kurangbaikSec,
      usedSec,
      loseSec,
      outSec,
      jumlahSec,
      baikBar,
      kurangbaikBar,
      usedBar,
      loseBar,
      outBar,
      jumlahBar,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const asset = await Asset.findAll({
      where: {
        id: assetId,
      },
    });

    if (!asset[0]) return res.status(400).json({ msg: "Asset is not exist" });


    try {
      await Asset.update(
        {
          asset_code: asset_code,
          item_code: item_code,
          baik: baik,
          kurangbaik: kurangbaik,
          used: used,
          lose: lose,
          out: out,
          jumlah: jumlah,
          baikHK: baikHK,
          kurangbaikHK: kurangbaikHK,
          usedHK: usedHK,
          loseHK: loseHK,
          outHK: outHK,
          jumlahHK: jumlahHK,
          baikFO: baikFO,
          kurangbaikFO: kurangbaikFO,
          usedFO: usedFO,
          loseFO: loseFO,
          outFO: outFO,
          jumlahFO: jumlahFO,
          baikFB: baikFB,
          kurangbaikFB: kurangbaikFB,
          usedFB: usedFB,
          loseFB: loseFB,
          outFB: outFB,
          jumlahFB: jumlahFB,
          baikBO: baikBO,
          kurangbaikBO: kurangbaikBO,
          usedBO: usedBO,
          loseBO: loseBO,
          outBO: outBO,
          jumlahBO: jumlahBO,
          baikEng: baikEng,
          kurangbaikEng: kurangbaikEng,
          usedEng: usedEng,
          loseEng: loseEng,
          outEng: outEng,
          jumlahEng: jumlahEng,
          baikPG: baikPG,
          kurangbaikPG: kurangbaikPG,
          usedPG: usedPG,
          losePG: losePG,
          outPG: outPG,
          jumlahPG: jumlahPG,
          baikSec: baikSec,
          kurangbaikSec: kurangbaikSec,
          usedSec: usedSec,
          loseSec: loseSec,
          outSec: outSec,
          jumlahSec: jumlahSec,
          baikBar: baikBar,
          kurangbaikBar: kurangbaikBar,
          usedBar: usedBar,
          loseBar: loseBar,
          outBar: outBar,
          jumlahBar: jumlahBar,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: assetId,
          },
        }
      );

      res.json({ msg: "Update Asset is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteAsset(req, res) {
    const assetId = req.params.id;
    //cek apakah user sdh ada
    const asset = await Asset.findAll({
      where: {
        id: assetId,
      },
    });

    if (!asset[0]) return res.status(400).json({ msg: "Asset is not exist" });

    try {
      await Asset.destroy({
        where: {
          id: assetId,
        },
      });

      res.json({ msg: "Delete Asset is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getAssetDetails(req, res) {
    const assetId = req.params.assetId;
    try {
      const assDetails = await Asset_Detail.findAll({
        where: {
          asset_id: assetId,
        },
      });
      res.status(200).json(assDetails);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createAssetDetail(req, res) {
    const assetId = req.params.assetId;
    const {
      qty,
      kondisi_awal,
      kondisi_akhir,
      department_id,
      department_name,
      operator_id,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const asset = await Asset.findAll({
      where: {
        id: assetId,
      },
    });

    if (!asset[0]) return res.status(400).json({ msg: "Asset code is not exist" });

    try {
      await Asset_Detail.create({
        asset_id: assetId,
        qty: qty,
        kondisi_awal: kondisi_awal,
        kondisi_akhir: kondisi_akhir,
        department_id: department_id,
        department_name: department_name,
        operator_id: operator_id,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Asset Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getAssetDetail(req, res) {
    const detailId = req.params.id;

    try {
      const assDetail = await Asset_Detail.findOne({
        where: {
          id: detailId,
        },
      });
      res.status(200).json(assDetail);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateAssetDetail(req, res) {
    const detailId = req.params.id;
    const {
      qty,
      kondisi_awal,
      kondisi_akhir,
      department_id,
      department_name,
      operator_id,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const assDetail = await Asset_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!assDetail[0]) return res.status(400).json({ msg: "Asset Detail is not exist" });


    try {
      await Asset_Detail.update(
        {
          qty: qty,
          kondisi_awal: kondisi_awal,
          kondisi_akhir: kondisi_akhir,
          department_id: department_id,
          department_name: department_name,
          operator_id: operator_id,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: detailId,
          },
        }
      );

      res.json({ msg: "Update Asset Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteAssetDetail(req, res) {
    const detailId = req.params.id;

    //cek apakah user sdh ada
    const assDetail = await Asset_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!assDetail[0]) return res.status(400).json({ msg: "Asset Detail is not exist" });

    try {
      await Asset_Detail.destroy({
        where: {
          id: detailId,
        },
      });

      res.json({ msg: "Delete Asset Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = AssetController;
