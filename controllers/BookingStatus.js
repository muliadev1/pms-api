const { BookingStatus } = require("../db/models/index");
const Sequelize = require('sequelize');

class BookingStatusController {
    static async getBookingStatus(req, res, next) {
        try {
          const data = await BookingStatus.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getBookingStatusDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await BookingStatus.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createBookingStatus(req, res, next) {
        const {
          booking_status_name,
          updatedby
        } = req.body;
    
        try {
          await BookingStatus.create({
            booking_status_name: booking_status_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Booking Status is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateBookingStatus(req, res) {
        const id = req.params.id;
        const {
           booking_status_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await BookingStatus.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Booking Status is not exist" });
    
    
        try {
          await BookingStatus.update(
            {
             booking_status_name:  booking_status_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Booking Status is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteBookingStatus(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const paytype = await BookingStatus.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Booking Status is not exist" });
    
        try {
          await BookingStatus.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Booking Status is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = BookingStatusController;