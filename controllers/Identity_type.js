const { Identity_type } = require("../db/models/index");
const Sequelize = require('sequelize');

class Identity_typeController {
    static async getIdentity_type(req, res, next) {
        try {
          const data = await Identity_type.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getIdentity_typeDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Identity_type.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createIdentity_type(req, res, next) {
        const {
          
          identity_type_name,
          updatedby
        } = req.body;
    
        try {
          await Identity_type.create({
             
            identity_type_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Identity type is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateIdentity_type(req, res) {
        const id = req.params.id;
        const {
           
           identity_type_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Identity_type.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Identity type is not exist" });
    
    
        try {
          await Identity_type.update(
            {
                
              identity_type_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Identity type is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteIdentity_type(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Identity_type.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Identity type is not exist" });
    
        try {
          await Identity_type.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Identity type is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = Identity_typeController;