const { MenuCategory } = require("../db/models/index");
const Sequelize = require('sequelize');

class MenuCategoryController {
  static async getMenuCategories_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const categories = await MenuCategory.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await MenuCategory.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          categories: categories,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getMenuCategories(req, res, next) {
    try {
      const categories = await MenuCategory.findAll({
      });
      res.status(200).json(categories);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createMenuCategory(req, res, next) {
    const {
      category_name,
      description,
      updatedby
    } = req.body;

    try {
      await MenuCategory.create({
        category_name: category_name,
        description: description,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Menu Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getMenuCategory(req, res) {
    const catId = req.params.id;
    try {
      const category = await MenuCategory.findOne({
        where: {
          id: catId,
        },
      });
      res.status(200).json(category);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateMenuCategory(req, res) {
    const catId = req.params.id;
    const {
      category_name,
      description,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const category = await MenuCategory.findAll({
      where: {
        id: catId,
      },
    });

    if (!category[0]) return res.status(400).json({ msg: "Menu Category is not exist" });


    try {
      await MenuCategory.update(
        {
          category_name: category_name,
          description: description,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: catId,
          },
        }
      );

      res.json({ msg: "Update Menu Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteMenuCategory(req, res) {
    const catId = req.params.id;
    //cek apakah user sdh ada
    const category = await MenuCategory.findAll({
      where: {
        id: catId,
      },
    });

    if (!category[0]) return res.status(400).json({ msg: "Menu Category is not exist" });

    try {
      await MenuCategory.destroy({
        where: {
          id: catId,
        },
      });

      res.json({ msg: "Delete Menu Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = MenuCategoryController;
