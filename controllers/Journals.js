const { Journal, Journal_Detail } = require("../db/models/index");
const Sequelize = require('sequelize');

class JournalController {
    //-----------------------
    // Journal Master
    //-----------------------
    static async getJournalMasters_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const masters = await Journal.findAll({
                include: ["journaldetail"],
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await Journal.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    masters: masters,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getJournalMasters(req, res, next) {
        try {
            const masters = await Journal.findAll({
                include: ["journaldetail"],
            });
            res.status(200).json(masters);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createJournalMaster(req, res, next) {
        const {
            journal_no, // get from funtion 
            bukti_transaksi,
            keterangan,
            tanggal_input,
            tanggal_sistem,
            operator_id,
            is_active,
            updatedby
        } = req.body;

        try {
            await Journal.create({
                journal_no: journal_no,
                bukti_transaksi: bukti_transaksi,
                keterangan: keterangan,
                tanggal_input: tanggal_input,
                tanggal_sistem: tanggal_sistem,
                operator_id: operator_id,
                is_active: is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Journal Master is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getJournalMaster(req, res) {
        const masterId = req.params.id;
        try {
            const master = await Journal.findOne({
                include: ["journaldetail"],
                where: {
                    id: masterId,
                },
            });
            res.status(200).json(master);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateJournalMaster(req, res) {
        const masterId = req.params.id;
        const {
            bukti_transaksi,
            keterangan,
            tanggal_input,
            tanggal_sistem,
            operator_id,
            is_active,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const master = await Journal.findAll({
            where: {
                id: masterId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "Journal Master is not exist" });


        try {
            await Journal.update(
                {
                    bukti_transaksi: bukti_transaksi,
                    keterangan: keterangan,
                    tanggal_input: tanggal_input,
                    tanggal_sistem: tanggal_sistem,
                    operator_id: operator_id,
                    is_active: is_active,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: masterId,
                    },
                }
            );

            res.json({ msg: "Update Journal Master is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteJournalMaster(req, res) {
        const masterId = req.params.id;
        //cek apakah user sdh ada
        const master = await Journal.findAll({
            where: {
                id: masterId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "Journal Master is not exist" });

        try {
            await Journal.destroy({
                where: {
                    id: masterId,
                },
            });

            res.json({ msg: "Delete Journal Master is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //----------------------------
    // Journal details
    //----------------------------
    static async getJournalDetails(req, res) {
        const journalId = req.params.journalId;
        try {
            const journalDetails = await Journal_Detail.findAll({
                where: {
                    journal_id: journalId,
                },
            });
            res.status(200).json(journalDetails);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createJournalDetail(req, res) {
        const journalId = req.params.journalId;
        const {
            account_no,
            debet,
            kredit,
            tanggal_input,
            tanggal_sistem,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const master = await Journal.findAll({
            where: {
                id: journalId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "Journal Master is not exist" });

        try {
            await Journal_Detail.create({
                journal_id: journalId,
                account_no: account_no,
                debet: debet,
                kredit: kredit,
                tanggal_input: tanggal_input,
                tanggal_sistem: tanggal_sistem,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Journal Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getJournalDetail(req, res) {
        const detailId = req.params.id;

        try {
            const journalDetail = await Journal_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(journalDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateJournalDetail(req, res) {
        const detailId = req.params.id;
        const {
            account_no,
            debet,
            kredit,
            tanggal_input,
            tanggal_sistem,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const journalDetail = await Journal_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!journalDetail[0]) return res.status(400).json({ msg: "Journal Detail is not exist" });


        try {
            await Journal_Detail.update(
                {
                    account_no: account_no,
                    debet: debet,
                    kredit: kredit,
                    tanggal_input: tanggal_input,
                    tanggal_sistem: tanggal_sistem,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Journal Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteJournalDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const journalDetail = await Journal_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!journalDetail[0]) return res.status(400).json({ msg: "Journal Detail is not exist" });

        try {
            await Journal_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Journal Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = JournalController;

