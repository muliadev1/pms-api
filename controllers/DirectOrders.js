const { PO_Main, Purchase_Order_Detail, Vendor } = require("../db/models/index");
const Sequelize = require('sequelize');

class DirectOrderController {
    static async getOrders_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const orders = await PO_Main.findAll({
                include: ["department"],
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await PO_Main.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    orders: orders,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getOrders(req, res, next) {
        try {
            const orders = await PO_Main.findAll({
                include: ["vendor"],
            });
            res.status(200).json(orders);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async createOrder(req, res, next) {
        const total_amount = 0;
        const {
            purchase_order_code,
            vendor_id,
            order_date,
            expected_date,
            total,
            method_id,
            status_id,
            is_checked,
            verify_by,
            notes,
            updatedby
        } = req.body;

        try {
            await PO_Main.create({
                purchase_order_code: purchase_order_code,
                vendor_id: vendor_id,
                order_date: order_date,
                expected_date: expected_date,
                total: total,
                method_id: method_id,
                status_id: status_id,
                is_checked: is_checked,
                verify_by: verify_by,
                notes: notes,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Purchase Order is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
            res.status(500).json({ msg: "Internal Server Error", error:error });
        }
    }

    static async getOrder(req, res) {
        const orderId = req.params.id;
        try {
            const order = await PO_Main.findOne({
                include: ["vendor"],
                where: {
                    id: orderId,
                },
            });
            res.status(200).json(order);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateOrder(req, res) {
        const orderId = req.params.id;
        const {
            purchase_order_code,
            vendor_id,
            order_date,
            expected_date,
            total,
            method_id,
            status_id,
            is_checked,
            verify_by,
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const order = await PO_Main.findAll({
            where: {
                id: orderId,
            },
        });

        if (!order[0]) return res.status(400).json({ msg: "Purchase Order is not exist" });


        try {
            await PO_Main.update(
                {
                    purchase_order_code: purchase_order_code,
                    vendor_id: vendor_id,
                    order_date: order_date,
                    expected_date: expected_date,
                    total: total,
                    method_id: method_id,
                    status_id: status_id,
                    is_checked: is_checked,
                    verify_by: verify_by,
                    notes: notes,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: orderId,
                    },
                }
            );

            res.json({ msg: "Update Purchase Order is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteOrder(req, res) {
        const orderId = req.params.id;
        //cek apakah user sdh ada
        const order = await PO_Main.findAll({
            where: {
                id: orderId,
            },
        });

        if (!order[0]) return res.status(400).json({ msg: "Purchase Order is not exist" });

        try {
            await PO_Main.destroy({
                where: {
                    id: orderId,
                },
            });

            res.json({ msg: "Delete Purchase Order is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getOrderDetails(req, res) {
        const orderCode = req.params.ordcode;
        try {
            const ordDetails = await Purchase_Order_Detail.findAll({
                where: {
                    purchase_order_code: orderCode,
                },
            });
            res.status(200).json(ordDetails);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createOrderDetail(req, res) {
        const orderCode = req.params.ordcode;
        const {
            item_code,
            description,
            unit,
            price,
            qty_order,
            qty_stock,
            subtotal, 
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const order = await PO_Main.findAll({
            where: {
                purchase_order_code: orderCode,
            },
        });

        if (!order[0]) return res.status(400).json({ msg: "Purchase Order is not exist" });

        try {
            await Purchase_Order_Detail.create({
                purchase_order_code: orderCode,
                item_code: item_code,
                description: description,
                unit: unit,
                price: price,
                qty_order: qty_order,
                qty_stock: qty_stock,
                subtotal: subtotal,
                notes: notes,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Purchase Order Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getOrderDetail(req, res) {
        const detailId = req.params.id;

        try {
            const ordDetail = await Purchase_Order_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(ordDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateOrderDetail(req, res) {
        const detailId = req.params.id;
        const {
            item_code,
            description,
            unit,
            price,
            qty_order,
            qty_stock,
            subtotal, 
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const reqDetail = await Purchase_Order_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!reqDetail[0]) return res.status(400).json({ msg: "Purchase Requesition Detail is not exist" });


        try {
            await Purchase_Order_Detail.update(
                {
                    item_code: item_code,
                    description: description,
                    unit: unit,
                    price: price,
                    qty_order: qty_order,
                    qty_stock: qty_stock,
                    subtotal: subtotal, 
                    notes: notes,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Purchase Order Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteOrderDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const ordDetail = await Purchase_Order_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!ordDetail[0]) return res.status(400).json({ msg: "Purchase Order Detail is not exist" });

        try {
            await Purchase_Order_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Purchase Order Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = DirectOrderController;

