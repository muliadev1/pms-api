const { Floor } = require("../db/models/index");
const Sequelize = require('sequelize');

class FloorController {
    static async getFloor(req, res, next) {
        try {
          const data = await Floor.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getFloorDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Floor.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createFloor(req, res, next) {
        const {
          floor_number,
          updatedby
        } = req.body;
    
        try {
          await Floor.create({
            floor_number: floor_number,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Floor is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateFloor(req, res) {
        const id = req.params.id;
        const {
           floor_number,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await Floor.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Floor is not exist" });
    
    
        try {
          await Floor.update(
            {
              floor_number:  floor_number,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Floor is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteFloor(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const paytype = await Floor.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Floor is not exist" });
    
        try {
          await Floor.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Floor is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = FloorController;