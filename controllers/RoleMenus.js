const { RolesMenu, Module, ModuleMenu } = require('../db/models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
   
   
class RoleMenuController {
    static async getRoleMenus(req, res) {   
        const roleId = req.params.roleId;
        try {
            const roles = await RolesMenu.findAll({
                include: [
                    {
                        model: Module,
                        attributes: ['module_name'],
                        as: 'module'
                    },   
                    {
                        model: ModuleMenu,
                        attributes: ['menu_name', 'url'],
                        as: 'modulemenu'
                    },
                ],
                where: {
                    role_id: roleId
                },
                order: [
                    ['module_id', 'ASC'], // Urutan berdasarkan menu_id secara menaik
                    ['order_no', 'ASC'] // Kemudian urut berdasarkan order_no secara menaik
                ],
                raw: true
            });

            // Mengonversi hasil menjadi format yang diinginkan
            const formattedRoles = roles.map(role => ({
                id: role.id,
                role_id: role.role_id,
                module_id: role.module_id,
                module_name: role['module.module_name'], // Mengakses kolom module_name dari model Module
                menu_id: role.menu_id,
                menu_name: role['modulemenu.menu_name'], // Mengakses kolom menu_name dari model Menu
                menu_url: role['modulemenu.url'],
                order_no: role.order_no,
                is_insert: role.is_insert,
                is_update: role.is_update,
                is_delete: role.is_delete,
                is_read: role.is_read,
            }));

            res.status(200).json(formattedRoles);
        } catch (error) {
            console.log(error);
        }
    };

    static async getAppMenus(req, res) {
        const roleId = req.params.roleId;
        try {
            const roles = await RolesMenu.findAll({
                include: [
                    {
                        model: Module,
                        attributes: ['module_name'],
                        as: 'module'
                    },
                    {
                        model: ModuleMenu,
                        attributes: ['menu_name', 'url'],
                        as: 'modulemenu'
                    },
                ],
                where: {
                    role_id: roleId
                },
                order: [
                    ['module_id', 'ASC'], // Urutan berdasarkan menu_id secara menaik  
                    ['order_no', 'ASC'] // Kemudian urut berdasarkan order_no secara menaik  
                ],
                raw: true
            });   

            // Mengonversi hasil menjadi format yang diinginkan
            const formattedRoles = roles.map(role => ({
                id: role.id,   
                role_id: role.role_id,
                module_id: role.module_id,
                module_name: role['module.module_name'], // Mengakses kolom module_name dari model Module
                menu_id: role.menu_id,
                menu_name: role['modulemenu.menu_name'], // Mengakses kolom menu_name dari model Menu
                menu_url: role['modulemenu.url'],
                order_no: role.order_no,  
                is_insert: role.is_insert,
                is_update: role.is_update,
                is_delete: role.is_delete,
                is_read: role.is_read,   
            }));

            // Objek untuk menyimpan struktur data baru
            const newData = {};

            newData['Dashboard'] = {   
                label: 'Dashboard',
                icon: 'pi pi-align-justify',
                items: [
                    {
                        label: 'Home',
                        icon: 'pi pi-fw pi-home',
                        to: '/'
                    }
                ],
            }

            // Iterasi melalui data
            formattedRoles.forEach(item => {
                // Mengekstrak nilai module_name dari setiap item
                const moduleName = item.module_name;

                // Jika moduleName belum ada dalam newData, inisialisasi dengan struktur baru
                if (!newData[moduleName]) {
                    newData[moduleName] = {
                        label: moduleName,
                        icon: 'pi pi-align-justify',
                        items: []
                    };
                }

                // Menambahkan item ke dalam items untuk module_name yang bersangkutan
                newData[moduleName].items.push({
                    label: item.menu_name,
                    icon: 'pi pi-fw pi-circle-off',
                    to: item.menu_url
                });
            });

            // Mengubah objek menjadi array
            const result = Object.values(newData);


            res.status(200).json(result);
        } catch (error) {
            console.log(error);
        }
    };


    static async createRoleMenu(req, res) {
        const { role_id, module_id, menu_id, order_no, is_insert, is_update, is_delete, is_read, updatedBy } = req.body;

        try {   
            await RolesMenu.create({
                role_id: role_id,
                module_id: module_id,
                menu_id: menu_id,
                order_no: order_no,
                is_insert: is_insert,
                is_update: is_update,
                is_delete: is_delete,
                is_read: is_read,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedBy,
                updatedAt: Sequelize.fn('NOW'),  
                updatedBy: updatedBy,
            });
            res.json({ status: 200, msg: "Add new Role menus successfully" });
        } catch (error) {
            console.log(error);   
        }
    };

    static async detailRoleMenu(req, res) {   

        const detailId = req.params.id;
        try {
            const role = await RolesMenu.findOne({
                where: {
                    id: detailId
                }
            });
            res.status(200).json(role);
        } catch (error) {
            console.log(error);   
        }
    };

    static async updateRoleMenu(req, res) {  
        const detailId = req.params.id;
        const { role_id, module_id, menu_id, order_no, is_insert, is_update, is_delete, is_read, updatedBy } = req.body;
        //cek apakah Role sdh ada  
        const role = await RolesMenu.findAll({  
            where: {
                id: detailId
            }
        });

        if (!role[0]) return res.status(400).json({ status: 400, msg: "Role menus is not exist" });

        try {

            await RolesMenu.update(
                {
                    role_id: role_id,
                    module_id: module_id,
                    menu_id: menu_id,
                    order_no: order_no,
                    is_insert: is_insert,
                    is_update: is_update,
                    is_delete: is_delete,
                    is_read: is_read,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedBy
                },
                {
                    where: {
                        id: detailId
                    }
                });

            res.json({ status: 200, msg: "Update Role menus is successfully" });
        } catch (error) {
            console.log(error);
        }
    };

    static async deleteRoleMenu(req, res) {
        const detailId = req.params.id;
        //cek apakah Role sdh ada
        const roles = await RolesMenu.findAll({
            where: {  
                id: detailId
            }
        });

        if (!roles[0]) return res.status(400).json({ status: 400, msg: "Role menus is not exist" });

        try {

            await RolesMenu.destroy(
                {
                    where: {
                        id: detailId
                    }
                });

            res.json({ status: 200, msg: "Delete Role menus is successfully" });
        } catch (error) {
            console.log(error);
        }
    };
}


module.exports = RoleMenuController







