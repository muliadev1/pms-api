const { Settings } = require("../db/models/index");
const Sequelize = require('sequelize');

class SettingsController {
    static async getSettings(req, res, next) {
        try {
          const data = await Settings.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getSettingsDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Settings.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createSettings(req, res, next) {
        const {
          hotel_id,
          logo,
          footer_text,
          min_booking,
          checkin_time,
          checkout_time,
          default_currency,
          default_language,
          default_date_format,
          advance_payment_percentage,
          taxes,
          is_maintenance,
          sender_email,
          sender_name,
          is_use_smtp,
          smtp_security,
          is_smtp_authentication,
          smtp_host,
          smtp_port,
          email_username,
          email_password,
          updatedby
        } = req.body;
    
        try {
          await Settings.create({
            hotel_id,
            logo,
            footer_text,
            min_booking,
            checkin_time,
            checkout_time,
            default_currency,
            default_language,
            default_date_format,
            advance_payment_percentage,
            taxes,
            is_maintenance,
            sender_email,
            sender_name,
            is_use_smtp,
            smtp_security,
            is_smtp_authentication,
            smtp_host,
            smtp_port,
            email_username,
            email_password,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Settings is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateSettings(req, res) {
        const id = req.params.id;
        const {
          hotel_id,
          logo,
          footer_text,
          min_booking,
          checkin_time,
          checkout_time,
          default_currency,
          default_language,
          default_date_format,
          advance_payment_percentage,
          taxes,
          is_maintenance,
          sender_email,
          sender_name,
          is_use_smtp,
          smtp_security,
          is_smtp_authentication,
          smtp_host,
          smtp_port,
          email_username,
          email_password,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Settings.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Settings is not exist" });
    
    
        try {
          await Settings.update(
            {
              hotel_id,
              logo,
              footer_text,
              min_booking,
              checkin_time,
              checkout_time,
              default_currency,
              default_language,
              default_date_format,
              advance_payment_percentage,
              taxes,
              is_maintenance,
              sender_email,
              sender_name,
              is_use_smtp,
              smtp_security,
              is_smtp_authentication,
              smtp_host,
              smtp_port,
              email_username,
              email_password,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Settings is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteSettings(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Settings.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Settings is not exist" });
    
        try {
          await Settings.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Settings is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = SettingsController;