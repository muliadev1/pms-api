const { Source } = require("../db/models/index");
const Sequelize = require('sequelize');

class SourceController {
  static async getSources_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const sources = await Source.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Source.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          sources: sources,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getSources(req, res, next) {
    try {
      const sources = await Source.findAll({
      });
      res.status(200).json(sources);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createSource(req, res, next) {
    const {
      source_name,
      is_active,
      updatedby
    } = req.body;

    try {
      await Source.create({
        source_name: source_name,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Source is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getSource(req, res) {
    const sourceId = req.params.id;
    try {
      const source = await Source.findOne({
        where: {
          id: sourceId,
        },
      });
      res.status(200).json(source);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateSource(req, res) {
    const sourceId = req.params.id;
    const {
      source_name,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const source = await Source.findAll({
      where: {
        id: sourceId,
      },
    });

    if (!source[0]) return res.status(400).json({ msg: "Source is not exist" });


    try {
      await Source.update(
        {
          source_name: source_name,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: sourceId,
          },
        }
      );

      res.json({ msg: "Update Source is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteSource(req, res) {
    const sourceId = req.params.id;
    //cek apakah user sdh ada
    const source = await Source.findAll({
      where: {
        id: sourceId,
      },
    });

    if (!source[0]) return res.status(400).json({ msg: "Source is not exist" });

    try {
      await Source.destroy({
        where: {
          id: sourceId,
        },
      });

      res.json({ msg: "Delete Source is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = SourceController;
