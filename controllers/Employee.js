const { Employee } = require("../db/models/index");
const Sequelize = require('sequelize');

class EmployeeController {
    static async getEmployee(req, res, next) {
        try {
          const data = await Employee.findAll({
           include:[
           'departement',
           'designation',
           'country',
            'state',
           'city',
           'idendiy_type'
           ]
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getEmployeeHk(req, res, next) {
        try {
          const data = await Employee.findAll({
           include:[
           'departement',
           'designation',
           'country',
            'state',
           'city',
           'idendiy_type'
           ],
           where:{
            departement_id:2
           }
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getEmployeeDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Employee.findOne({
            include:[
                'departement',
                'designation',
                'country',
                 'state',
                'city',
                'idendiy_type'
                ],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createEmployee(req, res, next) {
        const {
            title,
            gender,
            first_name,
            last_name,
            username,
            password,
            email,
            date_of_birth,
            phone,
            departement_id,
            designation_id,
            country_id,
            state_id,
            city_id,
            address,
            identity_type_id,
            identity_number,
            identity_image,
            date_of_joining,
            salary,
            updatedby
        } = req.body;
    
        try {
          await Employee.create({
            title,
            gender,
            first_name,
            last_name,
            username,
            password,
            email,
            date_of_birth,
            phone,
            departement_id,
            designation_id,
            country_id,
            state_id,
            city_id,
            address,
            identity_type_id,
            identity_number,
            identity_image,
            date_of_joining,
            salary,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Employee is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateEmployee(req, res) {
        const id = req.params.id;
        const {
            title,
            gender,
            first_name,
            last_name,
            username,
            password,
            email,
            date_of_birth,
            phone,
            departement_id,
            designation_id,
            country_id,
            state_id,
            city_id,
            address,
            identity_type_id,
            identity_number,
            identity_image,
            date_of_joining,
            salary,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Employee.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Employee is not exist" });
    
    
        try {
          await Employee.update(
            {
                title,
                gender,
                first_name,
                last_name,
                username,
                password,
                email,
                date_of_birth,
                phone,
                departement_id,
                designation_id,
                country_id,
                state_id,
                city_id,
                address,
                identity_type_id,
                identity_number,
                identity_image,
                date_of_joining,
                salary,

              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Employee is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteEmployee(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Employee.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Employee is not exist" });
    
        try {
          await Employee.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Employee is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = EmployeeController;