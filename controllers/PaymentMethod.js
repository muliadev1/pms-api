const { PaymentMethod } = require("../db/models/index");
const Sequelize = require('sequelize');

class PaymentMethodController {
    static async getPaymentsMethods(req, res, next) {
        try {
          const paymentsmethods = await PaymentMethod.findAll({});
          res.status(200).json(paymentsmethods);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getPaymentsMethodDetail(req, res, next) {
        const id = req.params.id;
        try {
          const payment = await PaymentMethod.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(payment);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createPaymentMethods(req, res, next) {
        const {
          payment_method_name,
          updatedby
        } = req.body;
    
        try {
          await PaymentMethod.create({
            payment_method_name: payment_method_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Payment methods is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updatePaymentMethods(req, res) {
        const id = req.params.id;
        const {
           payment_method_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await PaymentMethod.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Payment methods is not exist" });
    
    
        try {
          await PaymentMethod.update(
            {
             payment_method_name:  payment_method_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Payment methods is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deletePaymentMethods(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const paytype = await PaymentMethod.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Payment methods is not exist" });
    
        try {
          await PaymentMethod.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Payment methods is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = PaymentMethodController;
