const { Country,State } = require("../db/models/index");
const Sequelize = require('sequelize');

class CountryController {
    static async getCountry(req, res, next) {
        try {
          const data = await Country.findAll({
            include:[{
              model:State,
              as:'state',
              include:['city']
            }]
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getCountryDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Country.findOne({
            include:[{
              model:State,
              as:'state',
              include:['city']
            }],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createCountry(req, res, next) {
        const {
          country_name,
          updatedby
        } = req.body;
    
        try {
          await Country.create({
            country_name: country_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Country is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateCountry(req, res) {
        const id = req.params.id;
        const {
           country_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Country.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Country is not exist" });
    
    
        try {
          await Country.update(
            {
              country_name:  country_name,

              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Country is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteCountry(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Country.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Country is not exist" });
    
        try {
          await Country.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Country is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = CountryController;