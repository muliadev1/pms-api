const { User_Module} = require("../db/models/index");
const Sequelize = require('sequelize');
const user_module = require("../db/models/user_module");

class UserModuleController {
    static async getUserModules_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const usermodules = await User_Module.findAll({
                include: ["user", "module"],
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await user_module.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    usermodules: usermodules,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getUserModules(req, res, next) {
        try {
            const usermodules = await User_Module.findAll({
                include: ["user", "module"],
            });
            res.status(200).json(usermodules);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createUserModule(req, res, next) {
        const {
            user_id,
            module_id,
            is_create,
            is_read,
            is_update,
            is_delete,
            updatedby
        } = req.body;

        try {
            await User_Module.create({
                user_id: user_id,
                module_id: module_id,
                is_create: is_create,
                is_read: is_read,
                is_update: is_update,
                is_delete: is_delete,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add User Module is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getUserModule(req, res) {
        const Id = req.params.id;
        try {
            const usermodule = await User_Module.findOne({
                include: ["user", "module"],
                where: {
                    id: Id,
                },
            });
            res.status(200).json(usermodule);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateUserModule(req, res) {
        const Id = req.params.id;
        const {
            user_id,
            module_id,
            is_create,
            is_read,
            is_update,
            is_delete,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const usermodule = await User_Module.findAll({
            where: {
                id: Id,
            },
        });

        if (!usermodule[0]) return res.status(400).json({ msg: "User Module is not exist" });


        try {
            await User_Module.update(
                {
                    user_id: user_id,
                    module_id: module_id,
                    is_create: is_create,
                    is_read: is_read,
                    is_update: is_update,
                    is_delete: is_delete,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: Id,
                    },
                }
            );

            res.json({ msg: "Update User Module is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteUserModule(req, res) {
        const Id = req.params.id;
        //cek apakah user sdh ada
        const usermodule = await User_Module.findAll({
            where: {
                id: Id,
            },
        });

        if (!usermodule[0]) return res.status(400).json({ msg: "User Module is not exist" });

        try {
            await User_Module.destroy({
                where: {
                    id: Id,
                },
            });

            res.json({ msg: "Delete User Module is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

}

module.exports = UserModuleController;

