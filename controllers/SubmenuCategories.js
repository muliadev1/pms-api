const { SubmenuCategory, MenuCategory } = require("../db/models/index");
const Sequelize = require('sequelize');

class SubmenuCategoryController {
  static async getSubmenuCategories_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const subcategories = await SubmenuCategory.findAll({
        include: ["menucategory"],
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await SubmenuCategory.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          subcategories: subcategories,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getSubmenuCategories(req, res, next) {
    try {
      const subcategories = await SubmenuCategory.findAll({
        include: ["menucategory"],
      });
      res.status(200).json(subcategories);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createSubmenuCategory(req, res, next) {
    const {
      submenu_category_name,
      description,
      menu_category_id,
      updatedby
    } = req.body;

    try {
      await SubmenuCategory.create({
        submenu_category_name: submenu_category_name,
        description: description,
        menu_category_id: menu_category_id,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Menu Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getSubmenuCategory(req, res) {
    const scatId = req.params.id;
    try {
      const subcategory = await SubmenuCategory.findOne({
        where: {
          id: scatId,
        },
      });
      res.status(200).json(subcategory);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateSubmenuCategory(req, res) {
    const scatId = req.params.id;
    const {
      submenu_category_name,
      description,
      menu_category_id,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const subcategory = await SubmenuCategory.findAll({
      where: {
        id: scatId,
      },
    });

    if (!subcategory[0]) return res.status(400).json({ msg: "Submenu Category is not exist" });


    try {
      await SubmenuCategory.update(
        {
          submenu_category_name: submenu_category_name,
          description: description,
          menu_category_id: menu_category_id,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: scatId,
          },
        }
      );

      res.json({ msg: "Update Submenu Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteSubmenuCategory(req, res) {
    const scatId = req.params.id;
    //cek apakah user sdh ada
    const subcategory = await SubmenuCategory.findAll({
      where: {
        id: scatId,
      },
    });

    if (!subcategory[0]) return res.status(400).json({ msg: "Submenu Category is not exist" });

    try {
      await SubmenuCategory.destroy({
        where: {
          id: scatId,
        },
      });

      res.json({ msg: "Delete Submenu Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = SubmenuCategoryController;
