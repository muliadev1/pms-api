const { City } = require("../db/models/index");
const Sequelize = require('sequelize');

class CityController {
    static async getCity(req, res, next) {
        try {
          const data = await City.findAll({
            include:['state']
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getCityDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await City.findOne({
            include:['state'],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createCity(req, res, next) {
        const {
          state_id,
          city_name,
          updatedby
        } = req.body;
    
        try {
          await City.create({
            state_id: state_id,
            city_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add City is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateCity(req, res) {
        const id = req.params.id;
        const {
           state_id,
           city_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await City.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "City is not exist" });
    
    
        try {
          await City.update(
            {
              state_id:  state_id,
              city_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update City is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteCity(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await City.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "City is not exist" });
    
        try {
          await City.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete City is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = CityController;