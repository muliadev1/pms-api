const { BadStock, BadStock_Detail } = require("../db/models/index");
const Sequelize = require('sequelize');

class BadStockController {
    //-----------------------
    // BadStock Master
    //-----------------------
    static async getBadStocks_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const badstocks = await BadStock.findAll({
                include: ["badstockdetail"],
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await Journal.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    badstocks: badstocks,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getBadStocks(req, res, next) {
        try {
            const badstocks = await BadStock.findAll({
                include: ["badstockdetail"],
            });
            res.status(200).json(badstocks);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getBadStocksCode(req, res, next) {
        const {badcode} = req.query;
        try {
            const badstocks = await BadStock.findAll({ 
                include: ["badstockdetail"],
                where:{
                    badstock_code:badcode
                }
            });
            res.status(200).json(badstocks);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createBadStock(req, res, next) {
        const {
            badstock_code, // get from funtion 
            date_add,
            department_id,
            department_name,
            operator_id,
            description,
            updatedby
        } = req.body;

        try {
            await BadStock.create({
                badstock_code: badstock_code,
                date_add: date_add,
                department_id: department_id,
                department_name: department_name,
                operator_id: operator_id,
                description: description,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Badstock Master is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getBadStock(req, res) {
        const bstockId = req.params.id;
        try {
            const master = await BadStock.findOne({
                include: ["badstockdetail"],
                where: {
                    id: bstockId,
                },
            });
            res.status(200).json(master);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateBadStock(req, res) {
        const bstockId = req.params.id;
        const {
            badstock_code, // get from funtion 
            date_add,
            department_id,
            department_name,
            operator_id,
            description,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const master = await BadStock.findAll({
            where: {
                id: bstockId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "BadStock Master is not exist" });


        try {
            await BadStock.update(
                {
                    badstock_code: badstock_code,
                    date_add: date_add,
                    department_id: department_id,
                    department_name: department_name,
                    operator_id: operator_id,
                    description: description,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: bstockId,
                    },
                }
            );

            res.json({ msg: "Update BadStock Master is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteBadStock(req, res) {
        const bstockId = req.params.id;
        //cek apakah user sdh ada
        const master = await BadStock.findAll({
            where: {
                id: bstockId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "BadStock Master is not exist" });

        try {
            await BadStock.destroy({
                where: {
                    id: bstockId,
                },
            });

            res.json({ msg: "Delete BadStock Master is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //----------------------------
    // BadStock details
    //----------------------------
    static async getBadStockDetails(req, res) {
        const bstockId = req.params.bstockId;
        try {
            const bstockDetails = await BadStock_Detail.findAll({
                where: {
                    badstock_id: bstockId,
                },
            });
            res.status(200).json(bstockDetails);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createBadStockDetail(req, res) {
        const bstockId = req.params.bstockId;
        const {
            item_code,
            description,
            unit,
            price,
            qty,
            curr_stock,
            department_id,
            department_name,
            operator_id,
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const badstock = await BadStock.findAll({
            where: {
                id: bstockId,
            },
        });

        if (!badstock[0]) return res.status(400).json({ msg: "BadStock Master is not exist" });

        try {
            await BadStock_Detail.create({
                badstock_id: bstockId,
                item_code: item_code,
                description: description,
                unit: unit,
                price: price,
                qty: qty,
                curr_stock: curr_stock,
                department_id: department_id,
                department_name: department_name,
                operator_id: operator_id,
                notes: notes,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add BadStock Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getBadStockDetail(req, res) {
        const detailId = req.params.id;

        try {
            const bstockDetail = await BadStock_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(bstockDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateBadStockDetail(req, res) {
        const detailId = req.params.id;
        const {
            item_code,
            description,
            unit,
            price,
            qty,
            curr_stock,
            department_id,
            department_name,
            operator_id,
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const bstockDetail = await BadStock_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!bstockDetail[0]) return res.status(400).json({ msg: "BadStock Detail is not exist" });


        try {
            await BadStock_Detail.update(
                {
                    item_code: item_code,
                    description: description,
                    unit: unit,
                    price: price,
                    qty: qty,
                    curr_stock: curr_stock,
                    department_id: department_id,
                    department_name: department_name,
                    operator_id: operator_id,
                    notes: notes,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update BadStock Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteBadStockDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const bstockDetail = await BadStock_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!bstockDetail[0]) return res.status(400).json({ msg: "BadStock Detail is not exist" });

        try {
            await BadStock_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete BadStock Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = BadStockController;

