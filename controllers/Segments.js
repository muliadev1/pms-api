const { Segment, Segment_Group } = require("../db/models/index");
const Sequelize = require('sequelize');

class SegmentController {

  // --------------------------------------
  // SEGMENT GROUPS
  // --------------------------------------
  static async getGroups(req, res, next) {
    try {
      const groups = await Segment_Group.findAll({
      });
      res.status(200).json(groups);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createGroup(req, res, next) {
    const {
      segment_group_name,
      is_active,
      updatedby
    } = req.body;

    try {
      await Segment_Group.create({
        segment_group_name: segment_group_name,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Segment Group is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getGroup(req, res) {
    const groupId = req.params.id;
    try {
      const group = await Segment_Group.findOne({
        where: {
          id: groupId,
        },
      });
      res.status(200).json(group);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateGroup(req, res) {
    const groupId = req.params.id;
    const {
      segment_group_name,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const group = await Segment_Group.findAll({
      where: {
        id: groupId,
      },
    });

    if (!group[0]) return res.status(400).json({ msg: "Segment Group is not exist" });


    try {
      await Segment_Group.update(
        {
          segment_group_name: segment_group_name,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: groupId,
          },
        }
      );

      res.json({ msg: "Update Segment Group is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteGroup(req, res) {
    const groupId = req.params.id;
    //cek apakah user sdh ada
    const group = await Segment_Group.findAll({
      where: {
        id: groupId,
      },
    });

    if (!group[0]) return res.status(400).json({ msg: "Segment Group is not exist" });

    try {
      await Segment_Group.destroy({
        where: {
          id: groupId,
        },
      });

      res.json({ msg: "Delete Segment Group is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  // --------------------------------------
  // SEGMENT
  // --------------------------------------
  static async getSegments(req, res, next) {
    try {
      const segments = await Segment.findAll({
        include: [
          {
            model: Segment_Group,
            attributes: ['segment_group_name'],
            as: 'segmentgroup'
          },
        ],
      });
      res.status(200).json(segments);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createSegment(req, res, next) {
    const {
      segment_name,
      segment_group_id,
      is_active,
      updatedby
    } = req.body;

    try {
      await Segment.create({
        segment_name: segment_name,
        segment_group_id: segment_group_id,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Segment is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getSegment(req, res) {
    const segId = req.params.id;
    try {
      const segment = await Segment.findOne({
        include: [
          {
            model: Segment_Group,
            attributes: ['segment_group_name'],
            as: 'segmentgroup'
          },
        ],
        where: {
          id: segId,
        },
      });
      res.status(200).json(segment);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateSegment(req, res) {
    const segId = req.params.id;
    const {
      segment_name,
      segment_group_id,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const segment = await Segment.findAll({
      where: {
        id: segId,
      },
    });

    if (!segment[0]) return res.status(400).json({ msg: "Segment is not exist" });


    try {
      await Segment.update(
        {
          segment_name: segment_name,
          segment_group_id: segment_group_id,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: segId,
          },
        }
      );

      res.json({ msg: "Update Segment is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteSegment(req, res) {
    const segId = req.params.id;
    //cek apakah user sdh ada
    const segment = await Segment.findAll({
      where: {
        id: segId,
      },
    });

    if (!segment[0]) return res.status(400).json({ msg: "Segment is not exist" });

    try {
      await Segment.destroy({
        where: {
          id: segId,
        },
      });

      res.json({ msg: "Delete Segment is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = SegmentController;
