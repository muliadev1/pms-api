const { PaymentType } = require("../db/models/index");
const Sequelize = require('sequelize');

class PaymentTypeController {
  static async getPaymentTypes_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const paytypes = await PaymentType.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await PaymentType.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          paytypes: paytypes,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPaymentTypes(req, res, next) {
    try {
      const paytypes = await PaymentType.findAll({
      });
      res.status(200).json(paytypes);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createPaymentType(req, res, next) {
    const {
      payment_type_name,
      is_active,
      updatedby
    } = req.body;

    try {
      await PaymentType.create({
        payment_type_name: payment_type_name,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Payment Type is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPaymentType(req, res) {
    const typeId = req.params.id;
    try {
      const paytype = await PaymentType.findOne({
        where: {
          id: typeId,
        },
      });
      res.status(200).json(paytype);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updatePaymentType(req, res) {
    const typeId = req.params.id;
    const {
      payment_type_name,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const paytype = await PaymentType.findAll({
      where: {
        id: typeId,
      },
    });

    if (!paytype[0]) return res.status(400).json({ msg: "Payment Type is not exist" });


    try {
      await PaymentType.update(
        {
          payment_type_name: payment_type_name,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: typeId,
          },
        }
      );

      res.json({ msg: "Update Payment Type is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deletePaymentType(req, res) {
    const typeId = req.params.id;
    //cek apakah user sdh ada
    const paytype = await PaymentType.findAll({
      where: {
        id: typeId,
      },
    });

    if (!paytype[0]) return res.status(400).json({ msg: "Payment Type is not exist" });

    try {
      await PaymentType.destroy({
        where: {
          id: typeId,
        },
      });

      res.json({ msg: "Delete Payment Type is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = PaymentTypeController;
