const { ReservationStatus } = require("../db/models/index");
const Sequelize = require('sequelize');

class RsvStatusController {
  static async getReservationStatuses_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const rsvstatus = await ReservationStatus.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await ReservationStatus.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          rsvstatus: rsvstatus,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getReservationStatuses(req, res, next) {
    try {
      const rsvstatus = await ReservationStatus.findAll({
      });
      res.status(200).json(rsvstatus);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createReservationStatus(req, res, next) {
    const {
      status_name,
      description,
      updatedby
    } = req.body;

    try {
      await ReservationStatus.create({
        status_name: status_name,
        description: description,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Reservation Status is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getReservationStatus(req, res) {
    const statusId = req.params.id;
    try {
      const rsvstatus = await ReservationStatus.findOne({
        where: {
          id: statusId,
        },
      });
      res.status(200).json(rsvstatus);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateReservationStatus(req, res) {
    const statusId = req.params.id;
    const {
      status_name,
      description,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const rsvstatus = await ReservationStatus.findAll({
      where: {
        id: statusId,
      },
    });

    if (!rsvstatus[0]) return res.status(400).json({ msg: "Reservation Status is not exist" });


    try {
      await ReservationStatus.update(
        {
          status_name: status_name,
          description: description,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: statusId,
          },
        }
      );

      res.json({ msg: "Update Reservation Status is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteReservationStatus(req, res) {
    const statusId = req.params.id;
    //cek apakah user sdh ada
    const rsvstatus = await ReservationStatus.findAll({
      where: {
        id: statusId,
      },
    });

    if (!rsvstatus[0]) return res.status(400).json({ msg: "Reservation Status is not exist" });

    try {
      await ReservationStatus.destroy({
        where: {
          id: statusId,
        },
      });

      res.json({ msg: "Delete Reservation Status is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = RsvStatusController;
