const { Calendar,Room,HotelRoomType, Booking,RoomTypeRoomRate,RoomRate,BookingRoom } = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op } = Sequelize;

class CalendarController {
    static async getCalendar(req, res, next) {
        try {
          const data = await Calendar.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getCalendarHotel(req, res, next) {
        const hotelid = req.params.hotelid;
        try {
          const data = await HotelRoomType.findAll({
            include:['roomtype','hotel'],
            where:{
                hotel_id: hotelid
            }
          });

          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getCalendarHotelRoomtype(req, res, next) {
        const roomtypeid = req.params.roomtypeid;
        try {
          const data = await HotelRoomType.findAll({
            include:['roomtype'],
            where:{
                room_type_id: roomtypeid
            }
          });

          const room = await Room.findAll({
            where:{
                roomtype_id : roomtypeid
            }
          });

          res.status(200).json({Room_Type : data, Room:room});
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async loadDatesCalenadar (req, res){
        try {
            const { start, end, hotelid } = req.query;
            const roomId = req.params.hotel_room_id;
            const fromDate = new Date(start);
            const toDate = new Date(end);
    
            const data = [];

            const roomdata = await Room.findOne({
                where: {
                    id: roomId,
                }
            });
           

            for (let currentDate = new Date(fromDate); currentDate <= toDate; currentDate.setDate(currentDate.getDate() + 1)) {
                const dateStr = currentDate.toISOString().split('T')[0];
    
                const calendar = await Calendar.findOne({
                    where: {
                        room_id: roomId, // Ubah sesuai dengan parameter yang sesuai untuk room_id
                        hotel_id: hotelid,
                        start_date: dateStr + "T00:00:00Z"
                    }
                });

                const roomtyperoomrate = await RoomTypeRoomRate.findOne({
                    include: [{
                        model: RoomRate,
                        as: 'roomrate',
                        where: {
                            [Op.and]: [
                                { date_from: { [Op.lte]: dateStr } }, // Tanggal dari RoomRate kurang dari atau sama dengan tanggal saat ini
                                { date_to: { [Op.gt]: dateStr } }  // Tanggal hingga RoomRate lebih besar dari tanggal saat ini
                            ]
                        }
                    }],
                    where: {
                        room_type_id: roomdata.roomtype_id,
                    }
                });

                const bookingroom = await BookingRoom.findOne({
                    include:[
                        {
                            model:Booking,
                            as:'booking',
                            where:{
                                [Op.and]: [
                                    { checkin_date: { [Op.lt]: new Date(currentDate.getTime() + 24 * 60 * 60 * 1000).toISOString().split('T')[0] } }, // Tanggal check-in kurang dari tanggal berikutnya
                                    { checkout_date: { [Op.gt]: dateStr } }  // Tanggal hingga RoomRate lebih besar dari tanggal saat ini
                                ]
                            }
                        }
                    ],
                    where:{
                        room_id:roomId
                    }
                });

                let color = '#0077b6';
                if(bookingroom){
                    color = '#e63946';
                }
                
                let price = 0;
                if (roomtyperoomrate && roomtyperoomrate.roomrate && roomtyperoomrate.roomrate.base_price) {
                    price = roomtyperoomrate.roomrate.base_price;
                }

                if (calendar) {

                    let calendarcolor = '#2a9134'; // Default color is green

                    // Override default color based on conditions
                    if (bookingroom) {
                        calendarcolor = color; // Use color from booking logic if booking exists
                    } else if (calendar.allotment == 0) {
                        calendarcolor = '#e63946'; // Red color if allotment is 0
                    } else if (calendar.no_checkin == 1 && calendar.no_checkout == 0) {
                        calendarcolor = '#bc3908'; // Orange color if no checkin but checkout allowed
                    } else if (calendar.no_checkin == 0 && calendar.no_checkout == 1) {
                        calendarcolor = '#941b0c'; // Red color if no checkout but checkin allowed
                    } else if (calendar.no_checkin == 1 && calendar.no_checkout == 1) {
                        calendarcolor = '#621708'; // Dark red color if both no checkin and no checkout
                    }

                    data.push({
                        title: `Rp ${calendar.price.toLocaleString()}`,
                        start: dateStr,
                        end: new Date(currentDate.getTime() + 24 * 60 * 60 * 1000).toISOString().split('T')[0],
                        price: calendar.price,
                        color: calendarcolor,
                        allow: calendar.allotment,
                        night: calendar.min_stay,
                        nocheckin: calendar.no_checkin,
                        nocheckout: calendar.no_checkout,
                    });

                } else {
                    data.push({
                        title: `Rp ${price.toLocaleString()}`,
                        date: dateStr,
                        price: price,
                        allow: 1,
                        night: 1,
                        color: color,
                        nocheckin: 0,
                        nocheckout: 0
                    });
                }
            }
    
            res.json(data);
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Internal Server Error',error:error });
        }
    };


      static async createCalendar(req, res, next) {
        try {
            const { hotel_id, room_id, start_date, end_date, price, nocheckin, nocheckout, min_stay, allotment,updatedby } = req.body;
    
            const startDate = new Date(start_date);
            const endDate = new Date(end_date);
    
            let currentDate = new Date(startDate);
    
            while (currentDate <= endDate) {
                const year = currentDate.getFullYear();
                const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
                const day = currentDate.getDate().toString().padStart(2, '0');
                const currentDateStr = `${year}-${month}-${day}`;
    
                let hotelcalendar = await Calendar.findOne({
                    where: {
                        hotel_id,
                        room_id,
                        start_date: currentDateStr + "T00:00:00Z"
                    }
                });
    
                if (!hotelcalendar) {
                    // Jika entri kalender tidak ditemukan, buat entri baru
                    hotelcalendar = await Calendar.create({
                        hotel_id,
                        room_id,
                        start_period:start_date,
                        end_period:end_date,
                        start_date: currentDateStr,
                        end_date: currentDateStr, // Tanggal akhir sama dengan tanggal awal untuk entri harian
                        price,
                        no_checkin: nocheckin,
                        no_checkout: nocheckout,
                        min_stay,
                        allotment: allotment || 0,
                        active: (allotment !== null && allotment !== 0) ? 1 : 0,
                        createdAt: Sequelize.fn('NOW'),
                        createdBy: updatedby,
                        updatedAt: Sequelize.fn('NOW'),
                        updatedBy: updatedby,
                    });
                } else {
                    // Jika entri kalender sudah ada, update entri tersebut
                    hotelcalendar.price = price;
                    hotelcalendar.no_checkin = nocheckin;
                    hotelcalendar.no_checkout = nocheckout;
                    hotelcalendar.min_stay = min_stay;
                    hotelcalendar.createdAt = Sequelize.fn('NOW');
                    hotelcalendar.createdBy = updatedby;
                    hotelcalendar.updatedAt = Sequelize.fn('NOW');
                    hotelcalendar.updatedBy = updatedby;
                    
                    if (allotment == null || allotment == 0) {
                        hotelcalendar.allotment = 0;
                        hotelcalendar.active = 0;
                    } else {
                        hotelcalendar.allotment = allotment;
                        hotelcalendar.active = 1;
                    }
                    
                    await hotelcalendar.save(); // Simpan perubahan pada entri kalender
                }
    
                currentDate.setDate(currentDate.getDate() + 1); // Pindah ke hari berikutnya
            }
    
            res.status(200).json({ message: 'Data tersimpan dengan sukses' });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Terjadi Kesalahan Internal Server', error:error });
        }
    }
    
    
      static async deleteCalendar(req, res) {
        
        const { start, hotel_id,room_id } = req.query;

        const startt = new Date(start);
        const year = startt.getFullYear();
        const month = (startt.getMonth() + 1).toString().padStart(2, '0');
        const day = startt.getDate().toString().padStart(2, '0');
        const startStr = `${year}-${month}-${day}`;
        
        const data = await Calendar.findAll({
          where: {
            hotel_id,
            room_id,
            start_date: startStr + "T00:00:00Z"
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Add on is not exist" });
    
        try {
              
          await Calendar.destroy({
            where: {
                hotel_id,
                room_id,
                start_date: startStr + "T00:00:00Z"
            },
          });
    
          res.json({ msg: "Delete Add on is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = CalendarController;