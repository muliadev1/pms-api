const { Reservation,MasterBill, MasterBill_Detail } = require("../db/models/index");
const Sequelize = require('sequelize');
const reservation = require("../db/models/reservation");

class MasterBillController {
    //-----------------------
    // Masterbill
    //-----------------------
    static async getMasterBills_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const masters = await MasterBill.findAll({
                include: ["masterBillDetails"],
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await MasterBill.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    masters: masters,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getMasterBills(req, res, next) {
        try {
            const masters = await MasterBill.findAll({
                include: ["masterBillDetails","guest","reservation"],
            });
            res.status(200).json(masters);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getGuestMasterBills(req, res, next) {
        try {
            const masters = await MasterBill.findAll({
                include: ["masterBillDetails", "guest", "reservation"],
                attributes: [
                    'guest_id', 
                    'id', 
                    'masterbill_code',
                    'masterbill_date',
                    'bill_to',
                    'charge',
                    'payment',
                    'balance',
                    'is_roombill',
                    'reservation_id',
                    'createdBy',
                    [Sequelize.literal('`masterBillDetails`.`id`'), 'masterBillDetails.id']
                ],
                group: [
                    'guest_id',
                    'id',
                    'masterbill_code',
                    'masterbill_date',
                    'bill_to',
                    'charge',
                    'payment',
                    'balance',
                    'is_roombill',
                    'reservation_id',
                    'createdBy',
                    '`masterBillDetails`.`id`'
                ]
            });
            res.status(200).json(masters);
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }

    static async getReportMasterBills(req, res, next) {
        const { guest_id } = req.query;
        try {
            const masters = await MasterBill.findAll({
                include: [
                    "masterBillDetails","guest",
                    {
                        model: Reservation,
                        as: "reservation",
                    },
                    
                ],
                where: {
                    guest_id: guest_id
                }
            });
            res.status(200).json(masters);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
    
    
    

    static async createMasterBill(req, res, next) {
        const {
            masterbill_code, // get from funxtion 
            masterbill_date,
            bill_to,
            charge,
            payment,
            balance,
            is_roombill,
            guest_id,
            reservation_id,
            updatedby
        } = req.body;

        try {
            await MasterBill.create({
                masterbill_code: masterbill_code,
                masterbill_date: masterbill_date,
                bill_to: bill_to,
                charge: charge,
                payment: payment,
                balance: balance,
                is_roombill: is_roombill,
                guest_id: guest_id,
                reservation_id: reservation_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Master Bill is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getMasterBill(req, res) {
        const masterId = req.params.id;
        try {
            const master = await MasterBill.findOne({
                include: ["masterBillDetails"],
                where: {
                    id: masterId,
                },
            });
            res.status(200).json(master);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateMasterBill(req, res) {
        const masterId = req.params.id;
        const {
            masterbill_date,
            bill_to,
            charge,
            payment,
            balance,
            is_roombill,
            guest_id,
            reservation_id,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const master = await MasterBill.findAll({
            where: {
                id: masterId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "Master Billis not exist" });


        try {
            await MasterBill.update(
                {
                    masterbill_date: masterbill_date,
                    bill_to: bill_to,
                    charge: charge,
                    payment: payment,
                    balance: balance,
                    is_roombill: is_roombill,
                    guest_id: guest_id,
                    reservation_id: reservation_id,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: masterId,
                    },
                }
            );

            res.json({ msg: "Update Master Bill is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteMasterBill(req, res) {
        const masterId = req.params.id;
        //cek apakah user sdh ada
        const master = await MasterBill.findAll({
            where: {
                id: masterId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "Master Bill is not exist" });

        try {
            await MasterBill.destroy({
                where: {
                    id: masterId,
                },
            });

            res.json({ msg: "Delete Master Bill is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    //----------------------------
    // Masterbill details
    //----------------------------
    static async getMasterBillDetails(req, res) {
        const masterId = req.params.masterId;
        try {
            const masterDetails = await MasterBill_Detail.findAll({
                where: {
                    masterbill_id: masterId,
                },
            });
            res.status(200).json(masterDetails);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createMasterBillDetail(req, res) {
        const masterId = req.params.masterId;
        const {
            masterbill_date,
            reservation_id,
            invoice_manual,
            room_id,
            is_charge,
            package_id,
            description,
            charge,
            payment,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const master = await MasterBill.findAll({
            where: {
                id: masterId,
            },
        });

        if (!master[0]) return res.status(400).json({ msg: "Master Bill is not exist" });

        try {
            await MasterBill_Detail.create({
                masterbill_id: masterId,
                masterbill_date: masterbill_date,
                reservation_id: reservation_id,
                invoice_manual: invoice_manual,
                room_id: room_id,
                is_charge: is_charge,
                package_id: package_id,
                description: description,
                charge: charge,
                payment: payment,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Master Bill Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getMasterBillDetail(req, res) {
        const detailId = req.params.id;

        try {
            const masterDetail = await MasterBill_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(masterDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateMasterBillDetail(req, res) {
        const detailId = req.params.id;
        const {
            masterbill_date,
            reservation_id,
            invoice_manual,
            room_id,
            is_charge,
            package_id,
            description,
            charge,
            payment,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const masterDetail = await MasterBill_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!masterDetail[0]) return res.status(400).json({ msg: "Master Bill Detail is not exist" });


        try {
            await MasterBill_Detail.update(
                {
                    masterbill_date: masterbill_date,
                    reservation_id: reservation_id,
                    invoice_manual: invoice_manual,
                    room_id: room_id,
                    is_charge: is_charge,
                    package_id: package_id,
                    description: description,
                    charge: charge,
                    payment: payment,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Master Bill Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteMasterBillDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const masterDetail = await MasterBill_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!masterDetail[0]) return res.status(400).json({ msg: "Master Bill Detail is not exist" });

        try {
            await MasterBill_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Master Bill Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = MasterBillController;

