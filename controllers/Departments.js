const { Department } = require("../db/models/index");
const Sequelize = require('sequelize');

class DepartmentController {
  static async getDepartments_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const departments = await Department.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Department.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          departments: departments,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getDepartments(req, res, next) {
    try {
      const departments = await Department.findAll({
      });
      res.status(200).json(departments);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createDepartment(req, res, next) {
    const {
      department_name,
      is_active,
      updatedby
    } = req.body;

    try {
      await Department.create({
        department_name: department_name,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Department is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getDepartment(req, res) {
    const deptId = req.params.id;
    try {
      const department = await Department.findOne({
        where: {
          id: deptId,
        },
      });
      res.status(200).json(department);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateDepartment(req, res) {
    const deptId = req.params.id;
    const {
      department_name,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const department = await Department.findAll({
      where: {
        id: deptId,
      },
    });

    if (!department[0]) return res.status(400).json({ msg: "Department is not exist" });


    try {
      await Department.update(
        {
          department_name: department_name,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: deptId,
          },
        }
      );

      res.json({ msg: "Update Department is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteDepartment(req, res) {
    const deptId = req.params.id;
    //cek apakah user sdh ada
    const department = await Department.findAll({
      where: {
        id: deptId,
      },
    });
    
    if (!department[0]) return res.status(400).json({ msg: "Department is not exist" });

    try {
      await Department.destroy({
        where: {
          id: deptId,
        },
      });

      res.json({ msg: "Delete Department is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = DepartmentController;
