const { SR_Main, SR_Detail,SubCategoryDepartement,Item, Vendor } = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op } = Sequelize;

class StoreRequisitionController {
    static async getStoreReqs_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const stores = await SR_Main.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await SR_Main.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    stores: stores,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getStoreReqs(req, res, next) {
        try {
            const storeReqs = await SR_Main.findAll({
                include: ["storereqdetail"],
            });
            res.status(200).json(storeReqs);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getStoreReqsBycode(req, res, next) {
        const {reqscode} = req.query;

        try {
            const storeReqs = await SR_Main.findAll({
                include: ["storereqdetail"],
                where:{
                    store_req_code:reqscode,
                }
            });
            res.status(200).json(storeReqs);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getStoreReqsBydate(req, res, next) {
        const {startdate,enddate,iddepartement,itemcode} = req.query;

        try {
            const storeReqs = await SR_Main.findAll({
                include: [{
                    model: SR_Detail,
                    as: 'storereqdetail',
                    where: { item_code: itemcode }
                }],
                where: {
                    department_id: iddepartement,
                    store_date: {
                        [Op.between]: [startdate, enddate]
                    }
                }
            });
            res.status(200).json(storeReqs);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async createStoreReq(req, res, next) {
        const {
            store_req_code,
            department_id,
            department,
            store_date,
            operator_id,
            desciption,
            status_in_out,
            is_SO,
            updatedby
        } = req.body;

        try {
            await SR_Main.create({
                store_req_code: store_req_code,
                department_id: department_id,
                department: department,
                store_date: store_date,
                operator_id: operator_id,
                desciption: desciption,
                status_in_out: status_in_out,
                is_SO: is_SO,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add Store Requisition is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getStoreReq(req, res) {
        const storeId = req.params.id;
        try {
            const store = await SR_Main.findOne({
                include: ["storereqdetail"],
                where: {
                    id: storeId,
                },
            });
            res.status(200).json(store);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateStoreReq(req, res) {
        const storeId = req.params.id;
        const {
            department_id,
            department,
            store_date,
            operator_id,
            desciption,
            status_in_out,
            is_SO,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const store = await SR_Main.findAll({
            where: {
                id: storeId,
            },
        });

        if (!store[0]) return res.status(400).json({ msg: "Store Requisition is not exist" });


        try {
            await SR_Main.update(
                {
                    department_id: department_id,
                    department: department,
                    store_date: store_date,
                    operator_id: operator_id,
                    desciption: desciption,
                    status_in_out: status_in_out,
                    is_SO: is_SO,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: storeId,
                    },
                }
            );

            res.json({ msg: "Update Store Requisition is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteStoreReq(req, res) {
        const storeId = req.params.id;
        //cek apakah user sdh ada
        const store = await SR_Main.findAll({
            where: {
                id: storeId,
            },
        });

        if (!store[0]) return res.status(400).json({ msg: "Store Requisition is not exist" });

        try {
            await SR_Main.destroy({
                where: {
                    id: storeId,
                },
            });

            res.json({ msg: "Delete Store Requisition is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getStoreReqDetails(req, res) {
        const sreqId = req.params.sreqId;
        try {
            const storeDetails = await SR_Detail.findAll({
                where: {
                    store_req_id: sreqId,
                },
            });
            res.status(200).json(storeDetails);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createStoreReqDetail(req, res) {
        const sreqId = req.params.sreqId;
        const {
            item_code,
            description,
            unit,
            price,
            qty_order,
            curr_stock,
            department_id,
            department,
            operator_id,
            status_in_out,
            is_SO,
            notes,
            updatedby
        } = req.body;
    
        // Cek apakah Store Requisition sudah ada
        const store = await SR_Main.findByPk(sreqId);
        if (!store) return res.status(400).json({ msg: "Store Requisition does not exist" });
    
        try {
            // Tambahkan detail Store Requisition
            const createdDetail = await SR_Detail.create({
                store_req_id: sreqId,
                item_code: item_code,
                description: description,
                unit: unit,
                price: price,
                qty_order: qty_order,
                curr_stock: curr_stock,
                department_id: department_id,
                department: department,
                operator_id: operator_id,
                status_in_out: status_in_out,
                is_SO: is_SO,
                notes: notes,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
    
            // Cek apakah item sudah ada
            const item = await Item.findOne({
                where:{
                    item_code: item_code,
                }
            });
    
            if (item) {
                // Jika item ditemukan, tambahkan atau perbarui SubCategoryDepartement
                const [subsdep, created] = await SubCategoryDepartement.findOrCreate({
                    where: {
                        iddepartement: department_id,
                        item_id: item.id,
                    },
                    defaults: {
                        iddepartement: department_id,
                        item_id: item.id,
                        itemcode: item_code,
                        subcategory_id: item.subcategory_id,
                        description: description,
                    }
                });
    
                if (!created) {
                    // Jika tidak dibuat, maka update data yang ada
                    await subsdep.update({
                        itemcode: item_code,
                        subcategory_id: item.subcategory_id,
                        description: description,
                    });
                }
            } 
     
            res.status(200).json({ msg: "Add Store Requisition Detail is successful" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    

    static async getStoreReqDetail(req, res) {
        const detailId = req.params.id;

        try {
            const storeDetail = await SR_Detail.findOne({
                where: {
                    id: detailId,
                },
            });
            res.status(200).json(storeDetail);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }


    static async updateStoreReqDetail(req, res) {
        const detailId = req.params.id;
        const {
            item_code,
            description,
            unit,
            price,
            qty_order,
            curr_stock,
            department_id,
            department,
            operator_id,
            status_in_out,
            is_SO,
            notes,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const storeDetail = await SR_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!storeDetail[0]) return res.status(400).json({ msg: "Store Requesition Detail is not exist" });


        try {
            await SR_Detail.update(
                {
                    item_code: item_code,
                    description: description,
                    unit: unit,
                    price: price,
                    qty_order: qty_order,
                    curr_stock: curr_stock,
                    department_id: department_id,
                    department: department,
                    operator_id: operator_id,
                    status_in_out: status_in_out,
                    is_SO: is_SO,
                    notes: notes,
                    notes: notes,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: detailId,
                    },
                }
            );

            res.json({ msg: "Update Store Requisition Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteStoreReqDetail(req, res) {
        const detailId = req.params.id;

        //cek apakah user sdh ada
        const storeDetail = await SR_Detail.findAll({
            where: {
                id: detailId,
            },
        });

        if (!storeDetail[0]) return res.status(400).json({ msg: "Store Requisition Detail is not exist" });

        try {
            await SR_Detail.destroy({
                where: {
                    id: detailId,
                },
            });

            res.json({ msg: "Delete Store Requisitioin Detail is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = StoreRequisitionController;

