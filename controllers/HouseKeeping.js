const { HouseKeeping } = require("../db/models/index");
const Sequelize = require('sequelize');

class HouseKeepingController {
    static async getHouseKeeping(req, res, next) {
        try {
          const data = await HouseKeeping.findAll({
           include:['room',"room_status"]
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getHouseKeepingDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await HouseKeeping.findOne({
            include:['room',"room_status"],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createHouseKeeping(req, res, next) {
        const {
          room_id,
          room_status_id,
          assigned_date,
          assigned_to,
          remarks,
          updatedby
        } = req.body;
    
        try {
          await HouseKeeping.create({
            room_id,
            room_status_id,
            assigned_date,
            assigned_to,
            remarks,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add HouseKeeping is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateHouseKeeping(req, res) {
        const id = req.params.id;
        const {
          room_id,
          room_status_id,
          assigned_date,
          assigned_to,
          remarks,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await HouseKeeping.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "HouseKeeping is not exist" });
    
    
        try {
          await HouseKeeping.update(
            {
              room_id,
              room_status_id,
              assigned_date,
              assigned_to,
              remarks,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update HouseKeeping is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteHouseKeeping(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await HouseKeeping.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "HouseKeeping is not exist" });
    
        try {
          await HouseKeeping.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete HouseKeeping is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = HouseKeepingController;