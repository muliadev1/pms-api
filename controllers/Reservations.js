const {
  Reservation,
  Reservation_Detail,
  Reservation_Guest,
  FoTrx,
  FoTrx_Detail,
  MasterBill,
  MasterBill_Detail,
  Company,
} = require("../db/models/index");
const Sequelize = require("sequelize");
const reservation = require("../db/models/reservation");

class ReservationController {
  //-----------------------
  // reservations
  //-----------------------
  static async getReservations_pages(req, res, next) {
    let { page, pageSize } = req.query;

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const reservations = await Reservation.findAll({
        include: [
          // "details",
          "guest",
          // "room",
          "reservationguests",
          "status",
          // "roomtype",
          // "company",
          // "segment",
          // "purpose",
          // "source",
          "vipdetail",
          "paymenttype",
        ],
        offset: startIndex,
        limit: Number(pageSize),
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Reservation.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json({
        reservations: reservations,
        totalRecords: totalCount,
        currentPage: Number(page),
        totalPages: totalPages,
      });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async getReservatios(req, res, next) {
    try {
      const orders = await Reservation.findAll({
        include: [
          "details",
          "guest",
          "room",
          "reservationguests",
          "status",
          "roomtype",
          "company",
          "segment",
          "purpose",
          "source",
          "vipdetail",
          "paymenttype",
        ],
        order: [["createdAt", "DESC"]],
      });
      res.status(200).json(orders);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async createReservationBulk(req, res, next) {
    const {
      reservation_no,
      reservation_date,
      is_group,
      parent_group_id,
      room_data,
      reservation_status_id,
      guest_id,
      adult,
      child,
      check_in,
      check_out,
      purpose_id,
      source_id,
      segment_id,
      is_vip,
      company_id,
      deposit,
      cut_off_date,
      agent_it,
      remarks,
      is_roombill, //tambahan req body
      is_charge,
      charge,
      package_id,
      description,
      rate_price,
      is_running,
      vip_id,
      payment_type_id,
      updatedby,
    } = req.body;

    try {
      // Parsing tanggal dari "MM/DD/YYYY" ke format yang diinginkan ("YYYY-MM-DD")
      const [check_inMonth, check_inDay, check_inYear] = check_in.split("/");
      const [check_outMonth, check_outDay, check_outYear] =
        check_out.split("/");
      const [cut_off_dateMonth, cut_off_dateDay, cut_off_dateYear] =
        cut_off_date.split("/");
      const [reservation_dateMonth, reservation_dateDay, reservation_dateYear] =
        reservation_date.split("/");

      // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
      const formattedcheck_in = `${check_inYear}-${check_inMonth}-${check_inDay}`;
      const formattedcheck_out = `${check_outYear}-${check_outMonth}-${check_outDay}`;
      const formattedcut_off_date = `${cut_off_dateYear}-${cut_off_dateMonth}-${cut_off_dateDay}`;
      const formattedreservation_date = `${reservation_dateYear}-${reservation_dateMonth}-${reservation_dateDay}`;

      // Menghitung totalnight
      const check_inDate = new Date(formattedcheck_in);
      const check_outDate = new Date(formattedcheck_out);
      const timeDifference = check_outDate.getTime() - check_inDate.getTime();
      const Totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));

      // Buat reservation
      const reservation = await Reservation.create({
        reservation_no: reservation_no,
        reservation_date: formattedreservation_date + "T00:00:00Z",
        is_group: is_group,
        parent_group_id: parent_group_id,
        reservation_status_id: reservation_status_id,
        guest_id: guest_id,
        adult: adult,
        child: child,
        check_in: formattedcheck_in + "T00:00:00Z",
        check_out: formattedcheck_out + "T00:00:00Z",
        duration: Totalnight,
        purpose_id: purpose_id,
        source_id: source_id,
        segment_id: segment_id,
        is_vip: is_vip,
        company_id: company_id,
        deposit: deposit,
        cut_off_date: formattedcut_off_date + "T00:00:00Z",
        agent_it: agent_it,
        remarks: remarks,
        vip_id,
        payment_type_id,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      });

      const reservationdetail = room_data.map((detailData) => ({
        reservation_id: reservation.id,
        night_no: Totalnight,
        room_type_id: detailData.room_type_id,
        room_id: detailData.room_id,
        package_id: package_id,
        rate_price: rate_price,
        is_running: is_running,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      }));

      await Reservation_Detail.bulkCreate(reservationdetail);

      // Buat FoTrx
      const fotrx = await FoTrx.create({
        reservation_id: reservation.id,
        reservation_no: reservation_no,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      });

      // Mengambil nama perusahaan dari model Company berdasarkan company_id
      const company = await Company.findOne({ where: { id: company_id } });
      // Buat MasterBill
      // Buat MasterBill
      const currentDateMasterbill = new Date();
      const formattedDatemasterbill = currentDateMasterbill
        .toISOString()
        .slice(0, 10)
        .replace(/-/g, "");
      const randomCode = Math.random().toString(36).substring(2, 7);
      const masterbill_code = `BIL-${formattedDatemasterbill}-${randomCode}`;

      const master_bill = await MasterBill.create({
        masterbill_code: masterbill_code,
        masterbill_date: formattedreservation_date, // Menggunakan tanggal reservasi
        bill_to: company ? company.name : "Default Company Name", // Misalnya, Anda dapat menentukan bill_to secara default atau menyesuaikannya sesuai kebutuhan
        charge: charge, // Contoh: Menggunakan null untuk charge
        payment: deposit, // Contoh: Menggunakan null untuk payment
        balance: deposit - charge, // Contoh: Menggunakan null untuk balance
        is_roombill: is_roombill, // Contoh: Menggunakan null untuk is_roombill
        guest_id: guest_id,
        reservation_id: reservation.id,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      });

      if (master_bill) {
        const masterbildetail = room_data.map((detailData) => ({
          masterbill_id: master_bill.id,
          masterbill_date: master_bill.masterbill_date,
          reservation_id: reservation.id,
          invoice_manual: "-",
          room_id: detailData.room_id,
          is_charge: is_charge,
          package_id: package_id,
          description: description,
          charge: master_bill.charge,
          payment: master_bill.payment,
          createdAt: Sequelize.fn("NOW"),
          createdBy: updatedby,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        }));

        await MasterBill_Detail.bulkCreate(masterbildetail);
      }

      // Buat FoTrxDetail jika FoTrx sudah ada
      if (fotrx) {
        const invoice_no = `INV-${formattedDatemasterbill}-${randomCode}`;
        const fotrxdetail = room_data.map((detailData) => ({
          reservation_no: reservation_no,
          reservation_id: reservation.id,
          invoice_no: invoice_no, // Menggunakan reservation_no sebagai invoice_no
          room_type_id: detailData.room_type_id,
          room_id: detailData.room_id,
          checkout: formattedcheck_out + "T00:00:00Z", // Menggunakan check_out dari reservasi
          package_id: package_id, // Contoh: Menggunakan null untuk package_id
          description: remarks, // Menggunakan remarks dari reservasi sebagai deskripsi
          charge: charge, // Contoh: Menggunakan null untuk charge
          payment: deposit, // Contoh: Menggunakan null untuk payment
          createdAt: Sequelize.fn("NOW"),
          createdBy: updatedby,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        }));

        await FoTrx_Detail.bulkCreate(fotrxdetail);
      }

      res
        .status(200)
        .json({
          msg: "Add Reservation, Fo Trx, Master Bill, and Fo Trx Detail are successfully",
        });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async updateReservationBulk(req, res, next) {
    const resId = req.params.id;
    const {
      reservation_no,
      reservation_date,
      is_group,
      parent_group_id,
      room_data,
      reservation_status_id,
      guest_id,
      adult,
      child,
      check_in,
      check_out,
      purpose_id,
      source_id,
      segment_id,
      is_vip,
      company_id,
      deposit,
      cut_off_date,
      agent_it,
      remarks,
      is_roombill, //tambahan req body
      is_charge,
      charge,
      package_id,
      description,
      rate_price,
      is_running,
      vip_id,
      payment_type_id,
      updatedby,
    } = req.body;

    try {
      // Parsing tanggal dari "MM/DD/YYYY" ke format yang diinginkan ("YYYY-MM-DD")
      const [check_inMonth, check_inDay, check_inYear] = check_in.split("/");
      const [check_outMonth, check_outDay, check_outYear] =
        check_out.split("/");
      const [cut_off_dateMonth, cut_off_dateDay, cut_off_dateYear] =
        cut_off_date.split("/");
      const [reservation_dateMonth, reservation_dateDay, reservation_dateYear] =
        reservation_date.split("/");

      // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
      const formattedcheck_in = `${check_inYear}-${check_inMonth}-${check_inDay}`;
      const formattedcheck_out = `${check_outYear}-${check_outMonth}-${check_outDay}`;
      const formattedcut_off_date = `${cut_off_dateYear}-${cut_off_dateMonth}-${cut_off_dateDay}`;
      const formattedreservation_date = `${reservation_dateYear}-${reservation_dateMonth}-${reservation_dateDay}`;

      // Menghitung totalnight
      const check_inDate = new Date(formattedcheck_in);
      const check_outDate = new Date(formattedcheck_out);
      const timeDifference = check_outDate.getTime() - check_inDate.getTime();
      const Totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));

      // Perbarui reservation berdasarkan ID yang diberikan
      await Reservation.update(
        {
          reservation_no: reservation_no,
          reservation_date: formattedreservation_date + "T00:00:00Z",
          is_group: is_group,
          parent_group_id: parent_group_id,
          reservation_status_id: reservation_status_id,
          guest_id: guest_id,
          adult: adult,
          child: child,
          check_in: formattedcheck_in + "T00:00:00Z",
          check_out: formattedcheck_out + "T00:00:00Z",
          duration: Totalnight,
          purpose_id: purpose_id,
          source_id: source_id,
          segment_id: segment_id,
          is_vip: is_vip,
          company_id: company_id,
          deposit: deposit,
          cut_off_date: formattedcut_off_date + "T00:00:00Z",
          agent_it: agent_it,
          remarks: remarks,
          vip_id,
          payment_type_id,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        },
        {
          where: {
            id: resId,
          },
        }
      );

      // Perbarui detail reservasi
      // Hapus terlebih dahulu detail reservasi yang ada berdasarkan ID reservasi
      await Reservation_Detail.destroy({
        where: {
          reservation_id: resId,
        },
      });

      // Kemudian buat detail reservasi yang baru
      const reservationdetail = room_data.map((detailData) => ({
        reservation_id: resId,
        night_no: Totalnight,
        room_type_id: detailData.room_type_id,
        room_id: detailData.room_id,
        package_id: package_id,
        rate_price: rate_price,
        is_running: is_running,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      }));
      await Reservation_Detail.bulkCreate(reservationdetail);

      // Perbarui FoTrx jika sudah ada
      const fotrx = await FoTrx.findOne({ where: { reservation_id: resId } });
      if (fotrx) {
        await FoTrx.update(
          {
            reservation_no: reservation_no,
            updatedAt: Sequelize.fn("NOW"),
            updatedBy: updatedby,
          },
          {
            where: {
              reservation_id: resId,
            },
          }
        );

        // Perbarui detail FoTrx jika sudah ada
        // Hapus terlebih dahulu detail FoTrx yang ada berdasarkan ID reservasi
        await FoTrx_Detail.destroy({
          where: {
            reservation_id: resId,
          },
        });

        // Kemudian buat detail FoTrx yang baru
        const fotrxdetail = room_data.map((detailData) => ({
          reservation_no: reservation_no,
          reservation_id: resId,
          room_type_id: detailData.room_type_id,
          room_id: detailData.room_id,
          checkout: formattedcheck_out + "T00:00:00Z", // Menggunakan check_out dari reservasi
          package_id: package_id, // Contoh: Menggunakan null untuk package_id
          description: remarks, // Menggunakan remarks dari reservasi sebagai deskripsi
          charge: charge, // Contoh: Menggunakan null untuk charge
          payment: deposit, // Contoh: Menggunakan null untuk payment
          createdAt: Sequelize.fn("NOW"),
          createdBy: updatedby,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        }));
        await FoTrx_Detail.bulkCreate(fotrxdetail);
      }

      // Perbarui MasterBill jika sudah ada
      const masterBill = await MasterBill.findOne({
        where: { reservation_id: resId },
      });
      if (masterBill) {
        const company = await Company.findOne({ where: { id: company_id } });
        await MasterBill.update(
          {
            masterbill_date: formattedreservation_date, // Menggunakan tanggal reservasi
            bill_to: company ? company.name : "Default Company Name", // Misalnya, Anda dapat menentukan bill_to secara default atau menyesuaikannya sesuai kebutuhan
            charge: charge, // Contoh: Menggunakan null untuk charge
            payment: deposit, // Contoh: Menggunakan null untuk payment
            balance: deposit - charge, // Contoh: Menggunakan null untuk balance
            is_roombill: is_roombill, // Contoh: Menggunakan null untuk is_roombill
            guest_id: guest_id,
            updatedAt: Sequelize.fn("NOW"),
            updatedBy: updatedby,
          },
          {
            where: {
              reservation_id: resId,
            },
          }
        );

        // Perbarui detail MasterBill jika sudah ada
        // Hapus terlebih dahulu detail MasterBill yang ada berdasarkan ID reservasi
        await MasterBill_Detail.destroy({
          where: {
            reservation_id: resId,
          },
        });

        // Kemudian buat detail MasterBill yang baru
        const masterbildetail = room_data.map((detailData) => ({
          masterbill_id: masterBill.id,
          masterbill_date: masterBill.masterbill_date,
          reservation_id: resId,
          invoice_manual: "-",
          room_id: detailData.room_id,
          is_charge: is_charge,
          package_id: package_id,
          description: description,
          charge: masterBill.charge,
          payment: masterBill.payment,
          createdAt: Sequelize.fn("NOW"),
          createdBy: updatedby,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        }));
        await MasterBill_Detail.bulkCreate(masterbildetail);
      }

      res.status(200).json({ msg: "Reservation data updated successfully" });
    } catch (error) {
      res.status(500).json({ message: "Internal Server Error", error: error });
    }
  }

  static async createReservation(req, res, next) {
    const {
      reservation_no,
      reservation_date,
      is_group,
      parent_group_id,
      room_type_id,
      room_id,
      reservation_status_id,
      guest_id,
      adult,
      child,
      check_in,
      check_out,
      purpose_id,
      source_id,
      segment_id,
      is_vip,
      company_id,
      deposit,
      cut_off_date,
      agent_it,
      remarks,
      vip_id,
      payment_type_id,
      updatedby,
    } = req.body;

    try {
      // Parsing tanggal dari "MM/DD/YYYY" ke format yang diinginkan ("YYYY-MM-DD")
      const [check_inMonth, check_inDay, check_inYear] = check_in.split("/");
      const [check_outMonth, check_outDay, check_outYear] =
        check_out.split("/");
      const [cut_off_dateMonth, cut_off_dateDay, cut_off_dateYear] =
        cut_off_date.split("/");
      const [reservation_dateMonth, reservation_dateDay, reservation_dateYear] =
        reservation_date.split("/");

      // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
      const formattedcheck_in = `${check_inYear}-${check_inMonth}-${check_inDay}`;
      const formattedcheck_out = `${check_outYear}-${check_outMonth}-${check_outDay}`;
      const formattedcut_off_date = `${cut_off_dateYear}-${cut_off_dateMonth}-${cut_off_dateDay}`;
      const formattedreservation_date = `${reservation_dateYear}-${reservation_dateMonth}-${reservation_dateDay}`;

      // Menghitung totalnight
      const check_inDate = new Date(formattedcheck_in);
      const check_outDate = new Date(formattedcheck_out);
      const timeDifference = check_outDate.getTime() - check_inDate.getTime();
      const Totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));

      await Reservation.create({
        reservation_no: reservation_no,
        reservation_date: formattedreservation_date + "T00:00:00Z",
        is_group: is_group,
        parent_group_id: parent_group_id,
        room_type_id: room_type_id,
        room_id: room_id,
        reservation_status_id: reservation_status_id,
        guest_id: guest_id,
        adult: adult,
        child: child,
        check_in: formattedcheck_in + "T00:00:00Z",
        check_out: formattedcheck_out + "T00:00:00Z",
        duration: Totalnight,
        purpose_id: purpose_id,
        source_id: source_id,
        segment_id: segment_id,
        is_vip: is_vip,
        company_id: company_id,
        deposit: deposit,
        cut_off_date: formattedcut_off_date + "T00:00:00Z",
        agent_it: agent_it,
        remarks: remarks,
        vip_id,
        payment_type_id,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Reservation is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async getReservatioin(req, res) {
    const resId = req.params.id;
    try {
      const reservation = await Reservation.findOne({
        include: [
          "details",
          "guest",
          "room",
          "reservationguests",
          "status",
          "roomtype",
          "company",
          "segment",
          "purpose",
          "source",
          "vipdetail",
          "paymenttype",
        ],
        where: {
          id: resId,
        },
      });
      res.status(200).json(reservation);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async getByIdReservation(req, res, next) {
    const resId = req.params.id;

    try {
      // Mengambil data dari tabel Reservation
      const reservation = await Reservation.findByPk(resId, {
        include: [
          "details",
          "guest",
          "room",
          "reservationguests",
          "status",
          "roomtype",
          "company",
          "segment",
          "purpose",
          "source",
          "vipdetail",
          "paymenttype",
        ],
      });
      if (!reservation) {
        return res.status(404).json({ message: "Reservation not found" });
      }

      // Mengambil data dari tabel Reservation_Detail
      const reservationDetail = await Reservation_Detail.findOne({
        where: { reservation_id: resId },
      });

      // Mengambil data dari tabel FoTrx
      const fotrx = await FoTrx.findOne({
        where: { reservation_id: resId },
      });

      // Mengambil data dari tabel MasterBill
      const masterBill = await MasterBill.findOne({
        where: { reservation_id: resId },
      });

      // Mengambil data dari tabel MasterBill_Detail
      const masterBillDetail = await MasterBill_Detail.findOne({
        where: { reservation_id: resId },
      });

      // Mengambil data dari tabel FoTrx_Detail
      const fotrxDetail = await FoTrx_Detail.findOne({
        where: { reservation_id: resId },
      });

      // Gabungkan semua data menjadi satu objek dan kirim sebagai respons
      const responseData = {
        reservation,
        reservationDetail,
        fotrx,
        masterBill,
        masterBillDetail,
        fotrxDetail,
      };

      res.status(200).json(responseData);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
      res.status(500).json({ message: "Internal Server Error" });
    }
  }

  static async updateReservation(req, res) {
    const resId = req.params.id;
    const {
      reservation_date,
      is_group,
      parent_group_id,
      room_type_id,
      room_id,
      reservation_status_id,
      guest_id,
      adult,
      child,
      check_in,
      check_out,
      purpose_id,
      source_id,
      segment_id,
      is_vip,
      company_id,
      deposit,
      cut_off_date,
      agent_it,
      remarks,
      vip_id,
      payment_type_id,
      updatedby,
    } = req.body;

    //cek apakah user sdh ada
    const reservation = await Reservation.findAll({
      where: {
        id: resId,
      },
    });

    if (!reservation[0])
      return res.status(400).json({ msg: "Reservation is not exist" });

    try {
      // Parsing tanggal dari "MM/DD/YYYY" ke format yang diinginkan ("YYYY-MM-DD")
      const [check_inMonth, check_inDay, check_inYear] = check_in.split("/");
      const [check_outMonth, check_outDay, check_outYear] =
        check_out.split("/");
      const [cut_off_dateMonth, cut_off_dateDay, cut_off_dateYear] =
        cut_off_date.split("/");
      const [reservation_dateMonth, reservation_dateDay, reservation_dateYear] =
        reservation_date.split("/");

      // Format ulang tanggal sesuai kebutuhan ("YYYY-MM-DD")
      const formattedcheck_in = `${check_inYear}-${check_inMonth}-${check_inDay}`;
      const formattedcheck_out = `${check_outYear}-${check_outMonth}-${check_outDay}`;
      const formattedcut_off_date = `${cut_off_dateYear}-${cut_off_dateMonth}-${cut_off_dateDay}`;
      const formattedreservation_date = `${reservation_dateYear}-${reservation_dateMonth}-${reservation_dateDay}`;

      // Menghitung totalnight
      const check_inDate = new Date(formattedcheck_in);
      const check_outDate = new Date(formattedcheck_out);
      const timeDifference = check_outDate.getTime() - check_inDate.getTime();
      const Totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));

      await Reservation.update(
        {
          reservation_date: formattedreservation_date + "T00:00:00Z",
          is_group: is_group,
          parent_group_id: parent_group_id,
          room_type_id: room_type_id,
          room_id: room_id,
          reservation_status_id: reservation_status_id,
          guest_id: guest_id,
          adult: adult,
          child: child,
          check_in: formattedcheck_in + "T00:00:00Z",
          check_out: formattedcheck_out + "T00:00:00Z",
          duration: Totalnight,
          purpose_id: purpose_id,
          source_id: source_id,
          segment_id: segment_id,
          is_vip: is_vip,
          company_id: company_id,
          deposit: deposit,
          cut_off_date: formattedcut_off_date + "T00:00:00Z",
          agent_it: agent_it,
          remarks: remarks,
          vip_id,
          payment_type_id,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        },
        {
          where: {
            id: resId,
          },
        }
      );

      res.json({ msg: "Update Reservation is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async deleteReservation(req, res) {
    const resId = req.params.id;
    //cek apakah user sdh ada
    const reservation = await Reservation.findAll({
      where: {
        id: resId,
      },
    });

    if (!reservation[0])
      return res.status(400).json({ msg: "Reservation is not exist" });

    try {
      await Reservation.destroy({
        where: {
          id: resId,
        },
      });

      res.json({ msg: "Delete Purchase Order is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  //----------------------------
  // reservation details
  //----------------------------
  static async getReservationDetails(req, res) {
    const resId = req.params.resId;
    try {
      const resDetails = await Reservation_Detail.findAll({
        include: ["reservation", "package"],
        where: {
          reservation_id: resId,
        },
      });
      res.status(200).json(resDetails);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async createReservationDetail(req, res) {
    const resid = req.params.resId;
    const { night_no, package_id, rate_price, is_running, updatedby } =
      req.body;

    //cek apakah user sdh ada
    const reservation = await Reservation.findAll({
      where: {
        id: resid,
      },
    });

    if (!reservation[0])
      return res.status(400).json({ msg: "Reservations is not exist" });

    try {
      await Reservation_Detail.create({
        reservation_id: resid,
        night_no: night_no,
        package_id: package_id,
        rate_price: rate_price,
        is_running: is_running,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Reservation Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async getReservationDetail(req, res) {
    const detailId = req.params.id;

    try {
      const resDetail = await Reservation_Detail.findOne({
        include: ["reservation", "package"],
        where: {
          id: detailId,
        },
      });
      res.status(200).json(resDetail);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async updateReservationDetail(req, res) {
    const detailId = req.params.id;
    const { package_id, rate_price, is_running, updatedby } = req.body;

    //cek apakah user sdh ada
    const resDetail = await Reservation_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!resDetail[0])
      return res.status(400).json({ msg: "Reservation Detail is not exist" });

    try {
      await Reservation_Detail.update(
        {
          package_id: package_id,
          rate_price: rate_price,
          is_running: is_running,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        },
        {
          where: {
            id: detailId,
          },
        }
      );

      res.json({ msg: "Update Reservation Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async deleteReservationDetail(req, res) {
    const detailId = req.params.id;

    //cek apakah user sdh ada
    const resDetail = await Reservation_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!resDetail[0])
      return res.status(400).json({ msg: "Reservation Detail is not exist" });

    try {
      await Reservation_Detail.destroy({
        where: {
          id: detailId,
        },
      });

      res.json({ msg: "Delete Reservation Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }
  //------------------------
  // rserevation guests
  //------------------------
  static async getReservationGuests(req, res) {
    const resId = req.params.resId;
    try {
      const resGustas = await Reservation_Guest.findAll({
        include: ["guest"],
        where: {
          reservation_id: resId,
        },
      });
      res.status(200).json(resGustas);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async createReservationGuest(req, res) {
    const resid = req.params.resId;
    const { guest_id, updatedby } = req.body;

    //cek apakah user sdh ada
    const reservation = await Reservation.findAll({
      where: {
        id: resid,
      },
    });

    if (!reservation[0])
      return res.status(400).json({ msg: "Reservations is not exist" });

    try {
      await Reservation_Guest.create({
        reservation_id: resid,
        guest_id: guest_id,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Reservation Guest is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async getReservationGuest(req, res) {
    const detailId = req.params.id;

    try {
      const resGust = await Reservation_Guest.findOne({
        include: ["guest"],
        where: {
          id: detailId,
        },
      });
      res.status(200).json(resGust);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async updateReservationGuest(req, res) {
    const detailId = req.params.id;
    const { reservation_id, guest_id, updatedby } = req.body;

    //cek apakah user sdh ada
    const resGuest = await Reservation_Guest.findAll({
      where: {
        id: detailId,
      },
    });

    if (!resGuest[0])
      return res.status(400).json({ msg: "Reservation Guest is not exist" });

    try {
      await Reservation_Guest.update(
        {
          reservation_id: reservation_id,
          guest_id: guest_id,
          updatedAt: Sequelize.fn("NOW"),
          updatedBy: updatedby,
        },
        {
          where: {
            id: detailId,
          },
        }
      );

      res.json({ msg: "Update Reservation Guest is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async deleteReservationGuest(req, res) {
    const detailId = req.params.id;

    //cek apakah user sdh ada
    const resGuest = await Reservation_Guest.findAll({
      where: {
        id: detailId,
      },
    });

    if (!resGuest[0])
      return res.status(400).json({ msg: "Reservation Guest is not exist" });

    try {
      await Reservation_Guest.destroy({
        where: {
          id: detailId,
        },
      });

      res.json({ msg: "Delete Reservation Guest is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }
}

module.exports = ReservationController;
