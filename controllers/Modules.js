const { Module } = require("../db/models/index");
const Sequelize = require('sequelize');

class ModuleController {
    static async getModules_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const modules = await Module.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await Module.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    modules: modules,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async getModules(req, res, next) {
        try {
            const modules = await Module.findAll({});
            res.status(200).json(modules);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async getModulesByApp(req, res, next) {
        const appId = req.params.appid;
        try {
            const modules = await Module.findAll({
                where: {
                    module_app_id: appId,
                },
            });
            res.status(200).json(modules);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async createModule(req, res, next) {
        const {
            module_code,
            module_name,
            icon,
            order_no,
            url,
            is_parent,
            is_trx,
            module_app_id,
            updatedBy
        } = req.body;

        try {
            await Module.create({
                module_code: module_code,
                module_name: module_name,
                icon: icon,
                order_no: order_no,
                url: url,
                is_parent: is_parent,
                is_trx: is_trx,
                module_app_id: module_app_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedBy,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedBy
            });
            res.status(200).json({ msg: "Add Module is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async getModule(req, res) {
        const moduleId = req.params.id;
        try {
            const module = await Module.findOne({
                where: {
                    id: moduleId,
                },
            });
            res.status(200).json(module);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async updateModule(req, res) {
        const moduleId = req.params.id;
        const {
            module_code,
            module_name,
            icon,
            order_no,
            url,
            is_parent,
            is_trx,
            module_app_id,
            updatedBy
        } = req.body;

        //cek apakah user sdh ada
        const module = await Module.findAll({
            where: {
                id: moduleId,
            },
        });

        if (!module[0]) return res.status(400).json({ msg: "Module is not exist" });


        try {
            await Module.update(
                {
                    module_code: module_code,
                    module_name: module_name,
                    icon: icon,
                    order_no: order_no,
                    url: url,
                    is_parent: is_parent,
                    is_trx: is_trx,
                    module_app_id: module_app_id,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedBy
                },
                {
                    where: {
                        id: moduleId,
                    },
                }
            );

            res.json({ msg: "Update Module is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

    static async deleteModule(req, res) {
        const moduleId = req.params.id;
        //cek apakah user sdh ada
        const module = await Module.findAll({
            where: {
                id: moduleId,
            },
        });

        if (!module[0]) return res.status(400).json({ msg: "Module is not exist" });

        try {
            await Module.destroy({
                where: {
                    id: moduleId,
                },
            });

            res.json({ msg: "Delete Module is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error: error });
        }
    }

}

module.exports = ModuleController;
