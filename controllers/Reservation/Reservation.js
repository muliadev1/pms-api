const {
  Reservation,
  Reservation_Detail,
  Reservation_Guest,
  Rate_code,
  Guest,
  RateCode_roomrate,
  RateCode_surcharge,
  Ratecode_promo,
  RoomCalendar,
} = require("../../db/models/index");
const Sequelize = require("sequelize");
const { Op } = require('sequelize');
class ReservationController {
    static async createReservationBulk(req, res, next) {
        const {
          room_data,
          reservation_status_id,
          guest_id,
          adult,
          child,
          check_in,
          check_out,
          deposit,
          cut_off_date,
          agent_it,
          remarks,
          payment_type_id,
          payment_detail_json,
          hotel_special_intruction,
          reservation_comments,
          updatedby,
          guest_data,
        } = req.body;
    
        try {
    
          // Menghitung totalnight
          const check_inDate = new Date(check_in);
          const check_outDate = new Date(check_out);
          const timeDifference = check_outDate.getTime() - check_inDate.getTime();
          const Totalnight = Math.ceil(timeDifference / (1000 * 3600 * 24));
    
          // Buat reservation
          const reservation = await Reservation.create({
            reservation_no: `RES-${new Date().getFullYear()}${(new Date().getMonth() + 1).toString().padStart(2, '0')}${new Date().getDate().toString().padStart(2, '0')}-${Math.floor(1000 + Math.random() * 9000)}`,
            reservation_date: new Date().toISOString(),
            reservation_status_id: reservation_status_id,
            guest_id: guest_id,
            adult: adult,
            child: child,
            check_in: check_in,
            check_out: check_out,
            duration: Totalnight,
            deposit: deposit,
            cut_off_date: cut_off_date === "" || cut_off_date === null ? null : cut_off_date,
            agent_it: agent_it,
            remarks: remarks,
            payment_type_id,
            payment_detail_json: JSON.stringify(payment_detail_json),
            hotel_special_intruction,
            reservation_comments,
            createdAt: Sequelize.fn("NOW"),
            createdBy: updatedby,
            updatedAt: Sequelize.fn("NOW"),
            updatedBy: updatedby,
          });
    
          const reservationdetail = await Promise.all(
            room_data.map(async (detailData) => {
              const rateCode = await Rate_code.findByPk(detailData.ratecode_id, {
                include: [
                  'ratecode_element',
                  {
                    model: RateCode_surcharge,
                    as: 'surcharge_ratecode',
                  },
                ],
              });
              if (!rateCode) {
                throw new Error('Rate code not found');
              }
              const rateCodeRoomRates = await RateCode_roomrate.findAll({
                where: { rate_code_id: detailData.ratecode_id },
                include: ['roomtype'],
              });
              const promorateCodes = await Ratecode_promo.findAll({
                where: { id_ratecode: detailData.ratecode_id },
                include: ['promo'],
              });
              const promoJSON = promorateCodes.map((promocode) => promocode.toJSON());
              const rateCodeRoomRatesJSON = rateCodeRoomRates.map((rateCodeRoomRate) =>
                rateCodeRoomRate.toJSON()
              );
              rateCode.dataValues.ratecode_roomtype = rateCodeRoomRatesJSON;
              rateCode.dataValues.promocode = promoJSON;
              return {
                ratecode_json: JSON.stringify(rateCode),
                reservation_id: reservation.id,
                night_no: Totalnight,
                room_type_id: detailData.room_type_id,
                room_id: detailData.room_id,
                per_night_price: detailData.per_night_price,
                roomtype_name: detailData.roomtype_name,
                adult_roomtype: detailData.adult_roomtype,
                child_roomtype: detailData.child_roomtype,
                is_extrabed_roomtype: detailData.is_extrabed_roomtype,
                hotel_id: detailData.hotel_id,
                promo_json: JSON.stringify(promorateCodes),
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
              };
            })
          );
    
          if (guest_data) {
            const guests = await Guest.bulkCreate(
              guest_data.map((guest) => ({
                saluation: guest.saluation,
                firstname: guest.firstname,
                lastname: guest.lastname,
                address: guest.address,
                country: guest.country,
                state: guest.state,
                zipcode: guest.zipcode,
                email: guest.email,
                phone: guest.phone,
                gender: guest.gender,
                birthday: guest.birthday,
                idtype: guest.idtype,
                idnumber: guest.idnumber,
                description: guest.description,
                is_active: guest.is_active,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
              }))
            );
            const guestIds = guests.map((guest) => guest.id);
            const existingGuests = await Reservation_Guest.findAll({
              where: {
                reservation_id: reservation.id,
              },
              attributes: ['guest_id'],
              group: ['guest_id'],
            });
            const guestIdsToCreate = guestIds.filter(
              (guestId) => !existingGuests.some((existingGuest) => existingGuest.guest_id === guestId)
            );
            await Reservation_Guest.bulkCreate(
              guestIdsToCreate.map((guestId) => ({
                reservation_id: reservation.id,
                guest_id: guestId,
                createdAt: Sequelize.fn("NOW"),
                createdBy: updatedby,
                updatedAt: Sequelize.fn("NOW"),
                updatedBy: updatedby,
              }))
            );
          }
    
          await Reservation_Detail.bulkCreate(reservationdetail);
    
          res
            .status(200)
            .json({
              msg: "Add Reservation, Fo Trx, Master Bill, and Fo Trx Detail are successfully",
            });
        } catch (error) {
          res.status(500).json({ messege: "Internal Server Error", error: error.message });
        }
      }

      static async updateReservation(req, res) {
        const reservationId = req.params.id;
        const {
          reservation_status_id,
          guest_id,
          adult,
          child,
          check_in,
          check_out,
          deposit,
          cut_off_date,
          agent_it,
          remarks,
          hotel_special_intruction,
          reservation_comments,
          updatedby,
        } = req.body;
  
        try {
          const [updatedCount] = await Reservation.update(
            {
              reservation_status_id,
              guest_id,
              adult,
              child,
              check_in,
              check_out,
              duration: Math.ceil((new Date(check_out) - new Date(check_in)) / (1000 * 3600 * 24)),
              deposit,
              cut_off_date: cut_off_date === "" || cut_off_date === null ? null : cut_off_date,
              agent_it,
              remarks,
              hotel_special_intruction,
              reservation_comments,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby,
            },
            {
              where: {
                id: reservationId,
              },
            }
          );
  
          if (updatedCount === 0) {
            return res.status(404).json({ msg: 'Reservation not found' });
          }
  
          res.json({ msg: 'Reservation updated successfully' });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error: error.message });
        }
      }
      

      static async getReservations(req, res, next) {
        try {
          const reservations = await Reservation.findAll({
            attributes: [
              'id',
              'reservation_no',
              'reservation_date',
              'is_group',
              'parent_group_id',
              'reservation_status_id',
              'guest_id',
              'adult',
              'child',
              'check_in',
              'check_out',
              'duration',
              'purpose_id',
              'source_id',
              'segment_id',
              'is_vip',
              'company_id',
              'deposit',
              'cut_off_date',
              'agent_it',
              'remarks',
              [Sequelize.literal('JSON_UNQUOTE(payment_detail_json)'), 'payment_detail_json'],
              'hotel_special_intruction',
              'reservation_comments',
              'createdBy',
              'updatedBy',
              'createdAt',
              'updatedAt',
              'vip_id',
              'payment_type_id',
            ],
            order: [['createdAt', 'DESC']],
            include: [
              {
                model: Reservation_Detail,
                as: 'details',
                attributes: [
                  'reservation_id',
                  'ratecode_json',
                  'room_type_id',
                  'promo_json',
                  'per_night_price',
                  'roomtype_name',
                  'hotel_id'
                  // Add other attributes needed from Reservation_Detail
                ],
              },
              {
                model: Reservation_Guest,
                as: 'reservationguests',
                include:['guest'],
              },
            ],
          });
      
          // Proses data Reservation menjadi bentuk JSON yang sesuai
          const formattedReservations = reservations.map(reservation => {
            return {
              ...reservation.toJSON(),
              payment_detail_json: JSON.parse(reservation.get('payment_detail_json')),
              details: reservation.details.map(detail => ({
                reservation_id: detail.reservation_id,
                ratecode_json: JSON.parse(detail.ratecode_json),
                room_type_id:detail.room_type_id,
                  promo_json:JSON.parse(detail.promo_json),
                  per_night_price:detail.per_night_price,
                  roomtype_name: detail.roomtype_name,
                  hotel_id: detail.hotel_id
                // Add other attributes from Reservation_Detail as needed
              })),
            };
          });
      
          res.status(200).json(formattedReservations);
        } catch (error) {
          next(error);
        }
      }

      static async getReservationById(req, res, next) {
        const resId = req.params.id;
        try {
          const reservations = await Reservation.findAll({
            attributes: [
              'id',
              'reservation_no',
              'reservation_date',
              'is_group',
              'parent_group_id',
              'reservation_status_id',
              'guest_id',
              'adult',
              'child',
              'check_in',
              'check_out',
              'duration',
              'purpose_id',
              'source_id',
              'segment_id',
              'is_vip',
              'company_id',
              'deposit',
              'cut_off_date',
              'agent_it',
              'remarks',
              [Sequelize.literal('JSON_UNQUOTE(payment_detail_json)'), 'payment_detail_json'],
              'hotel_special_intruction',
              'reservation_comments',
              'createdBy',
              'updatedBy',
              'createdAt',
              'updatedAt',
              'vip_id',
              'payment_type_id',
            ],
            order: [['createdAt', 'DESC']],
            include: [
              {
                model: Reservation_Detail,
                as: 'details',
                attributes: [
                  'reservation_id',
                  'ratecode_json',
                  'room_type_id',
                  'promo_json',
                  'per_night_price',
                  'roomtype_name',
                  // Add other attributes needed from Reservation_Detail
                ],
              },
              {
                model: Reservation_Guest,
                as: 'reservationguests',
                include:['guest'],
              },
            ],
            where:{
                id:resId,
            }
          });
      
          // Proses data Reservation menjadi bentuk JSON yang sesuai
          const formattedReservations = reservations.map(reservation => {
            return {
              ...reservation.toJSON(),
              payment_detail_json: JSON.parse(reservation.get('payment_detail_json')),
              details: reservation.details.map(detail => ({
                reservation_id: detail.reservation_id,
                ratecode_json: JSON.parse(detail.ratecode_json),
                room_type_id:detail.room_type_id,
                  promo_json:JSON.parse(detail.promo_json),
                  per_night_price:detail.per_night_price,
                  roomtype_name: detail.roomtype_name,
                // Add other attributes from Reservation_Detail as needed
              })),
            };
          });
      
          res.status(200).json(formattedReservations);
        } catch (error) {
          next(error);
        }
      }

      static async getReservationsDay(req, res, next) {
        const { day } = req.query;
      
        try {
          // Validasi input `day`
          if (!day) {
            return res.status(400).json({ error: 'Parameter day is required' });
          }
      
          // Ambil semua data dari database
          const reservations = await Reservation.findAll({
            include: [
              {
                model: RoomCalendar,
                as: 'roomcalendar',
                include: ['room'
                ]
              },
              {
                model: Reservation_Guest,
                as: 'reservationguests',
                include:['guest'],
              },
              {
                model: Reservation_Detail,
                as: 'details',
                attributes: [
                  'reservation_id',
                  'room_type_id',
                  'per_night_price',
                  'roomtype_name',
                  // Add other attributes needed from Reservation_Detail
                ],
              },
            ],
            where: {
              check_in: {
                [Op.gte]: new Date(day) // Pastikan `day` adalah tanggal valid
              }
            }
          });
      
              // Format data
          const formattedData = reservations.map(reservation => {
            // Ambil data `RoomCalendar` dan kelompokkan berdasarkan `room_id`
            const roomCalendars = reservation.roomcalendar.map(roomCalendar => ({
              id: roomCalendar.id,
              reservation_id: roomCalendar.reservation_id,
              room_id: roomCalendar.room_id, // Anggap bahwa `room` adalah objek dengan `id`
              room: roomCalendar.room?.room_number, // Anda dapat menambahkan detail lebih lanjut tentang room
            }));

            // Hapus duplikasi berdasarkan `room_id`
            const uniqueRoomCalendars = Array.from(new Map(
              roomCalendars.map(item => [item.room_id, item])
            ).values());

      
            return {
              id: reservation.id,
              check_in: reservation.check_in,
              check_out: reservation.check_out,
              reservation_no : reservation.reservation_no,
              adult : reservation.adult,
              child : reservation.child,
              duration: reservation.duration,
              reservation_date: reservation.reservation_date,
              cut_off_date: reservation.cut_off_date,
              roomCalendars: uniqueRoomCalendars,
              reservationGuests: reservation.reservationguests,
              detail:reservation.details,
            };
          });
      
          res.status(200).json(formattedData);
        } catch (error) {
          console.error('Error in getReservationsDay:', error); // Log error untuk debugging
          next(error); // Teruskan error ke middleware berikutnya
        }
      }
      
}

module.exports = ReservationController;
