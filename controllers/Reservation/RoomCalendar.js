const {
  Calendar,
  Room,
  RoomType,
  RoomCalendar,
  Reservation,
  Reservation_Detail,
  HouseKeeping,
  RoomRate,
  BookingRoom,
} = require("../../db/models/index");
const Sequelize = require("sequelize");
const { Op } = Sequelize;
class RoomCalendarController {
    static async getRoomShowReservation(req, res) {
        const { hotel_id, room_type_id } = req.query;
    
        try {
            let whereClause = {
                hotel_id: hotel_id,
                room_type_id: room_type_id,
                checkin: {
                    [Op.gte]: new Date(new Date().toISOString().split('T')[0]),
                },
            };
    
            const roomCalendars = await RoomCalendar.findAll({
                include: [
                    {
                        model: Reservation,
                        as: "reservation",
                        include: [
                            'guest',
                            {
                                model: RoomCalendar,
                                as: 'roomcalendar',
                                include: ['room'],
                            },
                            {
                                model: Reservation_Detail,
                                as: 'details',
                                include: ['hotel'],
                                attributes: [
                                  'roomtype_name',
                                ],
                              },
                        ],
                    },
                ],
                where: whereClause,
            });
    
            const dataroomtype = await RoomType.findAll({
                where: { id: room_type_id, hotel_id: hotel_id },
                include: ['room'],
            });
    
            // Ambil data `RoomCalendar` dan kelompokkan berdasarkan `room_id`
            const roomCalendarsByRoom = roomCalendars.map(roomCalendar => ({
                id: roomCalendar.id,
                hotel_id: roomCalendar.hotel_id,
                reservation_id: roomCalendar.reservation_id,
                room_id: roomCalendar.room_id,
                room_number: roomCalendar.room?.room_number,
                reservation: roomCalendar.reservation // Simpan objek reservation di sini
            }));
    
            // Hapus duplikasi berdasarkan hotel_id, room_type_id, room_id
            const uniqueRoomCalendars = Array.from(
                new Map(
                    roomCalendarsByRoom.map(item => [
                        `${item.hotel_id}-${item.room_id}-${item.reservation_id}`,
                        item,
                    ])
                ).values()
            );
    
            // Gabungkan data berdasarkan room type
            const result = dataroomtype.map(roomType => {
                // Ambil room terkait dengan room type
                const rooms = roomType.room.map(room => {
                    // Filter reservation untuk room tertentu
                    const relatedReservations = uniqueRoomCalendars.filter(
                        calendar => calendar.room_id === room.id
                    );
    
                    // Hitung jumlah reservation untuk setiap room
                    const reservationsCount = relatedReservations.length;
    
                    return {
                        id: room.id,
                        room_number: room.room_number,
                        reservations_count: reservationsCount, // Jumlah reservation
                        reservations: relatedReservations.map(res => {
                            // Ambil data roomCalendars dari reservation
                            const roomCalendars = res.reservation?.roomcalendar.map(roomCalendar => ({
                                id: roomCalendar.id,
                                reservation_id: roomCalendar.reservation_id,
                                room_id: roomCalendar.room_id,
                                room: roomCalendar.room?.room_number,
                            }));
    
                            // Hapus duplikasi berdasarkan `room_id`
                            const uniqueRoomCalendars = Array.from(new Map(
                                roomCalendars.map(item => [item.room_id, item])
                            ).values());
    
                            return {
                                id: res.reservation?.id,
                                check_in: res.reservation?.check_in,
                                check_out: res.reservation?.check_out,
                                reservation_no: res.reservation?.reservation_no,
                                adult: res.reservation?.adult,
                                child: res.reservation?.child,
                                duration: res.reservation?.duration,
                                reservation_date: res.reservation?.reservation_date,
                                cut_off_date: res.reservation?.cut_off_date,
                                roomCalendars: uniqueRoomCalendars, // Include unique roomCalendars
                                guest: {
                                    saluation: res.reservation?.guest?.saluation,
                                    firstname: res.reservation?.guest?.firstname,
                                    lastname: res.reservation?.guest?.lastname,
                                },
                                details: res.reservation?.details?.map(detail => ({
                                    room_type_name: detail.roomtype_name,
                                    hotel_name: detail.hotel?.title,
                                })),
                            };
                        }),
                    };
                });
    
                return {
                    room_type_id: roomType.id,
                    room_type_name: roomType.roomtype_name, // Misalkan ada nama room type
                    rooms: rooms, // Menyertakan daftar room dengan jumlah reservation dan reservation_ids
                };
            });
    
            res.status(200).json(result);
        } catch (error) {
            res.status(500).json({ message: "Internal Server Error", error: error });
        }
    }
 
    static async getRoomForHousekeeping(req, res) {
      const { hotel_id, room_type_id } = req.query;
  
      try {
          let whereClause = {
              hotel_id: hotel_id,
              room_type_id: room_type_id,
              checkin:  new Date(new Date().toISOString().split('T')[0]),
          };
  
          const roomCalendars = await RoomCalendar.findAll({
              include: [
                  {
                      model: Reservation,
                      as: "reservation",
                      include: [
                          'guest',
                          {
                              model: RoomCalendar,
                              as: 'roomcalendar',
                              include: ['room'],
                          },
                          {
                              model: Reservation_Detail,
                              as: 'details',
                              include: ['hotel'],
                              attributes: [
                                'roomtype_name',
                              ],
                            },
                      ],
                  },
              ],
              where: whereClause,
          });
  
          const dataroomtype = await RoomType.findAll({
              where: { id: room_type_id, hotel_id: hotel_id },
              include: ['room'],
          });
  
          // Ambil data `RoomCalendar` dan kelompokkan berdasarkan `room_id`
          const roomCalendarsByRoom = roomCalendars.map(roomCalendar => ({
              id: roomCalendar.id,
              hotel_id: roomCalendar.hotel_id,
              reservation_id: roomCalendar.reservation_id,
              room_id: roomCalendar.room_id,
              room_number: roomCalendar.room?.room_number,
              reservation: roomCalendar.reservation // Simpan objek reservation di sini
          }));
  
          // Hapus duplikasi berdasarkan hotel_id, room_type_id, room_id
          const uniqueRoomCalendars = Array.from(
              new Map(
                  roomCalendarsByRoom.map(item => [
                      `${item.hotel_id}-${item.room_id}-${item.reservation_id}`,
                      item,
                  ])
              ).values()
          );
  
          // Gabungkan data berdasarkan room type
          const result = dataroomtype.map(roomType => {
              // Ambil room terkait dengan room type
              const rooms = roomType.room.map(room => {
                  // Filter reservation untuk room tertentu
                  const relatedReservations = uniqueRoomCalendars.filter(
                      calendar => calendar.room_id === room.id
                  );
  
                  // Hitung jumlah reservation untuk setiap room
                  const reservationsCount = relatedReservations.length;
  
                  return {
                      id: room.id,
                      room_number: room.room_number,
                      reservations_count: reservationsCount, // Jumlah reservation
                      reservations: relatedReservations.map(res => {
                          // Ambil data roomCalendars dari reservation
                          const roomCalendars = res.reservation?.roomcalendar.map(roomCalendar => ({
                              id: roomCalendar.id,
                              reservation_id: roomCalendar.reservation_id,
                              room_id: roomCalendar.room_id,
                              room: roomCalendar.room?.room_number,
                          }));
  
                          // Hapus duplikasi berdasarkan `room_id`
                          const uniqueRoomCalendars = Array.from(new Map(
                              roomCalendars.map(item => [item.room_id, item])
                          ).values());
  
                          return {
                              id: res.reservation?.id,
                              check_in: res.reservation?.check_in,
                              check_out: res.reservation?.check_out,
                              reservation_no: res.reservation?.reservation_no,
                              adult: res.reservation?.adult,
                              child: res.reservation?.child,
                              duration: res.reservation?.duration,
                              reservation_date: res.reservation?.reservation_date,
                              cut_off_date: res.reservation?.cut_off_date,
                              roomCalendars: uniqueRoomCalendars, // Include unique roomCalendars
                              guest: {
                                  saluation: res.reservation?.guest?.saluation,
                                  firstname: res.reservation?.guest?.firstname,
                                  lastname: res.reservation?.guest?.lastname,
                              },
                              details: res.reservation?.details?.map(detail => ({
                                  room_type_name: detail.roomtype_name,
                                  hotel_name: detail.hotel?.title,
                              })),
                          };
                      }),
                  };
              });
  
              return {
                  room_type_id: roomType.id,
                  room_type_name: roomType.roomtype_name, // Misalkan ada nama room type
                  rooms: rooms, // Menyertakan daftar room dengan jumlah reservation dan reservation_ids
                  today:new Date(),
              };
          });
  
          res.status(200).json(result);
      } catch (error) {
          res.status(500).json({ message: "Internal Server Error", error: error });
      }
  }
    
    
  static async loadDatesCalenadar(req, res) {
    try {
      const { start, end, hotelid, roomid } = req.query;
      const fromDate = new Date(start);
      const toDate = new Date(end);
      const data = [];

      for (
        let currentDate = new Date(fromDate);
        currentDate <= toDate;
        currentDate.setDate(currentDate.getDate() + 1)
      ) {
        const dateStr = currentDate.toISOString().split("T")[0];

        const calendar = await RoomCalendar.findOne({
          include: [
            {
              model: Reservation,
              as: "reservation",
              include: ["guest"],
              required: false,
            },
            "room",
          ],
          where: {
            room_id: roomid,
            hotel_id: hotelid,
            start_date: dateStr + "T00:00:00Z",
          },
        });

        let color = "#0077b6";

        const housekeeping = await HouseKeeping.findOne({
          include:['room',"room_status"],
          where: {
            room_id: roomid,
            assigned_date: dateStr + "T00:00:00Z",
          }
        });

        if (housekeeping?.room_status_id >= 1 && housekeeping?.room_status_id <= 6) {
          switch (housekeeping?.room_status_id) {
            case 1:
              color = "#e1ded5";
              break;
            case 2:
              color = "#b5c6e0";
              break;
            case 3:
              color = "#d4a373";
              break;
            case 4:
              color = "#a3d5ff";
              break;
            case 5:
              color = "#ffe799";
              break;
            case 6:
              color = "#80ff72";
              break;
            default:
              color = "#0077b6";
              break;
          }
        }
        

        if (calendar) {
          let calendarcolor = color; // Default color is green

          // Override default color based on conditions
          if (calendar.status == 2) {
            calendarcolor = "#e63946"; // Red color if allotment is 0
          } else if (calendar.status == 3) {
            calendarcolor = "#bc3908"; // Orange color if no checkin but checkout allowed
          } 
          

          const reservations = await Reservation.findAll({
            include: [
              {
                model: RoomCalendar,
                as: 'roomcalendar',
                include: ['room'
                ]
              },
            ],
            where: {
              id: calendar.reservation_id,
            }
          });

              // Format data
              const formattedData = reservations.map(reservation => {
                // Ambil data `RoomCalendar` dan kelompokkan berdasarkan `room_id`
                const roomCalendars = reservation.roomcalendar.map(roomCalendar => ({
                  id: roomCalendar.id,
                  reservation_id: roomCalendar.reservation_id,
                  room_id: roomCalendar.room_id, // Anggap bahwa `room` adalah objek dengan `id`
                  room: roomCalendar.room?.room_number, // Anda dapat menambahkan detail lebih lanjut tentang room
                }));
    
                // Hapus duplikasi berdasarkan `room_id`
                const uniqueRoomCalendars = Array.from(new Map(
                  roomCalendars.map(item => [item.room_id, item])
                ).values());
    
          
                return uniqueRoomCalendars;
              });
          
          // Add data to the array
          data.push({
            title: calendar.reservation?.guest
              ? calendar.reservation?.guest?.firstname +
                " " +
                calendar.reservation?.guest?.lastname
              : "Reserve",
            start: dateStr,
            end: new Date(currentDate.getTime() + 24 * 60 * 60 * 1000)
              .toISOString()
              .split("T")[0],
            color: calendarcolor,
            reservation: calendar.reservation,
            room: calendar.room,
            roomlist:formattedData[0],
            housekeeping:housekeeping,
          });
        } else {
          data.push({
            title: housekeeping ? housekeeping.room_status?.room_status_name : "VCI",
            date: dateStr,
            color: color,
            housekeeping: housekeeping,
          });
        }
      }

      res.json(data);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Internal Server Error", error: error });
    }
  }

  static async createAutoRoomCalendar(req, res) {
    const { reservasi_id, start_date, status, end_date, updatedby } = req.body;
    const startDate = new Date(start_date);
    let endDate = new Date(end_date);

    // Kurangi satu hari dari endDate
    endDate.setDate(endDate.getDate() - 1);

    // Langkah 1: Ambil semua reservation_detail berdasarkan reservasi_id
    let reservations = await Reservation_Detail.findAll({
      where: { reservation_id: reservasi_id },
      attributes: ["room_type_id", "hotel_id"],
    });

    if (reservations.length === 0) {
      return res
        .status(404)
        .json({ message: "No reservations found for the given ID" });
    }

    // Untuk menyimpan room yang sudah diproses untuk masing-masing room_type_id
    let processedRooms = new Map();

    // Langkah 2: Proses setiap reservation
    for (let reservation of reservations) {
      const { hotel_id, room_type_id } = reservation;

      // Langkah 3: Cari RoomCalendar yang ada dengan room_type_id, hotel_id, start_date, dan end_date
      let roomCalendars = await RoomCalendar.findAll({
        where: {
          hotel_id,
          room_type_id,
          start_date: { [Op.gte]: startDate.toISOString().split("T")[0] }, // Format YYYY-MM-DD
          end_date: { [Op.lte]: endDate.toISOString().split("T")[0] },
        },
      });

      // Langkah 4: Temukan room_id yang valid dan tidak ada di RoomCalendar
      let existingRoomIds = new Set(
        roomCalendars.map((calendar) => calendar.room_id)
      );

      // Hitung jumlah room yang dibutuhkan untuk room_type_id
      let neededRoomsCount = reservations.filter(
        (res) => res.room_type_id === room_type_id
      ).length;

      // Pastikan hanya mengambil room yang belum ada di RoomCalendar
      if (!processedRooms.has(room_type_id)) {
        // Temukan room yang belum ada di RoomCalendar dan sesuai jumlah yang dibutuhkan
        let availableRooms = await Room.findAll({
          where: {
            roomtype_id: room_type_id,
            id: { [Op.notIn]: Array.from(existingRoomIds) },
          },
          order: [["id", "ASC"]], // Mengambil room yang pertama berdasarkan ID (atau sesuai kriteria Anda)
        });

        if (availableRooms.length === 0) {
          return res
            .status(404)
            .json({ message: "No available rooms found for this room type" });
        }

        // Pilih room berdasarkan jumlah yang dibutuhkan
        let roomsToProcess = availableRooms.slice(0, neededRoomsCount);

        // Simpan room_id yang diproses
        processedRooms.set(
          room_type_id,
          roomsToProcess.map((room) => room.id)
        );

        // Langkah 5: Input data ke RoomCalendar untuk room_id yang tersedia
        for (let room of roomsToProcess) {
          let currentDate = new Date(startDate);

          while (currentDate <= endDate) {
            const year = currentDate.getFullYear();
            const month = (currentDate.getMonth() + 1)
              .toString()
              .padStart(2, "0");
            const day = currentDate.getDate().toString().padStart(2, "0");
            const currentDateStr = `${year}-${month}-${day}`;

            let room_id = room.id;

            let roomcalendar = await RoomCalendar.findOne({
              where: {
                hotel_id,
                room_id,
                start_date: currentDateStr + "T00:00:00Z",
              },
            });

            if (!roomcalendar) {
              roomcalendar = await RoomCalendar.create({
                hotel_id,
                room_id,
                reservation_id: reservasi_id,
                room_type_id,
                checkin: start_date,
                checkout: end_date,
                start_date: currentDateStr,
                end_date: currentDateStr,
                status,
                createdAt: Sequelize.fn("NOW"),
                createdBy: updatedby,
                updatedAt: Sequelize.fn("NOW"),
                updatedBy: updatedby,
              });
            } else {
              if (roomcalendar.reservation_id === reservasi_id) {
                // Jika sudah ada reservasi untuk tanggal tersebut
                res
                  .status(200)
                  .json({
                    message: "Reservation already exists for some dates",
                  });
                return;
              }
              // Update status dan informasi lainnya jika ada data
              roomcalendar.status = status;
              roomcalendar.reservation_id = reservasi_id;
              roomcalendar.room_type_id = room_type_id;
              roomcalendar.createdAt = Sequelize.fn("NOW");
              roomcalendar.createdBy = updatedby;
              roomcalendar.updatedAt = Sequelize.fn("NOW");
              roomcalendar.updatedBy = updatedby;
              await roomcalendar.save();
            }

            currentDate.setDate(currentDate.getDate() + 1);
          }
        }
      }
    }

    res
      .status(200)
      .json({ message: "Room calendar created/updated successfully" });
  }

  static async createRoomCalendar(req, res, next) {
    try {
      const {
        hotel_id,
        room_id,
        reservasi_id,
        room_type_id,
        start_date,
        status,
        end_date,
        updatedby,
      } = req.body;

      const startDate = new Date(start_date);
      let endDate = new Date(end_date);

      // Kurangi satu hari dari endDate
      endDate.setDate(endDate.getDate() - 1);

      let currentDate = new Date(startDate);

      while (currentDate <= endDate) {
        const year = currentDate.getFullYear();
        const month = (currentDate.getMonth() + 1).toString().padStart(2, "0");
        const day = currentDate.getDate().toString().padStart(2, "0");
        const currentDateStr = `${year}-${month}-${day}`;

        let roomcalendar = await RoomCalendar.findOne({
          where: {
            hotel_id,
            room_id,
            start_date: currentDateStr + "T00:00:00Z",
          },
        });

        if (!roomcalendar) {
          roomcalendar = await RoomCalendar.create({
            hotel_id,
            room_id,
            reservation_id: reservasi_id,
            room_type_id,
            checkin: start_date,
            checkout: end_date,
            start_date: currentDateStr,
            end_date: currentDateStr,
            status,
            createdAt: Sequelize.fn("NOW"),
            createdBy: updatedby,
            updatedAt: Sequelize.fn("NOW"),
            updatedBy: updatedby,
          });
        } else {
          if (roomcalendar.reservation_id === reservasi_id) {
            res.status(200).json({ message: "Reservation already exist" });
            return;
          }
          roomcalendar.status = status;
          roomcalendar.reservation_id = reservasi_id;
          roomcalendar.room_type_id = room_type_id;
          roomcalendar.createdAt = Sequelize.fn("NOW");
          roomcalendar.createdBy = updatedby;
          roomcalendar.updatedAt = Sequelize.fn("NOW");
          roomcalendar.updatedBy = updatedby;
          await roomcalendar.save();
        }

        currentDate.setDate(currentDate.getDate() + 1);
      }

      await Reservation.update(
        {
          reservation_status_id: 4,
        },
        {
          where: {
            id: reservasi_id,
          },
        }
      );

      res.status(200).json({ message: "Data saved successfully" });
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .json({ message: "Internal Server Error", error: error.message });
    }
  }

  static async deleteCalendar(req, res) {
    const { start, hotel_id, room_id } = req.query;

    const startt = new Date(start);
    const year = startt.getFullYear();
    const month = (startt.getMonth() + 1).toString().padStart(2, "0");
    const day = startt.getDate().toString().padStart(2, "0");
    const startStr = `${year}-${month}-${day}`;

    const data = await RoomCalendar.findAll({
      where: {
        hotel_id,
        room_id,
        start_date: startStr + "T00:00:00Z",
      },
    });

    if (!data[0]) return res.status(400).json({ msg: "Add on is not exist" });

    try {
      for (let dat of data) {
        await RoomCalendar.destroy({
          where: {
            hotel_id,
            room_id,
            reservation_id: dat.reservation_id,
          },
        });
      }

      res.json({ msg: "Delete Add on is successfully" });
    } catch (error) {
      res
        .status(500)
        .json({ messege: "Internal Server Error", error: error.message });
    }
  }
}

module.exports = RoomCalendarController;
