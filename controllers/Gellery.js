const { Gallery } = require("../db/models/index");
const Sequelize = require('sequelize');

class GalleryController {
    static async getGallery(req, res, next) {
        try {
          const data = await Gallery.findAll({
           
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getGalleryDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Gallery.findOne({
           
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createGallery(req, res, next) {
        const {
          gallery_name,
          is_active,
          updatedby
        } = req.body;
    
        try {
          await Gallery.create({
            gallery_name,
            is_active,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Gallery is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateGallery(req, res) {
        const id = req.params.id;
        const {
          gallery_name,
          is_active,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Gallery.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Gallery is not exist" });
    
    
        try {
          await Gallery.update(
            {
              gallery_name,
              is_active,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Gallery is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteGallery(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Gallery.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Gallery is not exist" });
    
        try {
          await Gallery.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Gallery is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = GalleryController;