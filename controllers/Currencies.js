const { Currencies } = require("../db/models/index");
const Sequelize = require('sequelize');

class CurrenciesController {
    static async getCurrencies(req, res, next) {
        try {
          const data = await Currencies.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getCurrenciesDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Currencies.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createCurrencies(req, res, next) {
        const {
            curr_code,
            curr_symbol,
            is_active,
            updatedby
        } = req.body;

        try {
          await Currencies.create({
            curr_code,
            curr_symbol,
            is_active,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Currencies is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateCurrencies(req, res) {
        const id = req.params.id;
        const {
            curr_code,
            curr_symbol,
            is_active,
            updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await Currencies.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Currencies is not exist" });
    
    
        try {
          await Currencies.update(
            {
                curr_code,
                curr_symbol,
                is_active,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Currencies is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteCurrencies(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Currencies.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Currencies is not exist" });
    
        try {
          await Currencies.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Currencies is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = CurrenciesController;