const { Vendor } = require("../db/models/index");
const Sequelize = require('sequelize');

class VendorController {
  static async getVendors_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const vendors = await Vendor.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Vendor.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          vendors: vendors,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getVendors(req, res, next) {
    try {
      const vendors = await Vendor.findAll({
      });
      res.status(200).json(vendors);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createVendor(req, res, next) {
    const {
      vendor_name,
      address,
      phone,
      email,
      contact,
      is_active,
      updatedby
    } = req.body;

    try {
      await Vendor.create({
        vendor_name: vendor_name,
        address: address,
        phone: phone,
        email: email,
        contact: contact,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Vendor is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getVendor(req, res) {
    const vendorId = req.params.id;
    try {
      const vendor = await Vendor.findOne({
        where: {
          id: vendorId,
        },
      });
      res.status(200).json(vendor);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateVendor(req, res) {
    const vendorId = req.params.id;
    const {
      vendor_name,
      address,
      phone,
      email,
      contact,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const vendor = await Vendor.findAll({
      where: {
        id: vendorId,
      },
    });

    if (!vendor[0]) return res.status(400).json({ msg: "Vendor is not exist" });


    try {
      await Vendor.update(
        {
          vendor_name: vendor_name,
          address: address,
          phone: phone,
          email: email,
          contact: contact,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: vendorId,
          },
        }
      );

      res.json({ msg: "Update Vendor is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteVendor(req, res) {
    const vendorId = req.params.id;
    //cek apakah user sdh ada
    const vendor = await Vendor.findAll({
      where: {
        id: vendorId,
      },
    });

    if (!vendor[0]) return res.status(400).json({ msg: "Vendor is not exist" });

    try {
      await Vendor.destroy({
        where: {
          id: vendorId,
        },
      });

      res.json({ msg: "Delete Vendor is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = VendorController;
