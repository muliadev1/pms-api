const { Purpose } = require("../db/models/index");
const Sequelize = require('sequelize');

class PurposeController {
  static async getPurposes_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const purposes = await Purpose.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Purpose.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          purposes: purposes,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPurposes(req, res, next) {
    try {
      const porpuses = await Purpose.findAll({
      });
      res.status(200).json(porpuses);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createPurpose(req, res, next) {
    const {
      purpose_name,
      is_active,
      updatedby
    } = req.body;

    try {
      await Purpose.create({
        purpose_name: purpose_name,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Purposes is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPurpose(req, res) {
    const purId = req.params.id;
    try {
      const purpose = await Purpose.findOne({
        where: {
          id: purId,
        },
      });
      res.status(200).json(purpose);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updatePurpose(req, res) {
    const purId = req.params.id;
    const {
      purpose_name,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const purpose = await Purpose.findAll({
      where: {
        id: purId,
      },
    });

    if (!purpose[0]) return res.status(400).json({ msg: "Purpose is not exist" });


    try {
      await Purpose.update(
        {
          purpose_name: purpose_name,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: purId,
          },
        }
      );

      res.json({ msg: "Update Purpose is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deletePurpose(req, res) {
    const purId = req.params.id;
    //cek apakah user sdh ada
    const purpose = await Purpose.findAll({
      where: {
        id: purId,
      },
    });

    if (!purpose[0]) return res.status(400).json({ msg: "Purpose is not exist" });

    try {
      await Purpose.destroy({
        where: {
          id: purId,
        },
      });

      res.json({ msg: "Delete Purpose is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = PurposeController;
