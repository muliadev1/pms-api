const { HistoryItemPrice, Vendor } = require("../db/models/index");
const Sequelize = require('sequelize');


class HistoryItemPriceController {
    static async getHistoryItemPrices_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const histories = await HistoryItemPrice.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await HistoryItemPrice.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    histories: histories,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getHistoryItemPrices(req, res, next) {
        try {
            const histories = await HistoryItemPrice.findAll({});
            res.status(200).json(histories);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createHistoryItemPrice(req, res, next) {
        const {
            item_code,
            price,
            new_price,
            history_date,
            vendor_id,
            price_hpp,
            receive_order_code,
            updatedby
        } = req.body;

        try {
            await HistoryItemPrice.create({
                item_code: item_code,
                price: price,
                new_price: new_price,
                history_date: history_date,
                vendor_id: vendor_id,
                price_hpp: price_hpp,
                receive_order_code: receive_order_code,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add History Item Price is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getHistoryItemPrice(req, res) {
        const historyId = req.params.id;
        try {
            const history = await HistoryItemPrice.findOne({
                where: {
                    id: historyId,
                },
            });
            res.status(200).json(history);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateHistoryItemPrice(req, res) {
        const historyId = req.params.id;
        const {
            item_code,
            price,
            new_price,
            history_date,
            vendor_id,
            price_hpp,
            receive_order_code,
            updatedby
        } = req.body;

        //cek apakah user sdh ada
        const history = await HistoryItemPrice.findAll({
            where: {
                id: historyId,
            },
        });

        if (!history[0]) return res.status(400).json({ msg: "History Item Price is not exist" });


        try {
            await HistoryItemPrice.update(
                {
                    item_code: item_code,
                    price: price,
                    new_price: new_price,
                    history_date: history_date,
                    vendor_id: vendor_id,
                    price_hpp: price_hpp,
                    receive_order_code: receive_order_code,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: historyId,
                    },
                }
            );

            res.json({ msg: "Update History Item Price is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteHistoryItemPrice(req, res) {
        const historyId = req.params.id;
        //cek apakah user sdh ada
        const history = await HistoryItemPrice.findAll({
            where: {
                id: historyId,
            },
        });

        if (!history[0]) return res.status(400).json({ msg: "History Item Price is not exist" });

        try {
            await HistoryItemPrice.destroy({
                where: {
                    id: historyId,
                },
            });

            res.json({ msg: "Delete History Item Price is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }
}

module.exports = HistoryItemPriceController;

