const { Blackout } = require("../db/models/index");
const Sequelize = require('sequelize');

class BlackoutController {
    static async getBlackout(req, res, next) {
        try {
          const data = await Blackout.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getBlackoutDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Blackout.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createBlackout(req, res, next) {
        const {
          date_from,
          date_to,
          updatedby
        } = req.body;

        try {
          await Blackout.create({
            date_from: date_from, // formatteddate_from + "T00:00:00Z",
            date_to: date_to,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Blackout is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateBlackout(req, res) {
        const id = req.params.id;
        const {
           date_from,
           date_to,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await Blackout.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Blackout is not exist" });
    
    
        try {
          await Blackout.update(
            {
              date_from:  date_from,
              date_to: date_to,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Blackout is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteBlackout(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Blackout.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Blackout is not exist" });
    
        try {
          await Blackout.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Blackout is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = BlackoutController;