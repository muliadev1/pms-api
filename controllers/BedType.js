const { BedType } = require("../db/models/index");
const Sequelize = require('sequelize');

class BedTypeController {
    static async getBedType(req, res, next) {
        try {
          const data = await BedType.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getBedTypeDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await BedType.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createBedType(req, res, next) {
        const {
          bed_type_name,
          updatedby
        } = req.body;
    
        try {
          await BedType.create({
            bed_type_name: bed_type_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add BedTypes is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateBedType(req, res) {
        const id = req.params.id;
        const {
           bed_type_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await BedType.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "BedTypes is not exist" });
    
    
        try {
          await BedType.update(
            {
              bed_type_name:  bed_type_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update BedTypes is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteBedType(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const paytype = await BedType.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "BedTypes is not exist" });
    
        try {
          await BedType.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete BedTypes is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = BedTypeController;