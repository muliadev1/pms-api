const { Role } = require("../db/models/index");
const Sequelize = require('sequelize');
const room = require("../db/models/room");
   
class RoleController {
    static async getRoles_pages(req, res, next) {
        let { page, pageSize } = req.query

        //! Calculate starting index and number of records to retrieve
        const startIndex = (Number(page) - 1) * Number(pageSize);
        const endIndex = Number(page) * Number(pageSize);
        try {
            const roles = await Role.findAll({
                offset: startIndex,
                limit: Number(pageSize)
            });
            //! Retrieve total count of records for pagination info
            const totalCount = await Role.count();
            //! Calculate total number of pages
            const totalPages = Math.ceil(totalCount / Number(pageSize));
            res.status(200).json(
                {
                    roles: roles,
                    totalRecords: totalCount,
                    currentPage: Number(page),
                    totalPages: totalPages
                });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getRoles(req, res, next) {
        const appId = req.params.appid;
        try {
            const roles = await Role.findAll({
                where: {
                    module_app_id: appId,
                },
            });
            res.status(200).json(roles);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async createRole(req, res, next) {
        const {
            role_name,
            role_description,
            is_active,
            module_app_id
        } = req.body;


        try {
            await Role.create({
                role_name: role_name,
                role_description: role_description,
                is_active: is_active,
                module_app_id: module_app_id,
                createdAt: Sequelize.fn('NOW'),
                updatedAt: Sequelize.fn('NOW')
            });
            res.status(200).json({ msg: "Add Role is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async getRole(req, res) {
        const roleId = req.params.id;
        try {
            const role = await Role.findOne({
                where: {
                    id: roleId,
                },
            });
            res.status(200).json(role);
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async updateRole(req, res) {
        const roleId = req.params.id;
        const {
            role_name,
            role_description,
            is_active,
            module_app_id,
        } = req.body;

        //cek apakah user sdh ada
        const role = await Role.findAll({
            where: {
                id: roleId,
            },
        });

        if (!role[0]) return res.status(400).json({ msg: "Role is not exist" });


        try {
            await Role.update(
                {
                    role_name: role_name,
                    role_description: role_description,
                    is_active: is_active,
                    module_app_id: module_app_id,
                    updatedAt: Sequelize.fn('NOW')
                },
                {
                    where: {
                        id: roleId,
                    },
                }
            );

            res.json({ msg: "Update Role is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

    static async deleteRole(req, res) {
        const roleId = req.params.id;
        //cek apakah user sdh ada
        const role = await Role.findAll({
            where: {
                id: roleId,
            },
        });

        if (!role[0]) return res.status(400).json({ msg: "Role is not exist" });

        try {
            await Role.destroy({
                where: {
                    id: roleId,
                },
            });

            res.json({ msg: "Delete Role is successfully" });
        } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
    }

}

module.exports = RoleController;
