const { Payment, Payment_Detail } = require("../db/models/index");
const Sequelize = require('sequelize');


class PaymentController {
  static async getPayments_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const payments = await Payment.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Payment.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          payments: payments,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPayments(req, res, next) {
    try {
      const payments = await Payment.findAll({
        include: ["paymentdetail"],
      });
      res.status(200).json(payments);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createPayment(req, res, next) {
    const {
      voucher_no,
      voucher_date,
      vendor,
      address,
      payment_method_id,
      bank,
      check_no,
      total,
      operator_id,
      updatedby
    } = req.body;

    try {
      await Payment.create({
        voucher_no: voucher_no,
        voucher_date: voucher_date,
        vendor: vendor,
        address: address,
        payment_method_id: payment_method_id,
        bank: bank,
        check_no: check_no,
        total: total,
        operator_id: operator_id,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Payment is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPaymentVoucher(req, res) {
    const { vouchercode } = req.query;
    try {
      const payment = await Payment.findAll({
        include: ["paymentdetail"],
        where: {
          voucher_no: vouchercode,
        },
      });
      res.status(200).json(payment);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPayment(req, res) {
    const payId = req.params.id;
    try {
      const payment = await Payment.findOne({
        include: ["paymentdetail"],
        where: {
          id: payId,
        },
      });
      res.status(200).json(payment);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updatePayment(req, res) {
    const payId = req.params.id;
    const {
      voucher_no,
      voucher_date,
      vendor,
      address,
      payment_method_id,
      bank,
      check_no,
      total,
      operator_id,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const payment = await Payment.findAll({
      where: {
        id: payId,
      },
    });

    if (!payment[0]) return res.status(400).json({ msg: "Payment is not exist" });


    try {
      await Payment.update(
        {
          voucher_no: voucher_no,
          voucher_date: voucher_date,
          vendor: vendor,
          address: address,
          payment_method_id: payment_method_id,
          bank: bank,
          check_no: check_no,
          total: total,
          operator_id: operator_id,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: payId,
          },
        }
      );

      res.json({ msg: "Update Payment is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deletePayment(req, res) {
    const payId = req.params.id;
    //cek apakah user sdh ada
    const payment = await Payment.findAll({
      where: {
        id: payId,
      },
    });

    if (!payment[0]) return res.status(400).json({ msg: "Payment is not exist" });

    try {
      await Payment.destroy({
        where: {
          id: payId,
        },
      });

      await Payment_Detail.destroy({
        where: {
          payment_id: payId,
        },
      });


      res.json({ msg: "Delete Payment is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPaymentDetails(req, res) {
    const payId = req.params.payId;
    try {
      const payDetails = await Payment_Detail.findAll({
        where: {
          payment_id: payId,
        },
      });
      res.status(200).json(payDetails);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createPaymentDetail(req, res) {
    const payId = req.params.payId;
    const {
      amount,
      description,
      receive_order_code,
      receive_order_date,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const payment = await Payment.findAll({
      where: {
        id: payId,
      },
    });

    if (!payment[0]) return res.status(400).json({ msg: "Payment Voucher is not exist" });

    try {
      await Payment_Detail.create({
        payment_id: payId,
        amount: amount,
        description: description,
        receive_order_code: receive_order_code,
        receive_order_date: receive_order_date,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Payment Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPaymentDetail(req, res) {
    const detailId = req.params.id;

    try {
      const payDetail = await Payment_Detail.findOne({
        where: {
          id: detailId,
        },
      });
      res.status(200).json(payDetail);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }


  static async updatePaymentDetail(req, res) {
    const detailId = req.params.id;
    const {
      amount,
      description,
      receive_order_code,
      receive_order_date,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const payDetail = await Payment_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!payDetail[0]) return res.status(400).json({ msg: "Payment Detail is not exist" });


    try {
      await Payment_Detail.update(
        {
          amount: amount,
          description: description,
          receive_order_code: receive_order_code,
          receive_order_date: receive_order_date,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: detailId,
          },
        }
      );

      res.json({ msg: "Update Payment Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deletePaymentDetail(req, res) {
    const detailId = req.params.id;

    //cek apakah user sdh ada
    const payDetail = await Payment_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!payDetail[0]) return res.status(400).json({ msg: "Payment Detail is not exist" });

    try {
      await Payment_Detail.destroy({
        where: {
          id: detailId,
        },
      });

      res.json({ msg: "Delete Payment Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = PaymentController;
