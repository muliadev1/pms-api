const { Surcharge } = require("../db/models/index");
const Sequelize = require('sequelize');

class SurchargeController {
    static async getSurcharge(req, res, next) {
        try {
          const data = await Surcharge.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getSurchargeDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Surcharge.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createSurcharge(req, res, next) {
        const {
          date_from,
          date_to,
          amount,
          updatedby
        } = req.body;

        try {
          await Surcharge.create({
            date_from: date_from, // formatteddate_from + "T00:00:00Z",
            date_to: date_to,
            amount : amount,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Surcharge is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateSurcharge(req, res) {
        const id = req.params.id;
        const {
           date_from,
           date_to,
           amount,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await Surcharge.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Surcharge is not exist" });
    
    
        try {
          await Surcharge.update(
            {
              date_from:  date_from,
              date_to: date_to,
              amount : amount,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Surcharge is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteSurcharge(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Surcharge.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Surcharge is not exist" });
    
        try {
          await Surcharge.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Surcharge is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = SurchargeController;