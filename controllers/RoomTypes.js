const {
  RoomType,
  RoomTypeBedType,
  RoomTypeFeature,
  RoomTypeRoomRate,
  CurrentYear_Roomtype,
  HotelRoomType,
  Room,
} = require("../db/models/index");
const Sequelize = require("sequelize");
const { Op } = require("sequelize");

class RoomtypeController {
  static async getRoomtypes_pages(req, res, next) {
    let { page, pageSize } = req.query;

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const roomtypes = await RoomType.findAll({
        offset: startIndex,
        limit: Number(pageSize),
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Source.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json({
        roomtypes: roomtypes,
        totalRecords: totalCount,
        currentPage: Number(page),
        totalPages: totalPages,
      });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async getRoomTypes(req, res, next) {
    try {
      const roomtypes = await RoomType.findAll({
        include: [
          {
            model: RoomTypeBedType,
            as: "roomtypebedtype",
            include: ["bedtype"],
          },
          {
            model: RoomTypeFeature,
            as: "roomtypefeature",
            include: ["feature"],
          },
          "hotel",
        ],
      });
      res.status(200).json(roomtypes);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async createRoomType(req, res, next) {
    const {
      roomtype_name,
      description,
      extrabed_price,
      extrabed_occupancy,
      is_extrabed,
      max_occupancy,
      min_occupancy,
      short_code,
      rate,
      bedtypedata,
      roomtypefeature,
      hotel_id,
      is_active,
      adult,
      child,
      updatedby,
    } = req.body;

    try {
      const roomtype = await RoomType.create({
        roomtype_name: roomtype_name,
        description: description,
        extrabed_price: extrabed_price,
        extrabed_occupancy,
        is_extrabed,
        max_occupancy,
        min_occupancy,
        short_code,
        rate: rate,
        hotel_id,
        adult,
        child,
        is_active: is_active,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      });

      await HotelRoomType.create({
        hotel_id,
        room_type_id: roomtype.id,
      });

      const bulkdata = roomtypefeature.map((subData) => ({
        room_type_id: roomtype.id,
        feature_id: subData.feature_id,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      }));

      const bulkbedtype = bedtypedata.map((subData) => ({
        room_type_id: roomtype.id,
        bed_type_id: subData.bed_type_id,
        createdAt: Sequelize.fn("NOW"),
        createdBy: updatedby,
        updatedAt: Sequelize.fn("NOW"),
        updatedBy: updatedby,
      }));

      await RoomTypeFeature.bulkCreate(bulkdata);
      await RoomTypeBedType.bulkCreate(bulkbedtype);

      res.status(200).json({ msg: "Add Roomtype is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }
  static async getRoomTypebyhotel(req, res) {
    const hotel = req.params.hotelid;
    let { adult, child } = req.query;

    try {
      let whereClause = { hotel_id: hotel };

      if (child) {
        if (child <= 1) {
          // Jika child 0 atau 1, tampilkan semua data
          // Tidak perlu menambahkan kondisi ke whereClause
        } else {
          // Jika child lebih dari 1, cari yang kurang dari atau sama dengan child
          // gte itu `>=` dan lte `<=`
          whereClause.child = { [Sequelize.Op.gte]: child };
        }
      }

      if (adult) {
        if (adult <= 1) {
          // Jika adult 0 atau 1, tampilkan semua data
          // Tidak perlu menambahkan kondisi ke whereClause
        } else {
          // Jika adult lebih dari 1, cari yang kurang dari atau sama dengan adult
          // gte itu `>=` dan lte `<=`
          whereClause.adult = { [Sequelize.Op.gte]: adult };
        }
      }

      const dataroomtype = await RoomType.findAll({
        where: whereClause,
      });

      res.status(200).json(dataroomtype);
    } catch (error) {
      res.status(500).json({ message: "Internal Server Error", error: error });
    }
  }

  static async getRoomTypebyhotelnochildandadult(req, res) {
    const hotel = req.params.hotelid;

    try {
      let whereClause = { hotel_id: hotel };

      const dataroomtype = await RoomType.findAll({
        where: whereClause,
        include:['room'],
        order:[['rate','ASC']]
      });

      res.status(200).json(dataroomtype);
    } catch (error) {
      res.status(500).json({ message: "Internal Server Error", error: error });
    }
  }
  static async getRoomTypePrice(req, res) {
    const roomtypeId = req.params.id;
    const stayDate = new Date(req.query.staydate); // Ambil tanggal yang diberikan dari query parameter staydate
    const year = stayDate.getFullYear(); // Ambil tahun dari tanggal yang diberikan

    const startOfYear = new Date(Date.UTC(year, 0, 1)); // Tanggal awal tahun yang diambil dari stayDate
    const endOfYear = new Date(Date.UTC(year, 11, 31)); // Tanggal akhir tahun yang diambil dari stayDate

    try {
      const dataroomtype = await RoomType.findOne({
        where: {
          id: roomtypeId,
        },
      });

      if (!dataroomtype) {
        return res.status(400).json({ msg: "Room type does not exist" });
      }

      const roomtype = await CurrentYear_Roomtype.findOne({
        where: {
          hotel_id: dataroomtype.hotel_id,
          start_date: {
            [Op.gte]: startOfYear, // Mulai dari awal tahun ini
          },
          end_date: {
            [Op.lte]: endOfYear, // Berakhir pada akhir tahun ini
          },
        },
      });

      let rate = 0;
      let amount = 0;

      if (dataroomtype) {
        rate = dataroomtype.rate;
      }

      if (roomtype) {
        amount = roomtype.amount;
      }

      const total_price = rate + amount;

      res
        .status(200)
        .json({ roomtype: dataroomtype, total_price: total_price });
    } catch (error) {
      res.status(500).json({ message: "Internal Server Error", error: error });
    }
  }

  static async getRoomType(req, res) {
    const roomtypeId = req.params.id;
    try {
      const roomtype = await RoomType.findOne({
        include: [
          {
            model: RoomTypeBedType,
            as: "roomtypebedtype",
            include: ["bedtype"],
          },
          {
            model: RoomTypeFeature,
            as: "roomtypefeature",
            include: ["feature"],
          },
          {
            model: HotelRoomType,
            as: "hotelroomtype",
            include: ["hotel"],
          },
        ],
        where: {
          id: roomtypeId,
        },
      });
      res.status(200).json(roomtype);
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }

  static async updateRoomType(req, res) {
    const roomtypeId = req.params.id;
    const {
      roomtype_name,
      description,
      extrabed_price,
      extrabed_occupancy,
      is_extrabed,
      max_occupancy,
      min_occupancy,
      short_code,
      rate,
      bedtypedata,
      roomtypefeature,
      hotel_id,
      adult,
      child,
      is_active,
      updatedby,
    } = req.body;

    try {
      // Cek apakah room type dengan ID yang diberikan ada
      const existingRoomType = await RoomType.findOne({
        where: {
          id: roomtypeId,
        },
      });

      if (!existingRoomType) {
        return res.status(400).json({ msg: "Room type does not exist" });
      }

      // Update data room type
      await RoomType.update(
        {
          roomtype_name,
          description,
          extrabed_price,
          extrabed_occupancy,
          is_extrabed,
          max_occupancy,
          min_occupancy,
          short_code,
          rate,
          adult,
          child,
          hotel_id,
          is_active,
          updatedAt: new Date(), // Menggunakan new Date() untuk mendapatkan waktu saat ini
          updatedBy: updatedby,
        },
        {
          where: {
            id: roomtypeId,
          },
        }
      );

      // Hapus data yang terkait dengan room type yang akan diperbarui
      await RoomTypeFeature.destroy({
        where: {
          room_type_id: roomtypeId,
        },
      });

      await RoomTypeBedType.destroy({
        where: {
          room_type_id: roomtypeId,
        },
      });

      await HotelRoomType.destroy({
        where: {
          hotel_id,
          room_type_id: roomtypeId,
        },
      });

      // Tambahkan data baru yang terkait dengan room type yang diperbarui
      const bulkRoomTypeFeature = roomtypefeature.map((subData) => ({
        room_type_id: roomtypeId,
        feature_id: subData.feature_id,
        createdAt: new Date(),
        createdBy: updatedby,
        updatedAt: new Date(),
        updatedBy: updatedby,
      }));

      const bulkBedTypeData = bedtypedata.map((subData) => ({
        room_type_id: roomtypeId,
        bed_type_id: subData.bed_type_id,
        createdAt: new Date(),
        createdBy: updatedby,
        updatedAt: new Date(),
        updatedBy: updatedby,
      }));

      await HotelRoomType.create({
        hotel_id,
        room_type_id: roomtypeId,
      });

      await RoomTypeFeature.bulkCreate(bulkRoomTypeFeature);
      await RoomTypeBedType.bulkCreate(bulkBedTypeData);

      res.json({ msg: "Room type updated successfully" });
    } catch (error) {
      console.error("Error in updateRoomType:", error);
      res
        .status(500)
        .json({ message: "Internal Server Error", error: error.message });
    }
  }

  static async deleteRoomType(req, res) {
    const roomtypeId = req.params.id;
    //cek apakah user sdh ada
    const roomtype = await RoomType.findAll({
      where: {
        id: roomtypeId,
      },
    });

    if (!roomtype[0])
      return res.status(400).json({ msg: "Roomtype is not exist" });

    try {
      await RoomType.destroy({
        where: {
          id: roomtypeId,
        },
      });

      // Hapus data yang terkait dengan room type yang akan diperbarui
      await RoomTypeFeature.destroy({
        where: {
          room_type_id: roomtypeId,
        },
      });

      await RoomTypeBedType.destroy({
        where: {
          room_type_id: roomtypeId,
        },
      });

      await HotelRoomType.destroy({
        where: {
          room_type_id: roomtypeId,
        },
      });

      res.json({ msg: "Delete Roomtype is successfully" });
    } catch (error) {
      res.status(500).json({ messege: "Internal Server Error", error: error });
    }
  }
}

module.exports = RoomtypeController;
