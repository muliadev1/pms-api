const { Category } = require("../db/models/index");
const Sequelize = require('sequelize');

class CategoryController {
  static async getCategories_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const categories = await Category.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Category.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          categories: categories,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getCategories(req, res, next) {
    try {
      const categories = await Category.findAll({
      });
      res.status(200).json(categories);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createCategory(req, res, next) {
    const {
      category_name,
      category_description,
      is_active,
      updatedby
    } = req.body;

    try {
      await Category.create({
        category_name: category_name,
        category_description: category_description,
        is_active: is_active,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getCategory(req, res) {
    const catId = req.params.id;
    try {
      const category = await Category.findOne({
        where: {
          id: catId,
        },
      });
      res.status(200).json(category);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateCategory(req, res) {
    const catId = req.params.id;
    const {
      category_name,
      category_description,
      is_active,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const category = await Category.findAll({
      where: {
        id: catId,
      },
    });

    if (!category[0]) return res.status(400).json({ msg: "Category is not exist" });


    try {
      await Category.update(
        {
          category_name: category_name,
          category_description: category_description,
          is_active: is_active,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: catId,
          },
        }
      );

      res.json({ msg: "Update Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteCategory(req, res) {
    const catId = req.params.id;
    //cek apakah user sdh ada
    const category = await Category.findAll({
      where: {
        id: catId,
      },
    });

    if (!category[0]) return res.status(400).json({ msg: "Category is not exist" });

    try {
      await Category.destroy({
        where: {
          id: catId,
        },
      });

      res.json({ msg: "Delete Category is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = CategoryController;
