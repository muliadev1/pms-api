const { Mail_template } = require("../db/models/index");
const Sequelize = require('sequelize');

class Mail_templateController {
    static async getMail_template(req, res, next) {
        try {
          const data = await Mail_template.findAll({
           
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getMail_templateDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Mail_template.findOne({
           
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createMail_template(req, res, next) {
        const {
          mail_template_name,
          subject,
          content,
          updatedby
        } = req.body;
    
        try {
          await Mail_template.create({
            mail_template_name,
            subject,
            content,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Mail_template is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateMail_template(req, res) {
        const id = req.params.id;
        const {
          mail_template_name,
          subject,
          content,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Mail_template.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Mail_template is not exist" });
    
    
        try {
          await Mail_template.update(
            {
              mail_template_name,
              subject,
              content,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Mail_template is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteMail_template(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Mail_template.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Mail_template is not exist" });
    
        try {
          await Mail_template.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Mail_template is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = Mail_templateController;