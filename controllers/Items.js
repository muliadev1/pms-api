const { Item, SubCategory, Category } = require("../db/models/index");
const Sequelize = require('sequelize');
//const subcategory = require("../db/models/subcategory");

class ItemController {
  static async getItems_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const items = await Item.findAll({
        include: ["subcategory"],
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Item.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          items: items,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getItems(req, res, next) {
    try {
      const items = await Item.findAll({
        attributes: [
          'id',
          'item_code',
          'item_description',
          'unit',
          'price',
          'stock',
        ],
        include: [
          {
            model: SubCategory,
            attributes: ['id', 'subcategory_name', 'subcategory_description'],
            as: 'subcategory',
            include: [
              {
                model: Category,
                attributes: ['id', 'category_name'],
                as: 'category'
              },
            ],
          },
        ],
      });
      res.status(200).json(items);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createItem(req, res, next) {
    const {
      item_code,
      item_description,
      subcategory_id,
      unit,
      price,
      updatedby
    } = req.body;

    try {
      await Item.create({
        item_code: item_code,
        item_description: item_description,
        subcategory_id: subcategory_id,
        unit: unit,
        price: price,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Item is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getItem(req, res) {
    const itemId = req.params.id;
    try {
      const item = await Item.findOne({
        attributes: [
          'id',
          'item_code',
          'item_description',
          'unit',
          'price',
          'stock',
        ],
        include: [
          {
            model: SubCategory,
            attributes: ['id', 'subcategory_name', 'subcategory_description'],
            as: 'subcategory',
            include: [
              {
                model: Category,
                attributes: ['id', 'category_name'],
                as: 'category'
              },
            ],
          },
        ],
        where: {
          id: itemId,
        },
      });
      res.status(200).json(item);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateItem(req, res) {
    const itemId = req.params.id;
    const {
      item_description,
      unit,
      price,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const item = await Item.findAll({
      where: {
        id: itemId,
      },
    });

    if (!item[0]) return res.status(400).json({ msg: "Item is not exist" });

    try {
      await Item.update(
        {
          item_description: item_description,
          unit: unit,
          price: price,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: itemId,
          },
        }
      );

      res.json({ msg: "Update Item is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateAccount(req, res) {
    const itemId = req.params.id;
    const {
      account_HK,
      account_FO,
      account_FB,
      account_BO,
      account_BAR,
      account_SEC,
      account_PG,
      account_ENG,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const item = await Item.findAll({
      where: {
        id: itemId,
      },
    });

    if (!item[0]) return res.status(400).json({ msg: "Item is not exist" });


    try {
      await Item.update(
        {
          account_HK: account_HK,
          account_FO: account_FO,
          account_FB: account_FB,
          account_BO: account_BO,
          account_BAR: account_BAR,
          account_SEC: account_SEC,
          account_PG: account_PG,
          account_ENG: account_ENG,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: itemId,
          },
        }
      );

      res.json({ msg: "Update Item Account is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteItem(req, res) {
    const itemId = req.params.id;
    //cek apakah user sdh ada
    const item = await Item.findAll({
      where: {
        id: itemId,
      },
    });

    if (!item[0]) return res.status(400).json({ msg: "Item is not exist" });

    try {
      await Item.destroy({
        where: {
          id: itemId,
        },
      });

      res.json({ msg: "Delete Item is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = ItemController;
