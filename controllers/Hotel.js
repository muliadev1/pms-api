const { Hotel,HotelFeature,HotelRoomType,Feature,RoomType } = require("../db/models/index");
const Sequelize = require('sequelize');

class HotelController {
    static async getHotel(req, res, next) {
        try {
          const data = await Hotel.findAll({
            include:[
              {
                  model: HotelFeature,
                  as: 'hotelfeature',
                  include: ["feature"]
              },
              {
                  model: HotelRoomType,
                  as: 'hotelroomtype',
                  include: ["roomtype"]
              }],
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getHotelDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Hotel.findOne({
            include:[
              {
                  model: HotelFeature,
                  as: 'hotelfeature',
                  include: ["feature"]
              },
              {
                  model: HotelRoomType,
                  as: 'hotelroomtype',
                  include: ["roomtype"]
              }],
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createHotel(req, res, next) {
        const {
            title,
            subtitle,
            alias,
            description,
            destination,
            kelass,
            phone,
            hotel_code,
            email,
            website,
            address,
            latitude,
            longitude,
            release_date,
            is_homepage,
            hotelfeature,
            hotelroomtype,
            max_booking,
            updatedby
        } = req.body;
    
        try {
          const hotel = await Hotel.create({
            title,
            subtitle,
            alias,
            description,
            destination,
            class:kelass,
            phone,
            hotel_code,
            email,
            website,
            address,
            latitude,
            max_booking,
            longtitude:longitude,
            release:release_date,
            is_homepage,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });

          const bulkdata = hotelfeature.map(subData => ({
            hotel_id: hotel.id,
            feature_id: subData.feature_id,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
        }));

        const roomtype = hotelroomtype.map(subData => ({
            hotel_id: hotel.id,
            room_type_id: subData.room_type_id,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
        }));

        await HotelFeature.bulkCreate(bulkdata);
        await HotelRoomType.bulkCreate(roomtype);


          res.status(200).json({ msg: "Add Hotel is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateHotel(req, res) {
        const id = req.params.id;
        const {
            title,
            subtitle,
            alias,
            description,
            destination,
            kelass,
            phone,
            hotel_code,
            email,
            website,
            address,
            latitude,
            longitude,
            release_date,
            is_homepage,
            max_booking,
            hotelfeature,
            hotelroomtype,
            updatedby
        } = req.body;
    
        try {
            // Cek apakah hotel dengan ID yang diberikan ada
            const existingHotel = await Hotel.findOne({
                where: {
                    id: id,
                },
            });
    
            if (!existingHotel) {
                return res.status(400).json({ msg: "Hotel does not exist" });
            }
    
            // Update data hotel
            await Hotel.update(
                {
                    title,
                    subtitle,
                    alias,
                    description,
                    destination,
                    class: kelass, // Perhatikan penggunaan `class` untuk `kelass`
                    phone,
                    email,
                    hotel_code,
                    website,
                    address,
                    latitude,
                    max_booking,
                    longitude,
                    release: release_date, // Perhatikan penggunaan `release` untuk `release_date`
                    is_homepage,
                    updatedAt: Sequelize.fn('NOW'), // Menggunakan `Sequelize.fn('NOW')` untuk mendapatkan waktu saat ini
                    updatedBy: updatedby
                },
                {
                    where: {
                        id: id,
                    },
                }
            );
    
            // Update atau tambahkan data fitur hotel
            await HotelFeature.destroy({
                where: {
                    hotel_id: id,
                },
            });
    
            const bulkHotelFeature = hotelfeature.map(subData => ({
                hotel_id: id,
                feature_id: subData.feature_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            await HotelFeature.bulkCreate(bulkHotelFeature);
    
            // Update atau tambahkan data tipe kamar hotel
            await HotelRoomType.destroy({
                where: {
                    hotel_id: id,
                },
            });
    
            const bulkHotelRoomType = hotelroomtype.map(subData => ({
                hotel_id: id,
                room_type_id: subData.room_type_id,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            await HotelRoomType.bulkCreate(bulkHotelRoomType);
    
            res.json({ msg: "Hotel updated successfully" });
        } catch (error) {
            console.error("Error in updateHotel:", error);
            res.status(500).json({ message: 'Internal Server Error', error: error.message });
       
        }
      }    
    
      static async deleteHotel(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Hotel.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "Hotel is not exist" });
    
        try {
          await Hotel.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Hotel is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = HotelController;