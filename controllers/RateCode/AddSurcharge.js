const { RateCode_surcharge } = require("../../db/models/index");
const Sequelize = require('sequelize');
const { Op } = require('sequelize');
const { v4: uuidv4 } = require('uuid'); // Library untuk generate UUID
class RateCodeSurchargeController {
  static async getRateCodeSurcharge(req, res, next) {
    const rateid = req.params.rateid;

    try {
        const data = await RateCode_surcharge.findAll({
            attributes: [
                'code',
                'ratecode_id',
                'amount',
                'start_date',
                'end_date',
                
            ],
            where: {
                ratecode_id: rateid
            },
            group: ['code', 'ratecode_id','amount','start_date','end_date',]
        });

        res.status(200).json(data);
    } catch (error) {
        console.error("Error in getRateCodeSurcharge:", error);
        res.status(500).json({ message: 'Internal Server Error', error: error });
    }
}
      static async getRateCodeSurchargeDetail(req, res, next) {
        const code = req.params.code;
        try {
          const data = await RateCode_surcharge.findOne({
            attributes: [
                'code',
                'ratecode_id',
                'amount',
                'start_date',
                'end_date',
                
            ],
            where: {
                code: code,
              },
            group: ['code', 'ratecode_id','amount','start_date','end_date',]
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createRateCodeSurcharge(req, res, next) {
        const {
            ratecode_id,
            start_date,
            end_date,
            amount,
            updatedby
        } = req.body;
    
        try {
            // Cek apakah data sudah ada
            const existingData = await RateCode_surcharge.findOne({
                where: {
                    ratecode_id,
                    period_start: {
                        [Op.lt]: end_date, // Mulai sebelum end_date yang baru
                    },
                    period_end: {
                        [Op.gt]: start_date, // Berakhir setelah start_date yang baru
                    }
                }
            });
    
            if (existingData) {
                // Jika data sudah ada, kirim pesan bahwa data sudah ada
                return res.status(400).json({ message: "Data already exists for this period and rate code" });
            }
    
            // Generate random code (contoh menggunakan UUID v4)
            const code = uuidv4().substring(0, 8);
    
            // Hitung jumlah hari dalam rentang tanggal
            const startDate = new Date(start_date);
            const endDate = new Date(end_date);
    
            // Simpan data per hari dalam rentang tanggal
            let currentDate = new Date(startDate);
            const dataToSave = [];
    
            while (currentDate <= endDate) {
                const year = currentDate.getFullYear();
                const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
                const day = currentDate.getDate().toString().padStart(2, '0');
                const currentDateStr = `${year}-${month}-${day}`;

                const data = {
                    ratecode_id,
                    start_date: start_date,
                    end_date: end_date, // Untuk entri harian, end_date sama dengan start_date
                    period_start: currentDateStr,
                    period_end: currentDateStr,
                    amount,
                    code,
                    createdAt: Sequelize.fn('NOW'),
                    createdBy: updatedby,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby,
                };
    
                dataToSave.push(data);
    
                currentDate.setDate(currentDate.getDate() + 1);
            }
    
            // Simpan semua data sekaligus menggunakan bulkCreate
            await RateCode_surcharge.bulkCreate(dataToSave);
    
            res.status(200).json({ msg: "Add RateCodeSurcharge is successful" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }

    static async updateRateCodeSurcharge(req, res) {
      const code = req.params.code;
      const {
          ratecode_id,
          start_date,
          end_date,
          amount,
          updatedby
      } = req.body;
  
      try {
          // Temukan data yang akan diupdate berdasarkan code
          let existingData = await RateCode_surcharge.findOne({
              where: {
                  code
              }
          });
  
          if (!existingData) {
              return res.status(404).json({ message: "RateCodeSurcharge not found" });
          }
  
              // Jika tanggal berbeda, hapus data yang lama dan buat data baru
              await RateCode_surcharge.destroy({
                  where: {
                      code
                  }
              });
  
              // Simpan data per hari dalam rentang tanggal yang baru
              const startDate = new Date(start_date);
              const endDate = new Date(end_date);
              let currentDate = new Date(startDate);
              const dataToSave = [];
  
              while (currentDate <= endDate) {
                const year = currentDate.getFullYear();
                const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
                const day = currentDate.getDate().toString().padStart(2, '0');
                const currentDateStr = `${year}-${month}-${day}`;
                  const data = {
                      ratecode_id,
                      start_date: start_date,
                      end_date: end_date, // Untuk entri terakhir, end_date sesuai dengan input end_date
                      period_start: currentDateStr,
                      period_end: currentDateStr,
                      amount,
                      code,
                      createdAt: Sequelize.fn('NOW'),
                      createdBy: updatedby,
                      updatedAt: Sequelize.fn('NOW'),
                      updatedBy: updatedby,
                  };
  
                  dataToSave.push(data);
                  currentDate.setDate(currentDate.getDate() + 1);
              }
  
              // Simpan semua data sekaligus menggunakan bulkCreate
              await RateCode_surcharge.bulkCreate(dataToSave);
          
  
          res.json({ msg: "Update RateCodeSurcharge is successful" });
      } catch (error) {
          console.error("Error in updateRateCodeSurcharge:", error);
          res.status(500).json({ message: 'Internal Server Error', error: error.message });
      }
  }
    
    
      static async deleteRateCodeSurcharge(req, res) {
        const code = req.params.code;
        //cek apakah user sdh ada
        const data = await RateCode_surcharge.findAll({
          where: {
            code: code,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "RateCodeSurcharge is not exist" });
    
        try {
          await RateCode_surcharge.destroy({
            where: {
              code: code,
            },
          });
    
          res.json({ msg: "Delete RateCodeSurcharge is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}


module.exports = RateCodeSurchargeController;