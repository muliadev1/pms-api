const { Promo_code } = require("../../db/models/index");
const Sequelize = require('sequelize');
const { Op } = require('sequelize');
class PromoCodeController {
    static async getPromoCode(req, res, next) {
        try {
          const data = await Promo_code.findAll({
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getPromoCodeDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Promo_code.findOne({
           
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createPromoCode(req, res, next) {
        const {
            code,
            start_booking,
            end_booking,
            start_stay,
            end_stay,
            amount,
            updatedby
        } = req.body;
    
        try {
            // Cek apakah data sudah ada
            const existingData = await Promo_code.findOne({
                where: {
                    code:code,
                }
            });
    
            if (existingData) {
                // Jika data sudah ada, kirim pesan bahwa data sudah ada
                return res.status(400).json({ message: "Data already exists for this promo code" });
            }
    
            // Jika data belum ada, simpan data baru
            await Promo_code.create({
                code,
                start_booking,
                end_booking,
                start_stay,
                end_stay,
                amount,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add PromoCode is successful" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    
      
      static async updatePromoCode(req, res) {
        const id = req.params.id;
        const {
            code,
            start_booking,
            end_booking,
            start_stay,
            end_stay,
            amount,
            updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Promo_code.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "PromoCode is not exist" });
    
    
        try {
          await Promo_code.update(
            {
                code,
                start_booking,
                end_booking,
                start_stay,
                end_stay,
                amount,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update PromoCode is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deletePromoCode(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Promo_code.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "PromoCode is not exist" });
    
        try {
          await Promo_code.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete PromoCode is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}


module.exports = PromoCodeController;