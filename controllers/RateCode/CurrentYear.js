const { CurrentYear_Roomtype,Hotel } = require("../../db/models/index");
const Sequelize = require('sequelize');
const { Op } = require('sequelize');
class CurrentYearRoomtypeController {
    static async getCurrentYearRoomtype(req, res, next) {
      const hotel_id = req.params.hotelid;
        try {
          const data = await CurrentYear_Roomtype.findAll({
            include:[
              {
                model: Hotel,
                as: 'hotel',
                attributes: ['hotel_code','title','subtitle']
              }
              
            ],
            where: {
              hotel_id: hotel_id,
          },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      static async getCurrentYearRoomtypeAll(req, res, next) {
          try {
            const data = await CurrentYear_Roomtype.findAll({
              include:[
                {
                  model: Hotel,
                  as: 'hotel',
                  attributes: ['hotel_code','title','subtitle']
                }
                
              ]
            });
            res.status(200).json(data);
          } catch (error) {
            res.status(500).json({ messege: 'Internal Server Error', error:error });
          }
        }

      static async getCurrentYearRoomtypeDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await CurrentYear_Roomtype.findAll({
            where: {
              id: id,
          },
           
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createCurrentYearRoomtype(req, res, next) {
        const {
            cuurentprice,
            hotel_id,
            updatedby
        } = req.body;
    
        try {
            // Default finalCurrentPrice if cuurentprice is not provided
            let finalCurrentPrice = cuurentprice;
            if (!cuurentprice || cuurentprice.length === 0) {
                const currentYear = new Date().getFullYear();
                finalCurrentPrice = [{
                    start_date: `${currentYear}-06-01`,
                    end_date: `${currentYear}-06-30`,
                    price: 0
                }];
            }
    
            const bulkprice = await Promise.all(finalCurrentPrice.map(async (subData) => {
                // Check if the roomtype already exists
                const existingRoomtype = await CurrentYear_Roomtype.findOne({
                    where: {
                        hotel_id: hotel_id,
                        start_date: {
                            [Sequelize.Op.lte]: subData.end_date // End date should be less than or equal to existing
                        },
                        end_date: {
                            [Sequelize.Op.gte]: subData.start_date // Start date should be greater than or equal to existing
                        }
                    }
                });
    
                // If roomtype exists, do not add to bulkprice
                if (existingRoomtype) {
                    return null;
                }
    
                // If roomtype does not exist, add to bulkprice
                return {
                    hotel_id: hotel_id,
                    start_date: subData.start_date,
                    end_date: subData.end_date,
                    amount: subData.price,
                    createdAt: Sequelize.fn('NOW'),
                    createdBy: updatedby,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby,
                };
            }));
    
            // Filter out null values (where roomtypes already exist)
            const filteredBulkPrice = bulkprice.filter(item => item !== null);
    
            if (filteredBulkPrice.length === 0) {
                return res.status(400).json({ message: 'Data for all roomtypes already exists in the specified date range' });
            }
            // Create only the filtered items
            await CurrentYear_Roomtype.bulkCreate(filteredBulkPrice);
    
            res.status(200).json({ msg: "Add CurrentYearRoomtype is successful ", filteredBulkPrice:filteredBulkPrice });
        } catch (error) {
            console.error('Error:', error);
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    
    
      
    static async updateCurrentYearRoomtype(req, res) {
        const id = req.params.id;
        const { 
          start_date, 
          end_date,
          price,
          updatedby
         } = req.body;
    
        try {
            
          await CurrentYear_Roomtype.update(
            {
                start_date: start_date,
                end_date: end_date,
                amount: price,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
              },
              {
                where:{
                  id:id
                }
              }
          );
    
            res.json({ msg: "Update CurrentYearRoomtype is successfully" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    
      static async deleteCurrentYearRoomtype(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await CurrentYear_Roomtype.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "CurrentYearRoomtype is not exist" });
    
        try {
          await CurrentYear_Roomtype.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete CurrentYearRoomtype is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}


module.exports = CurrentYearRoomtypeController;