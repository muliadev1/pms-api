const { Rate_code,RateCode_element,RateCode_roomrate,RoomType ,RateCode_surcharge,Ratecode_promo,Promo_code } = require("../../db/models/index");
const Sequelize = require('sequelize');
class RateCodeController {
  static async getRateCode(req, res, next) {
    try {
      const data = await Rate_code.findAll({
        include: ["ratecode_element", {
          model: RateCode_roomrate,
          as: 'ratecode_roomtype'
        },
        {
          model: Ratecode_promo,
          include: [
              {
                  model: Promo_code,
                  as: 'promo',
                  required: false
              }
          ],
          as: 'ratepromo'
      }],
      });
  
      // Proses data secara manual untuk menambahkan roomtype_name ke dalam setiap entri RateCode_roomrate
      const processedData = await Promise.all(data.map(async rateCode => {
        const rateCodeData = rateCode.toJSON();
        const rateCodeRoomTypes = rateCodeData.ratecode_roomtype;
  
        // Lakukan kueri terpisah untuk mendapatkan data RoomType
        const roomTypePromises = rateCodeRoomTypes.map(async rateCodeRoomType => {
          const roomType = await RoomType.findByPk(rateCodeRoomType.room_type_id);
          return {
            ...rateCodeRoomType,
            roomtype_name: roomType ? roomType.roomtype_name : null,
            short_code: roomType ? roomType.short_code : null,
            description: roomType ? roomType.description : null,
          };
        });
  
        // Tunggu semua kueri RoomType selesai
        const rateCodeRoomTypesWithData = await Promise.all(roomTypePromises);
  
        // Ganti ratecode_roomtype dengan data yang sudah diproses
        rateCodeData.ratecode_roomtype = rateCodeRoomTypesWithData;
        return rateCodeData;
      }));
  
      res.status(200).json(processedData);
    } catch (error) {
      res.status(500).json({ message: 'Internal Server Error', error: error });
    }
  }

  static async getRateCodeDetail(req, res, next) {
    const id = req.params.id;
    try {
        // Ambil data Rate_code berdasarkan ID
        const rateCode = await Rate_code.findByPk(id, {
            include: ['ratecode_element',
              {
                model:RateCode_surcharge,
                as:'surcharge_ratecode'
              }
              ]
        });

        // Jika data Rate_code tidak ditemukan, kirimkan respons 404
        if (!rateCode) {
            return res.status(404).json({ message: 'Rate code not found' });
        }

        // Ambil data RateCode_roomrate berdasarkan ID Rate_code
        const rateCodeRoomRates = await RateCode_roomrate.findAll({
            where: { rate_code_id: id },
            include: ['roomtype']
        });

        const promorateCodes = await Ratecode_promo.findAll({
            where: { id_ratecode: id },
            include: ['promo']
        });

        const promoJSON = promorateCodes.map(promocode => promocode.toJSON());

        // Mengonversi data Sequelize ke JSON
        const rateCodeRoomRatesJSON = rateCodeRoomRates.map(rateCodeRoomRate => rateCodeRoomRate.toJSON());

        // Ganti nilai ratecode_roomtype dengan data yang sudah diproses
        rateCode.dataValues.ratecode_roomtype = rateCodeRoomRatesJSON;
        rateCode.dataValues.promocode = promoJSON;

        // Kirimkan respons dengan data Rate_code yang sudah dimodifikasi
        res.status(200).json(rateCode);
    } catch (error) {
        res.status(500).json({ message: 'Internal Server Error', error: error });
    }
}

      static async createRateCode(req, res, next) {
        const {
          rate_code,
          rate_description,
          min_night,
          start_booking,
          end_booking,
          start_stay,
          end_stay,
          adult,
          child,
          pax,
          percentage,
          amount,
          category_rate,  
          hotel_id,
          promo_code,
          promo_id,
          element,
          roomtype,
          deposit_policy,
          cancelation_policy,
          other_conditions,
          is_sun,
          is_mon,
          is_tue, 
          is_wed, 
          is_thu, 
          is_fri, 
          is_sat,
          updatedby
        } = req.body;
      
        try {
          if (category_rate === 1) {
            const existingRateCode = await Rate_code.findOne({
              where: {
                hotel_id: hotel_id,
                category_rate: 1
              }
            });
        
            if (existingRateCode) {
              return res.status(400).json({ msg: "BAR rate code already exists for this hotel." });
            }
          }
        

          const ratecode = await Rate_code.create({
            rate_code,
            rate_description,
            min_night,
            start_booking,
            end_booking,
            start_stay,
            end_stay,
            adult,
            child,
            pax,
            hotel_id,
            promo_code,
            percentage,
            amount,
            is_sun,
            is_mon,
            is_tue, 
            is_wed, 
            is_thu, 
            is_fri, 
            is_sat,
            deposit_policy,
            cancelation_policy,
            other_conditions,
            category_rate,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
      
          let totalbulkelement = 0;
      
          const bulkdata = element.map(subData => {
            let typeprice = subData.price_type_id;
            let adult_price = 0;
            let total_price = 0;
            let child_price = 0;
            let peroomprice = 0;
      
            if (typeprice === 1) {
              adult_price = subData.adult_rate * subData.qty * adult;
              child_price = subData.child_rate * subData.qty * child;
              total_price = adult_price + child_price;
            } else {
              peroomprice = subData.per_room_rate * subData.qty;
              total_price = peroomprice;
            }
            totalbulkelement += total_price;

            return {
              rate_code_id: ratecode.id,
              element_id: subData.element_id,
              adult_rate: adult_price,
              child_rate: child_price,
              per_room_rate: peroomprice,
              child_price_input:subData.child_rate,
              adult_price_input:subData.adult_rate,
              per_room_input:subData.per_room_rate,
              price_type_id: subData.price_type_id,
              qty: subData.qty,
              name: subData.name,
              total_price: total_price,
              is_daily:subData.is_daily,
              createdAt: Sequelize.fn('NOW'),
              createdBy: updatedby,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby,
            };
          });
      
          await Ratecode_promo.create({
            id_ratecode: ratecode.id,
            id_promo: promo_id,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });

          const bulkdata2 = roomtype.map(detailData => ({
            rate_code_id: ratecode.id,
            room_type_id: detailData.roomtype_id,
            price_input: detailData.roomtype_price,
            per_night_price: (totalbulkelement + (detailData.roomtype_price * min_night)) / min_night,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          }));
      
          await RateCode_roomrate.bulkCreate(bulkdata2);
          await RateCode_element.bulkCreate(bulkdata);
      
          res.status(200).json({ msg: "Add RateCode is successful" });
        } catch (error) {
          res.status(500).json({ message: 'Internal Server Error', error: error });
        }
      }
      
      
      static async updateRateCode(req, res, next) {
        const id = req.params.id;
        const {
            rate_code,
            rate_description,
            min_night,
            start_booking,
            end_booking,
            start_stay,
            end_stay,
            adult,
            child,
            pax,
            percentage,
            amount,
            category_rate,
            hotel_id,
            promo_code,
            promo_id,
            element,
            roomtype,
            deposit_policy,
            cancelation_policy,
            other_conditions,
            is_sun,
            is_mon,
            is_tue, 
            is_wed, 
            is_thu, 
            is_fri, 
            is_sat,
            updatedby
        } = req.body;
    
        try {
            // Perbarui data Rate_code
            await Rate_code.update(
                {
                    rate_code,
                    rate_description,
                    min_night,
                    start_booking,
                    end_booking,
                    start_stay,
                    end_stay,
                    adult,
                    child,
                    pax,
                    hotel_id,
                    promo_code,
                    percentage,
                    amount,
                    deposit_policy,
                    cancelation_policy,
                    other_conditions,
                    is_sun,
                    is_mon,
                    is_tue, 
                    is_wed, 
                    is_thu, 
                    is_fri, 
                    is_sat,
                    category_rate,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby,
                },
                { where: { id } }
            );
    
            // Cari entri Ratecode_promo berdasarkan id_ratecode dan id_promo
            let promoratecode = await Ratecode_promo.findOne({
              where: {
                  id_ratecode: id,
                  id_promo: promo_id,
              }
          });

          if (promoratecode) {
              // Jika entri ditemukan, update data yang sudah ada
              await promoratecode.update({
                  createdAt: Sequelize.fn('NOW'),
                  createdBy: updatedby,
                  updatedAt: Sequelize.fn('NOW'),
                  updatedBy: updatedby,
              });
          } else {
              await Ratecode_promo.destroy({ where: { id_ratecode: id } });
              // Jika entri tidak ditemukan, buat entri baru
              await Ratecode_promo.create({
                  id_ratecode: id,
                  id_promo: promo_id,
                  createdAt: Sequelize.fn('NOW'),
                  createdBy: updatedby,
                  updatedAt: Sequelize.fn('NOW'),
                  updatedBy: updatedby,
              });
          }


            // Hapus data RateCode_element dan RateCode_roomrate yang terkait dengan rate code ini
            await RateCode_element.destroy({ where: { rate_code_id: id } });
            await RateCode_roomrate.destroy({ where: { rate_code_id: id } });
    
            // Buat kembali entri untuk RateCode_element
            let totalbulkelement = 0;
            const bulkdata = element.map(subData => {
                let typeprice = subData.price_type_id;
                let adult_price = 0;
                let total_price = 0;
                let child_price = 0;
                let peroomprice = 0;
    
                if (typeprice === 1) {
                    adult_price = subData.adult_rate * subData.qty * adult;
                    child_price = subData.child_rate * subData.qty * child;
                    total_price = adult_price + child_price;
                } else {
                    peroomprice = subData.per_room_rate * subData.qty;
                    total_price = peroomprice;
                }
                totalbulkelement += total_price;
                return {
                    rate_code_id: id,
                    element_id: subData.element_id,
                    adult_rate: subData.adult_rate,
                    child_rate: subData.child_rate,
                    per_room_rate: subData.per_room_rate,
                    qty: subData.qty,
                    name: subData.name,
                    child_price_input:subData.child_rate,
                    adult_price_input:subData.adult_rate,
                    per_room_input:subData.per_room_rate,
                    price_type_id: subData.price_type_id,
                    total_price: total_price,
                    is_daily:subData.is_daily,
                    createdAt: Sequelize.fn('NOW'),
                    createdBy: updatedby,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby,
                };
            });
    
            // Buat kembali entri untuk RateCode_roomrate
            const bulkdata2 = roomtype.map(detailData => ({
                rate_code_id: id,
                room_type_id: detailData.roomtype_id,
                price_input: detailData.roomtype_price,
                per_night_price: (totalbulkelement + (detailData.roomtype_price * min_night)) / min_night,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            }));
    
            await RateCode_element.bulkCreate(bulkdata);
            await RateCode_roomrate.bulkCreate(bulkdata2);
    
            res.status(200).json({ msg: "Update RateCode is successful" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    

      static async deleteRateCode(req, res) {
        try {
          const id = req.params.id;
          
          // Cek apakah RateCode dengan id yang diberikan ada
          const rateCode = await Rate_code.findByPk(id);
          if (!rateCode) {
            return res.status(400).json({ msg: "RateCode does not exist" });
          }
      
          // Hapus RateCode
          await Rate_code.destroy({
            where: {
              id: id,
            },
          });
      
          // Ambil semua RateCode_roomrate yang terkait dengan rate_code_id yang dihapus
          const deletedRateCodeRoomrates = await RateCode_roomrate.findAll({ where: { rate_code_id: id } });
      
          // Ambil id dari setiap RateCode_roomrate yang dihapus
          const deletedRateCodeRoomrateIds = deletedRateCodeRoomrates.map(rateCodeRoomrate => rateCodeRoomrate.id);
      
          // Hapus RateCode_surcharge yang terkait dengan RateCode_roomrate yang dihapus
          await RateCode_surcharge.destroy({ where: { ratecode_id: id } });
      
          // Hapus RateCode_roomrate
          await RateCode_roomrate.destroy({ where: { rate_code_id: id } });
          await RateCode_element.destroy({ where: { rate_code_id: id } });
          res.json({ msg: "Delete RateCode is successful" });
        } catch (error) {
          res.status(500).json({ message: 'Internal Server Error', error: error.message });
        }
      }
      
}

module.exports = RateCodeController;