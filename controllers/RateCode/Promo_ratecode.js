const { Ratecode_promo } = require("../../db/models/index");
const Sequelize = require('sequelize');
const { Op } = require('sequelize');
class PromoRatecodeController {
    static async getPromoRatecode(req, res, next) {
        try {
          const data = await Ratecode_promo.findAll({
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getPromoRatecodeDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Ratecode_promo.findOne({
           
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createPromoRatecode(req, res, next) {
        const {
            id_ratecode,
            id_promo,
            updatedby
        } = req.body;
    
        try {
    
            // Jika data belum ada, simpan data baru
            await Ratecode_promo.create({
                id_ratecode,
                id_promo,
                createdAt: Sequelize.fn('NOW'),
                createdBy: updatedby,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby,
            });
            res.status(200).json({ msg: "Add PromoRatecode is successful" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }
    
      
      static async updatePromoRatecode(req, res) {
        const id = req.params.id;
        const {
            id_ratecode,
            id_promo,
            updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const data = await Ratecode_promo.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "PromoRatecode is not exist" });
    
    
        try {
          await Ratecode_promo.update(
            {
                id_ratecode,
                id_promo,
                updatedAt: Sequelize.fn('NOW'),
                updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update PromoRatecode is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deletePromoRatecode(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const data = await Ratecode_promo.findAll({
          where: {
            id: id,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "PromoRatecode is not exist" });
    
        try {
          await Ratecode_promo.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete PromoRatecode is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}


module.exports = PromoRatecodeController;