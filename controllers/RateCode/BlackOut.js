const { BlackOutDate } = require("../../db/models/index");
const Sequelize = require('sequelize');
const { Op } = require('sequelize');
const { v4: uuidv4 } = require('uuid'); // Library untuk generate UUID
class BlackOutController {
  static async getBlackOut(req, res, next) {
    const rateid = req.params.rateid;

    try {
        const data = await BlackOutDate.findAll({
            attributes: [
                'code',
                'room_rate_id',
                'start_date',
                'end_date',
                
            ],
            where: {
                room_rate_id: rateid
            },
            group: ['code', 'room_rate_id','start_date','end_date',]
        });

        res.status(200).json(data);
    } catch (error) {
        console.error("Error in getBlackOut:", error);
        res.status(500).json({ message: 'Internal Server Error', error: error });
    }
}
      static async getBlackOutDetail(req, res, next) {
        const code = req.params.code;
        try {
          const data = await BlackOutDate.findOne({
            attributes: [
                'code',
                'room_rate_id',
                'start_date',
                'end_date',
                
            ],
            where: {
                code: code,
              },
            group: ['code', 'room_rate_id','start_date','end_date',]
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createBlackOut(req, res, next) {
        const {
            ratecode_id,
            start_date,
            end_date,
            updatedby
        } = req.body;

        try {
            // Cek apakah data sudah ada
            const existingData = await BlackOutDate.findOne({
                where: {
                    room_rate_id:ratecode_id,
                    start_period: {
                        [Op.lt]: end_date, // Mulai sebelum end_date yang baru
                    },
                    end_period: {
                        [Op.gt]: start_date, // Berakhir setelah start_date yang baru
                    }
                }
            });
    
            if (existingData) {
                // Jika data sudah ada, kirim pesan bahwa data sudah ada
                return res.status(400).json({ message: "Data already exists for this period and rate code" });
            }
    
            // Generate random code (contoh menggunakan UUID v4)
            const code = uuidv4().substring(0, 8);
    
            // Hitung jumlah hari dalam rentang tanggal
            const startDate = new Date(start_date);
            const endDate = new Date(end_date);
    
            // Simpan data per hari dalam rentang tanggal
            let currentDate = new Date(startDate);
            const dataToSave = [];
    
            while (currentDate <= endDate) {
              const year = currentDate.getFullYear();
              const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
              const day = currentDate.getDate().toString().padStart(2, '0');
              const currentDateStr = `${year}-${month}-${day}`;
                const data = {
                    room_rate_id:ratecode_id,
                    start_date: start_date,
                    end_date: end_date, // Untuk entri harian, end_date sama dengan start_date
                    start_period:  currentDateStr,
                    end_period: currentDateStr,
                    code,
                    createdAt: Sequelize.fn('NOW'),
                    createdBy: updatedby,
                    updatedAt: Sequelize.fn('NOW'),
                    updatedBy: updatedby,
                };
    
                dataToSave.push(data);
    
                currentDate.setDate(currentDate.getDate() + 1);
            }
    
            // Simpan semua data sekaligus menggunakan bulkCreate
            await BlackOutDate.bulkCreate(dataToSave);
    
            res.status(200).json({ msg: "Add BlackOut is successful" });
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
    }

    static async updateBlackOut(req, res) {
      const code = req.params.code;
      const {
        ratecode_id,
          start_date,
          end_date,
          
          updatedby
      } = req.body;
  
      try {
          // Temukan data yang akan diupdate berdasarkan code
          let existingData = await BlackOutDate.findOne({
              where: {
                  code
              }
          });
  
          if (!existingData) {
              return res.status(404).json({ message: "BlackOut not found" });
          }
  
              // Jika tanggal berbeda, hapus data yang lama dan buat data baru
              await BlackOutDate.destroy({
                  where: {
                      code
                  }
              });
  
              // Simpan data per hari dalam rentang tanggal yang baru
              const startDate = new Date(start_date);
              const endDate = new Date(end_date);
              let currentDate = new Date(startDate);
              const dataToSave = [];
  
              while (currentDate <= endDate) {
                const year = currentDate.getFullYear();
                const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
                const day = currentDate.getDate().toString().padStart(2, '0');
                const currentDateStr = `${year}-${month}-${day}`;
                  const data = {
                      room_rate_id:ratecode_id,
                      start_date: start_date,
                      end_date: endDate, // Untuk entri terakhir, end_date sesuai dengan input end_date
                      start_period: currentDateStr,
                      end_period: currentDateStr,
                      code,
                      createdAt: Sequelize.fn('NOW'),
                      createdBy: updatedby,
                      updatedAt: Sequelize.fn('NOW'),
                      updatedBy: updatedby,
                  };
  
                  dataToSave.push(data);
                  currentDate.setDate(currentDate.getDate() + 1);
              }
  
              // Simpan semua data sekaligus menggunakan bulkCreate
              await BlackOutDate.bulkCreate(dataToSave);
          
  
          res.json({ msg: "Update BlackOut is successful" });
      } catch (error) {
          console.error("Error in updateBlackOut:", error);
          res.status(500).json({ message: 'Internal Server Error', error: error.message });
      }
  }
    
    
      static async deleteBlackOut(req, res) {
        const code = req.params.code;
        //cek apakah user sdh ada
        const data = await BlackOutDate.findAll({
          where: {
            code: code,
          },
        });
    
        if (!data[0]) return res.status(400).json({ msg: "BlackOut is not exist" });
    
        try {
          await BlackOutDate.destroy({
            where: {
              code: code,
            },
          });
    
          res.json({ msg: "Delete BlackOut is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}


module.exports = BlackOutController;