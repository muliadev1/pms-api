const { Rate_code,Hotel,RateCode_roomrate,RateCode_surcharge,Ratecode_promo,Promo_code,BlackOutDate } = require("../../db/models/index");
const Sequelize = require('sequelize');
const {Op} = require('sequelize');
class RateCodeSearchController {
    static async getPackagesRev(req, res, next) {
        let { checkin, checkout, adult, child, night, promo_code, hotel_id } = req.query;
    
        try {
            const whereClause = {
                [Op.and]: [],
                [Op.or]: [],
                category_rate: {
                    [Op.in]: [1, 2] // Filter for category_rate equal to 1 or 2
                }
            };
    
            // Add filters if parameters are provided
            if (hotel_id) whereClause.hotel_id = hotel_id;
            if (child) whereClause.child = child;
            if (adult) whereClause.adult = adult;
    
            // Filter for number of nights (night)
            if (night) {
                whereClause[Op.and].push(Sequelize.where(Sequelize.col('Rate_code.min_night'), '<=', night));
            }
    
           // Filter for check-in and check-out dates
           if (checkin && checkout) {
            const today = new Date();

            whereClause[Op.or].push(
                {
                    [Op.and]: [
                        { start_booking: { [Op.lte]: today } },
                        { end_booking: { [Op.gte]: today.setDate(today.getDate() - 1) } }
                    ]
                }
            );
    
            // Filter for stay period
            whereClause[Op.and].push(
                { start_stay: { [Op.lt]: checkout } },
                { end_stay: { [Op.gte]: checkin } }
            );
        }
    
            // Prepare the include options separately
            const includeOptions = [
                {
                    model: Hotel,
                    as: 'hotel',
                    attributes: [
                        'title',
                        'subtitle',
                        'hotel_code',
                        'max_booking',
                        'address',
                        'email',
                        'phone'
                    ]
                },
                {
                    model: RateCode_surcharge,
                    as: 'surcharge_ratecode',
                    where: {
                        [Op.and]: [
                            { period_start: { [Op.lte]: new Date(checkout) } },
                            { period_end: { [Op.gte]: new Date(checkin) } }
                        ]
                    },
                    required: false
                },
                {
                    model: BlackOutDate,
                    as: 'blackoutdate',
                    attributes: [
                        'code',
                        'start_date',
                        'end_date',
                        'start_period',
                        'end_period'
                    ],
                    where: {
                        [Op.and]: [
                            { start_date: { [Op.lte]: new Date(checkout) } },
                            { end_date: { [Op.gte]: new Date(checkin) } }
                        ]
                    },
                    required: false
                },
                {
                    model: Ratecode_promo,
                    include: [
                        {
                            model: Promo_code,
                            as: 'promo',
                            where: {
                                code: promo_code,
                                ...(checkin && checkout && {
                                    [Op.or]: [
                                        {
                                            [Op.and]: [
                                                { start_stay: { [Op.lte]: new Date(checkout) } },
                                                { end_stay: { [Op.gte]: new Date(checkin) } }
                                            ]
                                        },
                                        {
                                            [Op.and]: [
                                                { start_booking: { [Op.lte]: new Date() } },
                                                { start_booking: { [Op.gte]: new Date(checkin) } },
                                                { end_booking: { [Op.gte]: new Date() } },
                                                { end_booking: { [Op.lte]: new Date(checkout) } }
                                            ]
                                        },
                                        {
                                            [Op.and]: [
                                                { start_stay: { [Op.gte]: new Date(checkin) } },
                                                { end_stay: { [Op.lte]: new Date(checkout) } }
                                            ]
                                        }
                                    ]
                                }),
                                ...(checkin && {
                                    [Op.or]: [
                                        { start_stay: { [Op.gte]: new Date(checkin) } },
                                        { start_booking: { [Op.gte]: new Date(checkin) } }
                                    ]
                                }),
                                ...(checkout && {
                                    [Op.or]: [
                                        { end_stay: { [Op.lte]: new Date(checkout) } },
                                        { end_booking: { [Op.lte]: new Date(checkout) } }
                                    ]
                                })
                            },
                            required: true
                        }
                    ],
                    
                    as: 'ratepromo'
                },
                {
                    model: RateCode_roomrate,
                    as: 'ratecode_roomtype',
                    include: ['roomtype']
                },
                'ratecode_element',
            ];
    
            // Fetch Rate_code with the prepared include options and where clause
            const packages = await Rate_code.findAll({
                include: includeOptions,
                where: whereClause,
                order: [['createdAt', 'DESC']]
            });
    
            let filteredPackages = packages;
            // Filter out packages where ratepromo is empty
            if (promo_code) {
                filteredPackages = packages.filter(pkg => pkg.ratepromo && pkg.ratepromo.length > 0);
            }
    
            res.status(200).json(filteredPackages);
    
        } catch (error) {
            console.error('Error in getPackagesRev:', error);
            res.status(500).json({ message: 'Internal Server Error', error: error, body: req.query });
        }
    }
    
    
    

    static async getBar(req, res, next) {
        const id = req.params.hotelid;
        try {
            const bar = await Rate_code.findOne({
                where:{
                  hotel_id: id,
                  category_rate : 1
                }
                ,include:['ratecode_element'],
              });
              // Jika data Rate_code tidak ditemukan, kirimkan respons 404
                if (!bar) {
                    return res.status(404).json({ message: 'Rate code not found' });
                }

                // Ambil data RateCode_roomrate berdasarkan ID Rate_code
                const rateCodeRoomRates = await RateCode_roomrate.findAll({
                    where: { rate_code_id: bar.id },
                    include: ['roomtype']
                });

                const promorateCodes = await Ratecode_promo.findAll({
                    where: { id_ratecode: id },
                    include: ['promo']
                });

                const promoJSON = promorateCodes.map(promocode => promocode.toJSON());

                // Mengonversi data Sequelize ke JSON
                const rateCodeRoomRatesJSON = rateCodeRoomRates.map(rateCodeRoomRate => rateCodeRoomRate.toJSON());

                // Ganti nilai ratecode_roomtype dengan data yang sudah diproses
                bar.dataValues.ratecode_roomtype = rateCodeRoomRatesJSON;
                bar.dataValues.promocode = promoJSON;
                res.status(200).json(bar);
        } catch (error) {
            res.status(500).json({ message: 'Internal Server Error', error: error });
        }
        
    }

    static async getCategoryRate(req, res, next) {
        let { hotel_id, category_rate } = req.query;
        
        try {
            const data = await Rate_code.findAll({
                where: {
                    hotel_id: hotel_id,
                    category_rate: category_rate
                },
                include: [
                    "ratecode_element",
                    {
                        model: RateCode_roomrate,
                        as: 'ratecode_roomtype'
                    },
                    {
                        model: Ratecode_promo,
                        include: [
                            {
                                model: Promo_code,
                                as: 'promo',
                                required: false
                            }
                        ],
                        as: 'ratepromo'
                    }
                ],
            });
    
            const processedData = data.map(rateCode => {
                const rateCodeData = rateCode.toJSON();
                const rateCodeRoomTypes = rateCodeData.ratecode_roomtype.map(rateCodeRoomType => ({
                    ...rateCodeRoomType,
                    roomtype_name: rateCodeRoomType.RoomType ? rateCodeRoomType.RoomType.roomtype_name : null,
                    short_code: rateCodeRoomType.RoomType ? rateCodeRoomType.RoomType.short_code : null,
                    description: rateCodeRoomType.RoomType ? rateCodeRoomType.RoomType.description : null,
                }));
    
                rateCodeData.ratecode_roomtype = rateCodeRoomTypes;
                return rateCodeData;
            });
    
            res.status(200).json(processedData);
        } catch (error) {
            console.error("Error in getCategoryRate:", error);
            res.status(500).json({ message: 'Internal Server Error', error: error.message });
        }
    }

    
}


module.exports = RateCodeSearchController;