const { Feature } = require("../db/models/index");
const Sequelize = require('sequelize');

class FeatureController {
    static async getFeature(req, res, next) {
        try {
          const data = await Feature.findAll({});
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getFeatureDetail(req, res, next) {
        const id = req.params.id;
        try {
          const data = await Feature.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(data);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createFeature(req, res, next) {
        const {
          feature_name,
          updatedby
        } = req.body;
    
        try {
          await Feature.create({
            feature_name: feature_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Feauture is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updateFeature(req, res) {
        const id = req.params.id;
        const {
           feature_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await Feature.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Feauture is not exist" });
    
    
        try {
          await Feature.update(
            {
              feature_name:  feature_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Feauture is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deleteFeature(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const paytype = await Feature.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Feauture is not exist" });
    
        try {
          await Feature.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Feauture is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = FeatureController;