const {  PR_Main, PR_Detail, Department} = require("../db/models/index");
const Sequelize = require('sequelize');
const { Op, literal, fn, col } = require('sequelize');

class RequisitionController {
  static async getRequesitions_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const requesitions = await PR_Main.findAll({
        include: ["department"],
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await PR_Main.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          requesitions: requesitions,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getRequisitions(req, res, next) {
    try {
      const requesitions = await PR_Main.findAll({
        include: ["purchasereqdetail","department"],
        order: [
            ['createdAt', 'DESC']
        ]
      });
      res.status(200).json(requesitions);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }
  
  static async getRequisitionsCode(req, res, next) {
    const { prcode } = req.query;
    try {
      const requesitions = await PR_Main.findAll({
        include: ["purchasereqdetail","department"],
        where:{
          purchase_req_code:prcode
        }
      });
      res.status(200).json(requesitions);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createRequisition(req, res, next) {
    const {
      purchase_req_code,
      purchase_req_date,
      department_id,
      notes,
      updatedby
    } = req.body;

    try {
      await PR_Main.create({
        purchase_req_code: purchase_req_code,
        purchase_req_date: purchase_req_date,
        department_id: department_id,
        notes: notes,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Purchase Requesition is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createRequisitionBulk(req, res, next) {
    const {
      purchase_req_code,
      purchase_req_date,
      department_id,
      notes,
      purchase_reqdetail,
      updatedby
    } = req.body;

    try {
      const prmain = await PR_Main.create({
        purchase_req_code: purchase_req_code,
        purchase_req_date: purchase_req_date,
        department_id: department_id,
        notes: notes,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });

      const packageSubEntries = purchase_reqdetail.map(subData => ({
          purchase_req_id: prmain.id,
          item_code: subData.item_code,
          description: subData.description,
          unit: subData.unit,
          price: subData.price,
          qty_order: subData.qty_order,
          current_stock: subData.current_stock,
          notes: notes,
          createdAt: Sequelize.fn('NOW'),
          createdBy: updatedby,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby,
      }));


    await PR_Detail.bulkCreate(packageSubEntries);

      res.status(200).json({ msg: "Add Purchase Requesition is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateRequisitionBulk(req, res, next) {
    const purchase_req_id = req.params.id;
    const {
      purchase_req_date,
      department_id,
      notes,
      purchase_reqdetail,
      updatedby
    } = req.body;

    try {
        // Update PR_Main
        await PR_Main.update(
            {
                purchase_req_date,
                department_id,
                notes,
                updatedBy: updatedby,
            },
            {
                where: { id: purchase_req_id }
            }
        );

        // Delete existing PR_Details
        await PR_Detail.destroy({
            where: { purchase_req_id }
        });

        // Create new PR_Details
        const packageSubEntries = purchase_reqdetail.map(subData => ({
            purchase_req_id,
            item_code: subData.item_code,
            description: subData.description,
            unit: subData.unit,
            price: subData.price,
            qty_order: subData.qty_order,
            current_stock: subData.current_stock,
            notes,
            createdBy: updatedby,
            updatedBy: updatedby,
        }));
        await PR_Detail.bulkCreate(packageSubEntries);

        res.status(200).json({ msg: "Update Purchase Requisition is successful" });
    } catch (error) {
        res.status(500).json({ message: 'Internal Server Error', error });
    }
}


  static async getRequisition(req, res) {
    const reqId = req.params.id;
    try {
      const requesition = await PR_Main.findOne({
        include: ["purchasereqdetail","department"],
        where: {
          id: reqId,
        },
      });
      res.status(200).json(requesition);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updateRequisition(req, res) {
    const reqId = req.params.id;
    const {
      purchase_req_code,
      purchase_req_date,
      department_id,
      notes,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const requesition = await PR_Main.findAll({
      where: {
        id: reqId,
      },
    });

    if (!requesition[0]) return res.status(400).json({ msg: "Purchase Requesition is not exist" });


    try {
      await PR_Main.update(
        {
          purchase_req_code: purchase_req_code,
          purchase_req_date: purchase_req_date,
          department_id: department_id,
          notes: notes,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: reqId,
          },
        }
      );

      res.json({ msg: "Update Purchase Requesition is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteRequisition(req, res) {
    const reqId = req.params.id;
    //cek apakah user sdh ada
    const requesition = await PR_Main.findAll({
      where: {
        id: reqId,
      },
    });

    if (!requesition[0]) return res.status(400).json({ msg: "Purchase Requesition is not exist" });

    try {
      await PR_Main.destroy({
        where: {
          id: reqId,
        },
      });

      res.json({ msg: "Delete Purchase Requesition is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getReqDetails(req, res) {
    const reqId = req.params.reqId;
    try {
      const reqDetails = await PR_Detail.findAll({
        // where: {
        //   purchase_req_id: reqId,
        // },
        include:[
          {
            model: PR_Main,
            as: 'purchasereqmain',
            where: {
              purchase_req_code: reqId // Filter reservation_status_id equal to 4
              
            }
          }
        ]
      });
      res.status(200).json({reqDetails});
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
      res.status(500).json({messege:"error server",error:error});
    }
  }

  static async createReqDetail(req, res) {
    const reqId = req.params.reqId;
    const {
      item_code,
      description,
      unit,
      price,
      qty_order,
      current_stock,
      notes,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const requesition = await PR_Main.findAll({
      where: {
        id: reqId,
      },
    });

    if (!requesition[0]) return res.status(400).json({ msg: "Purchase Requesition is not exist" });

    try {
      await PR_Detail.create({
        purchase_req_id: reqId,
        item_code: item_code,
        description: description,
        unit: unit,
        price: price,
        qty_order: qty_order,
        current_stock: current_stock,
        notes: notes,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Purchase Requesition Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getReqDetail(req, res) {
    const detailId = req.params.id;

    try {
      const reqDetail = await PR_Detail.findOne({
        where: {
          id: detailId,
        },
      });
      res.status(200).json(reqDetail);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }


  static async updateReqDetail(req, res) {
    const detailId = req.params.id;
    const {
      item_code,
      description,
      unit,
      price,
      qty_order,
      current_stock,
      notes,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const reqDetail = await PR_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!reqDetail[0]) return res.status(400).json({ msg: "Purchase Requesition Detail is not exist" });


    try {
      await PR_Detail.update(
        {
          item_code: item_code,
          description: description,
          unit: unit,
          price: price,
          qty_order: qty_order,
          current_stock: current_stock,
          notes: notes,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: detailId,
          },
        }
      );

      res.json({ msg: "Update Purchase Requesition Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deleteReqDetail(req, res) {
    const detailId = req.params.id;

    //cek apakah user sdh ada
    const reqDetail = await PR_Detail.findAll({
      where: {
        id: detailId,
      },
    });

    if (!reqDetail[0]) return res.status(400).json({ msg: "Purchase Requesition Detail is not exist" });
    
    try {
      await PR_Detail.destroy({
        where: {
          id: detailId,
        },
      });

      res.json({ msg: "Delete Purchase Requesition Detail is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = RequisitionController;
