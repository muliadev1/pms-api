const { Price_Type } = require("../db/models/index");
const Sequelize = require('sequelize');

class PriceTypeController {
  static async getPriceTypes_pages(req, res, next) {
    let { page, pageSize } = req.query

    //! Calculate starting index and number of records to retrieve
    const startIndex = (Number(page) - 1) * Number(pageSize);
    const endIndex = Number(page) * Number(pageSize);
    try {
      const pricetypes = await Price_Type.findAll({
        offset: startIndex,
        limit: Number(pageSize)
      });
      //! Retrieve total count of records for pagination info
      const totalCount = await Price_Type.count();
      //! Calculate total number of pages
      const totalPages = Math.ceil(totalCount / Number(pageSize));
      res.status(200).json(
        {
          pricetypes: pricetypes,
          totalRecords: totalCount,
          currentPage: Number(page),
          totalPages: totalPages
        });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPriceTypes(req, res, next) {
    try {
      const pricetypes = await Price_Type.findAll({
      });
      res.status(200).json(pricetypes);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async createPriceType(req, res, next) {
    const {
      price_type,
      status,
      updatedby
    } = req.body;

    try {
      await Price_Type.create({
        price_type: price_type,
        status: status,
        createdAt: Sequelize.fn('NOW'),
        createdBy: updatedby,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: updatedby,
      });
      res.status(200).json({ msg: "Add Price Type is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async getPriceType(req, res) {
    const typeId = req.params.id;
    try {
      const pricetype = await Price_Type.findOne({
        where: {
          id: typeId,
        },
      });
      res.status(200).json(pricetype);
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async updatePriceType(req, res) {
    const typeId = req.params.id;
    const {
      price_type,
      status,
      updatedby
    } = req.body;

    //cek apakah user sdh ada
    const pricetype = await Price_Type.findAll({
      where: {
        id: typeId,
      },
    });

    if (!pricetype[0]) return res.status(400).json({ msg: "Price Type is not exist" });


    try {
      await Price_Type.update(
        {
          price_type: price_type,
          status: status,
          updatedAt: Sequelize.fn('NOW'),
          updatedBy: updatedby
        },
        {
          where: {
            id: typeId,
          },
        }
      );

      res.json({ msg: "Update Price Type is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

  static async deletePriceType(req, res) {
    const typeId = req.params.id;
    //cek apakah user sdh ada
    const pricetype = await Price_Type.findAll({
      where: {
        id: typeId,
      },
    });

    if (!pricetype[0]) return res.status(400).json({ msg: "Price Type is not exist" });

    try {
      await Price_Type.destroy({
        where: {
          id: typeId,
        },
      });

      res.json({ msg: "Delete Price Type is successfully" });
    } catch (error) {
      res.status(500).json({ messege: 'Internal Server Error', error:error });
    }
  }

}

module.exports = PriceTypeController;
