const { PaymentStatus } = require("../db/models/index");
const Sequelize = require('sequelize');

class PaymentStatusController {

    static async getPaymentStatus(req, res, next) {
        try {
          const paymentsmethods = await PaymentStatus.findAll({});
          res.status(200).json(paymentsmethods);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async getPaymentStatusDetail(req, res, next) {
        const id = req.params.id;
        try {
          const payment = await PaymentStatus.findOne({
            where: {
              id: id,
            },
          });
          res.status(200).json(payment);
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }

      static async createPaymentStatus(req, res, next) {
        const {
          payment_status_name,
          updatedby
        } = req.body;
    
        try {
          await PaymentStatus.create({
            payment_status_name: payment_status_name,
            createdAt: Sequelize.fn('NOW'),
            createdBy: updatedby,
            updatedAt: Sequelize.fn('NOW'),
            updatedBy: updatedby,
          });
          res.status(200).json({ msg: "Add Payment Status is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
      
      static async updatePaymentStatus(req, res) {
        const id = req.params.id;
        const {
           payment_status_name,
          updatedby
        } = req.body;
    
        //cek apakah user sdh ada
        const paytype = await PaymentStatus.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Payment Status is not exist" });
    
    
        try {
          await PaymentStatus.update(
            {
             payment_status_name:  payment_status_name,
              updatedAt: Sequelize.fn('NOW'),
              updatedBy: updatedby
            },
            {
              where: {
                id: id,
              },
            }
          );
    
          res.json({ msg: "Update Payment Status is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
    
      static async deletePaymentStatus(req, res) {
        const id = req.params.id;
        //cek apakah user sdh ada
        const paytype = await PaymentStatus.findAll({
          where: {
            id: id,
          },
        });
    
        if (!paytype[0]) return res.status(400).json({ msg: "Payment Status is not exist" });
    
        try {
          await PaymentStatus.destroy({
            where: {
              id: id,
            },
          });
    
          res.json({ msg: "Delete Payment Status is successfully" });
        } catch (error) {
          res.status(500).json({ messege: 'Internal Server Error', error:error });
        }
      }
}

module.exports = PaymentStatusController;