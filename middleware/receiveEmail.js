// receiveEmail.js
const Imap = require('node-imap');

async function receiveEmail() {
  try {
    const imap = new Imap({
      user: 'your_username', // Replace with your POP3 username
      password: 'your_password', // Replace with your POP3 password
      host: 'your_pop3_host', // Replace with your POP3 server address
      port: 993,
      tls: true,
    });

    // Connect to the POP3 server
    imap.connect();

    // When connected
    imap.once('ready', () => {
      // Open the INBOX folder
      imap.openBox('INBOX', true, (error, mailbox) => {
        if (error) {
          console.error('Error opening INBOX:', error);
          return;
        }

        // Fetch all messages in the INBOX folder
        imap.search(['ALL'], (err, results) => {
          if (err) {
            console.error('Error searching for emails:', err);
            return;
          }

          const f = imap.fetch(results, { bodies: '' });

          f.on('message', (msg, seqno) => {
            msg.on('body', (stream, info) => {
              let buffer = '';
              stream.on('data', (chunk) => (buffer += chunk.toString()));
              stream.on('end', () => console.log('Email body:', buffer));
            });
          });

          f.once('end', () => imap.end());
        });
      });
    });

    // When disconnected
    imap.once('end', () => console.log('Disconnected from POP3 server.'));

    // If an error occurs during connection
    imap.once('error', (error) => console.error('Error during POP3 connection:', error));
  } catch (error) {
    console.error('Error receiving email:', error);
  }
}

module.exports = receiveEmail;