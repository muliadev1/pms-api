// sequelize.js

const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('staging_muliadb', 'root', 'P@ssw0rd!@#', {
    host: '127.0.0.1', // Ganti sesuai dengan IP address server database
    port: 3306, // Port yang digunakan oleh server database
    dialect: 'mysql',
  });


module.exports = sequelize;