require('dotenv').config();

const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const responseEnhancer = require('express-response-formatter');

const port = process.env.PORT || 4009;

const app = express();

// CORS options
const corsOptions = {
  origin: ["http://localhost:3000", "http://staging-fe.muliapms.my.id"],
  methods: ['GET', 'POST', 'PUT', 'DELETE'], // Allow the required HTTP methods
  credentials: true,
  optionsSuccessStatus: 200 // For legacy browser support
};

// Middleware
app.use(logger('dev'));
app.use(express.json());
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(responseEnhancer());

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Bootstrap routes
require('./routes/index.js')(app);

// Start server
app.listen(port, () => {
  console.log('API app started on port ' + port);
});
